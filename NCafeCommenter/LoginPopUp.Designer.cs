﻿namespace NCafeCommenter
{
    partial class LoginPopUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginPopUp));
            this.label1 = new System.Windows.Forms.Label();
            this.pb_Image = new System.Windows.Forms.PictureBox();
            this.cb_notshowing = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Image)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Location = new System.Drawing.Point(36, 473);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 12);
            this.label1.TabIndex = 16;
            this.label1.Text = "오늘하루 보지않기";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // pb_Image
            // 
            this.pb_Image.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Image.Location = new System.Drawing.Point(0, 0);
            this.pb_Image.Name = "pb_Image";
            this.pb_Image.Size = new System.Drawing.Size(54, 54);
            this.pb_Image.TabIndex = 17;
            this.pb_Image.TabStop = false;
            this.pb_Image.Visible = false;
            // 
            // cb_notshowing
            // 
            this.cb_notshowing.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_notshowing.BackgroundImage = global::NCafeCommenter.Properties.Resources.pop_chk;
            this.cb_notshowing.Cursor = System.Windows.Forms.Cursors.Default;
            this.cb_notshowing.FlatAppearance.BorderSize = 0;
            this.cb_notshowing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_notshowing.Location = new System.Drawing.Point(20, 470);
            this.cb_notshowing.Name = "cb_notshowing";
            this.cb_notshowing.Size = new System.Drawing.Size(16, 16);
            this.cb_notshowing.TabIndex = 15;
            this.cb_notshowing.UseVisualStyleBackColor = true;
            this.cb_notshowing.CheckedChanged += new System.EventHandler(this.cb_notshowing_CheckedChanged);
            // 
            // LoginPopUp
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(553, 502);
            this.Controls.Add(this.pb_Image);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cb_notshowing);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoginPopUp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "마케팅몬스터 안내";
            this.Click += new System.EventHandler(this.LoginPopUp_Click);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_Image;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cb_notshowing;
    }
}