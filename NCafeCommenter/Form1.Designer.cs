﻿namespace NCafeCommenter
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pn_work = new System.Windows.Forms.Panel();
            this.pn_URL = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_URL = new System.Windows.Forms.TextBox();
            this.pb_URL_File = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pb_URL_Add = new System.Windows.Forms.PictureBox();
            this.pictureBox60 = new System.Windows.Forms.PictureBox();
            this.pb_URL_SelDel = new System.Windows.Forms.PictureBox();
            this.pb_URL_Stop = new System.Windows.Forms.PictureBox();
            this.pb_URL_Start = new System.Windows.Forms.PictureBox();
            this.chk_URL_AllSel = new System.Windows.Forms.CheckBox();
            this.pictureBox59 = new System.Windows.Forms.PictureBox();
            this.lv_URL = new System.Windows.Forms.ListView();
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AddFile = new System.Windows.Forms.PictureBox();
            this.pn_Option = new System.Windows.Forms.Panel();
            this.tb_MonitorID = new System.Windows.Forms.TextBox();
            this.pictureBox57 = new System.Windows.Forms.PictureBox();
            this.chk_MonitorID = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.chk_Overlap = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.chk_while = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.chk_IdReplyMatch = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cb_ChangeNick = new System.Windows.Forms.CheckBox();
            this.cb_Tethering = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.pn_ExcepID = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.pb_ExcepIDSelDel = new System.Windows.Forms.PictureBox();
            this.pb_ExcepIDFile = new System.Windows.Forms.PictureBox();
            this.tb_ExcepID = new System.Windows.Forms.TextBox();
            this.pb_ExcepIDAdd = new System.Windows.Forms.PictureBox();
            this.pictureBox58 = new System.Windows.Forms.PictureBox();
            this.chk_ExcepAllSel = new System.Windows.Forms.CheckBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.lv_ExceptID = new System.Windows.Forms.ListView();
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.bt_ExceptID = new System.Windows.Forms.PictureBox();
            this.pb_Nick = new System.Windows.Forms.PictureBox();
            this.pn_Nick = new System.Windows.Forms.Panel();
            this.pb_Nick_File = new System.Windows.Forms.PictureBox();
            this.tb_Nick = new System.Windows.Forms.TextBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pb_Nick_Add = new System.Windows.Forms.PictureBox();
            this.pb_Nick_SelDel = new System.Windows.Forms.PictureBox();
            this.chk_Nick_AllSel = new System.Windows.Forms.CheckBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.lv_Nick = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pn_Reply = new System.Windows.Forms.Panel();
            this.pb_reply_clear = new System.Windows.Forms.PictureBox();
            this.bt_reply_stiker = new System.Windows.Forms.Button();
            this.bt_reply_image = new System.Windows.Forms.Button();
            this.pb_Reply_cancle = new System.Windows.Forms.PictureBox();
            this.pb_Reply_ok = new System.Windows.Forms.PictureBox();
            this.tb_Reply_write = new System.Windows.Forms.TextBox();
            this.pb_Option = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pn_Keyword_Write = new System.Windows.Forms.Panel();
            this.pb_keyword_Add = new System.Windows.Forms.PictureBox();
            this.tb_Keyword_word = new System.Windows.Forms.TextBox();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.pictureBox42 = new System.Windows.Forms.PictureBox();
            this.pictureBox43 = new System.Windows.Forms.PictureBox();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.pictureBox54 = new System.Windows.Forms.PictureBox();
            this.pictureBox55 = new System.Windows.Forms.PictureBox();
            this.pictureBox56 = new System.Windows.Forms.PictureBox();
            this.pictureBox51 = new System.Windows.Forms.PictureBox();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.pictureBox53 = new System.Windows.Forms.PictureBox();
            this.pictureBox45 = new System.Windows.Forms.PictureBox();
            this.pictureBox48 = new System.Windows.Forms.PictureBox();
            this.pictureBox50 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.chk_Selpage_one = new System.Windows.Forms.CheckBox();
            this.fc_Selpage_one_2 = new NCafeCommenter.FlatCombo();
            this.fc_Selpage_one_1 = new NCafeCommenter.FlatCombo();
            this.fc_Selpage_All_2 = new NCafeCommenter.FlatCombo();
            this.fc_Selpage_All_1 = new NCafeCommenter.FlatCombo();
            this.pictureBox40 = new System.Windows.Forms.PictureBox();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.fc_DelayTime = new NCafeCommenter.FlatCombo();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.fc_CafeMenuList = new NCafeCommenter.FlatCombo();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.fc_CafeURL = new NCafeCommenter.FlatCombo();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.fc_IdList = new NCafeCommenter.FlatCombo();
            this.chk_Allpage_All = new System.Windows.Forms.CheckBox();
            this.lv_Keyword_data = new System.Windows.Forms.ListView();
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.pictureBox49 = new System.Windows.Forms.PictureBox();
            this.pb_work_KeywordSelDel = new System.Windows.Forms.PictureBox();
            this.pb_work_KeywordEdit = new System.Windows.Forms.PictureBox();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.pb_keyword_Write = new System.Windows.Forms.PictureBox();
            this.pb_work_ContentList_Add = new System.Windows.Forms.PictureBox();
            this.pictureBox41 = new System.Windows.Forms.PictureBox();
            this.pb_work_ContentSelDel = new System.Windows.Forms.PictureBox();
            this.pb_option_Sel = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.chk_Selpage_All = new System.Windows.Forms.CheckBox();
            this.chk_1page_all = new System.Windows.Forms.CheckBox();
            this.chk_1page_one = new System.Windows.Forms.CheckBox();
            this.mc_work_crd = new System.Windows.Forms.MonthCalendar();
            this.tb_work_log = new System.Windows.Forms.TextBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.cb_work_WorktList_All = new System.Windows.Forms.CheckBox();
            this.pb_work_Worklistheader = new System.Windows.Forms.PictureBox();
            this.cb_work_ContentList_All = new System.Windows.Forms.CheckBox();
            this.pb_work_Contentheader = new System.Windows.Forms.PictureBox();
            this.pn_work_dateset = new System.Windows.Forms.Panel();
            this.tb_work_datetime_mm = new System.Windows.Forms.TextBox();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.tb_work_datetime_HH = new System.Windows.Forms.TextBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.tb_work_datetime = new System.Windows.Forms.TextBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pb_Hup = new System.Windows.Forms.PictureBox();
            this.pb_Hdown = new System.Windows.Forms.PictureBox();
            this.pb_secup = new System.Windows.Forms.PictureBox();
            this.pb_secdown = new System.Windows.Forms.PictureBox();
            this.pb_work_dateset_sel = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pb_work_datetime = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.lv_work_WorkList = new System.Windows.Forms.ListView();
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lv_work_ContentList = new System.Windows.Forms.ListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lv_work_IdList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cb_work_IdList_All = new System.Windows.Forms.CheckBox();
            this.tb_work_Pw = new System.Windows.Forms.TextBox();
            this.tb_work_Id = new System.Windows.Forms.TextBox();
            this.pb_work_Log_Save = new System.Windows.Forms.PictureBox();
            this.lb_work_Status = new System.Windows.Forms.Label();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pb_work_WorkList_Seldel = new System.Windows.Forms.PictureBox();
            this.pb_work_Stop = new System.Windows.Forms.PictureBox();
            this.pb_work_Start = new System.Windows.Forms.PictureBox();
            this.pb_work_IdList_Sel = new System.Windows.Forms.PictureBox();
            this.pb_work_CafeContentList_Sel = new System.Windows.Forms.PictureBox();
            this.pb_work_CafeURL_Search = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pb_work_contenttitle = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pb_work_IdSelDel = new System.Windows.Forms.PictureBox();
            this.pb_work_Idcert = new System.Windows.Forms.PictureBox();
            this.pb_work_Idheader = new System.Windows.Forms.PictureBox();
            this.tb_work_IdAdd = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pb_work_idtitle = new System.Windows.Forms.PictureBox();
            this.pb_work_headsubline = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pn_header = new System.Windows.Forms.Panel();
            this.lb_header_ver = new System.Windows.Forms.Label();
            this.lb_header_notice = new System.Windows.Forms.Label();
            this.pb_header_close = new System.Windows.Forms.PictureBox();
            this.pb_header_mini = new System.Windows.Forms.PictureBox();
            this.pb_header_homepage = new System.Windows.Forms.PictureBox();
            this.pn_header_notice = new System.Windows.Forms.PictureBox();
            this.pn_header_logo = new System.Windows.Forms.PictureBox();
            this.pn_Slogin = new System.Windows.Forms.Panel();
            this.lb_SLogin_Day = new System.Windows.Forms.Label();
            this.lb_SLogin_Hard2 = new System.Windows.Forms.Label();
            this.lb_SLogin_Hard = new System.Windows.Forms.Label();
            this.lb_SLogin_ID2 = new System.Windows.Forms.Label();
            this.lb_SLogin_Id = new System.Windows.Forms.Label();
            this.lb_SLogin_Hello = new System.Windows.Forms.Label();
            this.lb_SLogin_Day2 = new System.Windows.Forms.Label();
            this.pb_Slogin_Day = new System.Windows.Forms.PictureBox();
            this.pn_login = new System.Windows.Forms.Panel();
            this.tb_login_pw = new System.Windows.Forms.TextBox();
            this.tb_login_id = new System.Windows.Forms.TextBox();
            this.pb_login_reg = new System.Windows.Forms.PictureBox();
            this.pb_login_login = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pn_main_logo = new System.Windows.Forms.PictureBox();
            this.pb_Test = new System.Windows.Forms.PictureBox();
            this.pn_work.SuspendLayout();
            this.pn_URL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_URL_File)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_URL_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_URL_SelDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_URL_Stop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_URL_Start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddFile)).BeginInit();
            this.pn_Option.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).BeginInit();
            this.pn_ExcepID.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ExcepIDSelDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ExcepIDFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ExcepIDAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bt_ExceptID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Nick)).BeginInit();
            this.pn_Nick.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Nick_File)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Nick_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Nick_SelDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            this.pn_Reply.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_reply_clear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Reply_cancle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Reply_ok)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Option)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            this.pn_Keyword_Write.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_keyword_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_KeywordSelDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_KeywordEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_keyword_Write)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_ContentList_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_ContentSelDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_option_Sel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Worklistheader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Contentheader)).BeginInit();
            this.pn_work_dateset.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Hup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Hdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_secup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_secdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_dateset_sel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_datetime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Log_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_WorkList_Seldel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Stop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_IdList_Sel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_CafeContentList_Sel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_CafeURL_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_contenttitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_IdSelDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Idcert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Idheader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_work_IdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_idtitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_headsubline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            this.pn_header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_header_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_header_mini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_header_homepage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_header_notice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_header_logo)).BeginInit();
            this.pn_Slogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Slogin_Day)).BeginInit();
            this.pn_login.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_login_reg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_login_login)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_main_logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Test)).BeginInit();
            this.SuspendLayout();
            // 
            // pn_work
            // 
            this.pn_work.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(44)))), ((int)(((byte)(52)))));
            this.pn_work.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_conteats;
            this.pn_work.Controls.Add(this.pn_URL);
            this.pn_work.Controls.Add(this.AddFile);
            this.pn_work.Controls.Add(this.pn_Option);
            this.pn_work.Controls.Add(this.pn_ExcepID);
            this.pn_work.Controls.Add(this.bt_ExceptID);
            this.pn_work.Controls.Add(this.pb_Nick);
            this.pn_work.Controls.Add(this.pn_Nick);
            this.pn_work.Controls.Add(this.pn_Reply);
            this.pn_work.Controls.Add(this.pb_Option);
            this.pn_work.Controls.Add(this.pictureBox32);
            this.pn_work.Controls.Add(this.pn_Keyword_Write);
            this.pn_work.Controls.Add(this.pictureBox42);
            this.pn_work.Controls.Add(this.pictureBox43);
            this.pn_work.Controls.Add(this.pictureBox44);
            this.pn_work.Controls.Add(this.pictureBox54);
            this.pn_work.Controls.Add(this.pictureBox55);
            this.pn_work.Controls.Add(this.pictureBox56);
            this.pn_work.Controls.Add(this.pictureBox51);
            this.pn_work.Controls.Add(this.pictureBox52);
            this.pn_work.Controls.Add(this.pictureBox53);
            this.pn_work.Controls.Add(this.pictureBox45);
            this.pn_work.Controls.Add(this.pictureBox48);
            this.pn_work.Controls.Add(this.pictureBox50);
            this.pn_work.Controls.Add(this.label11);
            this.pn_work.Controls.Add(this.label10);
            this.pn_work.Controls.Add(this.chk_Selpage_one);
            this.pn_work.Controls.Add(this.fc_Selpage_one_2);
            this.pn_work.Controls.Add(this.fc_Selpage_one_1);
            this.pn_work.Controls.Add(this.fc_Selpage_All_2);
            this.pn_work.Controls.Add(this.fc_Selpage_All_1);
            this.pn_work.Controls.Add(this.pictureBox40);
            this.pn_work.Controls.Add(this.pictureBox38);
            this.pn_work.Controls.Add(this.pictureBox37);
            this.pn_work.Controls.Add(this.fc_DelayTime);
            this.pn_work.Controls.Add(this.pictureBox20);
            this.pn_work.Controls.Add(this.pictureBox22);
            this.pn_work.Controls.Add(this.pictureBox26);
            this.pn_work.Controls.Add(this.fc_CafeMenuList);
            this.pn_work.Controls.Add(this.pictureBox16);
            this.pn_work.Controls.Add(this.pictureBox17);
            this.pn_work.Controls.Add(this.pictureBox19);
            this.pn_work.Controls.Add(this.fc_CafeURL);
            this.pn_work.Controls.Add(this.pictureBox13);
            this.pn_work.Controls.Add(this.pictureBox12);
            this.pn_work.Controls.Add(this.pictureBox11);
            this.pn_work.Controls.Add(this.fc_IdList);
            this.pn_work.Controls.Add(this.chk_Allpage_All);
            this.pn_work.Controls.Add(this.lv_Keyword_data);
            this.pn_work.Controls.Add(this.checkBox1);
            this.pn_work.Controls.Add(this.pictureBox49);
            this.pn_work.Controls.Add(this.pb_work_KeywordSelDel);
            this.pn_work.Controls.Add(this.pb_work_KeywordEdit);
            this.pn_work.Controls.Add(this.pictureBox47);
            this.pn_work.Controls.Add(this.pb_keyword_Write);
            this.pn_work.Controls.Add(this.pb_work_ContentList_Add);
            this.pn_work.Controls.Add(this.pictureBox41);
            this.pn_work.Controls.Add(this.pb_work_ContentSelDel);
            this.pn_work.Controls.Add(this.pb_option_Sel);
            this.pn_work.Controls.Add(this.label9);
            this.pn_work.Controls.Add(this.label8);
            this.pn_work.Controls.Add(this.chk_Selpage_All);
            this.pn_work.Controls.Add(this.chk_1page_all);
            this.pn_work.Controls.Add(this.chk_1page_one);
            this.pn_work.Controls.Add(this.mc_work_crd);
            this.pn_work.Controls.Add(this.tb_work_log);
            this.pn_work.Controls.Add(this.pictureBox35);
            this.pn_work.Controls.Add(this.cb_work_WorktList_All);
            this.pn_work.Controls.Add(this.pb_work_Worklistheader);
            this.pn_work.Controls.Add(this.cb_work_ContentList_All);
            this.pn_work.Controls.Add(this.pb_work_Contentheader);
            this.pn_work.Controls.Add(this.pn_work_dateset);
            this.pn_work.Controls.Add(this.lv_work_WorkList);
            this.pn_work.Controls.Add(this.pictureBox5);
            this.pn_work.Controls.Add(this.label6);
            this.pn_work.Controls.Add(this.label5);
            this.pn_work.Controls.Add(this.label4);
            this.pn_work.Controls.Add(this.pictureBox6);
            this.pn_work.Controls.Add(this.lv_work_ContentList);
            this.pn_work.Controls.Add(this.pictureBox3);
            this.pn_work.Controls.Add(this.lv_work_IdList);
            this.pn_work.Controls.Add(this.pictureBox1);
            this.pn_work.Controls.Add(this.cb_work_IdList_All);
            this.pn_work.Controls.Add(this.tb_work_Pw);
            this.pn_work.Controls.Add(this.tb_work_Id);
            this.pn_work.Controls.Add(this.pb_work_Log_Save);
            this.pn_work.Controls.Add(this.lb_work_Status);
            this.pn_work.Controls.Add(this.pictureBox31);
            this.pn_work.Controls.Add(this.pb_work_WorkList_Seldel);
            this.pn_work.Controls.Add(this.pb_work_Stop);
            this.pn_work.Controls.Add(this.pb_work_Start);
            this.pn_work.Controls.Add(this.pb_work_IdList_Sel);
            this.pn_work.Controls.Add(this.pb_work_CafeContentList_Sel);
            this.pn_work.Controls.Add(this.pb_work_CafeURL_Search);
            this.pn_work.Controls.Add(this.pictureBox14);
            this.pn_work.Controls.Add(this.pb_work_contenttitle);
            this.pn_work.Controls.Add(this.pictureBox8);
            this.pn_work.Controls.Add(this.pb_work_IdSelDel);
            this.pn_work.Controls.Add(this.pb_work_Idcert);
            this.pn_work.Controls.Add(this.pb_work_Idheader);
            this.pn_work.Controls.Add(this.tb_work_IdAdd);
            this.pn_work.Controls.Add(this.pictureBox4);
            this.pn_work.Controls.Add(this.pictureBox2);
            this.pn_work.Controls.Add(this.pb_work_idtitle);
            this.pn_work.Controls.Add(this.pb_work_headsubline);
            this.pn_work.Controls.Add(this.pictureBox21);
            this.pn_work.Controls.Add(this.pictureBox15);
            this.pn_work.Controls.Add(this.pictureBox18);
            this.pn_work.Location = new System.Drawing.Point(15, 89);
            this.pn_work.Name = "pn_work";
            this.pn_work.Size = new System.Drawing.Size(1070, 589);
            this.pn_work.TabIndex = 2;
            // 
            // pn_URL
            // 
            this.pn_URL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.pn_URL.Controls.Add(this.label14);
            this.pn_URL.Controls.Add(this.label13);
            this.pn_URL.Controls.Add(this.tb_URL);
            this.pn_URL.Controls.Add(this.pb_URL_File);
            this.pn_URL.Controls.Add(this.label7);
            this.pn_URL.Controls.Add(this.pb_URL_Add);
            this.pn_URL.Controls.Add(this.pictureBox60);
            this.pn_URL.Controls.Add(this.pb_URL_SelDel);
            this.pn_URL.Controls.Add(this.pb_URL_Stop);
            this.pn_URL.Controls.Add(this.pb_URL_Start);
            this.pn_URL.Controls.Add(this.chk_URL_AllSel);
            this.pn_URL.Controls.Add(this.pictureBox59);
            this.pn_URL.Controls.Add(this.lv_URL);
            this.pn_URL.Location = new System.Drawing.Point(27, 253);
            this.pn_URL.Name = "pn_URL";
            this.pn_URL.Size = new System.Drawing.Size(394, 363);
            this.pn_URL.TabIndex = 303;
            this.pn_URL.Visible = false;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label14.Location = new System.Drawing.Point(321, 43);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 12);
            this.label14.TabIndex = 306;
            this.label14.Text = "진행";
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label13.Location = new System.Drawing.Point(157, 43);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 12);
            this.label13.TabIndex = 305;
            this.label13.Text = "게시글주소";
            // 
            // tb_URL
            // 
            this.tb_URL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.tb_URL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_URL.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_URL.Location = new System.Drawing.Point(108, 9);
            this.tb_URL.Name = "tb_URL";
            this.tb_URL.Size = new System.Drawing.Size(200, 14);
            this.tb_URL.TabIndex = 304;
            this.tb_URL.Text = "게시글 주소를 입력해주세요.";
            this.tb_URL.Enter += new System.EventHandler(this.tb_URL_Enter);
            this.tb_URL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_URL_KeyDown);
            this.tb_URL.Leave += new System.EventHandler(this.tb_URL_Leave);
            // 
            // pb_URL_File
            // 
            this.pb_URL_File.BackColor = System.Drawing.Color.Transparent;
            this.pb_URL_File.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_URL_File.BackgroundImage")));
            this.pb_URL_File.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_URL_File.Location = new System.Drawing.Point(314, 2);
            this.pb_URL_File.Name = "pb_URL_File";
            this.pb_URL_File.Size = new System.Drawing.Size(24, 25);
            this.pb_URL_File.TabIndex = 303;
            this.pb_URL_File.TabStop = false;
            this.pb_URL_File.Click += new System.EventHandler(this.pb_URL_File_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label7.Location = new System.Drawing.Point(-5, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 12);
            this.label7.TabIndex = 244;
            this.label7.Text = "·게시글주소입력";
            // 
            // pb_URL_Add
            // 
            this.pb_URL_Add.BackColor = System.Drawing.Color.Transparent;
            this.pb_URL_Add.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_add;
            this.pb_URL_Add.Location = new System.Drawing.Point(343, 2);
            this.pb_URL_Add.Name = "pb_URL_Add";
            this.pb_URL_Add.Size = new System.Drawing.Size(52, 24);
            this.pb_URL_Add.TabIndex = 243;
            this.pb_URL_Add.TabStop = false;
            this.pb_URL_Add.Click += new System.EventHandler(this.pb_URL_Add_Click);
            this.pb_URL_Add.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_URL_Add_MouseDown);
            this.pb_URL_Add.MouseEnter += new System.EventHandler(this.pb_URL_Add_MouseEnter);
            this.pb_URL_Add.MouseLeave += new System.EventHandler(this.pb_URL_Add_MouseLeave);
            this.pb_URL_Add.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_URL_Add_MouseUp);
            // 
            // pictureBox60
            // 
            this.pictureBox60.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox60.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_commonid;
            this.pictureBox60.Location = new System.Drawing.Point(101, 0);
            this.pictureBox60.Name = "pictureBox60";
            this.pictureBox60.Size = new System.Drawing.Size(213, 28);
            this.pictureBox60.TabIndex = 242;
            this.pictureBox60.TabStop = false;
            // 
            // pb_URL_SelDel
            // 
            this.pb_URL_SelDel.BackColor = System.Drawing.Color.Transparent;
            this.pb_URL_SelDel.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_seldel;
            this.pb_URL_SelDel.Location = new System.Drawing.Point(282, 335);
            this.pb_URL_SelDel.Name = "pb_URL_SelDel";
            this.pb_URL_SelDel.Size = new System.Drawing.Size(112, 28);
            this.pb_URL_SelDel.TabIndex = 139;
            this.pb_URL_SelDel.TabStop = false;
            this.pb_URL_SelDel.Click += new System.EventHandler(this.pb_URL_SelDel_Click);
            this.pb_URL_SelDel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_URL_SelDel_MouseDown);
            this.pb_URL_SelDel.MouseEnter += new System.EventHandler(this.pb_URL_SelDel_MouseEnter);
            this.pb_URL_SelDel.MouseLeave += new System.EventHandler(this.pb_URL_SelDel_MouseLeave);
            this.pb_URL_SelDel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_URL_SelDel_MouseUp);
            // 
            // pb_URL_Stop
            // 
            this.pb_URL_Stop.BackColor = System.Drawing.Color.Transparent;
            this.pb_URL_Stop.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_stop;
            this.pb_URL_Stop.Location = new System.Drawing.Point(165, 335);
            this.pb_URL_Stop.Name = "pb_URL_Stop";
            this.pb_URL_Stop.Size = new System.Drawing.Size(112, 28);
            this.pb_URL_Stop.TabIndex = 138;
            this.pb_URL_Stop.TabStop = false;
            this.pb_URL_Stop.Click += new System.EventHandler(this.pb_URL_Stop_Click);
            this.pb_URL_Stop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_URL_Stop_MouseDown);
            this.pb_URL_Stop.MouseEnter += new System.EventHandler(this.pb_URL_Stop_MouseEnter);
            this.pb_URL_Stop.MouseLeave += new System.EventHandler(this.pb_URL_Stop_MouseLeave);
            this.pb_URL_Stop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_URL_Stop_MouseUp);
            // 
            // pb_URL_Start
            // 
            this.pb_URL_Start.BackColor = System.Drawing.Color.Transparent;
            this.pb_URL_Start.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_start;
            this.pb_URL_Start.Location = new System.Drawing.Point(1, 335);
            this.pb_URL_Start.Name = "pb_URL_Start";
            this.pb_URL_Start.Size = new System.Drawing.Size(159, 28);
            this.pb_URL_Start.TabIndex = 137;
            this.pb_URL_Start.TabStop = false;
            this.pb_URL_Start.Click += new System.EventHandler(this.pb_URL_Start_Click);
            this.pb_URL_Start.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_URL_Start_MouseDown);
            this.pb_URL_Start.MouseEnter += new System.EventHandler(this.pb_URL_Start_MouseEnter);
            this.pb_URL_Start.MouseLeave += new System.EventHandler(this.pb_URL_Start_MouseLeave);
            this.pb_URL_Start.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_URL_Start_MouseUp);
            // 
            // chk_URL_AllSel
            // 
            this.chk_URL_AllSel.Appearance = System.Windows.Forms.Appearance.Button;
            this.chk_URL_AllSel.BackColor = System.Drawing.Color.Transparent;
            this.chk_URL_AllSel.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.chk_URL_AllSel.FlatAppearance.BorderSize = 0;
            this.chk_URL_AllSel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_URL_AllSel.Location = new System.Drawing.Point(9, 42);
            this.chk_URL_AllSel.Name = "chk_URL_AllSel";
            this.chk_URL_AllSel.Size = new System.Drawing.Size(12, 12);
            this.chk_URL_AllSel.TabIndex = 135;
            this.chk_URL_AllSel.UseVisualStyleBackColor = false;
            this.chk_URL_AllSel.CheckedChanged += new System.EventHandler(this.chk_URL_AllSel_CheckedChanged);
            // 
            // pictureBox59
            // 
            this.pictureBox59.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox59.BackgroundImage = global::NCafeCommenter.Properties.Resources.table_header;
            this.pictureBox59.Location = new System.Drawing.Point(1, 32);
            this.pictureBox59.Name = "pictureBox59";
            this.pictureBox59.Size = new System.Drawing.Size(393, 32);
            this.pictureBox59.TabIndex = 133;
            this.pictureBox59.TabStop = false;
            // 
            // lv_URL
            // 
            this.lv_URL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.lv_URL.BackgroundImageTiled = true;
            this.lv_URL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lv_URL.CheckBoxes = true;
            this.lv_URL.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15});
            this.lv_URL.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.lv_URL.FullRowSelect = true;
            this.lv_URL.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lv_URL.HideSelection = false;
            this.lv_URL.Location = new System.Drawing.Point(1, 64);
            this.lv_URL.Name = "lv_URL";
            this.lv_URL.Size = new System.Drawing.Size(391, 267);
            this.lv_URL.TabIndex = 134;
            this.lv_URL.UseCompatibleStateImageBehavior = false;
            this.lv_URL.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Width = 65;
            // 
            // columnHeader14
            // 
            this.columnHeader14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader14.Width = 249;
            // 
            // columnHeader15
            // 
            this.columnHeader15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AddFile
            // 
            this.AddFile.BackColor = System.Drawing.Color.Transparent;
            this.AddFile.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_file_off;
            this.AddFile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddFile.Location = new System.Drawing.Point(571, 17);
            this.AddFile.Name = "AddFile";
            this.AddFile.Size = new System.Drawing.Size(52, 24);
            this.AddFile.TabIndex = 307;
            this.AddFile.TabStop = false;
            this.AddFile.Click += new System.EventHandler(this.AddFile_Click);
            this.AddFile.MouseDown += new System.Windows.Forms.MouseEventHandler(this.AddFile_MouseDown);
            this.AddFile.MouseEnter += new System.EventHandler(this.AddFile_MouseEnter);
            this.AddFile.MouseLeave += new System.EventHandler(this.AddFile_MouseLeave);
            this.AddFile.MouseUp += new System.Windows.Forms.MouseEventHandler(this.AddFile_MouseUp);
            // 
            // pn_Option
            // 
            this.pn_Option.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.pn_Option.Controls.Add(this.tb_MonitorID);
            this.pn_Option.Controls.Add(this.pictureBox57);
            this.pn_Option.Controls.Add(this.chk_MonitorID);
            this.pn_Option.Controls.Add(this.label20);
            this.pn_Option.Controls.Add(this.chk_Overlap);
            this.pn_Option.Controls.Add(this.label18);
            this.pn_Option.Controls.Add(this.chk_while);
            this.pn_Option.Controls.Add(this.label16);
            this.pn_Option.Controls.Add(this.chk_IdReplyMatch);
            this.pn_Option.Controls.Add(this.label12);
            this.pn_Option.Controls.Add(this.cb_ChangeNick);
            this.pn_Option.Controls.Add(this.cb_Tethering);
            this.pn_Option.Controls.Add(this.label15);
            this.pn_Option.Controls.Add(this.label17);
            this.pn_Option.Location = new System.Drawing.Point(862, 100);
            this.pn_Option.Name = "pn_Option";
            this.pn_Option.Size = new System.Drawing.Size(394, 136);
            this.pn_Option.TabIndex = 254;
            this.pn_Option.Visible = false;
            // 
            // tb_MonitorID
            // 
            this.tb_MonitorID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.tb_MonitorID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_MonitorID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_MonitorID.Location = new System.Drawing.Point(7, 115);
            this.tb_MonitorID.Name = "tb_MonitorID";
            this.tb_MonitorID.Size = new System.Drawing.Size(374, 14);
            this.tb_MonitorID.TabIndex = 262;
            this.tb_MonitorID.Text = "모니터링할 아이디를 입력해주세요 , (쉼표) 로 구분합니다.";
            this.tb_MonitorID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_MonitorID.Enter += new System.EventHandler(this.tb_MonitorID_Enter);
            this.tb_MonitorID.Leave += new System.EventHandler(this.tb_MonitorID_Leave);
            // 
            // pictureBox57
            // 
            this.pictureBox57.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox57.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table1_input;
            this.pictureBox57.Location = new System.Drawing.Point(0, 109);
            this.pictureBox57.Name = "pictureBox57";
            this.pictureBox57.Size = new System.Drawing.Size(394, 24);
            this.pictureBox57.TabIndex = 261;
            this.pictureBox57.TabStop = false;
            // 
            // chk_MonitorID
            // 
            this.chk_MonitorID.Appearance = System.Windows.Forms.Appearance.Button;
            this.chk_MonitorID.BackColor = System.Drawing.Color.Transparent;
            this.chk_MonitorID.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.chk_MonitorID.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chk_MonitorID.FlatAppearance.BorderSize = 0;
            this.chk_MonitorID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_MonitorID.Location = new System.Drawing.Point(8, 92);
            this.chk_MonitorID.Name = "chk_MonitorID";
            this.chk_MonitorID.Size = new System.Drawing.Size(12, 12);
            this.chk_MonitorID.TabIndex = 260;
            this.chk_MonitorID.UseVisualStyleBackColor = false;
            this.chk_MonitorID.CheckedChanged += new System.EventHandler(this.chk_MonitorID_CheckedChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label20.Location = new System.Drawing.Point(26, 93);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(317, 12);
            this.label20.TabIndex = 259;
            this.label20.Text = "특정아이디에만 댓글작성 (하단 입력부에 작성해주세요)";
            // 
            // chk_Overlap
            // 
            this.chk_Overlap.Appearance = System.Windows.Forms.Appearance.Button;
            this.chk_Overlap.BackColor = System.Drawing.Color.Transparent;
            this.chk_Overlap.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.chk_Overlap.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chk_Overlap.FlatAppearance.BorderSize = 0;
            this.chk_Overlap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_Overlap.Location = new System.Drawing.Point(8, 74);
            this.chk_Overlap.Name = "chk_Overlap";
            this.chk_Overlap.Size = new System.Drawing.Size(12, 12);
            this.chk_Overlap.TabIndex = 258;
            this.chk_Overlap.UseVisualStyleBackColor = false;
            this.chk_Overlap.CheckedChanged += new System.EventHandler(this.chk_Overlap_CheckedChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label18.Location = new System.Drawing.Point(26, 75);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(191, 12);
            this.label18.TabIndex = 257;
            this.label18.Text = "댓글중복방지(내용, 아이디 검사)";
            // 
            // chk_while
            // 
            this.chk_while.Appearance = System.Windows.Forms.Appearance.Button;
            this.chk_while.BackColor = System.Drawing.Color.Transparent;
            this.chk_while.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.chk_while.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chk_while.FlatAppearance.BorderSize = 0;
            this.chk_while.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_while.Location = new System.Drawing.Point(8, 56);
            this.chk_while.Name = "chk_while";
            this.chk_while.Size = new System.Drawing.Size(12, 12);
            this.chk_while.TabIndex = 256;
            this.chk_while.UseVisualStyleBackColor = false;
            this.chk_while.CheckedChanged += new System.EventHandler(this.chk_while_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label16.Location = new System.Drawing.Point(26, 57);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(209, 12);
            this.label16.TabIndex = 255;
            this.label16.Text = "무한반복(작업목록을 계속해서 반복)";
            // 
            // chk_IdReplyMatch
            // 
            this.chk_IdReplyMatch.Appearance = System.Windows.Forms.Appearance.Button;
            this.chk_IdReplyMatch.BackColor = System.Drawing.Color.Transparent;
            this.chk_IdReplyMatch.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.chk_IdReplyMatch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chk_IdReplyMatch.FlatAppearance.BorderSize = 0;
            this.chk_IdReplyMatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_IdReplyMatch.Location = new System.Drawing.Point(8, 39);
            this.chk_IdReplyMatch.Name = "chk_IdReplyMatch";
            this.chk_IdReplyMatch.Size = new System.Drawing.Size(12, 12);
            this.chk_IdReplyMatch.TabIndex = 254;
            this.chk_IdReplyMatch.UseVisualStyleBackColor = false;
            this.chk_IdReplyMatch.CheckedChanged += new System.EventHandler(this.chk_IdReplyMatch_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label12.Location = new System.Drawing.Point(26, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(263, 12);
            this.label12.TabIndex = 253;
            this.label12.Text = "아이디당 댓글지정 (1번째아이디 = 1번째댓글)";
            // 
            // cb_ChangeNick
            // 
            this.cb_ChangeNick.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_ChangeNick.BackColor = System.Drawing.Color.Transparent;
            this.cb_ChangeNick.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.cb_ChangeNick.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb_ChangeNick.FlatAppearance.BorderSize = 0;
            this.cb_ChangeNick.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_ChangeNick.Location = new System.Drawing.Point(8, 5);
            this.cb_ChangeNick.Name = "cb_ChangeNick";
            this.cb_ChangeNick.Size = new System.Drawing.Size(12, 12);
            this.cb_ChangeNick.TabIndex = 250;
            this.cb_ChangeNick.UseVisualStyleBackColor = false;
            this.cb_ChangeNick.CheckedChanged += new System.EventHandler(this.cb_ChangeNick_CheckedChanged);
            // 
            // cb_Tethering
            // 
            this.cb_Tethering.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_Tethering.BackColor = System.Drawing.Color.Transparent;
            this.cb_Tethering.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.cb_Tethering.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb_Tethering.FlatAppearance.BorderSize = 0;
            this.cb_Tethering.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_Tethering.Location = new System.Drawing.Point(8, 22);
            this.cb_Tethering.Name = "cb_Tethering";
            this.cb_Tethering.Size = new System.Drawing.Size(12, 12);
            this.cb_Tethering.TabIndex = 248;
            this.cb_Tethering.UseVisualStyleBackColor = false;
            this.cb_Tethering.CheckedChanged += new System.EventHandler(this.cb_Tethering_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label15.Location = new System.Drawing.Point(25, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(263, 12);
            this.label15.TabIndex = 251;
            this.label15.Text = "닉네임자동변경 (댓글 작성시마다 닉네임변경)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label17.Location = new System.Drawing.Point(26, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(341, 12);
            this.label17.TabIndex = 174;
            this.label17.Text = "테더링설정 (휴대기기에 USB테더링이 허용 되었을때만 사용)";
            // 
            // pn_ExcepID
            // 
            this.pn_ExcepID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(10)))), ((int)(((byte)(14)))));
            this.pn_ExcepID.Controls.Add(this.label19);
            this.pn_ExcepID.Controls.Add(this.pb_ExcepIDSelDel);
            this.pn_ExcepID.Controls.Add(this.pb_ExcepIDFile);
            this.pn_ExcepID.Controls.Add(this.tb_ExcepID);
            this.pn_ExcepID.Controls.Add(this.pb_ExcepIDAdd);
            this.pn_ExcepID.Controls.Add(this.pictureBox58);
            this.pn_ExcepID.Controls.Add(this.chk_ExcepAllSel);
            this.pn_ExcepID.Controls.Add(this.pictureBox29);
            this.pn_ExcepID.Controls.Add(this.lv_ExceptID);
            this.pn_ExcepID.Controls.Add(this.pictureBox36);
            this.pn_ExcepID.Location = new System.Drawing.Point(798, 435);
            this.pn_ExcepID.Name = "pn_ExcepID";
            this.pn_ExcepID.Size = new System.Drawing.Size(394, 331);
            this.pn_ExcepID.TabIndex = 306;
            this.pn_ExcepID.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label19.Location = new System.Drawing.Point(93, 45);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(265, 12);
            this.label19.TabIndex = 319;
            this.label19.Text = "                      제외아이디                            ";
            // 
            // pb_ExcepIDSelDel
            // 
            this.pb_ExcepIDSelDel.BackColor = System.Drawing.Color.Transparent;
            this.pb_ExcepIDSelDel.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_delete_off;
            this.pb_ExcepIDSelDel.Location = new System.Drawing.Point(342, 3);
            this.pb_ExcepIDSelDel.Name = "pb_ExcepIDSelDel";
            this.pb_ExcepIDSelDel.Size = new System.Drawing.Size(52, 24);
            this.pb_ExcepIDSelDel.TabIndex = 318;
            this.pb_ExcepIDSelDel.TabStop = false;
            this.pb_ExcepIDSelDel.Click += new System.EventHandler(this.pb_ExcepIDSelDel_Click);
            // 
            // pb_ExcepIDFile
            // 
            this.pb_ExcepIDFile.BackColor = System.Drawing.Color.Transparent;
            this.pb_ExcepIDFile.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_file_off;
            this.pb_ExcepIDFile.Location = new System.Drawing.Point(228, 3);
            this.pb_ExcepIDFile.Name = "pb_ExcepIDFile";
            this.pb_ExcepIDFile.Size = new System.Drawing.Size(52, 24);
            this.pb_ExcepIDFile.TabIndex = 317;
            this.pb_ExcepIDFile.TabStop = false;
            this.pb_ExcepIDFile.Click += new System.EventHandler(this.pb_ExcepIDFile_Click);
            // 
            // tb_ExcepID
            // 
            this.tb_ExcepID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.tb_ExcepID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_ExcepID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_ExcepID.Location = new System.Drawing.Point(7, 8);
            this.tb_ExcepID.Name = "tb_ExcepID";
            this.tb_ExcepID.Size = new System.Drawing.Size(206, 14);
            this.tb_ExcepID.TabIndex = 316;
            this.tb_ExcepID.Text = "댓글 작성 제외아이디";
            this.tb_ExcepID.Enter += new System.EventHandler(this.tb_ExcepID_Enter);
            this.tb_ExcepID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_ExcepID_KeyDown);
            this.tb_ExcepID.Leave += new System.EventHandler(this.tb_ExcepID_Leave);
            // 
            // pb_ExcepIDAdd
            // 
            this.pb_ExcepIDAdd.BackColor = System.Drawing.Color.Transparent;
            this.pb_ExcepIDAdd.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_add;
            this.pb_ExcepIDAdd.Location = new System.Drawing.Point(285, 3);
            this.pb_ExcepIDAdd.Name = "pb_ExcepIDAdd";
            this.pb_ExcepIDAdd.Size = new System.Drawing.Size(52, 24);
            this.pb_ExcepIDAdd.TabIndex = 315;
            this.pb_ExcepIDAdd.TabStop = false;
            this.pb_ExcepIDAdd.Click += new System.EventHandler(this.pb_ExcepIDAdd_Click);
            // 
            // pictureBox58
            // 
            this.pictureBox58.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox58.BackgroundImage = global::NCafeCommenter.Properties.Resources.blank_ID;
            this.pictureBox58.Location = new System.Drawing.Point(0, 0);
            this.pictureBox58.Name = "pictureBox58";
            this.pictureBox58.Size = new System.Drawing.Size(222, 28);
            this.pictureBox58.TabIndex = 314;
            this.pictureBox58.TabStop = false;
            // 
            // chk_ExcepAllSel
            // 
            this.chk_ExcepAllSel.Appearance = System.Windows.Forms.Appearance.Button;
            this.chk_ExcepAllSel.BackColor = System.Drawing.Color.Transparent;
            this.chk_ExcepAllSel.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.chk_ExcepAllSel.FlatAppearance.BorderSize = 0;
            this.chk_ExcepAllSel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_ExcepAllSel.Location = new System.Drawing.Point(12, 44);
            this.chk_ExcepAllSel.Name = "chk_ExcepAllSel";
            this.chk_ExcepAllSel.Size = new System.Drawing.Size(12, 12);
            this.chk_ExcepAllSel.TabIndex = 312;
            this.chk_ExcepAllSel.UseVisualStyleBackColor = false;
            this.chk_ExcepAllSel.CheckedChanged += new System.EventHandler(this.chk_ExcepAllSel_CheckedChanged);
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox29.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table3_header;
            this.pictureBox29.Location = new System.Drawing.Point(0, 34);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(393, 32);
            this.pictureBox29.TabIndex = 310;
            this.pictureBox29.TabStop = false;
            // 
            // lv_ExceptID
            // 
            this.lv_ExceptID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.lv_ExceptID.BackgroundImageTiled = true;
            this.lv_ExceptID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lv_ExceptID.CheckBoxes = true;
            this.lv_ExceptID.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader16,
            this.columnHeader23,
            this.columnHeader24});
            this.lv_ExceptID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.lv_ExceptID.FullRowSelect = true;
            this.lv_ExceptID.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lv_ExceptID.HideSelection = false;
            this.lv_ExceptID.Location = new System.Drawing.Point(7, 67);
            this.lv_ExceptID.Name = "lv_ExceptID";
            this.lv_ExceptID.Size = new System.Drawing.Size(384, 263);
            this.lv_ExceptID.TabIndex = 311;
            this.lv_ExceptID.UseCompatibleStateImageBehavior = false;
            this.lv_ExceptID.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Width = 18;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Width = 38;
            // 
            // columnHeader24
            // 
            this.columnHeader24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader24.Width = 307;
            // 
            // pictureBox36
            // 
            this.pictureBox36.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox36.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table3;
            this.pictureBox36.Location = new System.Drawing.Point(0, 67);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(393, 264);
            this.pictureBox36.TabIndex = 313;
            this.pictureBox36.TabStop = false;
            // 
            // bt_ExceptID
            // 
            this.bt_ExceptID.BackColor = System.Drawing.Color.Transparent;
            this.bt_ExceptID.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_id_off;
            this.bt_ExceptID.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_ExceptID.Location = new System.Drawing.Point(761, 19);
            this.bt_ExceptID.Name = "bt_ExceptID";
            this.bt_ExceptID.Size = new System.Drawing.Size(97, 24);
            this.bt_ExceptID.TabIndex = 305;
            this.bt_ExceptID.TabStop = false;
            this.bt_ExceptID.Click += new System.EventHandler(this.bt_ExceptID_Click);
            // 
            // pb_Nick
            // 
            this.pb_Nick.BackColor = System.Drawing.Color.Transparent;
            this.pb_Nick.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_url_off;
            this.pb_Nick.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Nick.Location = new System.Drawing.Point(860, 19);
            this.pb_Nick.Name = "pb_Nick";
            this.pb_Nick.Size = new System.Drawing.Size(97, 24);
            this.pb_Nick.TabIndex = 304;
            this.pb_Nick.TabStop = false;
            this.pb_Nick.Click += new System.EventHandler(this.pb_Nick_Click);
            this.pb_Nick.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_Nick_MouseDown);
            this.pb_Nick.MouseEnter += new System.EventHandler(this.pb_Nick_MouseEnter);
            this.pb_Nick.MouseLeave += new System.EventHandler(this.pb_Nick_MouseLeave);
            this.pb_Nick.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_Nick_MouseUp);
            // 
            // pn_Nick
            // 
            this.pn_Nick.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.pn_Nick.Controls.Add(this.pb_Nick_File);
            this.pn_Nick.Controls.Add(this.tb_Nick);
            this.pn_Nick.Controls.Add(this.pictureBox24);
            this.pn_Nick.Controls.Add(this.pb_Nick_Add);
            this.pn_Nick.Controls.Add(this.pb_Nick_SelDel);
            this.pn_Nick.Controls.Add(this.chk_Nick_AllSel);
            this.pn_Nick.Controls.Add(this.pictureBox27);
            this.pn_Nick.Controls.Add(this.lv_Nick);
            this.pn_Nick.Controls.Add(this.pictureBox28);
            this.pn_Nick.Location = new System.Drawing.Point(0, 0);
            this.pn_Nick.Name = "pn_Nick";
            this.pn_Nick.Size = new System.Drawing.Size(393, 226);
            this.pn_Nick.TabIndex = 255;
            this.pn_Nick.Visible = false;
            // 
            // pb_Nick_File
            // 
            this.pb_Nick_File.BackColor = System.Drawing.Color.Transparent;
            this.pb_Nick_File.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_Nick_File.BackgroundImage")));
            this.pb_Nick_File.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Nick_File.Location = new System.Drawing.Point(83, 199);
            this.pb_Nick_File.Name = "pb_Nick_File";
            this.pb_Nick_File.Size = new System.Drawing.Size(24, 25);
            this.pb_Nick_File.TabIndex = 302;
            this.pb_Nick_File.TabStop = false;
            this.pb_Nick_File.Click += new System.EventHandler(this.pb_Nick_File_Click);
            // 
            // tb_Nick
            // 
            this.tb_Nick.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.tb_Nick.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Nick.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_Nick.Location = new System.Drawing.Point(3, 206);
            this.tb_Nick.Name = "tb_Nick";
            this.tb_Nick.Size = new System.Drawing.Size(77, 14);
            this.tb_Nick.TabIndex = 140;
            this.tb_Nick.Text = "닉네임 입력칸";
            this.tb_Nick.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_Nick.Enter += new System.EventHandler(this.tb_Nick_Enter);
            this.tb_Nick.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_Nick_KeyDown);
            this.tb_Nick.Leave += new System.EventHandler(this.tb_Nick_Leave);
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox24.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table1_input;
            this.pictureBox24.Location = new System.Drawing.Point(0, 198);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(107, 28);
            this.pictureBox24.TabIndex = 139;
            this.pictureBox24.TabStop = false;
            // 
            // pb_Nick_Add
            // 
            this.pb_Nick_Add.BackColor = System.Drawing.Color.Transparent;
            this.pb_Nick_Add.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_registration;
            this.pb_Nick_Add.Location = new System.Drawing.Point(115, 198);
            this.pb_Nick_Add.Name = "pb_Nick_Add";
            this.pb_Nick_Add.Size = new System.Drawing.Size(159, 28);
            this.pb_Nick_Add.TabIndex = 138;
            this.pb_Nick_Add.TabStop = false;
            this.pb_Nick_Add.Click += new System.EventHandler(this.pb_Nick_Add_Click);
            // 
            // pb_Nick_SelDel
            // 
            this.pb_Nick_SelDel.BackColor = System.Drawing.Color.Transparent;
            this.pb_Nick_SelDel.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_seldel;
            this.pb_Nick_SelDel.Location = new System.Drawing.Point(281, 198);
            this.pb_Nick_SelDel.Name = "pb_Nick_SelDel";
            this.pb_Nick_SelDel.Size = new System.Drawing.Size(112, 28);
            this.pb_Nick_SelDel.TabIndex = 137;
            this.pb_Nick_SelDel.TabStop = false;
            this.pb_Nick_SelDel.Click += new System.EventHandler(this.pb_Nick_SelDel_Click);
            // 
            // chk_Nick_AllSel
            // 
            this.chk_Nick_AllSel.Appearance = System.Windows.Forms.Appearance.Button;
            this.chk_Nick_AllSel.BackColor = System.Drawing.Color.Transparent;
            this.chk_Nick_AllSel.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.chk_Nick_AllSel.FlatAppearance.BorderSize = 0;
            this.chk_Nick_AllSel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chk_Nick_AllSel.Location = new System.Drawing.Point(12, 10);
            this.chk_Nick_AllSel.Name = "chk_Nick_AllSel";
            this.chk_Nick_AllSel.Size = new System.Drawing.Size(12, 12);
            this.chk_Nick_AllSel.TabIndex = 135;
            this.chk_Nick_AllSel.UseVisualStyleBackColor = false;
            this.chk_Nick_AllSel.CheckedChanged += new System.EventHandler(this.chk_Nick_AllSel_CheckedChanged);
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox27.BackgroundImage = global::NCafeCommenter.Properties.Resources.table_header;
            this.pictureBox27.Location = new System.Drawing.Point(0, 0);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(393, 32);
            this.pictureBox27.TabIndex = 133;
            this.pictureBox27.TabStop = false;
            // 
            // lv_Nick
            // 
            this.lv_Nick.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.lv_Nick.BackgroundImageTiled = true;
            this.lv_Nick.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lv_Nick.CheckBoxes = true;
            this.lv_Nick.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader12});
            this.lv_Nick.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.lv_Nick.FullRowSelect = true;
            this.lv_Nick.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lv_Nick.HideSelection = false;
            this.lv_Nick.Location = new System.Drawing.Point(1, 32);
            this.lv_Nick.Name = "lv_Nick";
            this.lv_Nick.Size = new System.Drawing.Size(391, 164);
            this.lv_Nick.TabIndex = 134;
            this.lv_Nick.UseCompatibleStateImageBehavior = false;
            this.lv_Nick.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Width = 65;
            // 
            // columnHeader12
            // 
            this.columnHeader12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader12.Width = 306;
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox28.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table3;
            this.pictureBox28.Location = new System.Drawing.Point(0, 31);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(393, 167);
            this.pictureBox28.TabIndex = 136;
            this.pictureBox28.TabStop = false;
            // 
            // pn_Reply
            // 
            this.pn_Reply.Controls.Add(this.pb_reply_clear);
            this.pn_Reply.Controls.Add(this.bt_reply_stiker);
            this.pn_Reply.Controls.Add(this.bt_reply_image);
            this.pn_Reply.Controls.Add(this.pb_Reply_cancle);
            this.pn_Reply.Controls.Add(this.pb_Reply_ok);
            this.pn_Reply.Controls.Add(this.tb_Reply_write);
            this.pn_Reply.Location = new System.Drawing.Point(438, 140);
            this.pn_Reply.Name = "pn_Reply";
            this.pn_Reply.Size = new System.Drawing.Size(276, 363);
            this.pn_Reply.TabIndex = 206;
            this.pn_Reply.Visible = false;
            // 
            // pb_reply_clear
            // 
            this.pb_reply_clear.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pb_reply_clear.BackgroundImage = global::NCafeCommenter.Properties.Resources.posts_btn_close_on;
            this.pb_reply_clear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_reply_clear.Location = new System.Drawing.Point(244, 3);
            this.pb_reply_clear.Name = "pb_reply_clear";
            this.pb_reply_clear.Size = new System.Drawing.Size(15, 15);
            this.pb_reply_clear.TabIndex = 215;
            this.pb_reply_clear.TabStop = false;
            this.pb_reply_clear.Visible = false;
            this.pb_reply_clear.Click += new System.EventHandler(this.pb_reply_clear_Click);
            // 
            // bt_reply_stiker
            // 
            this.bt_reply_stiker.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bt_reply_stiker.BackgroundImage = global::NCafeCommenter.Properties.Resources.icon_smile;
            this.bt_reply_stiker.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_reply_stiker.FlatAppearance.BorderSize = 0;
            this.bt_reply_stiker.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_reply_stiker.Location = new System.Drawing.Point(2, 306);
            this.bt_reply_stiker.Name = "bt_reply_stiker";
            this.bt_reply_stiker.Size = new System.Drawing.Size(23, 23);
            this.bt_reply_stiker.TabIndex = 212;
            this.bt_reply_stiker.UseVisualStyleBackColor = false;
            this.bt_reply_stiker.Click += new System.EventHandler(this.bt_reply_stiker_Click);
            // 
            // bt_reply_image
            // 
            this.bt_reply_image.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bt_reply_image.BackgroundImage = global::NCafeCommenter.Properties.Resources.icon_carmera;
            this.bt_reply_image.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_reply_image.FlatAppearance.BorderSize = 0;
            this.bt_reply_image.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_reply_image.Location = new System.Drawing.Point(35, 308);
            this.bt_reply_image.Name = "bt_reply_image";
            this.bt_reply_image.Size = new System.Drawing.Size(23, 19);
            this.bt_reply_image.TabIndex = 211;
            this.bt_reply_image.UseVisualStyleBackColor = false;
            this.bt_reply_image.Click += new System.EventHandler(this.bt_reply_image_Click);
            // 
            // pb_Reply_cancle
            // 
            this.pb_Reply_cancle.BackColor = System.Drawing.Color.Transparent;
            this.pb_Reply_cancle.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_can;
            this.pb_Reply_cancle.Location = new System.Drawing.Point(165, 335);
            this.pb_Reply_cancle.Name = "pb_Reply_cancle";
            this.pb_Reply_cancle.Size = new System.Drawing.Size(112, 28);
            this.pb_Reply_cancle.TabIndex = 210;
            this.pb_Reply_cancle.TabStop = false;
            this.pb_Reply_cancle.Click += new System.EventHandler(this.pb_Reply_cancle_Click);
            // 
            // pb_Reply_ok
            // 
            this.pb_Reply_ok.BackColor = System.Drawing.Color.Transparent;
            this.pb_Reply_ok.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_com;
            this.pb_Reply_ok.Location = new System.Drawing.Point(1, 335);
            this.pb_Reply_ok.Name = "pb_Reply_ok";
            this.pb_Reply_ok.Size = new System.Drawing.Size(159, 28);
            this.pb_Reply_ok.TabIndex = 209;
            this.pb_Reply_ok.TabStop = false;
            this.pb_Reply_ok.Click += new System.EventHandler(this.pb_Reply_ok_Click);
            // 
            // tb_Reply_write
            // 
            this.tb_Reply_write.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_Reply_write.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Reply_write.ForeColor = System.Drawing.Color.Gray;
            this.tb_Reply_write.Location = new System.Drawing.Point(0, 0);
            this.tb_Reply_write.Multiline = true;
            this.tb_Reply_write.Name = "tb_Reply_write";
            this.tb_Reply_write.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_Reply_write.Size = new System.Drawing.Size(274, 331);
            this.tb_Reply_write.TabIndex = 1;
            this.tb_Reply_write.Text = "댓글을 남겨보세요.";
            this.tb_Reply_write.Enter += new System.EventHandler(this.tb_Reply_write_Enter);
            this.tb_Reply_write.Leave += new System.EventHandler(this.tb_Reply_write_Leave);
            // 
            // pb_Option
            // 
            this.pb_Option.BackColor = System.Drawing.Color.Transparent;
            this.pb_Option.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_option_off;
            this.pb_Option.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Option.Location = new System.Drawing.Point(959, 19);
            this.pb_Option.Name = "pb_Option";
            this.pb_Option.Size = new System.Drawing.Size(97, 24);
            this.pb_Option.TabIndex = 253;
            this.pb_Option.TabStop = false;
            this.pb_Option.Click += new System.EventHandler(this.pb_Option_Click);
            this.pb_Option.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_Option_MouseDown);
            this.pb_Option.MouseEnter += new System.EventHandler(this.pb_Option_MouseEnter);
            this.pb_Option.MouseLeave += new System.EventHandler(this.pb_Option_MouseLeave);
            this.pb_Option.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_Option_MouseUp);
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox32.Location = new System.Drawing.Point(321, 443);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(2, 130);
            this.pictureBox32.TabIndex = 41;
            this.pictureBox32.TabStop = false;
            // 
            // pn_Keyword_Write
            // 
            this.pn_Keyword_Write.Controls.Add(this.pb_keyword_Add);
            this.pn_Keyword_Write.Controls.Add(this.tb_Keyword_word);
            this.pn_Keyword_Write.Controls.Add(this.pictureBox46);
            this.pn_Keyword_Write.Controls.Add(this.pictureBox39);
            this.pn_Keyword_Write.Location = new System.Drawing.Point(329, 271);
            this.pn_Keyword_Write.Name = "pn_Keyword_Write";
            this.pn_Keyword_Write.Size = new System.Drawing.Size(276, 24);
            this.pn_Keyword_Write.TabIndex = 205;
            this.pn_Keyword_Write.Visible = false;
            // 
            // pb_keyword_Add
            // 
            this.pb_keyword_Add.BackColor = System.Drawing.Color.Transparent;
            this.pb_keyword_Add.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_add;
            this.pb_keyword_Add.Location = new System.Drawing.Point(224, 0);
            this.pb_keyword_Add.Name = "pb_keyword_Add";
            this.pb_keyword_Add.Size = new System.Drawing.Size(52, 24);
            this.pb_keyword_Add.TabIndex = 208;
            this.pb_keyword_Add.TabStop = false;
            this.pb_keyword_Add.Click += new System.EventHandler(this.pb_keyword_Add_Click_1);
            this.pb_keyword_Add.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_keyword_Add_MouseDown);
            this.pb_keyword_Add.MouseEnter += new System.EventHandler(this.pb_keyword_Add_MouseEnter);
            this.pb_keyword_Add.MouseLeave += new System.EventHandler(this.pb_keyword_Add_MouseLeave);
            this.pb_keyword_Add.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_keyword_Add_MouseUp);
            // 
            // tb_Keyword_word
            // 
            this.tb_Keyword_word.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.tb_Keyword_word.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Keyword_word.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_Keyword_word.Location = new System.Drawing.Point(8, 6);
            this.tb_Keyword_word.Name = "tb_Keyword_word";
            this.tb_Keyword_word.Size = new System.Drawing.Size(201, 14);
            this.tb_Keyword_word.TabIndex = 206;
            this.tb_Keyword_word.Text = "이곳에 키워드를 입력해주세요.";
            this.tb_Keyword_word.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_Keyword_word.Enter += new System.EventHandler(this.tb_Keyword_word_Enter);
            this.tb_Keyword_word.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_Keyword_word_KeyPress);
            this.tb_Keyword_word.Leave += new System.EventHandler(this.tb_Keyword_word_Leave);
            // 
            // pictureBox46
            // 
            this.pictureBox46.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox46.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table1_input;
            this.pictureBox46.Location = new System.Drawing.Point(1, 0);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(217, 24);
            this.pictureBox46.TabIndex = 204;
            this.pictureBox46.TabStop = false;
            // 
            // pictureBox39
            // 
            this.pictureBox39.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox39.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table2_header;
            this.pictureBox39.Location = new System.Drawing.Point(-2, 25);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(276, 32);
            this.pictureBox39.TabIndex = 195;
            this.pictureBox39.TabStop = false;
            // 
            // pictureBox42
            // 
            this.pictureBox42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox42.Location = new System.Drawing.Point(192, 540);
            this.pictureBox42.Name = "pictureBox42";
            this.pictureBox42.Size = new System.Drawing.Size(43, 5);
            this.pictureBox42.TabIndex = 231;
            this.pictureBox42.TabStop = false;
            // 
            // pictureBox43
            // 
            this.pictureBox43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox43.Location = new System.Drawing.Point(192, 525);
            this.pictureBox43.Name = "pictureBox43";
            this.pictureBox43.Size = new System.Drawing.Size(43, 3);
            this.pictureBox43.TabIndex = 230;
            this.pictureBox43.TabStop = false;
            // 
            // pictureBox44
            // 
            this.pictureBox44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox44.Location = new System.Drawing.Point(192, 525);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(4, 20);
            this.pictureBox44.TabIndex = 229;
            this.pictureBox44.TabStop = false;
            // 
            // pictureBox54
            // 
            this.pictureBox54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox54.Location = new System.Drawing.Point(193, 565);
            this.pictureBox54.Name = "pictureBox54";
            this.pictureBox54.Size = new System.Drawing.Size(43, 5);
            this.pictureBox54.TabIndex = 240;
            this.pictureBox54.TabStop = false;
            // 
            // pictureBox55
            // 
            this.pictureBox55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox55.Location = new System.Drawing.Point(193, 550);
            this.pictureBox55.Name = "pictureBox55";
            this.pictureBox55.Size = new System.Drawing.Size(43, 3);
            this.pictureBox55.TabIndex = 239;
            this.pictureBox55.TabStop = false;
            // 
            // pictureBox56
            // 
            this.pictureBox56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox56.Location = new System.Drawing.Point(193, 550);
            this.pictureBox56.Name = "pictureBox56";
            this.pictureBox56.Size = new System.Drawing.Size(4, 20);
            this.pictureBox56.TabIndex = 238;
            this.pictureBox56.TabStop = false;
            // 
            // pictureBox51
            // 
            this.pictureBox51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox51.Location = new System.Drawing.Point(247, 540);
            this.pictureBox51.Name = "pictureBox51";
            this.pictureBox51.Size = new System.Drawing.Size(43, 5);
            this.pictureBox51.TabIndex = 237;
            this.pictureBox51.TabStop = false;
            // 
            // pictureBox52
            // 
            this.pictureBox52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox52.Location = new System.Drawing.Point(247, 525);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(43, 3);
            this.pictureBox52.TabIndex = 236;
            this.pictureBox52.TabStop = false;
            // 
            // pictureBox53
            // 
            this.pictureBox53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox53.Location = new System.Drawing.Point(247, 525);
            this.pictureBox53.Name = "pictureBox53";
            this.pictureBox53.Size = new System.Drawing.Size(4, 20);
            this.pictureBox53.TabIndex = 235;
            this.pictureBox53.TabStop = false;
            // 
            // pictureBox45
            // 
            this.pictureBox45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox45.Location = new System.Drawing.Point(248, 565);
            this.pictureBox45.Name = "pictureBox45";
            this.pictureBox45.Size = new System.Drawing.Size(43, 5);
            this.pictureBox45.TabIndex = 234;
            this.pictureBox45.TabStop = false;
            // 
            // pictureBox48
            // 
            this.pictureBox48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox48.Location = new System.Drawing.Point(248, 550);
            this.pictureBox48.Name = "pictureBox48";
            this.pictureBox48.Size = new System.Drawing.Size(43, 3);
            this.pictureBox48.TabIndex = 233;
            this.pictureBox48.TabStop = false;
            // 
            // pictureBox50
            // 
            this.pictureBox50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox50.Location = new System.Drawing.Point(248, 550);
            this.pictureBox50.Name = "pictureBox50";
            this.pictureBox50.Size = new System.Drawing.Size(4, 20);
            this.pictureBox50.TabIndex = 232;
            this.pictureBox50.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(235, 553);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 12);
            this.label11.TabIndex = 215;
            this.label11.Text = "~";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(235, 530);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(14, 12);
            this.label10.TabIndex = 214;
            this.label10.Text = "~";
            // 
            // chk_Selpage_one
            // 
            this.chk_Selpage_one.AutoSize = true;
            this.chk_Selpage_one.BackColor = System.Drawing.Color.Transparent;
            this.chk_Selpage_one.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.chk_Selpage_one.Location = new System.Drawing.Point(15, 529);
            this.chk_Selpage_one.Name = "chk_Selpage_one";
            this.chk_Selpage_one.Size = new System.Drawing.Size(178, 16);
            this.chk_Selpage_one.TabIndex = 177;
            this.chk_Selpage_one.Tag = "N";
            this.chk_Selpage_one.Text = "선택 페이지 1개씩 댓글 작성";
            this.chk_Selpage_one.UseVisualStyleBackColor = false;
            this.chk_Selpage_one.CheckedChanged += new System.EventHandler(this.chk_Selpage_one_CheckedChanged);
            // 
            // fc_Selpage_one_2
            // 
            this.fc_Selpage_one_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_Selpage_one_2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_Selpage_one_2.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None;
            this.fc_Selpage_one_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.fc_Selpage_one_2.FormattingEnabled = true;
            this.fc_Selpage_one_2.Location = new System.Drawing.Point(248, 525);
            this.fc_Selpage_one_2.Name = "fc_Selpage_one_2";
            this.fc_Selpage_one_2.Size = new System.Drawing.Size(43, 20);
            this.fc_Selpage_one_2.TabIndex = 247;
            // 
            // fc_Selpage_one_1
            // 
            this.fc_Selpage_one_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_Selpage_one_1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_Selpage_one_1.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None;
            this.fc_Selpage_one_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.fc_Selpage_one_1.FormattingEnabled = true;
            this.fc_Selpage_one_1.Location = new System.Drawing.Point(193, 525);
            this.fc_Selpage_one_1.Name = "fc_Selpage_one_1";
            this.fc_Selpage_one_1.Size = new System.Drawing.Size(43, 20);
            this.fc_Selpage_one_1.TabIndex = 246;
            // 
            // fc_Selpage_All_2
            // 
            this.fc_Selpage_All_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_Selpage_All_2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_Selpage_All_2.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None;
            this.fc_Selpage_All_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.fc_Selpage_All_2.FormattingEnabled = true;
            this.fc_Selpage_All_2.Location = new System.Drawing.Point(248, 550);
            this.fc_Selpage_All_2.Name = "fc_Selpage_All_2";
            this.fc_Selpage_All_2.Size = new System.Drawing.Size(43, 20);
            this.fc_Selpage_All_2.TabIndex = 245;
            // 
            // fc_Selpage_All_1
            // 
            this.fc_Selpage_All_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_Selpage_All_1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_Selpage_All_1.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None;
            this.fc_Selpage_All_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.fc_Selpage_All_1.FormattingEnabled = true;
            this.fc_Selpage_All_1.Location = new System.Drawing.Point(193, 550);
            this.fc_Selpage_All_1.Name = "fc_Selpage_All_1";
            this.fc_Selpage_All_1.Size = new System.Drawing.Size(43, 20);
            this.fc_Selpage_All_1.TabIndex = 244;
            // 
            // pictureBox40
            // 
            this.pictureBox40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox40.Location = new System.Drawing.Point(232, 457);
            this.pictureBox40.Name = "pictureBox40";
            this.pictureBox40.Size = new System.Drawing.Size(43, 5);
            this.pictureBox40.TabIndex = 228;
            this.pictureBox40.TabStop = false;
            // 
            // pictureBox38
            // 
            this.pictureBox38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox38.Location = new System.Drawing.Point(232, 442);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(43, 3);
            this.pictureBox38.TabIndex = 227;
            this.pictureBox38.TabStop = false;
            // 
            // pictureBox37
            // 
            this.pictureBox37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox37.Location = new System.Drawing.Point(232, 442);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(4, 20);
            this.pictureBox37.TabIndex = 226;
            this.pictureBox37.TabStop = false;
            // 
            // fc_DelayTime
            // 
            this.fc_DelayTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_DelayTime.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_DelayTime.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None;
            this.fc_DelayTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.fc_DelayTime.FormattingEnabled = true;
            this.fc_DelayTime.Location = new System.Drawing.Point(232, 442);
            this.fc_DelayTime.Name = "fc_DelayTime";
            this.fc_DelayTime.Size = new System.Drawing.Size(43, 20);
            this.fc_DelayTime.TabIndex = 243;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox20.Location = new System.Drawing.Point(763, 131);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(234, 6);
            this.pictureBox20.TabIndex = 155;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox22.Location = new System.Drawing.Point(763, 112);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(5, 26);
            this.pictureBox22.TabIndex = 154;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox26.Location = new System.Drawing.Point(763, 116);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(234, 3);
            this.pictureBox26.TabIndex = 153;
            this.pictureBox26.TabStop = false;
            // 
            // fc_CafeMenuList
            // 
            this.fc_CafeMenuList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_CafeMenuList.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_CafeMenuList.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None;
            this.fc_CafeMenuList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.fc_CafeMenuList.FormattingEnabled = true;
            this.fc_CafeMenuList.Location = new System.Drawing.Point(764, 116);
            this.fc_CafeMenuList.Name = "fc_CafeMenuList";
            this.fc_CafeMenuList.Size = new System.Drawing.Size(233, 20);
            this.fc_CafeMenuList.TabIndex = 242;
            this.fc_CafeMenuList.Click += new System.EventHandler(this.fc_CafeMenuList_Click);
            this.fc_CafeMenuList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fc_CafeMenuList_KeyDown_1);
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox16.Location = new System.Drawing.Point(763, 101);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(234, 6);
            this.pictureBox16.TabIndex = 152;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox17.Location = new System.Drawing.Point(763, 81);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(5, 26);
            this.pictureBox17.TabIndex = 151;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox19.Location = new System.Drawing.Point(763, 83);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(234, 4);
            this.pictureBox19.TabIndex = 150;
            this.pictureBox19.TabStop = false;
            // 
            // fc_CafeURL
            // 
            this.fc_CafeURL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_CafeURL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_CafeURL.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None;
            this.fc_CafeURL.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.fc_CafeURL.FormattingEnabled = true;
            this.fc_CafeURL.Location = new System.Drawing.Point(764, 84);
            this.fc_CafeURL.Name = "fc_CafeURL";
            this.fc_CafeURL.Size = new System.Drawing.Size(233, 20);
            this.fc_CafeURL.TabIndex = 241;
            this.fc_CafeURL.SelectedIndexChanged += new System.EventHandler(this.fc_CafeURL_SelectedIndexChanged);
            this.fc_CafeURL.Click += new System.EventHandler(this.fc_CafeURL_Click);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox13.Location = new System.Drawing.Point(763, 68);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(234, 6);
            this.pictureBox13.TabIndex = 149;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox12.Location = new System.Drawing.Point(763, 48);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(5, 26);
            this.pictureBox12.TabIndex = 148;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.pictureBox11.Location = new System.Drawing.Point(763, 52);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(234, 3);
            this.pictureBox11.TabIndex = 147;
            this.pictureBox11.TabStop = false;
            // 
            // fc_IdList
            // 
            this.fc_IdList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_IdList.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.fc_IdList.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None;
            this.fc_IdList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.fc_IdList.FormattingEnabled = true;
            this.fc_IdList.Location = new System.Drawing.Point(764, 52);
            this.fc_IdList.Name = "fc_IdList";
            this.fc_IdList.Size = new System.Drawing.Size(233, 20);
            this.fc_IdList.TabIndex = 35;
            this.fc_IdList.SelectedIndexChanged += new System.EventHandler(this.fc_IdList_SelectedIndexChanged);
            this.fc_IdList.Click += new System.EventHandler(this.fc_IdList_Click);
            this.fc_IdList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fc_IdList_KeyDown);
            // 
            // chk_Allpage_All
            // 
            this.chk_Allpage_All.AutoSize = true;
            this.chk_Allpage_All.BackColor = System.Drawing.Color.Transparent;
            this.chk_Allpage_All.Checked = true;
            this.chk_Allpage_All.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_Allpage_All.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.chk_Allpage_All.Location = new System.Drawing.Point(15, 467);
            this.chk_Allpage_All.Name = "chk_Allpage_All";
            this.chk_Allpage_All.Size = new System.Drawing.Size(168, 16);
            this.chk_Allpage_All.TabIndex = 212;
            this.chk_Allpage_All.Tag = "N";
            this.chk_Allpage_All.Text = "모든페이지 모두 댓글 작성";
            this.chk_Allpage_All.UseVisualStyleBackColor = false;
            this.chk_Allpage_All.CheckedChanged += new System.EventHandler(this.chk_Allpage_All_CheckedChanged);
            // 
            // lv_Keyword_data
            // 
            this.lv_Keyword_data.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.lv_Keyword_data.BackgroundImageTiled = true;
            this.lv_Keyword_data.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lv_Keyword_data.CheckBoxes = true;
            this.lv_Keyword_data.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader8,
            this.columnHeader17});
            this.lv_Keyword_data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.lv_Keyword_data.FullRowSelect = true;
            this.lv_Keyword_data.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lv_Keyword_data.HideSelection = false;
            this.lv_Keyword_data.Location = new System.Drawing.Point(355, 277);
            this.lv_Keyword_data.Name = "lv_Keyword_data";
            this.lv_Keyword_data.Size = new System.Drawing.Size(265, 98);
            this.lv_Keyword_data.TabIndex = 210;
            this.lv_Keyword_data.UseCompatibleStateImageBehavior = false;
            this.lv_Keyword_data.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "chk";
            this.columnHeader8.Width = 29;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "키워드";
            this.columnHeader17.Width = 213;
            // 
            // checkBox1
            // 
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.BackColor = System.Drawing.Color.Transparent;
            this.checkBox1.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.checkBox1.FlatAppearance.BorderSize = 0;
            this.checkBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox1.Location = new System.Drawing.Point(359, 253);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(12, 12);
            this.checkBox1.TabIndex = 211;
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // pictureBox49
            // 
            this.pictureBox49.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox49.BackgroundImage = global::NCafeCommenter.Properties.Resources._04_header;
            this.pictureBox49.Location = new System.Drawing.Point(347, 243);
            this.pictureBox49.Name = "pictureBox49";
            this.pictureBox49.Size = new System.Drawing.Size(276, 32);
            this.pictureBox49.TabIndex = 209;
            this.pictureBox49.TabStop = false;
            // 
            // pb_work_KeywordSelDel
            // 
            this.pb_work_KeywordSelDel.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_KeywordSelDel.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_seldel;
            this.pb_work_KeywordSelDel.Location = new System.Drawing.Point(512, 382);
            this.pb_work_KeywordSelDel.Name = "pb_work_KeywordSelDel";
            this.pb_work_KeywordSelDel.Size = new System.Drawing.Size(112, 28);
            this.pb_work_KeywordSelDel.TabIndex = 208;
            this.pb_work_KeywordSelDel.TabStop = false;
            this.pb_work_KeywordSelDel.Click += new System.EventHandler(this.pb_work_KeywordSelDel_Click);
            this.pb_work_KeywordSelDel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_KeywordSelDel_MouseDown);
            this.pb_work_KeywordSelDel.MouseEnter += new System.EventHandler(this.pb_work_KeywordSelDel_MouseEnter);
            this.pb_work_KeywordSelDel.MouseLeave += new System.EventHandler(this.pb_work_KeywordSelDel_MouseLeave);
            this.pb_work_KeywordSelDel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_KeywordSelDel_MouseUp);
            // 
            // pb_work_KeywordEdit
            // 
            this.pb_work_KeywordEdit.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_KeywordEdit.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_edit1;
            this.pb_work_KeywordEdit.Location = new System.Drawing.Point(348, 382);
            this.pb_work_KeywordEdit.Name = "pb_work_KeywordEdit";
            this.pb_work_KeywordEdit.Size = new System.Drawing.Size(159, 28);
            this.pb_work_KeywordEdit.TabIndex = 207;
            this.pb_work_KeywordEdit.TabStop = false;
            this.pb_work_KeywordEdit.Click += new System.EventHandler(this.pb_work_KeywordEdit_Click);
            this.pb_work_KeywordEdit.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_KeywordEdit_MouseDown);
            this.pb_work_KeywordEdit.MouseEnter += new System.EventHandler(this.pb_work_KeywordEdit_MouseEnter);
            this.pb_work_KeywordEdit.MouseLeave += new System.EventHandler(this.pb_work_KeywordEdit_MouseLeave);
            this.pb_work_KeywordEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_KeywordEdit_MouseUp);
            // 
            // pictureBox47
            // 
            this.pictureBox47.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox47.BackgroundImage = global::NCafeCommenter.Properties.Resources.title_04;
            this.pictureBox47.Location = new System.Drawing.Point(347, 212);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(85, 16);
            this.pictureBox47.TabIndex = 204;
            this.pictureBox47.TabStop = false;
            // 
            // pb_keyword_Write
            // 
            this.pb_keyword_Write.BackColor = System.Drawing.Color.Transparent;
            this.pb_keyword_Write.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_add;
            this.pb_keyword_Write.Location = new System.Drawing.Point(571, 212);
            this.pb_keyword_Write.Name = "pb_keyword_Write";
            this.pb_keyword_Write.Size = new System.Drawing.Size(52, 24);
            this.pb_keyword_Write.TabIndex = 201;
            this.pb_keyword_Write.TabStop = false;
            this.pb_keyword_Write.Click += new System.EventHandler(this.pb_keyword_Write_Click);
            this.pb_keyword_Write.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_keyword_Write_MouseDown);
            this.pb_keyword_Write.MouseEnter += new System.EventHandler(this.pb_keyword_Write_MouseEnter);
            this.pb_keyword_Write.MouseLeave += new System.EventHandler(this.pb_keyword_Write_MouseLeave);
            this.pb_keyword_Write.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_keyword_Write_MouseUp);
            // 
            // pb_work_ContentList_Add
            // 
            this.pb_work_ContentList_Add.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_ContentList_Add.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_registration;
            this.pb_work_ContentList_Add.Location = new System.Drawing.Point(347, 175);
            this.pb_work_ContentList_Add.Name = "pb_work_ContentList_Add";
            this.pb_work_ContentList_Add.Size = new System.Drawing.Size(159, 28);
            this.pb_work_ContentList_Add.TabIndex = 14;
            this.pb_work_ContentList_Add.TabStop = false;
            this.pb_work_ContentList_Add.Click += new System.EventHandler(this.pb_work_ContentList_Add_Click);
            this.pb_work_ContentList_Add.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_ContentList_Add_MouseDown);
            this.pb_work_ContentList_Add.MouseEnter += new System.EventHandler(this.pb_work_ContentList_Add_MouseEnter);
            this.pb_work_ContentList_Add.MouseLeave += new System.EventHandler(this.pb_work_ContentList_Add_MouseLeave);
            this.pb_work_ContentList_Add.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_ContentList_Add_MouseUp);
            // 
            // pictureBox41
            // 
            this.pictureBox41.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox41.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table2;
            this.pictureBox41.Location = new System.Drawing.Point(347, 271);
            this.pictureBox41.Name = "pictureBox41";
            this.pictureBox41.Size = new System.Drawing.Size(276, 107);
            this.pictureBox41.TabIndex = 198;
            this.pictureBox41.TabStop = false;
            // 
            // pb_work_ContentSelDel
            // 
            this.pb_work_ContentSelDel.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_ContentSelDel.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_seldel;
            this.pb_work_ContentSelDel.Location = new System.Drawing.Point(511, 175);
            this.pb_work_ContentSelDel.Name = "pb_work_ContentSelDel";
            this.pb_work_ContentSelDel.Size = new System.Drawing.Size(112, 28);
            this.pb_work_ContentSelDel.TabIndex = 17;
            this.pb_work_ContentSelDel.TabStop = false;
            this.pb_work_ContentSelDel.Click += new System.EventHandler(this.pb_work_ContentSelDel_Click);
            this.pb_work_ContentSelDel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_ContentSelDel_MouseDown);
            this.pb_work_ContentSelDel.MouseEnter += new System.EventHandler(this.pb_work_ContentSelDel_MouseEnter);
            this.pb_work_ContentSelDel.MouseLeave += new System.EventHandler(this.pb_work_ContentSelDel_MouseLeave);
            this.pb_work_ContentSelDel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_ContentSelDel_MouseUp);
            // 
            // pb_option_Sel
            // 
            this.pb_option_Sel.BackColor = System.Drawing.Color.Transparent;
            this.pb_option_Sel.BackgroundImage = global::NCafeCommenter.Properties.Resources.title_012;
            this.pb_option_Sel.Location = new System.Drawing.Point(15, 440);
            this.pb_option_Sel.Name = "pb_option_Sel";
            this.pb_option_Sel.Size = new System.Drawing.Size(66, 16);
            this.pb_option_Sel.TabIndex = 194;
            this.pb_option_Sel.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label9.Location = new System.Drawing.Point(277, 447);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 12);
            this.label9.TabIndex = 193;
            this.label9.Text = "초";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label8.Location = new System.Drawing.Point(191, 447);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 192;
            this.label8.Text = "딜레이";
            // 
            // chk_Selpage_All
            // 
            this.chk_Selpage_All.AutoSize = true;
            this.chk_Selpage_All.BackColor = System.Drawing.Color.Transparent;
            this.chk_Selpage_All.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.chk_Selpage_All.Location = new System.Drawing.Point(15, 551);
            this.chk_Selpage_All.Name = "chk_Selpage_All";
            this.chk_Selpage_All.Size = new System.Drawing.Size(172, 16);
            this.chk_Selpage_All.TabIndex = 178;
            this.chk_Selpage_All.Tag = "N";
            this.chk_Selpage_All.Text = "선택 페이지 모두 댓글 작성";
            this.chk_Selpage_All.UseVisualStyleBackColor = false;
            this.chk_Selpage_All.CheckedChanged += new System.EventHandler(this.chk_Selpage_All_CheckedChanged);
            // 
            // chk_1page_all
            // 
            this.chk_1page_all.AutoSize = true;
            this.chk_1page_all.BackColor = System.Drawing.Color.Transparent;
            this.chk_1page_all.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.chk_1page_all.Location = new System.Drawing.Point(15, 508);
            this.chk_1page_all.Name = "chk_1page_all";
            this.chk_1page_all.Size = new System.Drawing.Size(310, 16);
            this.chk_1page_all.TabIndex = 176;
            this.chk_1page_all.Tag = "N";
            this.chk_1page_all.Text = "1페이지 게시글에 모두 댓글작성 (1페이지 20개 기준)";
            this.chk_1page_all.UseVisualStyleBackColor = false;
            this.chk_1page_all.CheckedChanged += new System.EventHandler(this.chk_1page_all_CheckedChanged);
            // 
            // chk_1page_one
            // 
            this.chk_1page_one.AutoSize = true;
            this.chk_1page_one.BackColor = System.Drawing.Color.Transparent;
            this.chk_1page_one.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.chk_1page_one.Location = new System.Drawing.Point(15, 487);
            this.chk_1page_one.Name = "chk_1page_one";
            this.chk_1page_one.Size = new System.Drawing.Size(292, 16);
            this.chk_1page_one.TabIndex = 175;
            this.chk_1page_one.Tag = "N";
            this.chk_1page_one.Text = "1페이지 글에 1개만 댓글작성 (1페이지 20개 기준)";
            this.chk_1page_one.UseVisualStyleBackColor = false;
            this.chk_1page_one.CheckedChanged += new System.EventHandler(this.chk_1page_one_CheckedChanged);
            // 
            // mc_work_crd
            // 
            this.mc_work_crd.Location = new System.Drawing.Point(616, 435);
            this.mc_work_crd.Name = "mc_work_crd";
            this.mc_work_crd.TabIndex = 142;
            this.mc_work_crd.Visible = false;
            this.mc_work_crd.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.mc_work_crd_DateSelected);
            this.mc_work_crd.Leave += new System.EventHandler(this.mc_work_crd_Leave);
            // 
            // tb_work_log
            // 
            this.tb_work_log.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.tb_work_log.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_work_log.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_work_log.Location = new System.Drawing.Point(348, 470);
            this.tb_work_log.Multiline = true;
            this.tb_work_log.Name = "tb_work_log";
            this.tb_work_log.ReadOnly = true;
            this.tb_work_log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_work_log.Size = new System.Drawing.Size(708, 97);
            this.tb_work_log.TabIndex = 42;
            this.tb_work_log.Enter += new System.EventHandler(this.tb_work_log_Enter);
            // 
            // pictureBox35
            // 
            this.pictureBox35.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox35.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_log;
            this.pictureBox35.Location = new System.Drawing.Point(347, 467);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(708, 103);
            this.pictureBox35.TabIndex = 159;
            this.pictureBox35.TabStop = false;
            // 
            // cb_work_WorktList_All
            // 
            this.cb_work_WorktList_All.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_work_WorktList_All.BackColor = System.Drawing.Color.Transparent;
            this.cb_work_WorktList_All.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.cb_work_WorktList_All.FlatAppearance.BorderSize = 0;
            this.cb_work_WorktList_All.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_work_WorktList_All.Location = new System.Drawing.Point(674, 153);
            this.cb_work_WorktList_All.Name = "cb_work_WorktList_All";
            this.cb_work_WorktList_All.Size = new System.Drawing.Size(12, 12);
            this.cb_work_WorktList_All.TabIndex = 122;
            this.cb_work_WorktList_All.UseVisualStyleBackColor = false;
            this.cb_work_WorktList_All.CheckedChanged += new System.EventHandler(this.cb_work_WorktList_All_CheckedChanged);
            // 
            // pb_work_Worklistheader
            // 
            this.pb_work_Worklistheader.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_Worklistheader.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table3_header;
            this.pb_work_Worklistheader.Location = new System.Drawing.Point(662, 143);
            this.pb_work_Worklistheader.Name = "pb_work_Worklistheader";
            this.pb_work_Worklistheader.Size = new System.Drawing.Size(393, 32);
            this.pb_work_Worklistheader.TabIndex = 34;
            this.pb_work_Worklistheader.TabStop = false;
            // 
            // cb_work_ContentList_All
            // 
            this.cb_work_ContentList_All.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_work_ContentList_All.BackColor = System.Drawing.Color.Transparent;
            this.cb_work_ContentList_All.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.cb_work_ContentList_All.FlatAppearance.BorderSize = 0;
            this.cb_work_ContentList_All.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_work_ContentList_All.Location = new System.Drawing.Point(359, 57);
            this.cb_work_ContentList_All.Name = "cb_work_ContentList_All";
            this.cb_work_ContentList_All.Size = new System.Drawing.Size(12, 12);
            this.cb_work_ContentList_All.TabIndex = 121;
            this.cb_work_ContentList_All.UseVisualStyleBackColor = false;
            this.cb_work_ContentList_All.CheckedChanged += new System.EventHandler(this.cb_work_ContentList_All_CheckedChanged);
            // 
            // pb_work_Contentheader
            // 
            this.pb_work_Contentheader.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_Contentheader.BackgroundImage = global::NCafeCommenter.Properties.Resources._03_header;
            this.pb_work_Contentheader.Location = new System.Drawing.Point(347, 47);
            this.pb_work_Contentheader.Name = "pb_work_Contentheader";
            this.pb_work_Contentheader.Size = new System.Drawing.Size(276, 32);
            this.pb_work_Contentheader.TabIndex = 15;
            this.pb_work_Contentheader.TabStop = false;
            // 
            // pn_work_dateset
            // 
            this.pn_work_dateset.Controls.Add(this.tb_work_datetime_mm);
            this.pn_work_dateset.Controls.Add(this.pictureBox34);
            this.pn_work_dateset.Controls.Add(this.tb_work_datetime_HH);
            this.pn_work_dateset.Controls.Add(this.pictureBox33);
            this.pn_work_dateset.Controls.Add(this.tb_work_datetime);
            this.pn_work_dateset.Controls.Add(this.pictureBox30);
            this.pn_work_dateset.Controls.Add(this.label3);
            this.pn_work_dateset.Controls.Add(this.pb_Hup);
            this.pn_work_dateset.Controls.Add(this.pb_Hdown);
            this.pn_work_dateset.Controls.Add(this.pb_secup);
            this.pn_work_dateset.Controls.Add(this.pb_secdown);
            this.pn_work_dateset.Controls.Add(this.pb_work_dateset_sel);
            this.pn_work_dateset.Controls.Add(this.pictureBox10);
            this.pn_work_dateset.Controls.Add(this.pictureBox7);
            this.pn_work_dateset.Controls.Add(this.pb_work_datetime);
            this.pn_work_dateset.Controls.Add(this.pictureBox9);
            this.pn_work_dateset.Location = new System.Drawing.Point(662, 350);
            this.pn_work_dateset.Name = "pn_work_dateset";
            this.pn_work_dateset.Size = new System.Drawing.Size(393, 28);
            this.pn_work_dateset.TabIndex = 146;
            // 
            // tb_work_datetime_mm
            // 
            this.tb_work_datetime_mm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.tb_work_datetime_mm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_work_datetime_mm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_work_datetime_mm.Location = new System.Drawing.Point(286, 9);
            this.tb_work_datetime_mm.Name = "tb_work_datetime_mm";
            this.tb_work_datetime_mm.ReadOnly = true;
            this.tb_work_datetime_mm.Size = new System.Drawing.Size(23, 14);
            this.tb_work_datetime_mm.TabIndex = 154;
            this.tb_work_datetime_mm.Text = "00";
            this.tb_work_datetime_mm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_work_datetime_mm.Enter += new System.EventHandler(this.tb_work_datetime_mm_Enter);
            // 
            // pictureBox34
            // 
            this.pictureBox34.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox34.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_input_time;
            this.pictureBox34.Location = new System.Drawing.Point(280, 2);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(36, 24);
            this.pictureBox34.TabIndex = 162;
            this.pictureBox34.TabStop = false;
            // 
            // tb_work_datetime_HH
            // 
            this.tb_work_datetime_HH.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.tb_work_datetime_HH.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_work_datetime_HH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_work_datetime_HH.Location = new System.Drawing.Point(228, 9);
            this.tb_work_datetime_HH.Name = "tb_work_datetime_HH";
            this.tb_work_datetime_HH.ReadOnly = true;
            this.tb_work_datetime_HH.Size = new System.Drawing.Size(23, 14);
            this.tb_work_datetime_HH.TabIndex = 157;
            this.tb_work_datetime_HH.Text = "00";
            this.tb_work_datetime_HH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_work_datetime_HH.Enter += new System.EventHandler(this.tb_work_datetime_HH_Enter);
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox33.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_input_time;
            this.pictureBox33.Location = new System.Drawing.Point(221, 2);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(36, 24);
            this.pictureBox33.TabIndex = 161;
            this.pictureBox33.TabStop = false;
            // 
            // tb_work_datetime
            // 
            this.tb_work_datetime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.tb_work_datetime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_work_datetime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_work_datetime.Location = new System.Drawing.Point(99, 9);
            this.tb_work_datetime.Name = "tb_work_datetime";
            this.tb_work_datetime.ReadOnly = true;
            this.tb_work_datetime.Size = new System.Drawing.Size(96, 14);
            this.tb_work_datetime.TabIndex = 147;
            this.tb_work_datetime.Text = "9999.12.31";
            this.tb_work_datetime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_work_datetime.Click += new System.EventHandler(this.tb_work_datetime_Click);
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox30.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_input_day;
            this.pictureBox30.Location = new System.Drawing.Point(95, 2);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(104, 24);
            this.pictureBox30.TabIndex = 160;
            this.pictureBox30.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(31, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 159;
            this.label3.Text = "예약적용";
            // 
            // pb_Hup
            // 
            this.pb_Hup.BackColor = System.Drawing.Color.Transparent;
            this.pb_Hup.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_up;
            this.pb_Hup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Hup.Location = new System.Drawing.Point(259, 1);
            this.pb_Hup.Name = "pb_Hup";
            this.pb_Hup.Size = new System.Drawing.Size(16, 13);
            this.pb_Hup.TabIndex = 156;
            this.pb_Hup.TabStop = false;
            this.pb_Hup.Click += new System.EventHandler(this.pb_Hup_Click);
            // 
            // pb_Hdown
            // 
            this.pb_Hdown.BackColor = System.Drawing.Color.Transparent;
            this.pb_Hdown.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_down;
            this.pb_Hdown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Hdown.Location = new System.Drawing.Point(259, 14);
            this.pb_Hdown.Name = "pb_Hdown";
            this.pb_Hdown.Size = new System.Drawing.Size(16, 13);
            this.pb_Hdown.TabIndex = 155;
            this.pb_Hdown.TabStop = false;
            this.pb_Hdown.Click += new System.EventHandler(this.pb_Hdown_Click);
            // 
            // pb_secup
            // 
            this.pb_secup.BackColor = System.Drawing.Color.Transparent;
            this.pb_secup.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_up;
            this.pb_secup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_secup.Location = new System.Drawing.Point(318, 2);
            this.pb_secup.Name = "pb_secup";
            this.pb_secup.Size = new System.Drawing.Size(16, 13);
            this.pb_secup.TabIndex = 153;
            this.pb_secup.TabStop = false;
            this.pb_secup.Click += new System.EventHandler(this.pb_secup_Click);
            // 
            // pb_secdown
            // 
            this.pb_secdown.BackColor = System.Drawing.Color.Transparent;
            this.pb_secdown.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_down;
            this.pb_secdown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_secdown.Location = new System.Drawing.Point(318, 15);
            this.pb_secdown.Name = "pb_secdown";
            this.pb_secdown.Size = new System.Drawing.Size(16, 13);
            this.pb_secdown.TabIndex = 152;
            this.pb_secdown.TabStop = false;
            this.pb_secdown.Click += new System.EventHandler(this.pb_secdown_Click);
            // 
            // pb_work_dateset_sel
            // 
            this.pb_work_dateset_sel.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_dateset_sel.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_re;
            this.pb_work_dateset_sel.Location = new System.Drawing.Point(341, 2);
            this.pb_work_dateset_sel.Name = "pb_work_dateset_sel";
            this.pb_work_dateset_sel.Size = new System.Drawing.Size(52, 24);
            this.pb_work_dateset_sel.TabIndex = 151;
            this.pb_work_dateset_sel.TabStop = false;
            this.pb_work_dateset_sel.Click += new System.EventHandler(this.pb_work_dateset_sel_Click);
            this.pb_work_dateset_sel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_dateset_sel_MouseDown);
            this.pb_work_dateset_sel.MouseEnter += new System.EventHandler(this.pb_work_dateset_sel_MouseEnter);
            this.pb_work_dateset_sel.MouseLeave += new System.EventHandler(this.pb_work_dateset_sel_MouseLeave);
            this.pb_work_dateset_sel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_dateset_sel_MouseUp);
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox10.BackgroundImage = global::NCafeCommenter.Properties.Resources.bar_reservation;
            this.pictureBox10.Location = new System.Drawing.Point(85, 6);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(2, 16);
            this.pictureBox10.TabIndex = 150;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox7.BackgroundImage = global::NCafeCommenter.Properties.Resources.icon_clock1;
            this.pictureBox7.Location = new System.Drawing.Point(10, 6);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(16, 16);
            this.pictureBox7.TabIndex = 149;
            this.pictureBox7.TabStop = false;
            // 
            // pb_work_datetime
            // 
            this.pb_work_datetime.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_datetime.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_next;
            this.pb_work_datetime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_work_datetime.Location = new System.Drawing.Point(202, 1);
            this.pb_work_datetime.Name = "pb_work_datetime";
            this.pb_work_datetime.Size = new System.Drawing.Size(16, 26);
            this.pb_work_datetime.TabIndex = 148;
            this.pb_work_datetime.TabStop = false;
            this.pb_work_datetime.Click += new System.EventHandler(this.pb_work_datetime_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox9.Location = new System.Drawing.Point(0, 0);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(336, 28);
            this.pictureBox9.TabIndex = 146;
            this.pictureBox9.TabStop = false;
            // 
            // lv_work_WorkList
            // 
            this.lv_work_WorkList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.lv_work_WorkList.BackgroundImageTiled = true;
            this.lv_work_WorkList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lv_work_WorkList.CheckBoxes = true;
            this.lv_work_WorkList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader5});
            this.lv_work_WorkList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.lv_work_WorkList.FullRowSelect = true;
            this.lv_work_WorkList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lv_work_WorkList.HideSelection = false;
            this.lv_work_WorkList.Location = new System.Drawing.Point(669, 175);
            this.lv_work_WorkList.Name = "lv_work_WorkList";
            this.lv_work_WorkList.Size = new System.Drawing.Size(384, 173);
            this.lv_work_WorkList.TabIndex = 35;
            this.lv_work_WorkList.UseCompatibleStateImageBehavior = false;
            this.lv_work_WorkList.View = System.Windows.Forms.View.Details;
            this.lv_work_WorkList.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lv_work_WorkList_ItemChecked);
            this.lv_work_WorkList.SelectedIndexChanged += new System.EventHandler(this.lv_work_WorkList_SelectedIndexChanged);
            this.lv_work_WorkList.DoubleClick += new System.EventHandler(this.lv_work_WorkList_DoubleClick);
            // 
            // columnHeader18
            // 
            this.columnHeader18.Width = 18;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Width = 38;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Width = 90;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Width = 88;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Width = 87;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Width = 44;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table3;
            this.pictureBox5.Location = new System.Drawing.Point(662, 174);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(393, 177);
            this.pictureBox5.TabIndex = 132;
            this.pictureBox5.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label6.Location = new System.Drawing.Point(655, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 12);
            this.label6.TabIndex = 129;
            this.label6.Text = "·아이디 선택";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label5.Location = new System.Drawing.Point(655, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 12);
            this.label5.TabIndex = 128;
            this.label5.Text = "·게시판 목록";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.label4.Location = new System.Drawing.Point(655, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 127;
            this.label4.Text = "·카페주소입력";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = global::NCafeCommenter.Properties.Resources.title_03;
            this.pictureBox6.Location = new System.Drawing.Point(662, 19);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(66, 16);
            this.pictureBox6.TabIndex = 125;
            this.pictureBox6.TabStop = false;
            // 
            // lv_work_ContentList
            // 
            this.lv_work_ContentList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.lv_work_ContentList.BackgroundImageTiled = true;
            this.lv_work_ContentList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lv_work_ContentList.CheckBoxes = true;
            this.lv_work_ContentList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader6});
            this.lv_work_ContentList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.lv_work_ContentList.FullRowSelect = true;
            this.lv_work_ContentList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lv_work_ContentList.HideSelection = false;
            this.lv_work_ContentList.Location = new System.Drawing.Point(355, 81);
            this.lv_work_ContentList.Name = "lv_work_ContentList";
            this.lv_work_ContentList.Size = new System.Drawing.Size(265, 87);
            this.lv_work_ContentList.TabIndex = 16;
            this.lv_work_ContentList.UseCompatibleStateImageBehavior = false;
            this.lv_work_ContentList.View = System.Windows.Forms.View.Details;
            this.lv_work_ContentList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lv_work_ContentList_MouseDoubleClick);
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "chk";
            this.columnHeader9.Width = 29;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "댓글내용";
            this.columnHeader10.Width = 132;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "스티커";
            this.columnHeader11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader11.Width = 49;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "사진";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader6.Width = 38;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table2;
            this.pictureBox3.Location = new System.Drawing.Point(347, 78);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(276, 93);
            this.pictureBox3.TabIndex = 124;
            this.pictureBox3.TabStop = false;
            // 
            // lv_work_IdList
            // 
            this.lv_work_IdList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.lv_work_IdList.BackgroundImageTiled = true;
            this.lv_work_IdList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lv_work_IdList.CheckBoxes = true;
            this.lv_work_IdList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lv_work_IdList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.lv_work_IdList.FullRowSelect = true;
            this.lv_work_IdList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lv_work_IdList.HideSelection = false;
            this.lv_work_IdList.Location = new System.Drawing.Point(22, 113);
            this.lv_work_IdList.Name = "lv_work_IdList";
            this.lv_work_IdList.OwnerDraw = true;
            this.lv_work_IdList.Size = new System.Drawing.Size(266, 263);
            this.lv_work_IdList.TabIndex = 9;
            this.lv_work_IdList.UseCompatibleStateImageBehavior = false;
            this.lv_work_IdList.View = System.Windows.Forms.View.Details;
            this.lv_work_IdList.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lv_work_IdList_DrawColumnHeader);
            this.lv_work_IdList.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.lv_work_IdList_DrawSubItem);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "chk";
            this.columnHeader1.Width = 29;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "아이디";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 88;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "비밀번호";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 93;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "검증";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader4.Width = 37;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table1;
            this.pictureBox1.Location = new System.Drawing.Point(15, 113);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(276, 265);
            this.pictureBox1.TabIndex = 123;
            this.pictureBox1.TabStop = false;
            // 
            // cb_work_IdList_All
            // 
            this.cb_work_IdList_All.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_work_IdList_All.BackColor = System.Drawing.Color.Transparent;
            this.cb_work_IdList_All.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.cb_work_IdList_All.FlatAppearance.BorderSize = 0;
            this.cb_work_IdList_All.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_work_IdList_All.Location = new System.Drawing.Point(27, 91);
            this.cb_work_IdList_All.Name = "cb_work_IdList_All";
            this.cb_work_IdList_All.Size = new System.Drawing.Size(12, 12);
            this.cb_work_IdList_All.TabIndex = 120;
            this.cb_work_IdList_All.UseVisualStyleBackColor = false;
            this.cb_work_IdList_All.CheckedChanged += new System.EventHandler(this.cb_work_IdList_All_CheckedChanged);
            // 
            // tb_work_Pw
            // 
            this.tb_work_Pw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.tb_work_Pw.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_work_Pw.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_work_Pw.Location = new System.Drawing.Point(133, 55);
            this.tb_work_Pw.Name = "tb_work_Pw";
            this.tb_work_Pw.Size = new System.Drawing.Size(83, 14);
            this.tb_work_Pw.TabIndex = 46;
            this.tb_work_Pw.Text = "비밀번호";
            this.tb_work_Pw.Enter += new System.EventHandler(this.tb_work_Pw_Enter);
            this.tb_work_Pw.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_work_Pw_KeyPress);
            this.tb_work_Pw.Leave += new System.EventHandler(this.tb_work_Pw_Leave);
            // 
            // tb_work_Id
            // 
            this.tb_work_Id.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.tb_work_Id.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_work_Id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_work_Id.Location = new System.Drawing.Point(22, 55);
            this.tb_work_Id.Name = "tb_work_Id";
            this.tb_work_Id.Size = new System.Drawing.Size(83, 14);
            this.tb_work_Id.TabIndex = 45;
            this.tb_work_Id.Text = "아이디";
            this.tb_work_Id.Enter += new System.EventHandler(this.tb_work_Id_Enter);
            this.tb_work_Id.Leave += new System.EventHandler(this.tb_work_Id_Leave);
            // 
            // pb_work_Log_Save
            // 
            this.pb_work_Log_Save.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_Log_Save.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_save;
            this.pb_work_Log_Save.Location = new System.Drawing.Point(944, 435);
            this.pb_work_Log_Save.Name = "pb_work_Log_Save";
            this.pb_work_Log_Save.Size = new System.Drawing.Size(112, 28);
            this.pb_work_Log_Save.TabIndex = 44;
            this.pb_work_Log_Save.TabStop = false;
            this.pb_work_Log_Save.Click += new System.EventHandler(this.pb_work_Log_Save_Click);
            this.pb_work_Log_Save.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_Log_Save_MouseDown);
            this.pb_work_Log_Save.MouseEnter += new System.EventHandler(this.pb_work_Log_Save_MouseEnter);
            this.pb_work_Log_Save.MouseLeave += new System.EventHandler(this.pb_work_Log_Save_MouseLeave);
            this.pb_work_Log_Save.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_Log_Save_MouseUp);
            // 
            // lb_work_Status
            // 
            this.lb_work_Status.AutoSize = true;
            this.lb_work_Status.BackColor = System.Drawing.Color.Transparent;
            this.lb_work_Status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(57)))), ((int)(((byte)(93)))));
            this.lb_work_Status.Location = new System.Drawing.Point(419, 444);
            this.lb_work_Status.Name = "lb_work_Status";
            this.lb_work_Status.Size = new System.Drawing.Size(85, 12);
            this.lb_work_Status.TabIndex = 43;
            this.lb_work_Status.Text = "※ 작업 대기중";
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox31.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox31.BackgroundImage")));
            this.pictureBox31.Location = new System.Drawing.Point(347, 440);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(66, 16);
            this.pictureBox31.TabIndex = 40;
            this.pictureBox31.TabStop = false;
            // 
            // pb_work_WorkList_Seldel
            // 
            this.pb_work_WorkList_Seldel.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_WorkList_Seldel.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_seldel;
            this.pb_work_WorkList_Seldel.Location = new System.Drawing.Point(943, 382);
            this.pb_work_WorkList_Seldel.Name = "pb_work_WorkList_Seldel";
            this.pb_work_WorkList_Seldel.Size = new System.Drawing.Size(112, 28);
            this.pb_work_WorkList_Seldel.TabIndex = 38;
            this.pb_work_WorkList_Seldel.TabStop = false;
            this.pb_work_WorkList_Seldel.Click += new System.EventHandler(this.pb_work_WorkList_Seldel_Click);
            this.pb_work_WorkList_Seldel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_WorkList_Seldel_MouseDown);
            this.pb_work_WorkList_Seldel.MouseEnter += new System.EventHandler(this.pb_work_WorkList_Seldel_MouseEnter);
            this.pb_work_WorkList_Seldel.MouseLeave += new System.EventHandler(this.pb_work_WorkList_Seldel_MouseLeave);
            this.pb_work_WorkList_Seldel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_WorkList_Seldel_MouseUp);
            // 
            // pb_work_Stop
            // 
            this.pb_work_Stop.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_Stop.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_stop;
            this.pb_work_Stop.Location = new System.Drawing.Point(826, 382);
            this.pb_work_Stop.Name = "pb_work_Stop";
            this.pb_work_Stop.Size = new System.Drawing.Size(112, 28);
            this.pb_work_Stop.TabIndex = 37;
            this.pb_work_Stop.TabStop = false;
            this.pb_work_Stop.Click += new System.EventHandler(this.pb_work_Stop_Click);
            this.pb_work_Stop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_Stop_MouseDown);
            this.pb_work_Stop.MouseEnter += new System.EventHandler(this.pb_work_Stop_MouseEnter);
            this.pb_work_Stop.MouseLeave += new System.EventHandler(this.pb_work_Stop_MouseLeave);
            this.pb_work_Stop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_Stop_MouseUp);
            // 
            // pb_work_Start
            // 
            this.pb_work_Start.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_Start.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_start;
            this.pb_work_Start.Location = new System.Drawing.Point(662, 382);
            this.pb_work_Start.Name = "pb_work_Start";
            this.pb_work_Start.Size = new System.Drawing.Size(159, 28);
            this.pb_work_Start.TabIndex = 36;
            this.pb_work_Start.TabStop = false;
            this.pb_work_Start.Click += new System.EventHandler(this.pb_work_Start_Click);
            this.pb_work_Start.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_Start_MouseDown);
            this.pb_work_Start.MouseEnter += new System.EventHandler(this.pb_work_Start_MouseEnter);
            this.pb_work_Start.MouseLeave += new System.EventHandler(this.pb_work_Start_MouseLeave);
            this.pb_work_Start.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_Start_MouseUp);
            // 
            // pb_work_IdList_Sel
            // 
            this.pb_work_IdList_Sel.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_IdList_Sel.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_selec;
            this.pb_work_IdList_Sel.Location = new System.Drawing.Point(1004, 49);
            this.pb_work_IdList_Sel.Name = "pb_work_IdList_Sel";
            this.pb_work_IdList_Sel.Size = new System.Drawing.Size(52, 24);
            this.pb_work_IdList_Sel.TabIndex = 28;
            this.pb_work_IdList_Sel.TabStop = false;
            this.pb_work_IdList_Sel.Tag = "N";
            this.pb_work_IdList_Sel.Click += new System.EventHandler(this.pb_work_IdList_Sel_Click);
            this.pb_work_IdList_Sel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_IdList_Sel_MouseDown);
            this.pb_work_IdList_Sel.MouseEnter += new System.EventHandler(this.pb_work_IdList_Sel_MouseEnter);
            this.pb_work_IdList_Sel.MouseLeave += new System.EventHandler(this.pb_work_IdList_Sel_MouseLeave);
            this.pb_work_IdList_Sel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_IdList_Sel_MouseUp);
            // 
            // pb_work_CafeContentList_Sel
            // 
            this.pb_work_CafeContentList_Sel.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_CafeContentList_Sel.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_selec;
            this.pb_work_CafeContentList_Sel.Location = new System.Drawing.Point(1004, 113);
            this.pb_work_CafeContentList_Sel.Name = "pb_work_CafeContentList_Sel";
            this.pb_work_CafeContentList_Sel.Size = new System.Drawing.Size(52, 24);
            this.pb_work_CafeContentList_Sel.TabIndex = 24;
            this.pb_work_CafeContentList_Sel.TabStop = false;
            this.pb_work_CafeContentList_Sel.Click += new System.EventHandler(this.pb_work_CafeContentList_Sel_Click);
            this.pb_work_CafeContentList_Sel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_CafeContentList_Sel_MouseDown);
            this.pb_work_CafeContentList_Sel.MouseEnter += new System.EventHandler(this.pb_work_CafeContentList_Sel_MouseEnter);
            this.pb_work_CafeContentList_Sel.MouseLeave += new System.EventHandler(this.pb_work_CafeContentList_Sel_MouseLeave);
            this.pb_work_CafeContentList_Sel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_CafeContentList_Sel_MouseUp);
            // 
            // pb_work_CafeURL_Search
            // 
            this.pb_work_CafeURL_Search.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_CafeURL_Search.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_selec;
            this.pb_work_CafeURL_Search.Location = new System.Drawing.Point(1004, 81);
            this.pb_work_CafeURL_Search.Name = "pb_work_CafeURL_Search";
            this.pb_work_CafeURL_Search.Size = new System.Drawing.Size(52, 24);
            this.pb_work_CafeURL_Search.TabIndex = 21;
            this.pb_work_CafeURL_Search.TabStop = false;
            this.pb_work_CafeURL_Search.Click += new System.EventHandler(this.pb_work_CafeURL_Search_Click);
            this.pb_work_CafeURL_Search.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_CafeURL_Search_MouseDown);
            this.pb_work_CafeURL_Search.MouseEnter += new System.EventHandler(this.pb_work_CafeURL_Search_MouseEnter);
            this.pb_work_CafeURL_Search.MouseLeave += new System.EventHandler(this.pb_work_CafeURL_Search_MouseLeave);
            this.pb_work_CafeURL_Search.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_CafeURL_Search_MouseUp);
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox14.Location = new System.Drawing.Point(639, 47);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(2, 363);
            this.pictureBox14.TabIndex = 19;
            this.pictureBox14.TabStop = false;
            // 
            // pb_work_contenttitle
            // 
            this.pb_work_contenttitle.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_contenttitle.BackgroundImage = global::NCafeCommenter.Properties.Resources.title_02;
            this.pb_work_contenttitle.Location = new System.Drawing.Point(347, 19);
            this.pb_work_contenttitle.Name = "pb_work_contenttitle";
            this.pb_work_contenttitle.Size = new System.Drawing.Size(70, 16);
            this.pb_work_contenttitle.TabIndex = 13;
            this.pb_work_contenttitle.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.Location = new System.Drawing.Point(321, 47);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(2, 363);
            this.pictureBox8.TabIndex = 12;
            this.pictureBox8.TabStop = false;
            // 
            // pb_work_IdSelDel
            // 
            this.pb_work_IdSelDel.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_IdSelDel.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_seldel;
            this.pb_work_IdSelDel.Location = new System.Drawing.Point(179, 382);
            this.pb_work_IdSelDel.Name = "pb_work_IdSelDel";
            this.pb_work_IdSelDel.Size = new System.Drawing.Size(112, 28);
            this.pb_work_IdSelDel.TabIndex = 11;
            this.pb_work_IdSelDel.TabStop = false;
            this.pb_work_IdSelDel.Click += new System.EventHandler(this.pb_work_IdSelDel_Click);
            this.pb_work_IdSelDel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_IdSelDel_MouseDown);
            this.pb_work_IdSelDel.MouseEnter += new System.EventHandler(this.pb_work_IdSelDel_MouseEnter);
            this.pb_work_IdSelDel.MouseLeave += new System.EventHandler(this.pb_work_IdSelDel_MouseLeave);
            this.pb_work_IdSelDel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_IdSelDel_MouseUp);
            // 
            // pb_work_Idcert
            // 
            this.pb_work_Idcert.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_Idcert.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_idverification;
            this.pb_work_Idcert.Location = new System.Drawing.Point(15, 382);
            this.pb_work_Idcert.Name = "pb_work_Idcert";
            this.pb_work_Idcert.Size = new System.Drawing.Size(159, 28);
            this.pb_work_Idcert.TabIndex = 10;
            this.pb_work_Idcert.TabStop = false;
            this.pb_work_Idcert.Click += new System.EventHandler(this.pb_work_Idcert_Click);
            this.pb_work_Idcert.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_work_Idcert_MouseDown);
            this.pb_work_Idcert.MouseEnter += new System.EventHandler(this.pb_work_Idcert_MouseEnter);
            this.pb_work_Idcert.MouseLeave += new System.EventHandler(this.pb_work_Idcert_MouseLeave);
            this.pb_work_Idcert.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_work_Idcert_MouseUp);
            // 
            // pb_work_Idheader
            // 
            this.pb_work_Idheader.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_Idheader.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table1_header;
            this.pb_work_Idheader.Location = new System.Drawing.Point(15, 81);
            this.pb_work_Idheader.Name = "pb_work_Idheader";
            this.pb_work_Idheader.Size = new System.Drawing.Size(276, 32);
            this.pb_work_Idheader.TabIndex = 8;
            this.pb_work_Idheader.TabStop = false;
            // 
            // tb_work_IdAdd
            // 
            this.tb_work_IdAdd.BackColor = System.Drawing.Color.Transparent;
            this.tb_work_IdAdd.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_add;
            this.tb_work_IdAdd.Location = new System.Drawing.Point(239, 50);
            this.tb_work_IdAdd.Name = "tb_work_IdAdd";
            this.tb_work_IdAdd.Size = new System.Drawing.Size(52, 24);
            this.tb_work_IdAdd.TabIndex = 7;
            this.tb_work_IdAdd.TabStop = false;
            this.tb_work_IdAdd.Click += new System.EventHandler(this.tb_work_IdAdd_Click);
            this.tb_work_IdAdd.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tb_work_IdAdd_MouseDown);
            this.tb_work_IdAdd.MouseEnter += new System.EventHandler(this.tb_work_IdAdd_MouseEnter);
            this.tb_work_IdAdd.MouseLeave += new System.EventHandler(this.tb_work_IdAdd_MouseLeave);
            this.tb_work_IdAdd.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tb_work_IdAdd_MouseUp);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table1_input;
            this.pictureBox4.Location = new System.Drawing.Point(126, 47);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(107, 28);
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_table1_input;
            this.pictureBox2.Location = new System.Drawing.Point(15, 47);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(107, 28);
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // pb_work_idtitle
            // 
            this.pb_work_idtitle.BackColor = System.Drawing.Color.Transparent;
            this.pb_work_idtitle.BackgroundImage = global::NCafeCommenter.Properties.Resources.title_01;
            this.pb_work_idtitle.Location = new System.Drawing.Point(15, 19);
            this.pb_work_idtitle.Name = "pb_work_idtitle";
            this.pb_work_idtitle.Size = new System.Drawing.Size(98, 15);
            this.pb_work_idtitle.TabIndex = 4;
            this.pb_work_idtitle.TabStop = false;
            // 
            // pb_work_headsubline
            // 
            this.pb_work_headsubline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(57)))), ((int)(((byte)(79)))));
            this.pb_work_headsubline.Location = new System.Drawing.Point(0, 0);
            this.pb_work_headsubline.Name = "pb_work_headsubline";
            this.pb_work_headsubline.Size = new System.Drawing.Size(1070, 2);
            this.pb_work_headsubline.TabIndex = 3;
            this.pb_work_headsubline.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox21.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_commonid;
            this.pictureBox21.Location = new System.Drawing.Point(761, 47);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(237, 28);
            this.pictureBox21.TabIndex = 27;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox15.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_commonid;
            this.pictureBox15.Location = new System.Drawing.Point(761, 79);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(237, 28);
            this.pictureBox15.TabIndex = 20;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox18.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_commonid;
            this.pictureBox18.Location = new System.Drawing.Point(761, 111);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(237, 28);
            this.pictureBox18.TabIndex = 23;
            this.pictureBox18.TabStop = false;
            // 
            // pn_header
            // 
            this.pn_header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(64)))), ((int)(((byte)(70)))));
            this.pn_header.Controls.Add(this.lb_header_ver);
            this.pn_header.Controls.Add(this.lb_header_notice);
            this.pn_header.Controls.Add(this.pb_header_close);
            this.pn_header.Controls.Add(this.pb_header_mini);
            this.pn_header.Controls.Add(this.pb_header_homepage);
            this.pn_header.Controls.Add(this.pn_header_notice);
            this.pn_header.Controls.Add(this.pn_header_logo);
            this.pn_header.Location = new System.Drawing.Point(0, 0);
            this.pn_header.Name = "pn_header";
            this.pn_header.Size = new System.Drawing.Size(1100, 32);
            this.pn_header.TabIndex = 3;
            this.pn_header.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pn_header_MouseDown);
            // 
            // lb_header_ver
            // 
            this.lb_header_ver.AutoSize = true;
            this.lb_header_ver.BackColor = System.Drawing.Color.Transparent;
            this.lb_header_ver.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(172)))), ((int)(((byte)(171)))));
            this.lb_header_ver.Location = new System.Drawing.Point(160, 13);
            this.lb_header_ver.Name = "lb_header_ver";
            this.lb_header_ver.Size = new System.Drawing.Size(54, 12);
            this.lb_header_ver.TabIndex = 30;
            this.lb_header_ver.Text = "Ver.1.0.0";
            // 
            // lb_header_notice
            // 
            this.lb_header_notice.AutoSize = true;
            this.lb_header_notice.BackColor = System.Drawing.Color.Transparent;
            this.lb_header_notice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb_header_notice.ForeColor = System.Drawing.Color.White;
            this.lb_header_notice.Location = new System.Drawing.Point(258, 12);
            this.lb_header_notice.Name = "lb_header_notice";
            this.lb_header_notice.Size = new System.Drawing.Size(39, 12);
            this.lb_header_notice.TabIndex = 29;
            this.lb_header_notice.Text = "notice";
            this.lb_header_notice.Click += new System.EventHandler(this.lb_header_notice_Click);
            // 
            // pb_header_close
            // 
            this.pb_header_close.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_header_close;
            this.pb_header_close.Location = new System.Drawing.Point(1060, 8);
            this.pb_header_close.Name = "pb_header_close";
            this.pb_header_close.Size = new System.Drawing.Size(17, 17);
            this.pb_header_close.TabIndex = 5;
            this.pb_header_close.TabStop = false;
            this.pb_header_close.Click += new System.EventHandler(this.pb_header_close_Click);
            this.pb_header_close.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_header_close_MouseDown);
            this.pb_header_close.MouseEnter += new System.EventHandler(this.pb_header_close_MouseEnter);
            this.pb_header_close.MouseLeave += new System.EventHandler(this.pb_header_close_MouseLeave);
            this.pb_header_close.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_header_close_MouseUp);
            // 
            // pb_header_mini
            // 
            this.pb_header_mini.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_header_mini;
            this.pb_header_mini.Location = new System.Drawing.Point(1036, 8);
            this.pb_header_mini.Name = "pb_header_mini";
            this.pb_header_mini.Size = new System.Drawing.Size(17, 17);
            this.pb_header_mini.TabIndex = 4;
            this.pb_header_mini.TabStop = false;
            this.pb_header_mini.Click += new System.EventHandler(this.pb_header_mini_Click);
            this.pb_header_mini.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_header_mini_MouseDown);
            this.pb_header_mini.MouseEnter += new System.EventHandler(this.pb_header_mini_MouseEnter);
            this.pb_header_mini.MouseLeave += new System.EventHandler(this.pb_header_mini_MouseLeave);
            this.pb_header_mini.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_header_mini_MouseUp);
            // 
            // pb_header_homepage
            // 
            this.pb_header_homepage.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_hp;
            this.pb_header_homepage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_header_homepage.Location = new System.Drawing.Point(901, 4);
            this.pb_header_homepage.Name = "pb_header_homepage";
            this.pb_header_homepage.Size = new System.Drawing.Size(129, 23);
            this.pb_header_homepage.TabIndex = 3;
            this.pb_header_homepage.TabStop = false;
            this.pb_header_homepage.Click += new System.EventHandler(this.pb_header_homepage_Click);
            // 
            // pn_header_notice
            // 
            this.pn_header_notice.BackgroundImage = global::NCafeCommenter.Properties.Resources.icon_announce;
            this.pn_header_notice.Location = new System.Drawing.Point(239, 11);
            this.pn_header_notice.Name = "pn_header_notice";
            this.pn_header_notice.Size = new System.Drawing.Size(12, 12);
            this.pn_header_notice.TabIndex = 2;
            this.pn_header_notice.TabStop = false;
            // 
            // pn_header_logo
            // 
            this.pn_header_logo.BackgroundImage = global::NCafeCommenter.Properties.Resources.logo_title;
            this.pn_header_logo.Location = new System.Drawing.Point(15, 6);
            this.pn_header_logo.Name = "pn_header_logo";
            this.pn_header_logo.Size = new System.Drawing.Size(142, 20);
            this.pn_header_logo.TabIndex = 1;
            this.pn_header_logo.TabStop = false;
            // 
            // pn_Slogin
            // 
            this.pn_Slogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(38)))), ((int)(((byte)(44)))));
            this.pn_Slogin.Controls.Add(this.lb_SLogin_Day);
            this.pn_Slogin.Controls.Add(this.lb_SLogin_Hard2);
            this.pn_Slogin.Controls.Add(this.lb_SLogin_Hard);
            this.pn_Slogin.Controls.Add(this.lb_SLogin_ID2);
            this.pn_Slogin.Controls.Add(this.lb_SLogin_Id);
            this.pn_Slogin.Controls.Add(this.lb_SLogin_Hello);
            this.pn_Slogin.Controls.Add(this.lb_SLogin_Day2);
            this.pn_Slogin.Controls.Add(this.pb_Slogin_Day);
            this.pn_Slogin.Location = new System.Drawing.Point(588, 62);
            this.pn_Slogin.Name = "pn_Slogin";
            this.pn_Slogin.Size = new System.Drawing.Size(500, 25);
            this.pn_Slogin.TabIndex = 32;
            this.pn_Slogin.Visible = false;
            // 
            // lb_SLogin_Day
            // 
            this.lb_SLogin_Day.AutoSize = true;
            this.lb_SLogin_Day.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(44)))), ((int)(((byte)(52)))));
            this.lb_SLogin_Day.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_SLogin_Day.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(57)))), ((int)(((byte)(93)))));
            this.lb_SLogin_Day.Location = new System.Drawing.Point(412, 7);
            this.lb_SLogin_Day.Name = "lb_SLogin_Day";
            this.lb_SLogin_Day.Size = new System.Drawing.Size(57, 12);
            this.lb_SLogin_Day.TabIndex = 97;
            this.lb_SLogin_Day.Text = "정식버전";
            // 
            // lb_SLogin_Hard2
            // 
            this.lb_SLogin_Hard2.AutoSize = true;
            this.lb_SLogin_Hard2.BackColor = System.Drawing.Color.Transparent;
            this.lb_SLogin_Hard2.ForeColor = System.Drawing.Color.White;
            this.lb_SLogin_Hard2.Location = new System.Drawing.Point(81, 7);
            this.lb_SLogin_Hard2.Name = "lb_SLogin_Hard2";
            this.lb_SLogin_Hard2.Size = new System.Drawing.Size(81, 12);
            this.lb_SLogin_Hard2.TabIndex = 103;
            this.lb_SLogin_Hard2.Text = "하드웨어 이름";
            // 
            // lb_SLogin_Hard
            // 
            this.lb_SLogin_Hard.AutoSize = true;
            this.lb_SLogin_Hard.BackColor = System.Drawing.Color.Transparent;
            this.lb_SLogin_Hard.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_SLogin_Hard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(57)))), ((int)(((byte)(93)))));
            this.lb_SLogin_Hard.Location = new System.Drawing.Point(161, 7);
            this.lb_SLogin_Hard.Name = "lb_SLogin_Hard";
            this.lb_SLogin_Hard.Size = new System.Drawing.Size(92, 12);
            this.lb_SLogin_Hard.TabIndex = 102;
            this.lb_SLogin_Hard.Text = "[ABCDEFGH]";
            // 
            // lb_SLogin_ID2
            // 
            this.lb_SLogin_ID2.AutoSize = true;
            this.lb_SLogin_ID2.BackColor = System.Drawing.Color.Transparent;
            this.lb_SLogin_ID2.ForeColor = System.Drawing.Color.White;
            this.lb_SLogin_ID2.Location = new System.Drawing.Point(252, 7);
            this.lb_SLogin_ID2.Name = "lb_SLogin_ID2";
            this.lb_SLogin_ID2.Size = new System.Drawing.Size(11, 12);
            this.lb_SLogin_ID2.TabIndex = 101;
            this.lb_SLogin_ID2.Text = "|";
            // 
            // lb_SLogin_Id
            // 
            this.lb_SLogin_Id.AutoSize = true;
            this.lb_SLogin_Id.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lb_SLogin_Id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(57)))), ((int)(((byte)(93)))));
            this.lb_SLogin_Id.Location = new System.Drawing.Point(267, 7);
            this.lb_SLogin_Id.Name = "lb_SLogin_Id";
            this.lb_SLogin_Id.Size = new System.Drawing.Size(18, 12);
            this.lb_SLogin_Id.TabIndex = 100;
            this.lb_SLogin_Id.Text = "ID";
            // 
            // lb_SLogin_Hello
            // 
            this.lb_SLogin_Hello.AutoSize = true;
            this.lb_SLogin_Hello.ForeColor = System.Drawing.Color.White;
            this.lb_SLogin_Hello.Location = new System.Drawing.Point(292, 7);
            this.lb_SLogin_Hello.Name = "lb_SLogin_Hello";
            this.lb_SLogin_Hello.Size = new System.Drawing.Size(85, 12);
            this.lb_SLogin_Hello.TabIndex = 99;
            this.lb_SLogin_Hello.Text = "님 반갑습니다!";
            // 
            // lb_SLogin_Day2
            // 
            this.lb_SLogin_Day2.AutoSize = true;
            this.lb_SLogin_Day2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(44)))), ((int)(((byte)(52)))));
            this.lb_SLogin_Day2.ForeColor = System.Drawing.Color.White;
            this.lb_SLogin_Day2.Location = new System.Drawing.Point(393, 7);
            this.lb_SLogin_Day2.Name = "lb_SLogin_Day2";
            this.lb_SLogin_Day2.Size = new System.Drawing.Size(61, 12);
            this.lb_SLogin_Day2.TabIndex = 98;
            this.lb_SLogin_Day2.Text = "잔여일수 :";
            this.lb_SLogin_Day2.Visible = false;
            // 
            // pb_Slogin_Day
            // 
            this.pb_Slogin_Day.BackColor = System.Drawing.Color.Transparent;
            this.pb_Slogin_Day.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_remaining;
            this.pb_Slogin_Day.Location = new System.Drawing.Point(380, 0);
            this.pb_Slogin_Day.Name = "pb_Slogin_Day";
            this.pb_Slogin_Day.Size = new System.Drawing.Size(120, 25);
            this.pb_Slogin_Day.TabIndex = 32;
            this.pb_Slogin_Day.TabStop = false;
            this.pb_Slogin_Day.Click += new System.EventHandler(this.pb_Slogin_Day_Click);
            this.pb_Slogin_Day.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_Slogin_Day_MouseDown);
            this.pb_Slogin_Day.MouseEnter += new System.EventHandler(this.pb_Slogin_Day_MouseEnter);
            this.pb_Slogin_Day.MouseLeave += new System.EventHandler(this.pb_Slogin_Day_MouseLeave);
            this.pb_Slogin_Day.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_Slogin_Day_MouseUp);
            // 
            // pn_login
            // 
            this.pn_login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(38)))), ((int)(((byte)(44)))));
            this.pn_login.Controls.Add(this.tb_login_pw);
            this.pn_login.Controls.Add(this.tb_login_id);
            this.pn_login.Controls.Add(this.pb_login_reg);
            this.pn_login.Controls.Add(this.pb_login_login);
            this.pn_login.Controls.Add(this.pictureBox25);
            this.pn_login.Controls.Add(this.label2);
            this.pn_login.Controls.Add(this.pictureBox23);
            this.pn_login.Controls.Add(this.label1);
            this.pn_login.Location = new System.Drawing.Point(588, 46);
            this.pn_login.Name = "pn_login";
            this.pn_login.Size = new System.Drawing.Size(500, 25);
            this.pn_login.TabIndex = 31;
            // 
            // tb_login_pw
            // 
            this.tb_login_pw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.tb_login_pw.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_login_pw.ForeColor = System.Drawing.Color.White;
            this.tb_login_pw.Location = new System.Drawing.Point(245, 7);
            this.tb_login_pw.Name = "tb_login_pw";
            this.tb_login_pw.PasswordChar = '●';
            this.tb_login_pw.Size = new System.Drawing.Size(84, 14);
            this.tb_login_pw.TabIndex = 1;
            this.tb_login_pw.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_login_pw_KeyPress);
            // 
            // tb_login_id
            // 
            this.tb_login_id.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(37)))), ((int)(((byte)(45)))));
            this.tb_login_id.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_login_id.ForeColor = System.Drawing.Color.White;
            this.tb_login_id.Location = new System.Drawing.Point(70, 7);
            this.tb_login_id.Name = "tb_login_id";
            this.tb_login_id.Size = new System.Drawing.Size(85, 14);
            this.tb_login_id.TabIndex = 0;
            // 
            // pb_login_reg
            // 
            this.pb_login_reg.BackColor = System.Drawing.Color.Transparent;
            this.pb_login_reg.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_join;
            this.pb_login_reg.Location = new System.Drawing.Point(431, 0);
            this.pb_login_reg.Name = "pb_login_reg";
            this.pb_login_reg.Size = new System.Drawing.Size(69, 25);
            this.pb_login_reg.TabIndex = 33;
            this.pb_login_reg.TabStop = false;
            this.pb_login_reg.Click += new System.EventHandler(this.pb_login_reg_Click);
            this.pb_login_reg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_login_reg_MouseDown);
            this.pb_login_reg.MouseEnter += new System.EventHandler(this.pb_login_reg_MouseEnter);
            this.pb_login_reg.MouseLeave += new System.EventHandler(this.pb_login_reg_MouseLeave);
            this.pb_login_reg.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_login_reg_MouseUp);
            // 
            // pb_login_login
            // 
            this.pb_login_login.BackColor = System.Drawing.Color.Transparent;
            this.pb_login_login.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_login;
            this.pb_login_login.Location = new System.Drawing.Point(351, 0);
            this.pb_login_login.Name = "pb_login_login";
            this.pb_login_login.Size = new System.Drawing.Size(69, 25);
            this.pb_login_login.TabIndex = 32;
            this.pb_login_login.TabStop = false;
            this.pb_login_login.Click += new System.EventHandler(this.pb_login_login_Click);
            this.pb_login_login.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_login_login_MouseDown);
            this.pb_login_login.MouseEnter += new System.EventHandler(this.pb_login_login_MouseEnter);
            this.pb_login_login.MouseLeave += new System.EventHandler(this.pb_login_login_MouseLeave);
            this.pb_login_login.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_login_login_MouseUp);
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox25.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_idpassword;
            this.pictureBox25.Location = new System.Drawing.Point(237, 0);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(101, 25);
            this.pictureBox25.TabIndex = 31;
            this.pictureBox25.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(172, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 12);
            this.label2.TabIndex = 30;
            this.label2.Text = "· 비밀번호";
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox23.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_idpassword;
            this.pictureBox23.Location = new System.Drawing.Point(62, 0);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(101, 25);
            this.pictureBox23.TabIndex = 29;
            this.pictureBox23.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(10, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 12);
            this.label1.TabIndex = 28;
            this.label1.Text = "· 아이디";
            // 
            // pn_main_logo
            // 
            this.pn_main_logo.BackColor = System.Drawing.Color.Transparent;
            this.pn_main_logo.BackgroundImage = global::NCafeCommenter.Properties.Resources.img_logo_big;
            this.pn_main_logo.Location = new System.Drawing.Point(15, 48);
            this.pn_main_logo.Name = "pn_main_logo";
            this.pn_main_logo.Size = new System.Drawing.Size(249, 23);
            this.pn_main_logo.TabIndex = 33;
            this.pn_main_logo.TabStop = false;
            this.pn_main_logo.Click += new System.EventHandler(this.pn_main_logo_Click);
            // 
            // pb_Test
            // 
            this.pb_Test.BackColor = System.Drawing.Color.Transparent;
            this.pb_Test.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_add;
            this.pb_Test.Location = new System.Drawing.Point(270, 48);
            this.pb_Test.Name = "pb_Test";
            this.pb_Test.Size = new System.Drawing.Size(52, 24);
            this.pb_Test.TabIndex = 34;
            this.pb_Test.TabStop = false;
            this.pb_Test.Visible = false;
            this.pb_Test.Click += new System.EventHandler(this.pb_Test_Click);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg;
            this.ClientSize = new System.Drawing.Size(1100, 695);
            this.Controls.Add(this.pb_Test);
            this.Controls.Add(this.pn_login);
            this.Controls.Add(this.pn_main_logo);
            this.Controls.Add(this.pn_Slogin);
            this.Controls.Add(this.pn_header);
            this.Controls.Add(this.pn_work);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NCafeCommenter";
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.pn_work.ResumeLayout(false);
            this.pn_work.PerformLayout();
            this.pn_URL.ResumeLayout(false);
            this.pn_URL.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_URL_File)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_URL_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_URL_SelDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_URL_Stop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_URL_Start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddFile)).EndInit();
            this.pn_Option.ResumeLayout(false);
            this.pn_Option.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).EndInit();
            this.pn_ExcepID.ResumeLayout(false);
            this.pn_ExcepID.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ExcepIDSelDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ExcepIDFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ExcepIDAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bt_ExceptID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Nick)).EndInit();
            this.pn_Nick.ResumeLayout(false);
            this.pn_Nick.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Nick_File)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Nick_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Nick_SelDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            this.pn_Reply.ResumeLayout(false);
            this.pn_Reply.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_reply_clear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Reply_cancle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Reply_ok)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Option)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            this.pn_Keyword_Write.ResumeLayout(false);
            this.pn_Keyword_Write.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_keyword_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_KeywordSelDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_KeywordEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_keyword_Write)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_ContentList_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_ContentSelDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_option_Sel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Worklistheader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Contentheader)).EndInit();
            this.pn_work_dateset.ResumeLayout(false);
            this.pn_work_dateset.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Hup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Hdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_secup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_secdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_dateset_sel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_datetime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Log_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_WorkList_Seldel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Stop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_IdList_Sel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_CafeContentList_Sel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_CafeURL_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_contenttitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_IdSelDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Idcert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_Idheader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_work_IdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_idtitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_work_headsubline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            this.pn_header.ResumeLayout(false);
            this.pn_header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_header_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_header_mini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_header_homepage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_header_notice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_header_logo)).EndInit();
            this.pn_Slogin.ResumeLayout(false);
            this.pn_Slogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Slogin_Day)).EndInit();
            this.pn_login.ResumeLayout(false);
            this.pn_login.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_login_reg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_login_login)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pn_main_logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Test)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pn_work;
        private System.Windows.Forms.MonthCalendar mc_work_crd;
        private System.Windows.Forms.TextBox tb_work_log;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.CheckBox cb_work_WorktList_All;
        private System.Windows.Forms.PictureBox pb_work_Worklistheader;
        private System.Windows.Forms.CheckBox cb_work_ContentList_All;
        private System.Windows.Forms.PictureBox pb_work_Contentheader;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Panel pn_work_dateset;
        private System.Windows.Forms.TextBox tb_work_datetime_mm;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.TextBox tb_work_datetime_HH;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.TextBox tb_work_datetime;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pb_Hup;
        private System.Windows.Forms.PictureBox pb_Hdown;
        private System.Windows.Forms.PictureBox pb_secup;
        private System.Windows.Forms.PictureBox pb_secdown;
        private System.Windows.Forms.PictureBox pb_work_dateset_sel;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pb_work_datetime;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.ListView lv_work_WorkList;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.ListView lv_work_ContentList;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.ListView lv_work_IdList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox cb_work_IdList_All;
        private System.Windows.Forms.TextBox tb_work_Pw;
        private System.Windows.Forms.TextBox tb_work_Id;
        private System.Windows.Forms.PictureBox pb_work_Log_Save;
        private System.Windows.Forms.Label lb_work_Status;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pb_work_WorkList_Seldel;
        private System.Windows.Forms.PictureBox pb_work_Stop;
        private System.Windows.Forms.PictureBox pb_work_Start;
        private System.Windows.Forms.PictureBox pb_work_IdList_Sel;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pb_work_CafeContentList_Sel;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pb_work_CafeURL_Search;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pb_work_ContentSelDel;
        private System.Windows.Forms.PictureBox pb_work_ContentList_Add;
        private System.Windows.Forms.PictureBox pb_work_contenttitle;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pb_work_IdSelDel;
        private System.Windows.Forms.PictureBox pb_work_Idcert;
        private System.Windows.Forms.PictureBox pb_work_Idheader;
        private System.Windows.Forms.PictureBox tb_work_IdAdd;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pb_work_idtitle;
        private System.Windows.Forms.PictureBox pb_work_headsubline;
        private FlatCombo fc_work_CafeList;
        private FlatCombo fc_work_ContentList;
        private FlatCombo fc_work_IdList;
        private FlatCombo fc_work_CafeContentList;
        private System.Windows.Forms.Panel pn_header;
        private System.Windows.Forms.Label lb_header_ver;
        private System.Windows.Forms.Label lb_header_notice;
        private System.Windows.Forms.PictureBox pb_header_close;
        private System.Windows.Forms.PictureBox pb_header_mini;
        private System.Windows.Forms.PictureBox pb_header_homepage;
        private System.Windows.Forms.PictureBox pn_header_notice;
        private System.Windows.Forms.PictureBox pn_header_logo;
        private System.Windows.Forms.Panel pn_Slogin;
        private System.Windows.Forms.Label lb_SLogin_Day;
        private System.Windows.Forms.Label lb_SLogin_Hard2;
        private System.Windows.Forms.Label lb_SLogin_Hard;
        private System.Windows.Forms.Label lb_SLogin_ID2;
        private System.Windows.Forms.Label lb_SLogin_Id;
        private System.Windows.Forms.Label lb_SLogin_Hello;
        private System.Windows.Forms.Label lb_SLogin_Day2;
        private System.Windows.Forms.PictureBox pb_Slogin_Day;
        private System.Windows.Forms.Panel pn_login;
        private System.Windows.Forms.TextBox tb_login_pw;
        private System.Windows.Forms.TextBox tb_login_id;
        private System.Windows.Forms.PictureBox pb_login_reg;
        private System.Windows.Forms.PictureBox pb_login_login;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pn_main_logo;
        private System.Windows.Forms.PictureBox pictureBox47;
        private System.Windows.Forms.PictureBox pb_keyword_Write;
        private System.Windows.Forms.PictureBox pictureBox39;
        private System.Windows.Forms.PictureBox pictureBox41;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chk_Selpage_All;
        private System.Windows.Forms.CheckBox chk_Selpage_one;
        private System.Windows.Forms.CheckBox chk_1page_all;
        private System.Windows.Forms.CheckBox chk_1page_one;
        private System.Windows.Forms.Panel pn_Keyword_Write;
        private System.Windows.Forms.TextBox tb_Keyword_word;
        private System.Windows.Forms.PictureBox pictureBox46;
        private System.Windows.Forms.Panel pn_Reply;
        private System.Windows.Forms.TextBox tb_Reply_write;
        private System.Windows.Forms.PictureBox pb_work_KeywordSelDel;
        private System.Windows.Forms.PictureBox pb_work_KeywordEdit;
        private System.Windows.Forms.PictureBox pb_Reply_cancle;
        private System.Windows.Forms.PictureBox pb_Reply_ok;
        private System.Windows.Forms.Button bt_reply_stiker;
        private System.Windows.Forms.Button bt_reply_image;
        private System.Windows.Forms.PictureBox pb_keyword_Add;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.PictureBox pictureBox49;
        private System.Windows.Forms.ListView lv_Keyword_data;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.PictureBox pb_option_Sel;
        private System.Windows.Forms.CheckBox chk_Allpage_All;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pb_reply_clear;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private FlatCombo fc_Selpage_All_l;
        private FlatCombo fc_Selpage_All_f;
        private FlatCombo fc_Selpage_one_l;
        private FlatCombo fc_Selpage_one_f;
        private FlatCombo fc_Delay;
        private System.Windows.Forms.PictureBox pictureBox40;
        private System.Windows.Forms.PictureBox pictureBox38;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.PictureBox pictureBox54;
        private System.Windows.Forms.PictureBox pictureBox55;
        private System.Windows.Forms.PictureBox pictureBox56;
        private System.Windows.Forms.PictureBox pictureBox51;
        private System.Windows.Forms.PictureBox pictureBox52;
        private System.Windows.Forms.PictureBox pictureBox53;
        private System.Windows.Forms.PictureBox pictureBox45;
        private System.Windows.Forms.PictureBox pictureBox48;
        private System.Windows.Forms.PictureBox pictureBox50;
        private System.Windows.Forms.PictureBox pictureBox42;
        private System.Windows.Forms.PictureBox pictureBox43;
        private System.Windows.Forms.PictureBox pictureBox44;
        private FlatCombo fc_IdList;
        private FlatCombo fc_CafeMenuList;
        private FlatCombo fc_CafeURL;
        private FlatCombo fc_Selpage_one_2;
        private FlatCombo fc_Selpage_one_1;
        private FlatCombo fc_Selpage_All_2;
        private FlatCombo fc_Selpage_All_1;
        private FlatCombo fc_DelayTime;
        private System.Windows.Forms.PictureBox pb_Test;
        private System.Windows.Forms.CheckBox cb_Tethering;
        private System.Windows.Forms.CheckBox cb_ChangeNick;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel pn_Option;
        private System.Windows.Forms.CheckBox chk_IdReplyMatch;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pb_Option;
        private System.Windows.Forms.Panel pn_Nick;
        private System.Windows.Forms.PictureBox pb_Nick_SelDel;
        private System.Windows.Forms.CheckBox chk_Nick_AllSel;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.ListView lv_Nick;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.TextBox tb_Nick;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pb_Nick_Add;
        private System.Windows.Forms.PictureBox pb_Nick_File;
        private System.Windows.Forms.Panel pn_URL;
        private System.Windows.Forms.PictureBox pb_URL_SelDel;
        private System.Windows.Forms.PictureBox pb_URL_Stop;
        private System.Windows.Forms.PictureBox pb_URL_Start;
        private System.Windows.Forms.CheckBox chk_URL_AllSel;
        private System.Windows.Forms.PictureBox pictureBox59;
        private System.Windows.Forms.ListView lv_URL;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_URL;
        private System.Windows.Forms.PictureBox pb_URL_File;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pb_URL_Add;
        private System.Windows.Forms.PictureBox pictureBox60;
        private System.Windows.Forms.PictureBox pb_Nick;
        private System.Windows.Forms.CheckBox chk_while;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chk_Overlap;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel pn_ExcepID;
        private System.Windows.Forms.TextBox tb_ExcepID;
        private System.Windows.Forms.PictureBox pb_ExcepIDAdd;
        private System.Windows.Forms.PictureBox pictureBox58;
        private System.Windows.Forms.CheckBox chk_ExcepAllSel;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.ListView lv_ExceptID;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.PictureBox bt_ExceptID;
        private System.Windows.Forms.PictureBox pb_ExcepIDSelDel;
        private System.Windows.Forms.PictureBox pb_ExcepIDFile;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tb_MonitorID;
        private System.Windows.Forms.PictureBox pictureBox57;
        private System.Windows.Forms.CheckBox chk_MonitorID;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox AddFile;
    }
}

