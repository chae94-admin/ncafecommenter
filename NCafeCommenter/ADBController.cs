﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NCafeCommenter
{
    public class ADBController
    {
        public static string Adb_Shell(String argument,string value = null)
        {
            string result = string.Empty;
            ProcessStartInfo cmd = new ProcessStartInfo();
            Process process = new Process();
            cmd.FileName = @"cmd";
            cmd.WindowStyle = ProcessWindowStyle.Hidden;
            cmd.CreateNoWindow = true;

            cmd.UseShellExecute = false;
            cmd.RedirectStandardOutput = true;
            cmd.RedirectStandardInput = true;
            cmd.RedirectStandardError = true;

            process.EnableRaisingEvents = false;
            process.StartInfo = cmd;
            process.Start();

            process.StandardInput.Write(argument + Environment.NewLine);
            process.StandardInput.Close();

            result = process.StandardOutput.ReadToEnd();
            if (value == null)
            {
                process.WaitForExit();
                process.Close();

                result = result.Replace(System.Windows.Forms.Application.StartupPath, "").Replace(argument, "");
                result = result.Substring(result.IndexOf("All rights reserved.")+ "All rights reserved.".Length).Replace(">","");
                return result;
            }
            else
            {
                if (argument.IndexOf("devices") > -1)
                {
                    if (result.IndexOf("List of devices attached") <= 0)
                    {
                        return string.Empty;
                    }

                    if (Regex.Split(Regex.Split(result, "List of devices attached")[1], @"\r\n")[1] == "")
                    {
                        return string.Empty;
                    }
                }
                process.WaitForExit();
                process.Close();
                return result.Replace(System.Windows.Forms.Application.StartupPath, "").Replace(argument, "");
            }
         
        }

        public static bool Adb_IsDevice()
        {
            string DEVICE_RET = Adb_Shell("adb devices");
            DEVICE_RET = DEVICE_RET.Substring(DEVICE_RET.LastIndexOf("attached")+8).Replace(">","").Trim();
        
            if (DEVICE_RET.Equals(string.Empty))
                return false;

            return true;
        }

        public static bool GET_SCREEN(string DeviceID)
        {
            while(true)
            {
                string initpath = Application.StartupPath + @"\screen.png";
                if (System.IO.File.Exists(initpath)) System.IO.File.Delete(initpath);
                Adb_Shell("adb -s " + DeviceID + " shell screencap -p /sdcard/screen.png");
                Adb_Shell("adb -s " + DeviceID + " pull /sdcard/screen.png");
                //Adb_Shell("adb -s " + DeviceID + " shell rm -rf /sdcard/screen.png");
                if (System.IO.File.Exists(initpath)) return true;
            }
        }
        public static bool CONTENT_SCREEN(string DeviceID)
        {
            while (true)
            {
                string initpath = Application.StartupPath + @"\screen.png";
                if (System.IO.File.Exists(initpath)) System.IO.File.Delete(initpath);
                Adb_Shell("adb -s " + DeviceID + " shell screencap -p /sdcard/screen.png");
                Adb_Shell("adb -s " + DeviceID + " pull /sdcard/screen.png");
                //Adb_Shell("adb -s " + DeviceID + " shell rm -rf /sdcard/screen.png");
                if (System.IO.File.Exists(initpath)) return true;
            }
        }
        public static string TAP(string Serialnum, int X, int Y)
        {
          
            return Adb_Shell("adb -s " + Serialnum + " shell input tap " + X + " " + Y);
        }
       
        public static string SWIPE(string Serialnum,int X, int Y)
        {
            string[] strArray = new string[10];
            int index1 = 0;
            string str1 = "-s ";
            strArray[index1] = str1;
            int index2 = 1;
            string str2 = Serialnum;
            strArray[index2] = str2;
            int index3 = 2;
            string str3 = " shell input swipe ";
            strArray[index3] = str3;
            int index4 = 3;
            string str4 = X.ToString();
            strArray[index4] = str4;
            int index5 = 4;
            string str5 = " ";
            strArray[index5] = str5;
            int index6 = 5;
            string str6 = Y.ToString();
            strArray[index6] = str6;
            int index7 = 6;
            string str7 = " ";
            strArray[index7] = str7;
            int index8 = 7;
            string str8 = X.ToString();
            strArray[index8] = str8;
            int index9 = 8;
            string str9 = " ";
            strArray[index9] = str9;
            int index10 = 9;
            string str10 = Y.ToString();
            strArray[index10] = str10;
            return Adb_Shell(string.Concat(strArray));
        }
    }
}
