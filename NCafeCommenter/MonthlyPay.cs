﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NCafeCommenter
{
    public partial class MonthlyPay : Form
    {
        string CODE = "";
        string Price = "";
        public MonthlyPay(string code, string price)
        {
            InitializeComponent();
            Uri uri = new Uri("https://marketingmonster.kr/NSpay/pay.html?class=" + Form1.Classname);
            webBrowser1.Url = uri;
            CODE = code;
            Price = price;
        }

        private void bt_Paycheck_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            CardPayEvent card = new CardPayEvent(CODE, Price, "");
            card.ShowDialog();
            this.Visible = true;
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webBrowser1.Document.BackColor = Color.FromArgb(76, 85, 103);
        }
    }
}
