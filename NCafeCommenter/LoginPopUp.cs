﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace NCafeCommenter
{
    public partial class LoginPopUp : Form
    {
        string linker = "";
        public LoginPopUp(string link, string imglink)
        {
            InitializeComponent();
            pb_Image.Load(Form1.url + Form1.URL + imglink);
            this.BackgroundImage = pb_Image.Image;
            this.Size = new Size(567, 529);
            linker = link;
        }
        private void XMLCreate(string selection)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateXmlDeclaration("1.0", "utf-8", "yes"));

            XmlNode time = doc.CreateElement("", "date", "");
            doc.AppendChild(time);
            XmlElement elem = doc.CreateElement("time");
            elem.InnerText = DateTime.Now.ToString();
            XmlElement select = doc.CreateElement("bool");
            select.InnerText = selection;
            doc.DocumentElement.AppendChild(elem);
            doc.DocumentElement.AppendChild(select);
            doc.Save("timeconfig.xml");
        }


        private void cb_notshowing_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_notshowing.Checked)
            {
                cb_notshowing.Margin = new Padding(-1, -1, 0, 0);
                cb_notshowing.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.pop_chk_on));
                XMLCreate("Y");
                this.Close();
            }
            else
                cb_notshowing.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.pop_chk));
        }

        private void LoginPopUp_Click(object sender, EventArgs e)
        {
            try { Process.Start(linker); }
            catch { }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            XMLCreate("Y");
            this.Close();
        }
    }
}
