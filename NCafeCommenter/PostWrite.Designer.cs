﻿namespace NCafeCommenter
{
    partial class PostWrite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PostWrite));
            this.pn_keyword = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.cb_keyword_all = new System.Windows.Forms.CheckBox();
            this.tb_keyworkd_word = new System.Windows.Forms.TextBox();
            this.tb_keyworkd_cnt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bt_keyworkd_add = new System.Windows.Forms.Button();
            this.bt_keyworkd_seldel = new System.Windows.Forms.Button();
            this.lv_keyworkd_data = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.pn_reply = new System.Windows.Forms.Panel();
            this.cb_reply_all = new System.Windows.Forms.CheckBox();
            this.lb_reply_clear = new System.Windows.Forms.Label();
            this.bt_reply_stiker = new System.Windows.Forms.Button();
            this.bt_reply_image = new System.Windows.Forms.Button();
            this.bt_reply_Add = new System.Windows.Forms.Button();
            this.bt_reply_seldel = new System.Windows.Forms.Button();
            this.chk_RandReply = new System.Windows.Forms.CheckBox();
            this.lv_reply_data = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chk_Like = new System.Windows.Forms.CheckBox();
            this.tb_reply_write = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chk_1page_one = new System.Windows.Forms.CheckBox();
            this.chk_1page_all = new System.Windows.Forms.CheckBox();
            this.chk_Selpage_one = new System.Windows.Forms.CheckBox();
            this.chk_Selpage_All = new System.Windows.Forms.CheckBox();
            this.tb_work_datetime_mm = new System.Windows.Forms.TextBox();
            this.tb_work_datetime_HH = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_delay = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.bt_save = new System.Windows.Forms.Button();
            this.bt_load = new System.Windows.Forms.Button();
            this.lb_load = new System.Windows.Forms.Label();
            this.lb_save = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pb_Hup = new System.Windows.Forms.PictureBox();
            this.pb_Hdown = new System.Windows.Forms.PictureBox();
            this.pb_secup = new System.Windows.Forms.PictureBox();
            this.pb_secdown = new System.Windows.Forms.PictureBox();
            this.pn_keyword.SuspendLayout();
            this.pn_reply.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Hup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Hdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_secup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_secdown)).BeginInit();
            this.SuspendLayout();
            // 
            // pn_keyword
            // 
            this.pn_keyword.Controls.Add(this.checkBox1);
            this.pn_keyword.Controls.Add(this.cb_keyword_all);
            this.pn_keyword.Controls.Add(this.tb_keyworkd_word);
            this.pn_keyword.Controls.Add(this.tb_keyworkd_cnt);
            this.pn_keyword.Controls.Add(this.label2);
            this.pn_keyword.Controls.Add(this.bt_keyworkd_add);
            this.pn_keyword.Controls.Add(this.bt_keyworkd_seldel);
            this.pn_keyword.Controls.Add(this.lv_keyworkd_data);
            this.pn_keyword.Location = new System.Drawing.Point(8, 295);
            this.pn_keyword.Name = "pn_keyword";
            this.pn_keyword.Size = new System.Drawing.Size(533, 142);
            this.pn_keyword.TabIndex = 0;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(208, 123);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(228, 16);
            this.checkBox1.TabIndex = 180;
            this.checkBox1.Text = "특정키워드가 있는 제목에만 댓글달기";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // cb_keyword_all
            // 
            this.cb_keyword_all.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_keyword_all.BackColor = System.Drawing.Color.Transparent;
            this.cb_keyword_all.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.cb_keyword_all.FlatAppearance.BorderSize = 0;
            this.cb_keyword_all.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_keyword_all.Location = new System.Drawing.Point(15, 8);
            this.cb_keyword_all.Name = "cb_keyword_all";
            this.cb_keyword_all.Size = new System.Drawing.Size(12, 12);
            this.cb_keyword_all.TabIndex = 179;
            this.cb_keyword_all.UseVisualStyleBackColor = false;
            this.cb_keyword_all.CheckedChanged += new System.EventHandler(this.cb_keyword_all_CheckedChanged);
            // 
            // tb_keyworkd_word
            // 
            this.tb_keyworkd_word.Location = new System.Drawing.Point(312, 4);
            this.tb_keyworkd_word.Name = "tb_keyworkd_word";
            this.tb_keyworkd_word.Size = new System.Drawing.Size(78, 21);
            this.tb_keyworkd_word.TabIndex = 7;
            this.tb_keyworkd_word.Text = "키워드";
            this.tb_keyworkd_word.Enter += new System.EventHandler(this.tb_keyworkd_word_Enter);
            this.tb_keyworkd_word.Leave += new System.EventHandler(this.tb_keyworkd_word_Leave);
            // 
            // tb_keyworkd_cnt
            // 
            this.tb_keyworkd_cnt.Location = new System.Drawing.Point(396, 4);
            this.tb_keyworkd_cnt.Name = "tb_keyworkd_cnt";
            this.tb_keyworkd_cnt.Size = new System.Drawing.Size(54, 21);
            this.tb_keyworkd_cnt.TabIndex = 6;
            this.tb_keyworkd_cnt.Text = "개수";
            this.tb_keyworkd_cnt.Enter += new System.EventHandler(this.tb_keyworkd_cnt_Enter);
            this.tb_keyworkd_cnt.Leave += new System.EventHandler(this.tb_keyworkd_cnt_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(265, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "키워드";
            // 
            // bt_keyworkd_add
            // 
            this.bt_keyworkd_add.Location = new System.Drawing.Point(456, 2);
            this.bt_keyworkd_add.Name = "bt_keyworkd_add";
            this.bt_keyworkd_add.Size = new System.Drawing.Size(58, 23);
            this.bt_keyworkd_add.TabIndex = 4;
            this.bt_keyworkd_add.Text = "추가";
            this.bt_keyworkd_add.UseVisualStyleBackColor = true;
            this.bt_keyworkd_add.Click += new System.EventHandler(this.bt_keyworkd_add_Click);
            // 
            // bt_keyworkd_seldel
            // 
            this.bt_keyworkd_seldel.Location = new System.Drawing.Point(439, 116);
            this.bt_keyworkd_seldel.Name = "bt_keyworkd_seldel";
            this.bt_keyworkd_seldel.Size = new System.Drawing.Size(75, 23);
            this.bt_keyworkd_seldel.TabIndex = 3;
            this.bt_keyworkd_seldel.Text = "선택삭제";
            this.bt_keyworkd_seldel.UseVisualStyleBackColor = true;
            this.bt_keyworkd_seldel.Click += new System.EventHandler(this.bt_keyworkd_seldel_Click);
            // 
            // lv_keyworkd_data
            // 
            this.lv_keyworkd_data.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lv_keyworkd_data.CheckBoxes = true;
            this.lv_keyworkd_data.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lv_keyworkd_data.Location = new System.Drawing.Point(8, 3);
            this.lv_keyworkd_data.Name = "lv_keyworkd_data";
            this.lv_keyworkd_data.Size = new System.Drawing.Size(506, 134);
            this.lv_keyworkd_data.TabIndex = 1;
            this.lv_keyworkd_data.UseCompatibleStateImageBehavior = false;
            this.lv_keyworkd_data.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ㅁ";
            this.columnHeader1.Width = 24;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "번호";
            this.columnHeader2.Width = 41;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "키워드";
            this.columnHeader3.Width = 103;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "개수";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(214, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "댓글 작업 지정";
            // 
            // pn_reply
            // 
            this.pn_reply.Controls.Add(this.cb_reply_all);
            this.pn_reply.Controls.Add(this.lb_reply_clear);
            this.pn_reply.Controls.Add(this.bt_reply_stiker);
            this.pn_reply.Controls.Add(this.bt_reply_image);
            this.pn_reply.Controls.Add(this.bt_reply_Add);
            this.pn_reply.Controls.Add(this.bt_reply_seldel);
            this.pn_reply.Controls.Add(this.chk_RandReply);
            this.pn_reply.Controls.Add(this.lv_reply_data);
            this.pn_reply.Controls.Add(this.chk_Like);
            this.pn_reply.Controls.Add(this.tb_reply_write);
            this.pn_reply.Location = new System.Drawing.Point(8, 47);
            this.pn_reply.Name = "pn_reply";
            this.pn_reply.Size = new System.Drawing.Size(533, 242);
            this.pn_reply.TabIndex = 4;
            // 
            // cb_reply_all
            // 
            this.cb_reply_all.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_reply_all.BackColor = System.Drawing.Color.Transparent;
            this.cb_reply_all.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_chk;
            this.cb_reply_all.FlatAppearance.BorderSize = 0;
            this.cb_reply_all.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_reply_all.Location = new System.Drawing.Point(15, 119);
            this.cb_reply_all.Name = "cb_reply_all";
            this.cb_reply_all.Size = new System.Drawing.Size(12, 12);
            this.cb_reply_all.TabIndex = 178;
            this.cb_reply_all.UseVisualStyleBackColor = false;
            this.cb_reply_all.CheckedChanged += new System.EventHandler(this.cb_reply_all_CheckedChanged);
            // 
            // lb_reply_clear
            // 
            this.lb_reply_clear.AutoSize = true;
            this.lb_reply_clear.Location = new System.Drawing.Point(485, 7);
            this.lb_reply_clear.Name = "lb_reply_clear";
            this.lb_reply_clear.Size = new System.Drawing.Size(29, 12);
            this.lb_reply_clear.TabIndex = 174;
            this.lb_reply_clear.Text = "취소";
            this.lb_reply_clear.Click += new System.EventHandler(this.lb_reply_clear_Click);
            // 
            // bt_reply_stiker
            // 
            this.bt_reply_stiker.Location = new System.Drawing.Point(12, 85);
            this.bt_reply_stiker.Name = "bt_reply_stiker";
            this.bt_reply_stiker.Size = new System.Drawing.Size(36, 23);
            this.bt_reply_stiker.TabIndex = 8;
            this.bt_reply_stiker.Text = "스티커";
            this.bt_reply_stiker.UseVisualStyleBackColor = true;
            this.bt_reply_stiker.Click += new System.EventHandler(this.bt_reply_stiker_Click);
            // 
            // bt_reply_image
            // 
            this.bt_reply_image.Location = new System.Drawing.Point(54, 85);
            this.bt_reply_image.Name = "bt_reply_image";
            this.bt_reply_image.Size = new System.Drawing.Size(36, 23);
            this.bt_reply_image.TabIndex = 7;
            this.bt_reply_image.Text = "사진";
            this.bt_reply_image.UseVisualStyleBackColor = true;
            this.bt_reply_image.Click += new System.EventHandler(this.bt_reply_image_Click);
            // 
            // bt_reply_Add
            // 
            this.bt_reply_Add.Location = new System.Drawing.Point(439, 85);
            this.bt_reply_Add.Name = "bt_reply_Add";
            this.bt_reply_Add.Size = new System.Drawing.Size(75, 23);
            this.bt_reply_Add.TabIndex = 4;
            this.bt_reply_Add.Text = "추가";
            this.bt_reply_Add.UseVisualStyleBackColor = true;
            this.bt_reply_Add.Click += new System.EventHandler(this.bt_reply_Add_Click);
            // 
            // bt_reply_seldel
            // 
            this.bt_reply_seldel.Location = new System.Drawing.Point(439, 218);
            this.bt_reply_seldel.Name = "bt_reply_seldel";
            this.bt_reply_seldel.Size = new System.Drawing.Size(75, 23);
            this.bt_reply_seldel.TabIndex = 3;
            this.bt_reply_seldel.Text = "선택삭제";
            this.bt_reply_seldel.UseVisualStyleBackColor = true;
            this.bt_reply_seldel.Click += new System.EventHandler(this.bt_reply_seldel_Click);
            // 
            // chk_RandReply
            // 
            this.chk_RandReply.AutoSize = true;
            this.chk_RandReply.Location = new System.Drawing.Point(333, 223);
            this.chk_RandReply.Name = "chk_RandReply";
            this.chk_RandReply.Size = new System.Drawing.Size(100, 16);
            this.chk_RandReply.TabIndex = 6;
            this.chk_RandReply.Text = "댓글 랜덤기능";
            this.chk_RandReply.UseVisualStyleBackColor = true;
            // 
            // lv_reply_data
            // 
            this.lv_reply_data.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lv_reply_data.CheckBoxes = true;
            this.lv_reply_data.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9});
            this.lv_reply_data.FullRowSelect = true;
            this.lv_reply_data.Location = new System.Drawing.Point(8, 114);
            this.lv_reply_data.Name = "lv_reply_data";
            this.lv_reply_data.Size = new System.Drawing.Size(506, 125);
            this.lv_reply_data.TabIndex = 1;
            this.lv_reply_data.UseCompatibleStateImageBehavior = false;
            this.lv_reply_data.View = System.Windows.Forms.View.Details;
            this.lv_reply_data.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lv_reply_data_ItemSelectionChanged);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "ㅁ";
            this.columnHeader5.Width = 23;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "번호";
            this.columnHeader6.Width = 36;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "댓글내용";
            this.columnHeader7.Width = 336;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "스티커";
            this.columnHeader8.Width = 49;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "사진";
            this.columnHeader9.Width = 40;
            // 
            // chk_Like
            // 
            this.chk_Like.AutoSize = true;
            this.chk_Like.Location = new System.Drawing.Point(96, 92);
            this.chk_Like.Name = "chk_Like";
            this.chk_Like.Size = new System.Drawing.Size(60, 16);
            this.chk_Like.TabIndex = 10;
            this.chk_Like.Text = "좋아요";
            this.chk_Like.UseVisualStyleBackColor = true;
            // 
            // tb_reply_write
            // 
            this.tb_reply_write.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_reply_write.Location = new System.Drawing.Point(8, 4);
            this.tb_reply_write.Multiline = true;
            this.tb_reply_write.Name = "tb_reply_write";
            this.tb_reply_write.Size = new System.Drawing.Size(506, 104);
            this.tb_reply_write.TabIndex = 0;
            this.tb_reply_write.Text = "댓글을 남겨보세요.";
            this.tb_reply_write.Enter += new System.EventHandler(this.tb_reply_write_Enter);
            this.tb_reply_write.Leave += new System.EventHandler(this.tb_reply_write_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 441);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "|옵션설정";
            // 
            // chk_1page_one
            // 
            this.chk_1page_one.AutoSize = true;
            this.chk_1page_one.Location = new System.Drawing.Point(8, 455);
            this.chk_1page_one.Name = "chk_1page_one";
            this.chk_1page_one.Size = new System.Drawing.Size(180, 16);
            this.chk_1page_one.TabIndex = 8;
            this.chk_1page_one.Tag = "N";
            this.chk_1page_one.Text = "1페이지 글에 1개만 댓글작성";
            this.chk_1page_one.UseVisualStyleBackColor = true;
            this.chk_1page_one.CheckedChanged += new System.EventHandler(this.chk_1page_one_CheckedChanged);
            // 
            // chk_1page_all
            // 
            this.chk_1page_all.AutoSize = true;
            this.chk_1page_all.Location = new System.Drawing.Point(8, 475);
            this.chk_1page_all.Name = "chk_1page_all";
            this.chk_1page_all.Size = new System.Drawing.Size(198, 16);
            this.chk_1page_all.TabIndex = 9;
            this.chk_1page_all.Tag = "N";
            this.chk_1page_all.Text = "1페이지 모든글에 모두 댓글작성";
            this.chk_1page_all.UseVisualStyleBackColor = true;
            this.chk_1page_all.CheckedChanged += new System.EventHandler(this.chk_1page_all_CheckedChanged);
            // 
            // chk_Selpage_one
            // 
            this.chk_Selpage_one.AutoSize = true;
            this.chk_Selpage_one.Location = new System.Drawing.Point(8, 497);
            this.chk_Selpage_one.Name = "chk_Selpage_one";
            this.chk_Selpage_one.Size = new System.Drawing.Size(230, 16);
            this.chk_Selpage_one.TabIndex = 12;
            this.chk_Selpage_one.Tag = "N";
            this.chk_Selpage_one.Text = "선택 페이지 모든글에 1개씩 댓글 작성";
            this.chk_Selpage_one.UseVisualStyleBackColor = true;
            this.chk_Selpage_one.CheckedChanged += new System.EventHandler(this.chk_Selpage_one_CheckedChanged);
            // 
            // chk_Selpage_All
            // 
            this.chk_Selpage_All.AutoSize = true;
            this.chk_Selpage_All.Location = new System.Drawing.Point(8, 519);
            this.chk_Selpage_All.Name = "chk_Selpage_All";
            this.chk_Selpage_All.Size = new System.Drawing.Size(224, 16);
            this.chk_Selpage_All.TabIndex = 13;
            this.chk_Selpage_All.Tag = "N";
            this.chk_Selpage_All.Text = "선택 페이지 모든글에 모두 댓글 작성";
            this.chk_Selpage_All.UseVisualStyleBackColor = true;
            this.chk_Selpage_All.CheckedChanged += new System.EventHandler(this.chk_Selpage_All_CheckedChanged);
            // 
            // tb_work_datetime_mm
            // 
            this.tb_work_datetime_mm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.tb_work_datetime_mm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_work_datetime_mm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_work_datetime_mm.Location = new System.Drawing.Point(298, 497);
            this.tb_work_datetime_mm.Name = "tb_work_datetime_mm";
            this.tb_work_datetime_mm.ReadOnly = true;
            this.tb_work_datetime_mm.Size = new System.Drawing.Size(23, 14);
            this.tb_work_datetime_mm.TabIndex = 160;
            this.tb_work_datetime_mm.Text = "00";
            this.tb_work_datetime_mm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_work_datetime_HH
            // 
            this.tb_work_datetime_HH.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.tb_work_datetime_HH.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_work_datetime_HH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.tb_work_datetime_HH.Location = new System.Drawing.Point(240, 497);
            this.tb_work_datetime_HH.Name = "tb_work_datetime_HH";
            this.tb_work_datetime_HH.ReadOnly = true;
            this.tb_work_datetime_HH.Size = new System.Drawing.Size(23, 14);
            this.tb_work_datetime_HH.TabIndex = 163;
            this.tb_work_datetime_HH.Text = "00";
            this.tb_work_datetime_HH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.textBox5.Location = new System.Drawing.Point(298, 519);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(23, 14);
            this.textBox5.TabIndex = 166;
            this.textBox5.Text = "00";
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(175)))), ((int)(((byte)(201)))));
            this.textBox6.Location = new System.Drawing.Point(240, 519);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(23, 14);
            this.textBox6.TabIndex = 169;
            this.textBox6.Text = "00";
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(281, 544);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(151, 34);
            this.button8.TabIndex = 7;
            this.button8.Text = "작성완료";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(112, 544);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(151, 34);
            this.button9.TabIndex = 170;
            this.button9.Text = "작성취소";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(427, 521);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 172;
            this.label8.Text = "딜레이";
            // 
            // tb_delay
            // 
            this.tb_delay.Location = new System.Drawing.Point(471, 517);
            this.tb_delay.Name = "tb_delay";
            this.tb_delay.Size = new System.Drawing.Size(51, 21);
            this.tb_delay.TabIndex = 171;
            this.tb_delay.Tag = "30";
            this.tb_delay.Text = "30";
            this.tb_delay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(524, 521);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 12);
            this.label9.TabIndex = 173;
            this.label9.Text = "초";
            // 
            // bt_save
            // 
            this.bt_save.BackColor = System.Drawing.Color.Transparent;
            this.bt_save.FlatAppearance.BorderSize = 0;
            this.bt_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_save.ForeColor = System.Drawing.Color.Black;
            this.bt_save.Location = new System.Drawing.Point(422, 33);
            this.bt_save.Name = "bt_save";
            this.bt_save.Size = new System.Drawing.Size(11, 11);
            this.bt_save.TabIndex = 174;
            this.bt_save.UseVisualStyleBackColor = false;
            // 
            // bt_load
            // 
            this.bt_load.BackColor = System.Drawing.Color.Transparent;
            this.bt_load.FlatAppearance.BorderSize = 0;
            this.bt_load.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_load.Location = new System.Drawing.Point(487, 33);
            this.bt_load.Name = "bt_load";
            this.bt_load.Size = new System.Drawing.Size(11, 11);
            this.bt_load.TabIndex = 175;
            this.bt_load.UseVisualStyleBackColor = false;
            // 
            // lb_load
            // 
            this.lb_load.AutoSize = true;
            this.lb_load.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb_load.Font = new System.Drawing.Font("굴림", 8.25F);
            this.lb_load.Location = new System.Drawing.Point(495, 33);
            this.lb_load.Name = "lb_load";
            this.lb_load.Size = new System.Drawing.Size(49, 11);
            this.lb_load.TabIndex = 177;
            this.lb_load.Text = "불러오기";
            // 
            // lb_save
            // 
            this.lb_save.AutoSize = true;
            this.lb_save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb_save.Font = new System.Drawing.Font("굴림", 8.25F);
            this.lb_save.Location = new System.Drawing.Point(432, 33);
            this.lb_save.Name = "lb_save";
            this.lb_save.Size = new System.Drawing.Size(49, 11);
            this.lb_save.TabIndex = 176;
            this.lb_save.Text = "작업저장";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Location = new System.Drawing.Point(271, 511);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 13);
            this.pictureBox1.TabIndex = 168;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Location = new System.Drawing.Point(271, 524);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 13);
            this.pictureBox2.TabIndex = 167;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Location = new System.Drawing.Point(330, 512);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 13);
            this.pictureBox3.TabIndex = 165;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Location = new System.Drawing.Point(330, 525);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(16, 13);
            this.pictureBox4.TabIndex = 164;
            this.pictureBox4.TabStop = false;
            // 
            // pb_Hup
            // 
            this.pb_Hup.BackColor = System.Drawing.Color.Transparent;
            this.pb_Hup.Location = new System.Drawing.Point(271, 489);
            this.pb_Hup.Name = "pb_Hup";
            this.pb_Hup.Size = new System.Drawing.Size(16, 13);
            this.pb_Hup.TabIndex = 162;
            this.pb_Hup.TabStop = false;
            // 
            // pb_Hdown
            // 
            this.pb_Hdown.BackColor = System.Drawing.Color.Transparent;
            this.pb_Hdown.Location = new System.Drawing.Point(271, 502);
            this.pb_Hdown.Name = "pb_Hdown";
            this.pb_Hdown.Size = new System.Drawing.Size(16, 13);
            this.pb_Hdown.TabIndex = 161;
            this.pb_Hdown.TabStop = false;
            // 
            // pb_secup
            // 
            this.pb_secup.BackColor = System.Drawing.Color.Transparent;
            this.pb_secup.Location = new System.Drawing.Point(330, 490);
            this.pb_secup.Name = "pb_secup";
            this.pb_secup.Size = new System.Drawing.Size(16, 13);
            this.pb_secup.TabIndex = 159;
            this.pb_secup.TabStop = false;
            // 
            // pb_secdown
            // 
            this.pb_secdown.BackColor = System.Drawing.Color.Transparent;
            this.pb_secdown.Location = new System.Drawing.Point(330, 503);
            this.pb_secdown.Name = "pb_secdown";
            this.pb_secdown.Size = new System.Drawing.Size(16, 13);
            this.pb_secdown.TabIndex = 158;
            this.pb_secdown.TabStop = false;
            // 
            // PostWrite
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(550, 590);
            this.Controls.Add(this.bt_save);
            this.Controls.Add(this.bt_load);
            this.Controls.Add(this.lb_load);
            this.Controls.Add(this.lb_save);
            this.Controls.Add(this.pn_keyword);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tb_delay);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.tb_work_datetime_mm);
            this.Controls.Add(this.tb_work_datetime_HH);
            this.Controls.Add(this.pb_Hup);
            this.Controls.Add(this.pb_Hdown);
            this.Controls.Add(this.pb_secup);
            this.Controls.Add(this.pb_secdown);
            this.Controls.Add(this.chk_Selpage_All);
            this.Controls.Add(this.chk_Selpage_one);
            this.Controls.Add(this.chk_1page_all);
            this.Controls.Add(this.chk_1page_one);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pn_reply);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PostWrite";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "PostWrite";
            this.pn_keyword.ResumeLayout(false);
            this.pn_keyword.PerformLayout();
            this.pn_reply.ResumeLayout(false);
            this.pn_reply.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Hup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Hdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_secup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_secdown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pn_keyword;
        private System.Windows.Forms.TextBox tb_keyworkd_cnt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bt_keyworkd_add;
        private System.Windows.Forms.Button bt_keyworkd_seldel;
        private System.Windows.Forms.ListView lv_keyworkd_data;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pn_reply;
        private System.Windows.Forms.Button bt_reply_image;
        private System.Windows.Forms.Button bt_reply_Add;
        private System.Windows.Forms.Button bt_reply_seldel;
        private System.Windows.Forms.ListView lv_reply_data;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.TextBox tb_reply_write;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chk_RandReply;
        private System.Windows.Forms.CheckBox chk_1page_one;
        private System.Windows.Forms.CheckBox chk_1page_all;
        private System.Windows.Forms.CheckBox chk_Like;
        private System.Windows.Forms.CheckBox chk_Selpage_one;
        private System.Windows.Forms.CheckBox chk_Selpage_All;
        private System.Windows.Forms.TextBox tb_work_datetime_mm;
        private System.Windows.Forms.TextBox tb_work_datetime_HH;
        private System.Windows.Forms.PictureBox pb_Hup;
        private System.Windows.Forms.PictureBox pb_Hdown;
        private System.Windows.Forms.PictureBox pb_secup;
        private System.Windows.Forms.PictureBox pb_secdown;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_delay;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_keyworkd_word;
        private System.Windows.Forms.Button bt_reply_stiker;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.Label lb_reply_clear;
        private System.Windows.Forms.Button bt_save;
        private System.Windows.Forms.Button bt_load;
        private System.Windows.Forms.Label lb_load;
        private System.Windows.Forms.Label lb_save;
        private System.Windows.Forms.CheckBox cb_keyword_all;
        private System.Windows.Forms.CheckBox cb_reply_all;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}