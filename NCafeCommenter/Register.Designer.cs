﻿namespace NCafeCommenter
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Register));
            this.pn_Register2 = new System.Windows.Forms.Panel();
            this.tb_Register2_num3 = new System.Windows.Forms.TextBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.tb_Register2_num2 = new System.Windows.Forms.TextBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.tb_Register2_num1 = new System.Windows.Forms.TextBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.bt_Register2_certchk = new System.Windows.Forms.Button();
            this.bt_Register2_cert = new System.Windows.Forms.Button();
            this.bt_Register2_emailchk = new System.Windows.Forms.Button();
            this.bt_Register2_idchk = new System.Windows.Forms.Button();
            this.tb_Register2_cert = new System.Windows.Forms.TextBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.tb_Register2_email = new System.Windows.Forms.TextBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.tb_Register2_pwchk = new System.Windows.Forms.TextBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.tb_Register2_pw = new System.Windows.Forms.TextBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.tb_Register2_id = new System.Windows.Forms.TextBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.tb_Register2_name = new System.Windows.Forms.TextBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pn_Register2_Regidit = new System.Windows.Forms.Button();
            this.pn_Register2_cancel = new System.Windows.Forms.Button();
            this.tb_Register_temp1 = new System.Windows.Forms.TextBox();
            this.tb_Register_temp2 = new System.Windows.Forms.TextBox();
            this.bt_Register_cancel = new System.Windows.Forms.Button();
            this.bt_Register_Agree = new System.Windows.Forms.Button();
            this.pb_Register_temp2 = new System.Windows.Forms.PictureBox();
            this.pb_Register_temp3 = new System.Windows.Forms.PictureBox();
            this.pb_Register_temp1 = new System.Windows.Forms.PictureBox();
            this.pn_Register2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Register_temp2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Register_temp3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Register_temp1)).BeginInit();
            this.SuspendLayout();
            // 
            // pn_Register2
            // 
            this.pn_Register2.BackgroundImage = global::NCafeCommenter.Properties.Resources.NCP_회원가입;
            this.pn_Register2.Controls.Add(this.tb_Register2_num3);
            this.pn_Register2.Controls.Add(this.pictureBox29);
            this.pn_Register2.Controls.Add(this.tb_Register2_num2);
            this.pn_Register2.Controls.Add(this.pictureBox30);
            this.pn_Register2.Controls.Add(this.tb_Register2_num1);
            this.pn_Register2.Controls.Add(this.pictureBox31);
            this.pn_Register2.Controls.Add(this.bt_Register2_certchk);
            this.pn_Register2.Controls.Add(this.bt_Register2_cert);
            this.pn_Register2.Controls.Add(this.bt_Register2_emailchk);
            this.pn_Register2.Controls.Add(this.bt_Register2_idchk);
            this.pn_Register2.Controls.Add(this.tb_Register2_cert);
            this.pn_Register2.Controls.Add(this.pictureBox28);
            this.pn_Register2.Controls.Add(this.tb_Register2_email);
            this.pn_Register2.Controls.Add(this.pictureBox27);
            this.pn_Register2.Controls.Add(this.tb_Register2_pwchk);
            this.pn_Register2.Controls.Add(this.pictureBox26);
            this.pn_Register2.Controls.Add(this.tb_Register2_pw);
            this.pn_Register2.Controls.Add(this.pictureBox25);
            this.pn_Register2.Controls.Add(this.tb_Register2_id);
            this.pn_Register2.Controls.Add(this.pictureBox24);
            this.pn_Register2.Controls.Add(this.tb_Register2_name);
            this.pn_Register2.Controls.Add(this.pictureBox23);
            this.pn_Register2.Controls.Add(this.pn_Register2_Regidit);
            this.pn_Register2.Controls.Add(this.pn_Register2_cancel);
            this.pn_Register2.Location = new System.Drawing.Point(0, 0);
            this.pn_Register2.Name = "pn_Register2";
            this.pn_Register2.Size = new System.Drawing.Size(454, 612);
            this.pn_Register2.TabIndex = 128;
            this.pn_Register2.Visible = false;
            // 
            // tb_Register2_num3
            // 
            this.tb_Register2_num3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Register2_num3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
            this.tb_Register2_num3.Location = new System.Drawing.Point(245, 421);
            this.tb_Register2_num3.Name = "tb_Register2_num3";
            this.tb_Register2_num3.Size = new System.Drawing.Size(47, 14);
            this.tb_Register2_num3.TabIndex = 7;
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.Color.White;
            this.pictureBox29.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_phone_textbox2;
            this.pictureBox29.Location = new System.Drawing.Point(241, 415);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(55, 25);
            this.pictureBox29.TabIndex = 102;
            this.pictureBox29.TabStop = false;
            // 
            // tb_Register2_num2
            // 
            this.tb_Register2_num2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Register2_num2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
            this.tb_Register2_num2.Location = new System.Drawing.Point(183, 421);
            this.tb_Register2_num2.Name = "tb_Register2_num2";
            this.tb_Register2_num2.Size = new System.Drawing.Size(47, 14);
            this.tb_Register2_num2.TabIndex = 6;
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.Color.White;
            this.pictureBox30.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_phone_textbox2;
            this.pictureBox30.Location = new System.Drawing.Point(179, 415);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(55, 25);
            this.pictureBox30.TabIndex = 101;
            this.pictureBox30.TabStop = false;
            // 
            // tb_Register2_num1
            // 
            this.tb_Register2_num1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Register2_num1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
            this.tb_Register2_num1.Location = new System.Drawing.Point(129, 421);
            this.tb_Register2_num1.Name = "tb_Register2_num1";
            this.tb_Register2_num1.Size = new System.Drawing.Size(38, 14);
            this.tb_Register2_num1.TabIndex = 5;
            this.tb_Register2_num1.Text = "010";
            this.tb_Register2_num1.Enter += new System.EventHandler(this.tb_Register2_num1_Enter);
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackColor = System.Drawing.Color.White;
            this.pictureBox31.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_phone_textbox1;
            this.pictureBox31.Location = new System.Drawing.Point(126, 415);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(45, 25);
            this.pictureBox31.TabIndex = 100;
            this.pictureBox31.TabStop = false;
            // 
            // bt_Register2_certchk
            // 
            this.bt_Register2_certchk.BackColor = System.Drawing.Color.White;
            this.bt_Register2_certchk.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_confirm;
            this.bt_Register2_certchk.Enabled = false;
            this.bt_Register2_certchk.FlatAppearance.BorderSize = 0;
            this.bt_Register2_certchk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Register2_certchk.ForeColor = System.Drawing.Color.White;
            this.bt_Register2_certchk.Location = new System.Drawing.Point(302, 481);
            this.bt_Register2_certchk.Name = "bt_Register2_certchk";
            this.bt_Register2_certchk.Size = new System.Drawing.Size(109, 27);
            this.bt_Register2_certchk.TabIndex = 96;
            this.bt_Register2_certchk.UseVisualStyleBackColor = false;
            this.bt_Register2_certchk.Click += new System.EventHandler(this.bt_Register2_certchk_Click);
            // 
            // bt_Register2_cert
            // 
            this.bt_Register2_cert.BackColor = System.Drawing.Color.White;
            this.bt_Register2_cert.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_authentication;
            this.bt_Register2_cert.FlatAppearance.BorderSize = 0;
            this.bt_Register2_cert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Register2_cert.ForeColor = System.Drawing.Color.White;
            this.bt_Register2_cert.Location = new System.Drawing.Point(302, 414);
            this.bt_Register2_cert.Name = "bt_Register2_cert";
            this.bt_Register2_cert.Size = new System.Drawing.Size(109, 27);
            this.bt_Register2_cert.TabIndex = 95;
            this.bt_Register2_cert.UseVisualStyleBackColor = false;
            this.bt_Register2_cert.Click += new System.EventHandler(this.bt_Register2_cert_Click);
            // 
            // bt_Register2_emailchk
            // 
            this.bt_Register2_emailchk.BackColor = System.Drawing.Color.White;
            this.bt_Register2_emailchk.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_email_duplication;
            this.bt_Register2_emailchk.FlatAppearance.BorderSize = 0;
            this.bt_Register2_emailchk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Register2_emailchk.ForeColor = System.Drawing.Color.White;
            this.bt_Register2_emailchk.Location = new System.Drawing.Point(302, 348);
            this.bt_Register2_emailchk.Name = "bt_Register2_emailchk";
            this.bt_Register2_emailchk.Size = new System.Drawing.Size(109, 27);
            this.bt_Register2_emailchk.TabIndex = 94;
            this.bt_Register2_emailchk.UseVisualStyleBackColor = false;
            this.bt_Register2_emailchk.Click += new System.EventHandler(this.bt_Register2_emailchk_Click);
            // 
            // bt_Register2_idchk
            // 
            this.bt_Register2_idchk.BackColor = System.Drawing.Color.White;
            this.bt_Register2_idchk.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_id_duplication;
            this.bt_Register2_idchk.FlatAppearance.BorderSize = 0;
            this.bt_Register2_idchk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Register2_idchk.ForeColor = System.Drawing.Color.White;
            this.bt_Register2_idchk.Location = new System.Drawing.Point(302, 149);
            this.bt_Register2_idchk.Name = "bt_Register2_idchk";
            this.bt_Register2_idchk.Size = new System.Drawing.Size(109, 27);
            this.bt_Register2_idchk.TabIndex = 93;
            this.bt_Register2_idchk.UseVisualStyleBackColor = false;
            this.bt_Register2_idchk.Click += new System.EventHandler(this.bt_Register2_idchk_Click);
            // 
            // tb_Register2_cert
            // 
            this.tb_Register2_cert.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Register2_cert.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
            this.tb_Register2_cert.Location = new System.Drawing.Point(133, 488);
            this.tb_Register2_cert.Name = "tb_Register2_cert";
            this.tb_Register2_cert.Size = new System.Drawing.Size(155, 14);
            this.tb_Register2_cert.TabIndex = 8;
            this.tb_Register2_cert.Text = "인증번호";
            this.tb_Register2_cert.Enter += new System.EventHandler(this.tb_Register2_cert_Enter);
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_textbox;
            this.pictureBox28.Location = new System.Drawing.Point(126, 482);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(170, 25);
            this.pictureBox28.TabIndex = 91;
            this.pictureBox28.TabStop = false;
            // 
            // tb_Register2_email
            // 
            this.tb_Register2_email.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Register2_email.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
            this.tb_Register2_email.Location = new System.Drawing.Point(133, 356);
            this.tb_Register2_email.Name = "tb_Register2_email";
            this.tb_Register2_email.Size = new System.Drawing.Size(155, 14);
            this.tb_Register2_email.TabIndex = 4;
            this.tb_Register2_email.Text = "이메일";
            this.tb_Register2_email.Enter += new System.EventHandler(this.tb_Register2_email_Enter);
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_textbox;
            this.pictureBox27.Location = new System.Drawing.Point(126, 350);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(170, 25);
            this.pictureBox27.TabIndex = 89;
            this.pictureBox27.TabStop = false;
            // 
            // tb_Register2_pwchk
            // 
            this.tb_Register2_pwchk.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Register2_pwchk.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
            this.tb_Register2_pwchk.Location = new System.Drawing.Point(133, 300);
            this.tb_Register2_pwchk.Name = "tb_Register2_pwchk";
            this.tb_Register2_pwchk.PasswordChar = '●';
            this.tb_Register2_pwchk.Size = new System.Drawing.Size(155, 14);
            this.tb_Register2_pwchk.TabIndex = 3;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_textbox;
            this.pictureBox26.Location = new System.Drawing.Point(126, 294);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(170, 25);
            this.pictureBox26.TabIndex = 87;
            this.pictureBox26.TabStop = false;
            // 
            // tb_Register2_pw
            // 
            this.tb_Register2_pw.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Register2_pw.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
            this.tb_Register2_pw.Location = new System.Drawing.Point(133, 234);
            this.tb_Register2_pw.Name = "tb_Register2_pw";
            this.tb_Register2_pw.PasswordChar = '●';
            this.tb_Register2_pw.Size = new System.Drawing.Size(155, 14);
            this.tb_Register2_pw.TabIndex = 2;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_textbox;
            this.pictureBox25.Location = new System.Drawing.Point(126, 228);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(170, 25);
            this.pictureBox25.TabIndex = 85;
            this.pictureBox25.TabStop = false;
            // 
            // tb_Register2_id
            // 
            this.tb_Register2_id.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Register2_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
            this.tb_Register2_id.Location = new System.Drawing.Point(133, 156);
            this.tb_Register2_id.Name = "tb_Register2_id";
            this.tb_Register2_id.Size = new System.Drawing.Size(155, 14);
            this.tb_Register2_id.TabIndex = 1;
            this.tb_Register2_id.Text = "아이디";
            this.tb_Register2_id.Enter += new System.EventHandler(this.tb_Register2_id_Enter);
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_textbox;
            this.pictureBox24.Location = new System.Drawing.Point(126, 150);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(170, 25);
            this.pictureBox24.TabIndex = 83;
            this.pictureBox24.TabStop = false;
            // 
            // tb_Register2_name
            // 
            this.tb_Register2_name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Register2_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
            this.tb_Register2_name.Location = new System.Drawing.Point(133, 111);
            this.tb_Register2_name.Name = "tb_Register2_name";
            this.tb_Register2_name.Size = new System.Drawing.Size(155, 14);
            this.tb_Register2_name.TabIndex = 0;
            this.tb_Register2_name.Text = "이름을 입력해주세요";
            this.tb_Register2_name.Enter += new System.EventHandler(this.tb_Register2_name_Enter);
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_textbox;
            this.pictureBox23.Location = new System.Drawing.Point(126, 105);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(170, 25);
            this.pictureBox23.TabIndex = 81;
            this.pictureBox23.TabStop = false;
            // 
            // pn_Register2_Regidit
            // 
            this.pn_Register2_Regidit.BackColor = System.Drawing.Color.White;
            this.pn_Register2_Regidit.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_register;
            this.pn_Register2_Regidit.FlatAppearance.BorderSize = 0;
            this.pn_Register2_Regidit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pn_Register2_Regidit.ForeColor = System.Drawing.Color.White;
            this.pn_Register2_Regidit.Location = new System.Drawing.Point(230, 553);
            this.pn_Register2_Regidit.Name = "pn_Register2_Regidit";
            this.pn_Register2_Regidit.Size = new System.Drawing.Size(196, 30);
            this.pn_Register2_Regidit.TabIndex = 80;
            this.pn_Register2_Regidit.UseVisualStyleBackColor = false;
            this.pn_Register2_Regidit.Click += new System.EventHandler(this.pn_Register2_Regidit_Click);
            // 
            // pn_Register2_cancel
            // 
            this.pn_Register2_cancel.BackColor = System.Drawing.Color.White;
            this.pn_Register2_cancel.BackgroundImage = global::NCafeCommenter.Properties.Resources.HARD_btn_cancel;
            this.pn_Register2_cancel.FlatAppearance.BorderSize = 0;
            this.pn_Register2_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pn_Register2_cancel.ForeColor = System.Drawing.Color.White;
            this.pn_Register2_cancel.Location = new System.Drawing.Point(29, 553);
            this.pn_Register2_cancel.Name = "pn_Register2_cancel";
            this.pn_Register2_cancel.Size = new System.Drawing.Size(196, 30);
            this.pn_Register2_cancel.TabIndex = 79;
            this.pn_Register2_cancel.UseVisualStyleBackColor = false;
            this.pn_Register2_cancel.Click += new System.EventHandler(this.pn_Register2_cancel_Click);
            // 
            // tb_Register_temp1
            // 
            this.tb_Register_temp1.Location = new System.Drawing.Point(45, 179);
            this.tb_Register_temp1.Multiline = true;
            this.tb_Register_temp1.Name = "tb_Register_temp1";
            this.tb_Register_temp1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_Register_temp1.Size = new System.Drawing.Size(364, 110);
            this.tb_Register_temp1.TabIndex = 124;
            this.tb_Register_temp1.Text = resources.GetString("tb_Register_temp1.Text");
            // 
            // tb_Register_temp2
            // 
            this.tb_Register_temp2.Location = new System.Drawing.Point(45, 342);
            this.tb_Register_temp2.Multiline = true;
            this.tb_Register_temp2.Name = "tb_Register_temp2";
            this.tb_Register_temp2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_Register_temp2.Size = new System.Drawing.Size(364, 110);
            this.tb_Register_temp2.TabIndex = 123;
            this.tb_Register_temp2.Text = resources.GetString("tb_Register_temp2.Text");
            // 
            // bt_Register_cancel
            // 
            this.bt_Register_cancel.BackColor = System.Drawing.Color.White;
            this.bt_Register_cancel.BackgroundImage = global::NCafeCommenter.Properties.Resources.HARD_btn_cancel;
            this.bt_Register_cancel.FlatAppearance.BorderSize = 0;
            this.bt_Register_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Register_cancel.ForeColor = System.Drawing.Color.White;
            this.bt_Register_cancel.Location = new System.Drawing.Point(29, 480);
            this.bt_Register_cancel.Name = "bt_Register_cancel";
            this.bt_Register_cancel.Size = new System.Drawing.Size(196, 30);
            this.bt_Register_cancel.TabIndex = 122;
            this.bt_Register_cancel.UseVisualStyleBackColor = false;
            this.bt_Register_cancel.Click += new System.EventHandler(this.bt_Register_cancel_Click);
            // 
            // bt_Register_Agree
            // 
            this.bt_Register_Agree.BackColor = System.Drawing.Color.White;
            this.bt_Register_Agree.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_terms_agree;
            this.bt_Register_Agree.FlatAppearance.BorderSize = 0;
            this.bt_Register_Agree.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Register_Agree.ForeColor = System.Drawing.Color.White;
            this.bt_Register_Agree.Location = new System.Drawing.Point(229, 480);
            this.bt_Register_Agree.Name = "bt_Register_Agree";
            this.bt_Register_Agree.Size = new System.Drawing.Size(196, 30);
            this.bt_Register_Agree.TabIndex = 121;
            this.bt_Register_Agree.UseVisualStyleBackColor = false;
            this.bt_Register_Agree.Click += new System.EventHandler(this.bt_Register_Agree_Click);
            // 
            // pb_Register_temp2
            // 
            this.pb_Register_temp2.BackColor = System.Drawing.Color.Transparent;
            this.pb_Register_temp2.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_checkbox;
            this.pb_Register_temp2.Location = new System.Drawing.Point(367, 152);
            this.pb_Register_temp2.Name = "pb_Register_temp2";
            this.pb_Register_temp2.Size = new System.Drawing.Size(18, 18);
            this.pb_Register_temp2.TabIndex = 127;
            this.pb_Register_temp2.TabStop = false;
            this.pb_Register_temp2.Click += new System.EventHandler(this.pb_Register_temp2_Click);
            // 
            // pb_Register_temp3
            // 
            this.pb_Register_temp3.BackColor = System.Drawing.Color.Transparent;
            this.pb_Register_temp3.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_checkbox;
            this.pb_Register_temp3.Location = new System.Drawing.Point(367, 314);
            this.pb_Register_temp3.Name = "pb_Register_temp3";
            this.pb_Register_temp3.Size = new System.Drawing.Size(18, 18);
            this.pb_Register_temp3.TabIndex = 126;
            this.pb_Register_temp3.TabStop = false;
            this.pb_Register_temp3.Click += new System.EventHandler(this.pb_Register_temp3_Click);
            // 
            // pb_Register_temp1
            // 
            this.pb_Register_temp1.BackColor = System.Drawing.Color.Transparent;
            this.pb_Register_temp1.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_checkbox;
            this.pb_Register_temp1.Location = new System.Drawing.Point(44, 109);
            this.pb_Register_temp1.Name = "pb_Register_temp1";
            this.pb_Register_temp1.Size = new System.Drawing.Size(18, 18);
            this.pb_Register_temp1.TabIndex = 125;
            this.pb_Register_temp1.TabStop = false;
            this.pb_Register_temp1.Click += new System.EventHandler(this.pb_Register_temp1_Click);
            // 
            // Register
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::NCafeCommenter.Properties.Resources.bg_terms;
            this.ClientSize = new System.Drawing.Size(454, 612);
            this.Controls.Add(this.pn_Register2);
            this.Controls.Add(this.pb_Register_temp2);
            this.Controls.Add(this.pb_Register_temp3);
            this.Controls.Add(this.tb_Register_temp1);
            this.Controls.Add(this.pb_Register_temp1);
            this.Controls.Add(this.tb_Register_temp2);
            this.Controls.Add(this.bt_Register_cancel);
            this.Controls.Add(this.bt_Register_Agree);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Register";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Register";
            this.pn_Register2.ResumeLayout(false);
            this.pn_Register2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Register_temp2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Register_temp3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Register_temp1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pn_Register2;
        private System.Windows.Forms.TextBox tb_Register2_num3;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.TextBox tb_Register2_num2;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.TextBox tb_Register2_num1;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.Button bt_Register2_certchk;
        private System.Windows.Forms.Button bt_Register2_cert;
        private System.Windows.Forms.Button bt_Register2_emailchk;
        private System.Windows.Forms.Button bt_Register2_idchk;
        private System.Windows.Forms.TextBox tb_Register2_cert;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.TextBox tb_Register2_email;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.TextBox tb_Register2_pwchk;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.TextBox tb_Register2_pw;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.TextBox tb_Register2_id;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.TextBox tb_Register2_name;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.Button pn_Register2_Regidit;
        private System.Windows.Forms.Button pn_Register2_cancel;
        private System.Windows.Forms.PictureBox pb_Register_temp2;
        private System.Windows.Forms.PictureBox pb_Register_temp3;
        private System.Windows.Forms.TextBox tb_Register_temp1;
        private System.Windows.Forms.PictureBox pb_Register_temp1;
        private System.Windows.Forms.TextBox tb_Register_temp2;
        private System.Windows.Forms.Button bt_Register_cancel;
        private System.Windows.Forms.Button bt_Register_Agree;
    }
}