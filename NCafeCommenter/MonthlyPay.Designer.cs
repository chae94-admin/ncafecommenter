﻿namespace NCafeCommenter
{
    partial class MonthlyPay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MonthlyPay));
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.bt_Paycheck = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScrollBarsEnabled = false;
            this.webBrowser1.Size = new System.Drawing.Size(395, 340);
            this.webBrowser1.TabIndex = 13;
            this.webBrowser1.Url = new System.Uri("", System.UriKind.Relative);
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // bt_Paycheck
            // 
            this.bt_Paycheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(44)))), ((int)(((byte)(52)))));
            this.bt_Paycheck.BackgroundImage = global::NCafeCommenter.Properties.Resources.monthly_btn_payment;
            this.bt_Paycheck.FlatAppearance.BorderSize = 0;
            this.bt_Paycheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Paycheck.Location = new System.Drawing.Point(191, 136);
            this.bt_Paycheck.Name = "bt_Paycheck";
            this.bt_Paycheck.Size = new System.Drawing.Size(164, 60);
            this.bt_Paycheck.TabIndex = 14;
            this.bt_Paycheck.UseVisualStyleBackColor = false;
            this.bt_Paycheck.Click += new System.EventHandler(this.bt_Paycheck_Click);
            // 
            // MonthlyPay
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(395, 340);
            this.Controls.Add(this.bt_Paycheck);
            this.Controls.Add(this.webBrowser1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MonthlyPay";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "월결제";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bt_Paycheck;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}