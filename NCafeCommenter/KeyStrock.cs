﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NCafeCommenter
{
    class KeyStrock
    {
        #region SendMessage Constants

        private const int WM_LBUTTONDOWN = 0x0201;
        private const int WM_LBUTTONUP = 0x0202;
        private const int WM_LBUTTONDBLCLK = 0x0203;
        private const int WM_RBUTTONDOWN = 0x0204;
        private const int WM_RBUTTONUP = 0x0205;
        private const int WM_RBUTTONDBLCLK = 0x0206;
        private const int WM_MOUSEMOVE = 0x0200;

        private const int WM_KEYDOWN = 0x100;
        private const int WM_KEYUP = 0x0101;
        private const int WM_CHAR = 0x0102;

        #endregion SendMessage Constants

        #region Virtual Keys Constants

        public const int VK_0 = 0x30;
        public const int VK_1 = 0x31;
        public const int VK_2 = 0x32;
        public const int VK_3 = 0x33;
        public const int VK_4 = 0x34;
        public const int VK_5 = 0x35;
        public const int VK_6 = 0x36;
        public const int VK_7 = 0x37;
        public const int VK_8 = 0x38;
        public const int VK_9 = 0x39;
        public const int VK_A = 0x41;
        public const int VK_B = 0x42;
        public const int VK_C = 0x43;
        public const int VK_D = 0x44;
        public const int VK_E = 0x45;
        public const int VK_F = 0x46;
        public const int VK_G = 0x47;
        public const int VK_H = 0x48;
        public const int VK_I = 0x49;
        public const int VK_J = 0x4A;
        public const int VK_K = 0x4B;
        public const int VK_L = 0x4C;
        public const int VK_M = 0x4D;
        public const int VK_N = 0x4E;
        public const int VK_O = 0x4F;
        public const int VK_P = 0x50;
        public const int VK_Q = 0x51;
        public const int VK_R = 0x52;
        public const int VK_S = 0x53;
        public const int VK_T = 0x54;
        public const int VK_U = 0x55;
        public const int VK_V = 0x56;
        public const int VK_W = 0x57;
        public const int VK_X = 0x58;
        public const int VK_Y = 0x59;
        public const int VK_Z = 0x5A;

        public const int VK_BACK = 0x08;
        public const int VK_TAB = 0x09;
        public const int VK_CLEAR = 0x0C;
        public const int VK_RETURN = 0x0D;
        public const int VK_SHIFT = 0x10;
        public const int VK_CONTROL = 0x11;
        public const int VK_MENU = 0x12;//
        public const int VK_PAUSE = 0x13;//
        public const int VK_CAPITAL = 0x14;//
        public const int VK_KANA = 0x15;
        public const int VK_HANGEUL = 0x15;
        public const int VK_HANGUL = 0x15;
        public const int VK_JUNJA = 0x17;
        public const int VK_FINAL = 0x18;//
        public const int VK_HANJA = 0x19;
        public const int VK_KANJI = 0x19;
        public const int VK_ESCAPE = 0x1B;
        public const int VK_CONVERT = 0x1C;//
        public const int VK_NONCONVERT = 0x1D;//
        public const int VK_ACCEPT = 0x1E;//
        public const int VK_MODECHANGE = 0x1F;//
        public const int VK_SPACE = 0x20;
        public const int VK_PRIOR = 0x21;//
        public const int VK_NEXT = 0x22;
        public const int VK_END = 0x23;
        public const int VK_HOME = 0x24;
        public const int VK_LEFT = 0x25;
        public const int VK_UP = 0x26;
        public const int VK_RIGHT = 0x27;
        public const int VK_DOWN = 0x28;
        public const int VK_SELECT = 0x29;//
        public const int VK_PRINT = 0x2A;//
        public const int VK_EXECUTE = 0x2B;//
        public const int VK_SNAPSHOT = 0x2C;//
        public const int VK_INSERT = 0x2D;
        public const int VK_DELETE = 0x2E;
        public const int VK_HELP = 0x2F;//
        public const int VK_LWIN = 0x5B;//
        public const int VK_RWIN = 0x5C;//
        public const int VK_APPS = 0x5D;//
        public const int VK_SLEEP = 0x5F;
        public const int VK_NUMPAD0 = 0x60;
        public const int VK_NUMPAD1 = 0x61;
        public const int VK_NUMPAD2 = 0x62;
        public const int VK_NUMPAD3 = 0x63;
        public const int VK_NUMPAD4 = 0x64;
        public const int VK_NUMPAD5 = 0x65;
        public const int VK_NUMPAD6 = 0x66;
        public const int VK_NUMPAD7 = 0x67;
        public const int VK_NUMPAD8 = 0x68;
        public const int VK_NUMPAD9 = 0x69;
        public const int VK_MULTIPLY = 0x6A;//
        public const int VK_ADD = 0x6B;//
        public const int VK_SEPARATOR = 0x6C;//
        public const int VK_SUBTRACT = 0x6D;//
        public const int VK_DECIMAL = 0x6E;//
        public const int VK_DIVIDE = 0x6F;//
        public const int VK_F1 = 0x70;
        public const int VK_F2 = 0x71;
        public const int VK_F3 = 0x72;
        public const int VK_F4 = 0x73;
        public const int VK_F5 = 0x74;
        public const int VK_F6 = 0x75;
        public const int VK_F7 = 0x76;
        public const int VK_F8 = 0x77;
        public const int VK_F9 = 0x78;
        public const int VK_F10 = 0x79;
        public const int VK_F11 = 0x7A;
        public const int VK_F12 = 0x7B;
        public const int VK_F13 = 0x7C;
        public const int VK_F14 = 0x7D;
        public const int VK_F15 = 0x7E;
        public const int VK_F16 = 0x7F;
        public const int VK_F17 = 0x80;
        public const int VK_F18 = 0x81;
        public const int VK_F19 = 0x82;
        public const int VK_F20 = 0x83;
        public const int VK_F21 = 0x84;
        public const int VK_F22 = 0x85;
        public const int VK_F23 = 0x86;
        public const int VK_F24 = 0x87;
        public const int VK_NUMLOCK = 0x90;
        public const int VK_SCROLL = 0x91;//

        #endregion Virtual Keys Constants

        /*************************************화면캡쳐*********************************************************/
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool PrintWindow(IntPtr hwnd, IntPtr hDC, uint nFlags);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetWindowRgn(IntPtr hWnd, IntPtr hRgn);
        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateRectRgn(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect);
        /*************************************화면캡쳐*********************************************************/
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32", CharSet = CharSet.Unicode)]
        public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, string lParam);

        [DllImport("user32")]
        public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, string lParam);

        [DllImport("user32")]
        public static extern IntPtr FindWindowEx(IntPtr hWnd, IntPtr hWndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindow(IntPtr hWnd, int uCmd);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32")]
        public static extern IntPtr FindWindow(String lpClassName, String lpWindowName);

        [DllImport("user32.dll")]
        public static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, uint dwExtraInfo);

        [DllImport("User32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool CloseHandle(IntPtr handle);

        [DllImport("User32")]
        public static extern IntPtr ShowWindow(IntPtr hwnd, int nCmdShow);

        public static void SetFocus(IntPtr handle)
        {
            SetForegroundWindow(handle);
        }
        public static IntPtr MakeLParam(int LoWord, int HiWord)
        {
            return (IntPtr)((HiWord << 16) | (LoWord & 0xFFFF));
        }
        public static void profileinit(IntPtr handle, int x, int y)
        {
            SendMessage(handle, WM_LBUTTONDOWN, new IntPtr(0x0001), MakeLParam(x, y));
            SendMessage(handle, WM_LBUTTONUP, new IntPtr(0x0001), MakeLParam(280, 315));
        }
        public static void SendLeftButtonClick(IntPtr handle, int x, int y)
        {
            SendMessage(handle, WM_LBUTTONDOWN, new IntPtr(0x0001), MakeLParam(x, y));
            SendMessage(handle, WM_LBUTTONUP, new IntPtr(0x0001), MakeLParam(x, y));
        }
        public static void SendLeftButtonDoubleClick(IntPtr handle, int x, int y)
        {
            SendMessage(handle, WM_LBUTTONDBLCLK, new IntPtr(0x0001), MakeLParam(x, y));
        }
        public static void SendKeyPress(IntPtr handle, int key)
        {
            PostMessage(handle, WM_KEYDOWN, new IntPtr(key), null);
            PostMessage(handle, WM_KEYUP, new IntPtr(key), null);
        }
        public static void SendMessage(IntPtr handle, string text)
        {
            SendMessage(handle, 0xC, IntPtr.Zero, text);
        }
        public static void SendCtrlI(IntPtr hWnd)
        {
            uint KEYEVENTF_KEYUP = 2;
            byte VK_CONTROL = 0x11;
            SetForegroundWindow(hWnd);
            keybd_event(VK_CONTROL, 0, 0, 0);
            keybd_event(VK_I, 0, 0, 0); //Send the C key (43 is "C")
            keybd_event(VK_I, 0, KEYEVENTF_KEYUP, 0);
            keybd_event(VK_CONTROL, 0, KEYEVENTF_KEYUP, 0);// 'Left Control Up
        }
        public static void SendAltN(IntPtr hWnd)
        {
            uint KEYEVENTF_KEYUP = 2;
            SetForegroundWindow(hWnd);
            keybd_event(VK_MENU, 0, 0, 0);
            keybd_event(VK_N, 0, 0, 0); //Send the C key (43 is "C")
            keybd_event(VK_N, 0, KEYEVENTF_KEYUP, 0);
            keybd_event(VK_MENU, 0, KEYEVENTF_KEYUP, 0);// 'Left Control Up
        }
        public static void SendCtrlN(IntPtr hWnd)
        {
            uint KEYEVENTF_KEYUP = 2;
            byte VK_CONTROL = 0x11;
            SetForegroundWindow(hWnd);
            keybd_event(VK_CONTROL, 0, 0, 0);
            keybd_event(VK_N, 0, 0, 0); //Send the C key (43 is "C")
            keybd_event(VK_N, 0, KEYEVENTF_KEYUP, 0);
            keybd_event(VK_CONTROL, 0, KEYEVENTF_KEYUP, 0);// 'Left Control Up
        }
        public static void SendCtrlT(IntPtr hWnd)
        {
            uint KEYEVENTF_KEYUP = 2;
            byte VK_CONTROL = 0x11;
            SetForegroundWindow(hWnd);
            keybd_event(VK_CONTROL, 0, 0, 0);
            keybd_event(VK_T, 0, 0, 0); //Send the C key (43 is "C")
            keybd_event(VK_T, 0, KEYEVENTF_KEYUP, 0);
            keybd_event(VK_CONTROL, 0, KEYEVENTF_KEYUP, 0);// 'Left Control Up
        }

        public static void SendCtrlC(IntPtr hWnd)
        {
            uint KEYEVENTF_KEYUP = 2;
            byte VK_CONTROL = 0x11;
            SetForegroundWindow(hWnd);
            keybd_event(VK_CONTROL, 0, 0, 0);
            keybd_event(VK_C, 0, 0, 0); //Send the C key (43 is "C")
            keybd_event(VK_C, 0, KEYEVENTF_KEYUP, 0);
            keybd_event(VK_CONTROL, 0, KEYEVENTF_KEYUP, 0);// 'Left Control Up
        }
        public static void SendCtrlA(IntPtr hWnd)
        {
            uint KEYEVENTF_KEYUP = 2;
            byte VK_CONTROL = 0x11;
            SetForegroundWindow(hWnd);
            keybd_event(VK_CONTROL, 0, 0, 0);
            keybd_event(VK_A, 0, 0, 0); //Send the C key (43 is "C")
            keybd_event(VK_A, 0, KEYEVENTF_KEYUP, 0);
            keybd_event(VK_CONTROL, 0, KEYEVENTF_KEYUP, 0);// 'Left Control Up
        }

        public static void PrintWindow(IntPtr hwnd)
        {
            Rectangle rc = Rectangle.Empty;
            Graphics gfxWin = Graphics.FromHwnd(hwnd);
            rc = Rectangle.Round(gfxWin.VisibleClipBounds);
            Bitmap bmp = new Bitmap(rc.Width, rc.Height, PixelFormat.Format32bppArgb);
            Graphics gfxBmp = Graphics.FromImage(bmp);
            IntPtr hdcBitmap = gfxBmp.GetHdc();
            bool succeeded = PrintWindow(hwnd, hdcBitmap, 1);
            gfxBmp.ReleaseHdc(hdcBitmap);
            if (!succeeded)
            {
                gfxBmp.FillRectangle(
                    new SolidBrush(Color.Gray),
                    new Rectangle(System.Drawing.Point.Empty, bmp.Size));
            }
            IntPtr hRgn = CreateRectRgn(0, 0, 0, 0);
            GetWindowRgn(hwnd, hRgn);
            Region region = Region.FromHrgn(hRgn);
            if (!region.IsEmpty(gfxBmp))
            {
                gfxBmp.ExcludeClip(region);
                gfxBmp.Clear(Color.Transparent);
            }
            gfxBmp.Dispose();
            bmp.Save(Application.StartupPath + @"\view\image.png", System.Drawing.Imaging.ImageFormat.Png);
            bmp.Dispose();
            bmp = null;
        }
        public static void set_PrintWindow(IntPtr hwnd)
        {
            Rectangle rc = Rectangle.Empty;
            Graphics gfxWin = Graphics.FromHwnd(hwnd);
            rc = Rectangle.Round(gfxWin.VisibleClipBounds);

            Bitmap bmp = new Bitmap(
                rc.Width, rc.Height,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            Graphics gfxBmp = Graphics.FromImage(bmp);
            IntPtr hdcBitmap = gfxBmp.GetHdc();
            bool succeeded = PrintWindow(hwnd, hdcBitmap, 0);
            gfxBmp.ReleaseHdc(hdcBitmap);
            if (!succeeded)
            {
                gfxBmp.FillRectangle(
                    new SolidBrush(Color.Gray),
                    new Rectangle(Point.Empty, bmp.Size));
            }
            IntPtr hRgn = CreateRectRgn(0, 0, 0, 0);
            GetWindowRgn(hwnd, hRgn);
            Region region = Region.FromHrgn(hRgn);
            if (!region.IsEmpty(gfxBmp))
            {
                gfxBmp.ExcludeClip(region);
                gfxBmp.Clear(Color.Transparent);
            }
            gfxBmp.Dispose();
            bmp.Save(Application.StartupPath + @"\view\image.png", System.Drawing.Imaging.ImageFormat.Png);
        }


        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 50, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new System.Drawing.Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new System.Drawing.Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        [DllImport("user32")]
        public static extern int GetWindowRect(IntPtr hwnd, ref RECT lpRect);
        public struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }
        public static string GetWindowCaption(IntPtr hwnd)
        {
            StringBuilder sb = new StringBuilder(500);
            GetWindowText(hwnd, sb, 500);
            return sb.ToString();
        }
        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder title, int size);

        [DllImport("user32.dll")]
        public static extern long GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        public static extern IntPtr GetClassLong(IntPtr hwnd, int nIndex);

        [DllImport("user32.dll")]
        public static extern IntPtr SetWindowPos(IntPtr hwnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);

        public const int SWP_HIDEWINDOW = 0x128;
        public const int SWP_SHOWWINDOW = 0x64;

        const int GCL_HICON = -14; //GetWindowLong을 호출할 때 쓸 인자
        const int GCL_HMODULE = -16;
        ImageList imgList;//ListView의 Image로 쓸 리스트

        public delegate bool EnumWindowCallback(IntPtr hwnd, int lParam);

        [DllImport("user32.dll")]
        public static extern int EnumWindows(EnumWindowCallback callback, int y);

        [DllImport("user32.dll")]
        public static extern int GetParent(IntPtr hWnd);

        public bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam)
        {
            //윈도우 핸들로 그 윈도우의 스타일을 얻어옴
            UInt32 style = (UInt32)GetWindowLong(hWnd, GCL_HMODULE);
            //해당 윈도우의 캡션이 존재하는지 확인
            if ((style & 0x10000000L) == 0x10000000L && (style & 0x00C00000L) == 0x00C00000L)
            {
                //부모가 바탕화면인지 확인
                if (GetParent(hWnd) == 0)
                {
                    StringBuilder Buf = new StringBuilder(256);
                    //응용프로그램의 이름을 얻어온다
                    if (GetWindowText(hWnd, Buf, 256) > 0)
                    {
                        try
                        {
                            //HICON 아이콘 핸들을 얻어온다
                            IntPtr hIcon = GetClassLong((IntPtr)hWnd, GCL_HICON);
                            //아이콘 핸들로 Icon 객체를 만든다
                            Icon icon = Icon.FromHandle(hIcon);
                            imgList.Images.Add(icon);
                        }
                        catch (Exception)
                        {
                            //예외의 경우는 자기 자신의 윈도우인 경우이다.
                            //imgList.Images.Add(this.Icon);
                        }
                        MessageBox.Show(Buf.ToString());
                    }
                }
            }
            return true;
        }

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern IntPtr GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);
        public static string Get_WinClassName(IntPtr hWnd)
        {
            int nRet;
            StringBuilder ClassName = new StringBuilder(256);
            GetClassName(hWnd, ClassName, ClassName.Capacity);
            if (ClassName.ToString() != string.Empty)
            {
                return ClassName.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        [DllImport("user32")]
        static extern bool IsWindow(IntPtr hWnd);
        public static bool IsValid_InviteFriend_Window(IntPtr hWnd)
        {
            if (!IsWindow(hWnd))
                return false;

            IntPtr hChlid1, hChlid1_1, hChlid1_2, hChlid1_2_1, hChlid1_3, hChlid1_3_1, hChlid1_4, hChlid1_4_1;
            hChlid1 = FindWindowEx(hWnd, IntPtr.Zero, "#32770", null);
            hChlid1_1 = FindWindowEx(hChlid1, IntPtr.Zero, "Edit", null);
            hChlid1_2 = FindWindowEx(hChlid1, IntPtr.Zero, "EVA_VH_ListControl_Dblclk", null);
            hChlid1_2_1 = FindWindowEx(hChlid1_2, IntPtr.Zero, "_EVA_CustomScrollCtrl", null);
            hChlid1_3 = GetWindow(hChlid1_2, 2);
            hChlid1_3_1 = FindWindowEx(hChlid1_3, IntPtr.Zero, "_EVA_CustomScrollCtrl", null);
            hChlid1_4 = FindWindowEx(hChlid1, IntPtr.Zero, "EVA_Window", null);
            hChlid1_4_1 = FindWindowEx(hChlid1_4, IntPtr.Zero, "_EVA_CustomScrollCtrl", null);
            return (hChlid1 != IntPtr.Zero) && (hChlid1_1 != IntPtr.Zero) && (hChlid1_2 != IntPtr.Zero) && (hChlid1_2_1 != IntPtr.Zero)
                 && (hChlid1_3 != IntPtr.Zero) && (hChlid1_3_1 != IntPtr.Zero) && (hChlid1_4 != IntPtr.Zero) && (hChlid1_4_1 != IntPtr.Zero);
        }

        public static bool IsValidChatWindow(IntPtr hWnd)
        {
            if (!IsWindow(hWnd))
                return false;

            IntPtr hEdit, hSEdit, hList, hScrl;
            hEdit = FindWindowEx(hWnd, IntPtr.Zero, "RichEdit20W", null);
            hSEdit = FindWindowEx(hWnd, IntPtr.Zero, "Edit", null);
            hList = FindWindowEx(hWnd, IntPtr.Zero, "EVA_VH_ListControl_Dblclk", null);
            hScrl = FindWindowEx(hList, IntPtr.Zero, "_EVA_CustomScrollCtrl", null);
            //MessageBox.Show(hEdit.ToString("x8") + " | " + hSEdit.ToString("x8") + " | " + hList.ToString("x8") + " | " + hScrl.ToString("x8"));
            return (hEdit != IntPtr.Zero) && (hSEdit != IntPtr.Zero) && (hList != IntPtr.Zero) && (hScrl != IntPtr.Zero);
        }

        public static bool IsValidOpenChatWindow(IntPtr hWnd)
        {
            bool chk = false;
            if (!IsWindow(hWnd))
                chk = false;
            IntPtr hChild;
            hChild = FindWindowEx(hWnd, IntPtr.Zero, "EVA_ChildWindow", null);
            if ((hChild != IntPtr.Zero) && GetWindowCaption(hChild).Equals(""))
                chk = true;
            return chk;
        }

        public static bool IsValid_profilesettingRoom_Window(IntPtr hWnd)
        {
            bool chk = false;
            if (!IsWindow(hWnd))
                chk = false;
            IntPtr hChild;
            hChild = FindWindowEx(hWnd, IntPtr.Zero, "Edit", null);
            if ((hChild != IntPtr.Zero) && GetWindowCaption(hWnd).Equals(""))
                chk = true;
            return chk;
        }

        public static bool IsValid_PlusFriend_Window(IntPtr hWnd)
        {
            bool chk = false;
            if (!IsWindow(hWnd))
                chk = false;
            IntPtr hChild1_1, hChild1_2, hChild1_2_1, hChild1_3, hChild1_3_1;
            hChild1_1 = FindWindowEx(hWnd, IntPtr.Zero, "Edit", null);
            hChild1_2 = FindWindowEx(hWnd, IntPtr.Zero, "EVA_VH_ListControl_Dblclk", null);
            hChild1_2_1 = FindWindowEx(hChild1_2, IntPtr.Zero, "_EVA_CustomScrollCtrl", null);
            hChild1_3 = GetWindow(hChild1_2, 2);
            hChild1_3_1 = FindWindowEx(hChild1_3, IntPtr.Zero, "_EVA_CustomScrollCtrl", null);
            //MessageBox.Show(hChild1_1.ToString("x8") + "\r\n" +
            //    hChild1_2.ToString("x8") + "\r\n" +
            //    hChild1_2_1.ToString("x8") + "\r\n" +
            //    hChild1_3.ToString("x8") + "\r\n" +
            //    hChild1_3_1.ToString("x8") + "\r\n");
            if ((hChild1_1 != IntPtr.Zero) && (hChild1_2 != IntPtr.Zero) && (hChild1_2_1 != IntPtr.Zero) &&
                (hChild1_3 != IntPtr.Zero) && (hChild1_3_1 != IntPtr.Zero) && GetWindowCaption(hWnd).Contains("플러스친구"))
                chk = true;
            return chk;
        }
        //public static void SendLeftButtonDown(IntPtr handle,int x, int y)
        //{
        //    PostMessage(handle, WM_LBUTTONDOWN, 0, new IntPtr(y * 0x10000 + x));
        //}

        //public static void SendLeftButtonUp(int x, int y)
        //{
        //    PostMessage(handle, WM_LBUTTONUP, 0, new IntPtr(y * 0x10000 + x));
        //}

        //public static void SendLeftButtondblclick(int x, int y)
        //{
        //    PostMessage(handle, WM_LBUTTONDBLCLK, 0, new IntPtr(y * 0x10000 + x));
        //}

        //public static void SendRightButtonDown(int x, int y)
        //{
        //    PostMessage(handle, WM_RBUTTONDOWN, 0, new IntPtr(y * 0x10000 + x));
        //}

        //public static void SendRightButtonUp(int x, int y)
        //{
        //    PostMessage(handle, WM_RBUTTONUP, 0, new IntPtr(y * 0x10000 + x));
        //}

        //public static void SendRightButtondblclick(int x, int y)
        //{
        //    PostMessage(handle, WM_RBUTTONDBLCLK, 0, new IntPtr(y * 0x10000 + x));
        //}

        //public void SendMouseMove(int x, int y)
        //{
        //    PostMessage(handle, WM_MOUSEMOVE, 0, new IntPtr(y * 0x10000 + x));
        //}

        //public void SendKeyDown(int key)
        //{
        //    PostMessage(handle, WM_KEYDOWN, key, IntPtr.Zero);
        //}

        //public void SendKeyUp(int key)
        //{
        //    PostMessage(handle, WM_KEYUP, key, new IntPtr(1));
        //}

        //public void SendChar(char c)
        //{
        //    SendMessage(handle, WM_CHAR, c, IntPtr.Zero);
        //}

        //public void SendString(string s)
        //{
        //    foreach (char c in s) SendChar(c);
        //}
    }
}
