﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NCafeCommenter
{
    public partial class HardWareForm : Form
    {
        string CODE = "";
        string Price = "";
        public HardWareForm(string code, string price)
        {
            InitializeComponent();
            CODE = code;
            Price = price;
        }

        private void bt_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_Agreee_Click(object sender, EventArgs e)
        {
            CardPayEvent card = new CardPayEvent(CODE, Price);
            card.ShowDialog();
        }
    }
}
