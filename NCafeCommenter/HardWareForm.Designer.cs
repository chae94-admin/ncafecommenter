﻿namespace NCafeCommenter
{
    partial class HardWareForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HardWareForm));
            this.bt_close = new System.Windows.Forms.Button();
            this.bt_Agreee = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bt_close
            // 
            this.bt_close.BackgroundImage = global::NCafeCommenter.Properties.Resources.HARD_btn_cancel;
            this.bt_close.FlatAppearance.BorderSize = 0;
            this.bt_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_close.Location = new System.Drawing.Point(29, 398);
            this.bt_close.Name = "bt_close";
            this.bt_close.Size = new System.Drawing.Size(196, 30);
            this.bt_close.TabIndex = 27;
            this.bt_close.UseVisualStyleBackColor = true;
            this.bt_close.Click += new System.EventHandler(this.bt_close_Click);
            // 
            // bt_Agreee
            // 
            this.bt_Agreee.BackgroundImage = global::NCafeCommenter.Properties.Resources.HARD_btn_next;
            this.bt_Agreee.FlatAppearance.BorderSize = 0;
            this.bt_Agreee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_Agreee.Location = new System.Drawing.Point(229, 398);
            this.bt_Agreee.Name = "bt_Agreee";
            this.bt_Agreee.Size = new System.Drawing.Size(196, 30);
            this.bt_Agreee.TabIndex = 26;
            this.bt_Agreee.UseVisualStyleBackColor = true;
            this.bt_Agreee.Click += new System.EventHandler(this.bt_Agreee_Click);
            // 
            // HardWareForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::NCafeCommenter.Properties.Resources.HARD_img;
            this.ClientSize = new System.Drawing.Size(454, 460);
            this.Controls.Add(this.bt_close);
            this.Controls.Add(this.bt_Agreee);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HardWareForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "하드웨어 결제";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bt_close;
        private System.Windows.Forms.Button bt_Agreee;
    }
}