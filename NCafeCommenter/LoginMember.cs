﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace NCafeCommenter
{
    class LoginMember
    {
        public static string Memcode = "-1";
        public static bool Payment = false;
        public static string RemainDate = "-1";
        public static string ExpireDate = "-1";
        public static string hdd()
        {
            ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"C:\"");
            disk.Get();
            return disk["VolumeSerialNumber"].ToString();
        }
        public static bool Login(string id, string pwd)
        {
            Newtonsoft.Json.Linq.JObject mapList = null;
            try
            {
                string url = "https://marketingmonster.kr/lib/control.siso";
                mapList = WebController.ServerSender(url,
                    string.Format("m_id={0}&m_pass={1}&dbControl={2}&HARDWARE_NO={3}&m_lite_is=N&CLASS=" + Form1.Classname + "&CONNECTCODE=PC&siteUrl=marketingmonster.kr",
                    System.Web.HttpUtility.UrlEncode(id) , System.Web.HttpUtility.UrlEncode(pwd), "setMemberUserLoginCk", hdd()));
                if (((string)mapList["result"]).Equals("Y"))
                {
                    Memcode = (string)mapList["MEMCODE"];
                    ExpireDate = ((string)mapList["expire_datetime"]);
                    if (ExpireDate.CompareTo(DateTime.Now.ToString("yyyy-MM:dd hh:mm:ss")) >= 0)
                    {
                        Payment = true;
                        RemainDate = ((string)mapList["period"]);
                    }
                    if (mapList["pop_is"].ToString().Equals("Y"))
                    {
                        try
                        {
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.Load("timeconfig.xml");
                            XmlElement root = xmldoc.DocumentElement;
                            XmlNodeList nodes = root.SelectNodes("/date");
                            string time = "";
                            string selection = "";
                            foreach (XmlNode node in nodes)
                            {
                                time = node["time"].InnerText;
                                selection = node["bool"].InnerText;
                            }
                            if (selection.Equals("Y"))
                            {
                                //MessageBox.Show(time.Split('-')[2].Split(' ')[0].Trim() + " | " +DateTime.Now.ToString().Split('-')[2].Split(' ')[0].Trim());
                                if (int.Parse(time.Split('-')[2].Split(' ')[0].Trim()) != int.Parse(DateTime.Now.ToString().Split('-')[2].Split(' ')[0].Trim()))
                                {
                                    LoginPopUp lp = new LoginPopUp(mapList["pop_link"].ToString(), mapList["pop_img"].ToString());
                                    lp.Location = new System.Drawing.Point((Form1.ActiveForm.Width - lp.Width) / 2, (Form1.ActiveForm.Height - lp.Height) / 2);
                                    lp.ShowDialog();
                                }
                            }
                            else
                            {
                                LoginPopUp lp = new LoginPopUp(mapList["pop_link"].ToString(), mapList["pop_img"].ToString());
                                lp.Location = new System.Drawing.Point((Form1.ActiveForm.Width - lp.Width) / 2, (Form1.ActiveForm.Height - lp.Height) / 2);
                                lp.ShowDialog();
                            }
                        }
                        catch
                        {
                            LoginPopUp lp = new LoginPopUp(mapList["pop_link"].ToString(), mapList["pop_img"].ToString());
                            lp.Location = new System.Drawing.Point((Form1.ActiveForm.Width - lp.Width) / 2, (Form1.ActiveForm.Height - lp.Height) / 2);
                            lp.ShowDialog();
                        }

                    }
                  
                }
                else if (((string)mapList["result"]).Equals("MACPAY"))
                {
                    HardWareForm Hform = new HardWareForm((string)mapList["MACCODE"], (string)mapList["PRICE"]);
                    Hform.ShowDialog();
                }
                else
                {
                    MessageBox.Show((string)mapList["message"]);
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
            }
            return ((string)mapList["result"]).Equals("Y");
            //  return false;
        }
    }
}
