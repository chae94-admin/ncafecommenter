﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NCafeCommenter
{
    public partial class Charge : Form
    {
        public Charge()
        {
            InitializeComponent();
            Bank();
        }

        private void pb_Charge_close_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.chrge_icon_close_on;
        }

        private void pb_Charge_close_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.chrge_icon_close_on;
        }

        private void pb_Charge_close_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.chrge_icon_close;
        }

        private void pb_Charge_close_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.chrge_icon_close_on;
        }

        private void pb_Charge_SMS_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_sms_over;
        }

        private void pb_Charge_SMS_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_sms_on;
        }

        private void pb_Charge_SMS_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_sms;
        }

        private void pb_Charge_SMS_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_sms;
        }

        private void pb_Charge_Card_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_card_over;
        }

        private void pb_Charge_Card_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_card_on;
        }

        private void pb_Charge_Card_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_card;
        }

        private void pb_Charge_Card_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_card;
        }

        private void pb_Charge_SMS_Click(object sender, EventArgs e)
        {
            JObject jObject = WebController.ServerSender(Form1.url + Form1.URL + "/lib/control.siso",
string.Format("dbControl=setPurchageBankSMSSendPC&MEMCODE={0}&_LITEIS=Y&CONNECTCODE=PC&siteUrl=" + Form1.URL + "&CLASS={1}&HARDWARE_NO={2}"
     , LoginMember.Memcode, Form1.Classname, LoginMember.hdd()));
            if (((string)jObject["result"]).Equals("Y"))
                MessageBox.Show((string)jObject["message"]);
            else
                MessageBox.Show((string)jObject["message"]);
        }

        private void pb_Charge_Card_Click(object sender, EventArgs e)
        {
            CardPayEvent cardPayEvent = new CardPayEvent();
            cardPayEvent.ShowDialog();
        }

        private void pb_Charge_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void Bank()
        {
            string url = "https://" + "marketingmonster.kr" + "/lib/control.siso";

            //name phone mac seller
            try
            {
                Dictionary<string, string> Head = new Dictionary<string, string>();
                Head.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");
                Head.Add("Accept", "text/html, application/xhtml+xml, image/jxr, */*"); ;
                Head.Add("Content-Type", "application/x-www-form-urlencoded");
                Head.Add("Accept-Language", "ko-KR");
                Head.Add("Accept-Encoding", "gzip, deflate");

                Dictionary<string, string> MAP = new Dictionary<string, string>();
                MAP.Add("dbControl", "getBankInfo");
                MAP.Add("CLASS", Form1.Classname);
                MAP.Add("CONNECTCODE", "PC");
                MAP.Add("siteUrl", Form1.URL);
                string RET = WebController.POST(MAP, url, null, null, Encoding.UTF8, null, Head);


                Newtonsoft.Json.Linq.JObject mapList = Newtonsoft.Json.Linq.JObject.Parse(RET);

                if (((string)mapList["result"]).Equals("Y"))
                {
                    // GETDatas = mapList;
                    label1.Text = (string)mapList["data"]["BNAME"] + " " + (string)mapList["data"]["BNUM"];
                    label2.Text = "(" + (string)mapList["data"]["UNAME"];
                    //lb_Price.Text = (string)mapList["data"]["PRICE_VIEW"];
                }
                else
                {
                    MessageBox.Show("메시지 전송 실패했습니다.");
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
            }
        }
    }
}
