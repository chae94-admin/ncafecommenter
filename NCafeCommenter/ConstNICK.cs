﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCafeCommenter
{
    public class ConstNICK
    {
        public static List<string> LIST_NICK = new List<string>();
        //public ConstNICK()
        //{
        //    foreach (string inner in LIST_NICK)
        //    {
        //        LIST_NICK.Add(inner);
        //    }
        //}

        public static void Save()
        {
            if (LIST_NICK.Count < 1) return;
            JObject jobj = new JObject();
            JArray jary = new JArray();
            foreach (var nick in LIST_NICK)
            {
                if (!jary.Contains(nick)) jary.Add(nick);
            }
            jobj.Add("NICKS", jary);

            var lines = jobj.ToString().Split('\n');
            using (StreamWriter outputFile = new StreamWriter(@"NICK.json"))
            {
                foreach (string line in lines) outputFile.WriteLine(line);
            }
                
        }

        public static void Load()
        {
            if (!File.Exists("NICK.json")) return;
            FileStream fs = null;
            StreamReader objReader = null;
            try
            {
               // Encoding encode = System.Text.Encoding.Default;
                fs = new FileStream(@"NICK.json", FileMode.Open, FileAccess.Read);
                objReader = new StreamReader(fs);
                JObject jobj = JObject.Parse(objReader.ReadToEnd());
                foreach (var jtkn in jobj["NICKS"] as JArray) LIST_NICK.Add(jtkn.ToString());
            }
            catch(Exception ex)
            {
                MessageBoxEx.Show(ex.ToString());
            }
            finally
            {
                fs.Close();
                objReader.Close();
            }
        }
    }
}
