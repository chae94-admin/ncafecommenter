﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NCafeCommenter
{
    class ControlsEvent
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public static void SetHeight(ListView LV, int height)
        {
            ImageList imgList = new ImageList();
            imgList.ImageSize = new Size(1, height);
            LV.SmallImageList = imgList;
        }

        public static void TEXTBOX_ENTERED(TextBox tb, string text)
        {
            if (tb.Text.Trim().Equals(string.Empty) || tb.Text.Trim().Equals(text)) tb.Text = string.Empty;
        }

        public static void TEXTBOX_LEAVED(TextBox tb, string text)
        {
            if (tb.Text.Trim().Equals(string.Empty) || tb.Text.Trim().Equals(text)) tb.Text = text;
        }

        public static void OpenFile(ListView lv, string ExceptionString)
        {
            try
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.Filter = "허용파일 (*.txt, *csv)|*.txt;*.csv";
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    lv.Items.Clear();
                    Encoding encode = System.Text.Encoding.Default;
                    FileStream fs = new FileStream(fd.FileName, FileMode.Open, FileAccess.Read);
                    StreamReader objReader = new StreamReader(fs, encode);

                    string sLine = "";
                    List<string> arrText = new List<string>();
                    while (sLine != null)
                    {
                        sLine = objReader.ReadLine();
                        if (sLine != null)
                            arrText.Add(sLine);
                    }
                    objReader.Close();
                    foreach (string sOutput in arrText)
                    {
                        try
                        {
                            if (!(sOutput.Split(',')[0].Trim().Equals(string.Empty)))
                            {
                                ListViewItem lvi = new ListViewItem(string.Empty);
                                lvi.SubItems.Add((lv.Items.Count + 1).ToString());
                                lvi.SubItems.Add(sOutput.Split(',')[0].Trim());
                                lv.Items.Add(lvi);
                                lv.EnsureVisible(lv.Items.Count - 1);
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show(ExceptionString);
            }
        }

        public static void CHECKBOX_CHECKED(CheckBox chk, ListView lv)
        {
            if (chk.Checked)
            {
                if(lv!=null)
                {
                    foreach (ListViewItem lvi in lv.Items)
                    {
                        lvi.Checked = true;
                    }
                }
                chk.BackgroundImage = Properties.Resources.bg_chk_on; // 이부분은 직접 하던 매개변수로 받아서 처리하던 편한대로 하면됨
            }
            else
            {
                if(lv!=null)
                {
                    foreach (ListViewItem lvi in lv.Items)
                    {
                        lvi.Checked = false;
                    }
                }
                chk.BackgroundImage = Properties.Resources.bg_chk;
            }
        }

        public static void SELDEL(ListView lv)
        {
            if (lv.CheckedItems.Count < 1)
            {
                MessageBox.Show("항목을 선택해주세요");
                return;
            }
            if (MessageBox.Show("정말 삭제하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                foreach (ListViewItem lvi in lv.CheckedItems) lvi.Remove();

            for (int i = 0; i < lv.Items.Count; i++)
                lv.Items[i].Text = (i + 1).ToString();
        }

        public static void ALLDEL(ListView lv)
        {
            if (lv.Items.Count < 1)
            {
                MessageBox.Show("삭제할 항목이 없습니다.");
                return;
            }
            if (MessageBox.Show("정말 삭제하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                lv.Items.Clear();
        }

        public static void LISTVIEW_CHANGED_COLOR(ListView lv)
        {
            foreach (ListViewItem lvi in lv.Items)
            {
                if ((lvi.Index) % 2 == 1)
                {
                    lvi.BackColor = Color.FromArgb(246, 247, 247);
                }
            }
        }

        //public static void ReadToFile(string filepath)
        //{
        //    Encoding encode = System.Text.Encoding.Default;
        //    if (File.Exists(filepath))
        //    {
        //        FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read);
        //        StreamReader objReader = new StreamReader(fs, encode);

        //        Newtonsoft.Json.Linq.JObject JOBJ = Common.Checker.JsonReader_Checker(objReader.ReadToEnd());
        //        if (JOBJ["Message"] == null && (JOBJ["URLS"] as JArray).Count > 0)
        //        {
        //            foreach (var JTKN in (JOBJ["URLS"] as JArray))
        //                Parser.Parser.Except_URL.Add(JTKN.ToString().Trim());
        //        }
        //        Form1.AddLog("사용한 URL 리스트를 불러왔습니다.");
        //    }
        //}
    }
}
