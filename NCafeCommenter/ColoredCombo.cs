﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Collections;
using System.Data;
using System.Diagnostics;

namespace NCafeCommenter
{
    public class FlatCombo : ComboBox
    {
        private const int WM_PAINT = 0xF;
        private int buttonWidth = SystemInformation.HorizontalScrollBarArrowWidth;

        private Color _borderColor = Color.FromArgb(35, 37, 45);
        private ButtonBorderStyle _borderStyle = ButtonBorderStyle.None;
        private static int WM_PAINT2 = 0x000F;

        private Brush BorderBrush = new SolidBrush(Color.FromArgb(35, 37, 45));
        private Brush ArrowBrush = new SolidBrush(Color.Gray);
        public FlatCombo()
        {
            this.SetStyle(System.Windows.Forms.ControlStyles.OptimizedDoubleBuffer | System.Windows.Forms.ControlStyles.AllPaintingInWmPaint, true);
            //this.SetStyle(System.Windows.Forms.ControlStyles.UserPaint, true);

            this.SetStyle(System.Windows.Forms.ControlStyles.EnableNotifyMessage, true);
        }
        protected override void OnNotifyMessage(System.Windows.Forms.Message m)
        {
            
            if (m.Msg != 0x0014) // WM_ERASEBKGND == 0X0014
            {
                base.OnNotifyMessage(m);
            }
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);


            if (m.Msg == WM_PAINT)
            {

                var p = new Pen(Color.FromArgb(35, 37, 45));
                var g = Graphics.FromHwnd(Handle);
                g.DrawRectangle(p, 0, 0, Width - 1, Height - 1);
                g.DrawLine(p, Width - buttonWidth - 1, 0, Width - buttonWidth - 1, Height);
                //button Color 
                g.FillRectangle(BorderBrush, this.ClientRectangle);

                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                System.Drawing.Drawing2D.GraphicsPath pth = new System.Drawing.Drawing2D.GraphicsPath();
                //g.Dispose();
                PointF TopLeft = new PointF(this.Width - 13, (this.Height - 5) / 2);
                PointF TopRight = new PointF(this.Width - 6, (this.Height - 5) / 2);
                PointF Bottom = new PointF(this.Width - 9, (this.Height + 2) / 2);
                pth.AddLine(TopLeft, TopRight);
                pth.AddLine(TopRight, Bottom);
                g.FillPath(ArrowBrush, pth);

                //g.Dispose();
                //Create the path for the arrow
                //Determine the arrow's color.
                if (this.DroppedDown)
                {
                    ArrowBrush = new SolidBrush(Color.FromArgb(234, 57, 93));
                }
                else
                {
                    ArrowBrush = new SolidBrush(Color.FromArgb(234, 57, 93));
                }
                //Draw the arrow
            }
        }
        [Category("Appearance")]
        public Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                _borderColor = value;
                Invalidate(); // causes control to be redrawn
            }
        }

        [Category("Appearance")]
        public ButtonBorderStyle BorderStyle
        {
            get { return _borderStyle; }
            set
            {
                _borderStyle = value;
                Invalidate();
            }
        }
    }
}
