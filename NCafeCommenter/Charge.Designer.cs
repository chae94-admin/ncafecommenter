﻿namespace NCafeCommenter
{
    partial class Charge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Charge));
            this.pb_Charge_close = new System.Windows.Forms.PictureBox();
            this.pb_Charge_Card = new System.Windows.Forms.PictureBox();
            this.pb_Charge_SMS = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Charge_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Charge_Card)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Charge_SMS)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pb_Charge_close
            // 
            this.pb_Charge_close.BackColor = System.Drawing.Color.Transparent;
            this.pb_Charge_close.BackgroundImage = global::NCafeCommenter.Properties.Resources.chrge_icon_close2;
            this.pb_Charge_close.Location = new System.Drawing.Point(411, 13);
            this.pb_Charge_close.Name = "pb_Charge_close";
            this.pb_Charge_close.Size = new System.Drawing.Size(12, 12);
            this.pb_Charge_close.TabIndex = 111;
            this.pb_Charge_close.TabStop = false;
            this.pb_Charge_close.Click += new System.EventHandler(this.pb_Charge_close_Click);
            this.pb_Charge_close.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_Charge_close_MouseDown);
            this.pb_Charge_close.MouseEnter += new System.EventHandler(this.pb_Charge_close_MouseEnter);
            this.pb_Charge_close.MouseLeave += new System.EventHandler(this.pb_Charge_close_MouseLeave);
            this.pb_Charge_close.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_Charge_close_MouseUp);
            // 
            // pb_Charge_Card
            // 
            this.pb_Charge_Card.BackColor = System.Drawing.Color.Transparent;
            this.pb_Charge_Card.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_card;
            this.pb_Charge_Card.Location = new System.Drawing.Point(240, 252);
            this.pb_Charge_Card.Name = "pb_Charge_Card";
            this.pb_Charge_Card.Size = new System.Drawing.Size(164, 32);
            this.pb_Charge_Card.TabIndex = 110;
            this.pb_Charge_Card.TabStop = false;
            this.pb_Charge_Card.Click += new System.EventHandler(this.pb_Charge_Card_Click);
            this.pb_Charge_Card.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_Charge_Card_MouseDown);
            this.pb_Charge_Card.MouseEnter += new System.EventHandler(this.pb_Charge_Card_MouseEnter);
            this.pb_Charge_Card.MouseLeave += new System.EventHandler(this.pb_Charge_Card_MouseLeave);
            this.pb_Charge_Card.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_Charge_Card_MouseUp);
            // 
            // pb_Charge_SMS
            // 
            this.pb_Charge_SMS.BackColor = System.Drawing.Color.Transparent;
            this.pb_Charge_SMS.BackgroundImage = global::NCafeCommenter.Properties.Resources.btn_sms;
            this.pb_Charge_SMS.Location = new System.Drawing.Point(30, 252);
            this.pb_Charge_SMS.Name = "pb_Charge_SMS";
            this.pb_Charge_SMS.Size = new System.Drawing.Size(164, 32);
            this.pb_Charge_SMS.TabIndex = 109;
            this.pb_Charge_SMS.TabStop = false;
            this.pb_Charge_SMS.Click += new System.EventHandler(this.pb_Charge_SMS_Click);
            this.pb_Charge_SMS.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pb_Charge_SMS_MouseDown);
            this.pb_Charge_SMS.MouseEnter += new System.EventHandler(this.pb_Charge_SMS_MouseEnter);
            this.pb_Charge_SMS.MouseLeave += new System.EventHandler(this.pb_Charge_SMS_MouseLeave);
            this.pb_Charge_SMS.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb_Charge_SMS_MouseUp);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(46, 223);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(140, 20);
            this.panel2.TabIndex = 113;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(24, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 12);
            this.label2.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(28)))), ((int)(((byte)(32)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(32, 203);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(162, 20);
            this.panel1.TabIndex = 112;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(6, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 12);
            this.label1.TabIndex = 0;
            // 
            // Charge
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::NCafeCommenter.Properties.Resources.N_Cafe_Post_payment;
            this.ClientSize = new System.Drawing.Size(435, 325);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pb_Charge_close);
            this.Controls.Add(this.pb_Charge_Card);
            this.Controls.Add(this.pb_Charge_SMS);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Charge";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Charge";
            ((System.ComponentModel.ISupportInitialize)(this.pb_Charge_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Charge_Card)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Charge_SMS)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_Charge_close;
        private System.Windows.Forms.PictureBox pb_Charge_Card;
        private System.Windows.Forms.PictureBox pb_Charge_SMS;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
    }
}