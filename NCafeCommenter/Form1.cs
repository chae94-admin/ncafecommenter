﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using Microsoft.Win32;
using OpenQA.Selenium.Support.UI;

namespace NCafeCommenter
{
    public partial class Form1 : Form
    {
        Thread Main_thr;
        Thread Cert_thr;
        Thread Time_thr;
        Thread CheckPayment;
        Thread load_thr;
        public static string Productname = "NCafeCommenter";
        public static string Classname = "ncafecommenter";
        public const string url = "https://";
        public const string URL = "marketingmonster.kr";

        public const string ver = "1.3.4";
        public static string refreshTime = "";
        public static string marketingmonsterID = "";
        public static string noticeUrl = "";

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Shown(object sender, EventArgs e)
        {
            Overlap_Process();
            CheckVer();
            GetNotice();

           
            //killedprocess();
            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            UpdateStyles();
            tb_work_datetime.Text = DateTime.Now.ToString("yyyy.MM.dd");
            tb_work_datetime_HH.Text = DateTime.Now.ToString("HH");
            tb_work_datetime_mm.Text = DateTime.Now.ToString("mm");


            for (int i = 30; i <= 60; i++)
                fc_DelayTime.Items.Add(i);
            fc_DelayTime.SelectedIndex = 0;
            for (int i = 1; i <= 1000; i++)
            {
                fc_Selpage_one_1.Items.Add(i);
                fc_Selpage_one_2.Items.Add(i);
                fc_Selpage_All_1.Items.Add(i);
                fc_Selpage_All_2.Items.Add(i);
            }
            fc_Selpage_one_1.SelectedIndex = 0;
            fc_Selpage_one_2.SelectedIndex = 1;
            fc_Selpage_All_1.SelectedIndex = 0;
            fc_Selpage_All_2.SelectedIndex = 1;

            ConstNICK.Load();
            foreach (var nick in ConstNICK.LIST_NICK)
            {
                ListViewItem LVI_NICK = new ListViewItem((lv_Nick.Items.Count+1).ToString());
                LVI_NICK.SubItems.Add(nick.Trim());
                lv_Nick.Items.Add(LVI_NICK);
                lv_Nick.EnsureVisible(lv_Nick.Items.Count - 1);
            }
            //var data = Encoding.Default.GetBytes("한글한글");
            //string C = string.Empty;
            //foreach(var d in data)
            //{
            //    C += d.ToString("x8");
            //}
            //Log(C);
            //C = string.Empty;
            //data = Encoding.UTF8.GetBytes("한글한글");
            //foreach (var d in data)
            //{
            //    C += d.ToString("x8");
            //}
            //Log(C);
            //Log(data.ToString());
            tb_login_id.Focus();
        }

        private void Overlap_Process()
        {
            try
            {
                Process[] p = Process.GetProcessesByName(Application.ProductName);
                if (p.GetLength(0) > 1)
                {
                    MessageBox.Show("이전 프로그램이 실행 중입니다. 현재 프로그램을 종료하겠습니다.");
                    this.Close();
                }
            }
            catch { }
        }

        private void pb_header_mini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pb_header_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void pn_header_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(this.Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        private void tb_work_IdAdd_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (tb_work_Id.Text == string.Empty || tb_work_Id.Text.Equals("아이디") || tb_work_Pw.Text == string.Empty || tb_work_Pw.Text.Equals("비밀번호"))
            {
                MessageBoxEx.Show(this, "아이디나 비밀번호중 공란이 있습니다. 다시한번 확인해주세요", "확인");
                return;
            }
            try
            {
                if (lv_work_IdList.FindItemWithText(tb_work_Id.Text.Trim()).Index.ToString() != null)
                {
                    MessageBoxEx.Show(this, "동일한 아이디가 이미 아이디 리스트에 있습니다. 추가시 아이디 리스트에 있는\r\n" +
                        (lv_work_IdList.FindItemWithText(tb_work_Id.Text.Trim()).Index + 1).ToString()
                        + "번째의 아이디 [ " + lv_work_IdList.Items[(lv_work_IdList.FindItemWithText(tb_work_Id.Text.Trim())).Index].SubItems[1].Text + " ] 패스워드 " +
                        "[ " + lv_work_IdList.Items[(lv_work_IdList.FindItemWithText(tb_work_Id.Text.Trim())).Index].SubItems[2].Text + " ]를 삭제해주세요.");
                    return;
                }
            }
            catch (NullReferenceException) { }

            ListViewItem lvi = new ListViewItem();
            lvi.SubItems.Add(tb_work_Id.Text);
            lvi.SubItems.Add(tb_work_Pw.Text);
            lvi.SubItems.Add("X");
            lv_work_IdList.Items.Add(lvi);
            lv_work_IdList.EnsureVisible(lv_work_IdList.Items.Count - 1);
            tb_work_Id.Text = "아이디";
            tb_work_Pw.PasswordChar = default(char);
            tb_work_Pw.Text = "비밀번호";
            SavedFormData_server();
        }

        private void tb_work_Pw_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (tb_work_Id.Text == string.Empty || tb_work_Id.Text.Equals("아이디") || tb_work_Pw.Text == string.Empty || tb_work_Pw.Text.Equals("비밀번호"))
                {
                    MessageBoxEx.Show(this, "아이디나 비밀번호중 공란이 있습니다. 다시한번 확인해주세요", "확인");
                    return;
                }
                try
                {
                    if (lv_work_IdList.FindItemWithText(tb_work_Id.Text.Trim()).Index.ToString() != null)
                    {
                        MessageBoxEx.Show(this, "동일한 아이디가 이미 아이디 리스트에 있습니다. 추가시 아이디 리스트에 있는\r\n" +
                            (lv_work_IdList.FindItemWithText(tb_work_Id.Text.Trim()).Index + 1).ToString()
                            + "번째의 아이디 [ " + lv_work_IdList.Items[(lv_work_IdList.FindItemWithText(tb_work_Id.Text.Trim())).Index].SubItems[1].Text + " ] 패스워드 " +
                            "[ " + lv_work_IdList.Items[(lv_work_IdList.FindItemWithText(tb_work_Id.Text.Trim())).Index].SubItems[2].Text + " ]를 삭제해주세요.");
                        return;
                    }
                }
                catch (NullReferenceException) { }
                ListViewItem lvi = new ListViewItem();
                lvi.SubItems.Add(tb_work_Id.Text);
                lvi.SubItems.Add(tb_work_Pw.Text);
                lvi.SubItems.Add("X");
                lv_work_IdList.Items.Add(lvi);
                lv_work_IdList.EnsureVisible(lv_work_IdList.Items.Count - 1);
                tb_work_Id.Text = "아이디";
                tb_work_Pw.PasswordChar = default(char);
                tb_work_Pw.Text = "비밀번호";

                tb_work_Id.Focus();
                SavedFormData_server();
            }
        }

        private void tb_work_Pw_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("비밀번호"))
            {
                tb.Text = "";
                tb.PasswordChar = '●';
            }
        }

        private void tb_work_Pw_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("비밀번호") || tb.Text.Equals(""))
            {
                tb.Text = "비밀번호";
                tb.PasswordChar = default(char);
            }
        }
        private void Log(string text)
        {
            tb_work_log.AppendText("[ " + DateTime.Now.ToString("yyyy.MM.dd.HH:mm") + " ] " + text + "\r\n");
        }
        private void Set_Status(string text)
        {
            lb_work_Status.Text = text;
        }
        public double GetChromeDriverVersion()
        {
            try
            {
                string VersionStr = string.Empty;
                double Version = -1;
                var RET = WebController.POST(new Dictionary<string, string>() { { "dbControl", "getStartCheck" }, { "siteUrl", URL }, { "CONNECTCODE", "PC" }, { "CLASS", "Chromedriver" },
                }, string.Format(url + URL + "/lib/control.siso"), null, null, Encoding.UTF8);
                if (RET.Equals(string.Empty)) return -1;
                JObject JOBJ_RET = JObject.Parse(RET);
                if (JOBJ_RET["updateFile"].ToString().Contains("_"))
                    VersionStr = JOBJ_RET["updateFile"].ToString().Split('_')[JOBJ_RET["updateFile"].ToString().Split('_').Length - 1];
                if (VersionStr.Contains(".zip")) VersionStr = VersionStr.Replace(".zip", string.Empty);
                if (double.TryParse(VersionStr, out Version)) return Version;
                else return -1;
            }
            catch (ThreadAbortException)
            {
                return -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("크롬드라이버 버전 로드에 실패하였습니다. 잠시후 다시시도해주세요." + ex.ToString());
                return -1;
            }
        }
        public string GetChromePath()
        {
            try
            {
                string Chrome32Path = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
                string Chrome64Path = @"C:\Program Files\Google\Chrome\Application\chrome.exe";
                bool Chrome32PathExist = File.Exists(Chrome32Path);
                bool Chrome64PathExist = File.Exists(Chrome64Path);
                double ChromeDriverVersion = GetChromeDriverVersion();
                double Chrome32Version = -1;
                double Chrome64Version = -1;

                if (ChromeDriverVersion < 0)
                {
                    MessageBox.Show("크롬드라이버 버전 로드에 실패하였습니다. 잠시 후 다시 시도해주세요.");
                    return string.Empty;
                }
                if (Chrome64PathExist)
                {
                    Chrome64Version = double.Parse(FileVersionInfo.GetVersionInfo(Chrome64Path).FileVersion.Replace(".", ""));

                    if (Chrome64Version > -1 && Chrome64Version < ChromeDriverVersion)
                    {
                        MessageBox.Show("▶현재 설치된 크롬이 최신버전인지 확인 후 최신 크롬 업데이트를 받아주세요.");
                        Log("▶현재 설치된 크롬이 최신버전인지 확인 후 최신 크롬 업데이트를 받아주세요.");
                        ChromeDriverUpdateModule();
                        return string.Empty;
                    }
                    else
                    {
                        return Chrome64Path;
                    }

                }
                if (Chrome32PathExist)
                {
                    Chrome32Version = double.Parse(FileVersionInfo.GetVersionInfo(Chrome32Path).FileVersion.Replace(".", ""));
                    if (Chrome32Version > -1 && Chrome32Version < ChromeDriverVersion)
                    {
                        MessageBox.Show("▶현재 설치된 크롬이 최신버전인지 확인 후 최신 크롬 업데이트를 받아주세요.");
                        Log("▶현재 설치된 크롬이 최신버전인지 확인 후 최신 크롬 업데이트를 받아주세요.");
                        ChromeDriverUpdateModule();
                        return string.Empty;
                    }
                    else
                    {
                        return Chrome32Path;
                    }
                }
                MessageBox.Show("크롬이 C드라이브내에 설치되어있는지 확인해주세요.");
                return string.Empty;
            }
            catch (ThreadAbortException) { return string.Empty; }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return string.Empty;
            }
        }
        private void pb_work_Idcert_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (lv_work_IdList.CheckedItems.Count < 1)
            {
                MessageBoxEx.Show(this, "선택된 항목이 없습니다. 항목을 선택해주세요.");
                return;
            }
            if (!GetChrome_Install())
            {
                MessageBox.Show("해당 PC에 크롬이 없습니다 설치페이지로 이동합니다.");
                try { Process.Start("https://www.google.co.kr/chrome/"); } catch { }
                return;
            }
            if(Cert_thr !=null && Cert_thr.IsAlive)
            {
                MessageBoxEx.Show(this, "현재 검증과정을 진행중입니다. 잠시만 기다려주세요.");
                return;
            }
            Log("아이디 검증과정을 진행합니다.");
            Set_Status("아이디 검증과정을 진행합니다.");
            Cert_thr = new Thread(new ThreadStart(delegate
            {
                int cnt = 0;
                for (int i = 0; i < lv_work_IdList.CheckedItems.Count; i++)
                {
                    CookieContainer cookieContainer = new CookieContainer();
                    AutoTethering();
                    cookieContainer = NaverLogin(lv_work_IdList.CheckedItems[i].SubItems[1].Text, lv_work_IdList.CheckedItems[i].SubItems[2].Text);
                    if (cookieContainer != null)
                    {
                        List<object> Total = new List<object>();
                        List<string> CafeList = new List<string>();
                        CookieContainer Save_Cookie = cookieContainer;
                     
                        //lv_work_IdList.CheckedItems[i].SubItems[3].
                        string result = WebController.GET("https://section.cafe.naver.com/cafe-home-api/v1/config/join-cafes/groups?page=1", "https://section.cafe.naver.com/cafe-home/mycafe/join", cookieContainer, Encoding.UTF8);
                        //Log(result);

                        if (result == string.Empty)
                        {
                            MessageBoxEx.Show(this, (i + 1) + " 번째 [ " + lv_work_IdList.CheckedItems[i].SubItems[1].Text + " ] 아이디의 검증과정중 카페리스트를 로드하던 중 서버와의 문제가 생겼습니다, 잠시후 다시 검증해주세요");
                            Log((i + 1) + " 번째 [ " + lv_work_IdList.CheckedItems[i].SubItems[1].Text + " ] 아이디의 검증과정중 카페리스트를 로드하던 중 서버와의 문제가 생겼습니다, 잠시후 다시 검증해주세요");
                            continue;
                        }

                        JObject jobj = JObject.Parse(result);
                        if (!jobj["message"]["error"]["code"].ToString().Trim().Equals(string.Empty))
                        {
                            MessageBoxEx.Show(this, (i + 1) + " 번째 [ " + lv_work_IdList.CheckedItems[i].SubItems[1].Text + " ] 아이디의 검증과정중 카페리스트를 로드하던 중 서버와의 문제가 생겼습니다, 잠시후 다시 검증해주세요");
                            Log((i + 1) + " 번째 [ " + lv_work_IdList.CheckedItems[i].SubItems[1].Text + " ] 아이디의 검증과정중 카페리스트를 로드하던 중 서버와의 문제가 생겼습니다, 잠시후 다시 검증해주세요");
                            continue;
                        }
                        double totalcafe = double.Parse(jobj["message"]["result"]["pageInfo"]["totalCount"].ToString().Replace(",", "").Trim());
                        double maxpage = 0;
                        maxpage = Math.Ceiling(totalcafe / 15);
                        for (int set_cnt = 0; set_cnt < maxpage; set_cnt++)
                        {
                            result = WebController.GET("https://section.cafe.naver.com/cafe-home-api/v1/config/join-cafes/groups?page=" + (set_cnt + 1), "https://section.cafe.naver.com/cafe-home/mycafe/join", cookieContainer, Encoding.UTF8);
                            if (result == string.Empty)
                            {
                                MessageBoxEx.Show(this, (i + 1) + " 번째 [ " + lv_work_IdList.CheckedItems[i].SubItems[1].Text + " ] 아이디의 검증과정중 카페리스트를 로드하던 중 서버와의 문제가 생겼습니다, 잠시후 다시 검증해주세요");
                                Log((i + 1) + " 번째 [ " + lv_work_IdList.CheckedItems[i].SubItems[1].Text + " ] 아이디의 검증과정중 카페리스트를 로드하던 중 서버와의 문제가 생겼습니다, 잠시후 다시 검증해주세요");
                                continue;
                            }
                            if (!jobj["message"]["error"]["code"].ToString().Trim().Equals(string.Empty))
                            {
                                MessageBoxEx.Show(this, (i + 1) + " 번째 [ " + lv_work_IdList.CheckedItems[i].SubItems[1].Text + " ] 아이디의 검증과정중 카페리스트를 로드하던 중 서버와의 문제가 생겼습니다, 잠시후 다시 검증해주세요");
                                Log((i + 1) + " 번째 [ " + lv_work_IdList.CheckedItems[i].SubItems[1].Text + " ] 아이디의 검증과정중 카페리스트를 로드하던 중 서버와의 문제가 생겼습니다, 잠시후 다시 검증해주세요");
                                continue;
                            }
                            jobj = JObject.Parse(result);
                            //Log(jobj.ToString());
                            foreach (var _item in jobj["message"]["result"]["groups"] as JArray)
                            {
                                var jobj_Set = _item as JObject;
                                var jary_Set = jobj_Set["cafes"] as JArray;
                                foreach (var _inneritem in jary_Set)
                                {
                                    CafeList.Add("https://cafe.naver.com/" + _inneritem["cafeUrl"].ToString().Trim());
                                }
                            }
                            lv_work_IdList.Refresh();
                        }

                        Total.Add(CafeList);
                        Total.Add(cookieContainer);
                        lv_work_IdList.CheckedItems[i].SubItems[3].Text = "O";

                        Log((i + 1) + "번째 아이디 [ " + lv_work_IdList.CheckedItems[i].SubItems[1].Text + " ] - 검증완료");
                        Set_Status((i + 1) + "번째 아이디 [ " + lv_work_IdList.CheckedItems[i].SubItems[1].Text + " ] - 검증완료");
                        lv_work_IdList.CheckedItems[i].Tag = Total;
                        fc_IdList.Items.Add(lv_work_IdList.CheckedItems[i].SubItems[1].Text);

                    }
                    else
                    {
                        lv_work_IdList.CheckedItems[i].SubItems[3].Text = "X";
                        Log((i + 1) + "번째 아이디 [ " + lv_work_IdList.CheckedItems[i].SubItems[1].Text + " ] - 검증실패");
                        Set_Status((i + 1) + "번째 아이디 [ " + lv_work_IdList.CheckedItems[i].SubItems[1].Text + " ] - 검증실패");
                    }
                    cnt++;
                }
                Log("총 " + cnt + "개의 아이디 검증과정을 완료하였습니다.");
                Set_Status("※ 작업 대기중 - 마지막 작업 [ 아이디검증 ]");
            }));
            Cert_thr.IsBackground = true;
            Cert_thr.Start();
        }

        private bool GetChrome_Install()
        {
            bool real = false;
            RegistryKey SoftwareKey = Registry.LocalMachine.OpenSubKey("Software", true).OpenSubKey("Microsoft", true).OpenSubKey("Windows", true).OpenSubKey("CurrentVersion", true).OpenSubKey("Uninstall", true);
            string[] arrStrKeyName = SoftwareKey.GetSubKeyNames();
            string tempName = "";
            for (int i = 0; i < arrStrKeyName.Length; i++)
            {
                tempName = SoftwareKey.OpenSubKey(arrStrKeyName[i]).GetValue("DisplayName", "N").ToString();
                if (tempName == "Chrome" || tempName == "Google Chrome" || tempName == "Google chrome")
                {
                    real = true;
                    break;
                }
            }
            return real;
        }

        private CookieContainer NaverLogin(string ID, string PW)
        {
            IntPtr current_chrome = IntPtr.Zero;
            try
            {
                CookieContainer cookieContainer = new CookieContainer();
                IWebDriver driver;
                //DirectoryInfo di = new DirectoryInfo(@"C:\Program Files (x86)\Google\Chrome");
                //if(!di.Exists)
                //{
                //    var dir = new DirectoryInfo(@"C:\Program Files\Google\Chrome");
                //    //File.co
                //}
                //string[] PATH_Chrome = { @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe", @"C:\Program Files\Google\Chrome\Application\chrome.exe" };
                //string BinaryLoc = string.Empty;
                //foreach(var path in PATH_Chrome) if (!File.Exists(path)) BinaryLoc = path;
                //if(BinaryLoc.Equals(string.Empty))
                //{
                //    MessageBox.Show("크롬이 정상경로에 설치되어있지않습니다. 구글크롬을 삭제 후 재설치해주세요.");
                //    return null;
                //}
                ChromeDriverService service = ChromeDriverService.CreateDefaultService();
                service.HideCommandPromptWindow = true;
                var options = new ChromeOptions();
                //options.AddArguments("headless");
                //options.BinaryLocation = BinaryLoc;
                options.AddArguments("--incognito");
                options.AddArgument("no-sandbox");
                var ChromePath = GetChromePath();
                if (ChromePath.Equals(string.Empty)) return null;
                else options.BinaryLocation = ChromePath;
                driver = new ChromeDriver(service, options, TimeSpan.FromMinutes(3));

                current_chrome = KeyStrock.FindWindow("Chrome_WidgetWin_1", "data:, - Chrome");
                //KeyStrock.ShowWindow(current_chrome, 0);
                driver.Navigate().GoToUrl("https://www.naver.com/");
                Thread.Sleep(5000);
                var NLogin = driver.FindElement(By.XPath("//a[@class='link_login']"), 180);
                NLogin.Click();
                var inputID = driver.FindElement(By.XPath("//input[@type='text']"), 180);
                var inputPW = driver.FindElement(By.XPath("//input[@type='password']"), 180);

                try
                {

                    if (driver.FindElement(By.XPath("//label[@for='switch']")).Text.Equals("on"))
                        driver.FindElement(By.XPath("//label[@for='switch']")).Click();
                }
                catch
                {

                }
                try
                {
                    if (driver.FindElement(By.XPath("//label[@for='ip_on']")).GetAttribute("class").Equals("security on"))
                        driver.FindElement(By.XPath("//label[@for='ip_on']")).Click();
                }
                catch
                {

                }

                Invoke(new Action(delegate () { Clipboard.SetText(ID); }));
                //Clipboard.SetText(ID);
                inputID.SendKeys(OpenQA.Selenium.Keys.Control + "v");
                Invoke(new Action(delegate () { Clipboard.SetText(PW); }));
                //Clipboard.SetText(PW);
                inputPW.SendKeys(OpenQA.Selenium.Keys.Control + "v" + "\n");
                Thread.Sleep(10000);

                if (driver.Url.ToString().Trim().Equals("https://www.naver.com/"))
                {
                    System.Net.CookieCollection initcookie = new System.Net.CookieCollection();
                    for (int i = 0; i < driver.Manage().Cookies.AllCookies.Count; i++)
                    {
                        initcookie.Add(new System.Net.Cookie(driver.Manage().Cookies.AllCookies[i].Name, driver.Manage().Cookies.AllCookies[i].Value));
                    }
                    cookieContainer.Add(new Uri("https://blog.editor.naver.com/"), initcookie);
                    cookieContainer.Add(new Uri("https://section.cafe.naver.com/"), initcookie);
                    cookieContainer.Add(new Uri("https://m.cafe.naver.com/"), initcookie);
                    cookieContainer.Add(new Uri("https://cafe.naver.com/"), initcookie);
                    cookieContainer.Add(new Uri("https://up.cafe.naver.com/"), initcookie);
                    cookieContainer.Add(new Uri("https://m.blog.naver.com/"), initcookie);
                    cookieContainer.Add(new Uri("https://apis.naver.com/"), initcookie);
                    cookieContainer.Add(new Uri("https://static.nid.naver.com/"), initcookie);
                    cookieContainer.Add(new Uri("https://apis.naver.com/"), initcookie);
                    cookieContainer.Add(new Uri("https://nid.naver.com/"), initcookie);
                    KeyStrock.SendMessage(current_chrome, 0x0010, IntPtr.Zero, IntPtr.Zero);
                    return cookieContainer;
                }
                else
                {
                    try
                    {
                        if (driver.FindElement(By.XPath("//div[@class='captcha']")) != null)
                        {
                            MessageBox.Show("캡챠발생! 수동으로 캡챠해제 이후 \"확인\"을 눌러주세요.");
                            System.Net.CookieCollection initcookie = new System.Net.CookieCollection();
                            for (int i = 0; i < driver.Manage().Cookies.AllCookies.Count; i++)
                            {
                                initcookie.Add(new System.Net.Cookie(driver.Manage().Cookies.AllCookies[i].Name, driver.Manage().Cookies.AllCookies[i].Value));
                            }
                            cookieContainer.Add(new Uri("https://blog.editor.naver.com/"), initcookie);
                            cookieContainer.Add(new Uri("https://section.cafe.naver.com/"), initcookie);
                            cookieContainer.Add(new Uri("https://m.cafe.naver.com/"), initcookie);
                            cookieContainer.Add(new Uri("https://cafe.naver.com/"), initcookie);
                            cookieContainer.Add(new Uri("https://up.cafe.naver.com/"), initcookie);
                            cookieContainer.Add(new Uri("https://m.blog.naver.com/"), initcookie);
                            cookieContainer.Add(new Uri("https://apis.naver.com/"), initcookie);
                            cookieContainer.Add(new Uri("https://static.nid.naver.com/"), initcookie);
                            cookieContainer.Add(new Uri("https://apis.naver.com/"), initcookie);
                            cookieContainer.Add(new Uri("https://nid.naver.com/"), initcookie);
                            KeyStrock.SendMessage(current_chrome, 0x0010, IntPtr.Zero, IntPtr.Zero);
                            return cookieContainer;
                        }
                        if (driver.FindElement(By.XPath("//h2[@class='title']")) != null)
                        {
                            if (driver.FindElement(By.XPath("//h2[@class='title']")).Text.Trim().Contains("2단계 인증"))
                            {
                                MessageBox.Show("2단계 인증을 받으신 이후 \"확인\"을 눌러주세요.");
                                System.Net.CookieCollection initcookie = new System.Net.CookieCollection();
                                for (int i = 0; i < driver.Manage().Cookies.AllCookies.Count; i++)
                                {
                                    initcookie.Add(new System.Net.Cookie(driver.Manage().Cookies.AllCookies[i].Name, driver.Manage().Cookies.AllCookies[i].Value));
                                }
                                cookieContainer.Add(new Uri("https://blog.editor.naver.com/"), initcookie);
                                cookieContainer.Add(new Uri("https://section.cafe.naver.com/"), initcookie);
                                cookieContainer.Add(new Uri("https://m.cafe.naver.com/"), initcookie);
                                cookieContainer.Add(new Uri("https://cafe.naver.com/"), initcookie);
                                cookieContainer.Add(new Uri("https://up.cafe.naver.com/"), initcookie);
                                cookieContainer.Add(new Uri("https://m.blog.naver.com/"), initcookie);
                                cookieContainer.Add(new Uri("https://apis.naver.com/"), initcookie);
                                cookieContainer.Add(new Uri("https://static.nid.naver.com/"), initcookie);
                                cookieContainer.Add(new Uri("https://apis.naver.com/"), initcookie);
                                cookieContainer.Add(new Uri("https://nid.naver.com/"), initcookie);
                                KeyStrock.SendMessage(current_chrome, 0x0010, IntPtr.Zero, IntPtr.Zero);
                                return cookieContainer;
                            }
                            else return null;
                        }
                    }
                    catch (NoSuchElementException) { }
                    try
                    {
                        driver.FindElement(By.XPath("//span[@class='btn_cancel']")).Click();
                        System.Net.CookieCollection initcookie = new System.Net.CookieCollection();
                        for (int i = 0; i < driver.Manage().Cookies.AllCookies.Count; i++)
                        {
                            initcookie.Add(new System.Net.Cookie(driver.Manage().Cookies.AllCookies[i].Name, driver.Manage().Cookies.AllCookies[i].Value));
                        }
                        cookieContainer.Add(new Uri("https://blog.editor.naver.com/"), initcookie);
                        cookieContainer.Add(new Uri("https://section.cafe.naver.com/"), initcookie);
                        cookieContainer.Add(new Uri("https://m.cafe.naver.com/"), initcookie);
                        cookieContainer.Add(new Uri("https://cafe.naver.com/"), initcookie);
                        cookieContainer.Add(new Uri("https://up.cafe.naver.com/"), initcookie);
                        cookieContainer.Add(new Uri("https://apis.naver.com/"), initcookie);
                        cookieContainer.Add(new Uri("https://m.blog.naver.com/"), initcookie);
                        cookieContainer.Add(new Uri("https://static.nid.naver.com/"), initcookie);
                        cookieContainer.Add(new Uri("https://apis.naver.com/"), initcookie);
                        cookieContainer.Add(new Uri("https://nid.naver.com/"), initcookie);
                        KeyStrock.SendMessage(current_chrome, 0x0010, IntPtr.Zero, IntPtr.Zero);
                        return cookieContainer;
                    }
                    catch
                    {
                        KeyStrock.SendMessage(current_chrome, 0x0010, IntPtr.Zero, IntPtr.Zero);
                        return null;
                    }
                }
            }
            catch (ThreadAbortException)
            {
                KeyStrock.SendMessage(current_chrome, 0x0010, IntPtr.Zero, IntPtr.Zero);
                return null;
            }
            catch (InvalidOperationException e)
            {
                if (e.ToString().Contains("version"))
                {
                    KeyStrock.SendMessage(current_chrome, 0x0010, IntPtr.Zero, IntPtr.Zero);
                    MessageBox.Show("※크롬드라이버 버전이 맞지 않아 프로그램내 드라이버 업데이트를 진행하겠습니다." +
                        "\r\n.▶2-3회 업데이트를 해도 실패할 경우 서버에서 최신드라이버를 업데이트 중이니 1~2시간 이후 재시도해주세요." +
                        "\r\n\r\n▶V3나 알약과 같은 백신이 켜져있을경우 종료해주세요.");
                    Log("※크롬드라이버 버전이 맞지 않아 프로그램내 드라이버 업데이트를 진행하겠습니다." +
                        "\r\n.▶2-3회 업데이트를 해도 실패할 경우 서버에서 최신드라이버를 업데이트 중이니 1~2시간 이후 재시도해주세요." +
                        "\r\n\r\n▶V3나 알약과 같은 백신이 켜져있을경우 종료해주세요.");
                    KeyStrock.SendMessage(current_chrome, 0x0010, IntPtr.Zero, IntPtr.Zero);
                    ChromeDriverUpdateModule();
                }
                return null;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                KeyStrock.SendMessage(current_chrome, 0x0010, IntPtr.Zero, IntPtr.Zero);
                return null;
            }
        }
        private void KillChromeDriver()
        {
            try
            {
                Process[] p = Process.GetProcessesByName("chromedriver");
                if (p.GetLength(0) > 1) foreach (Process cp in p) if (cp.Id.ToString() != Process.GetCurrentProcess().Id.ToString()) cp.Kill();
                Process[] d = Process.GetProcessesByName("conhost");
                if (d.GetLength(0) > 1) foreach (Process cp in d) if (cp.Id.ToString() != Process.GetCurrentProcess().Id.ToString()) cp.Kill();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        private void ChromeDriverUpdateModule()
        {
            try
            {
                KillChromeDriver();

                Process proc = new Process();
                proc.StartInfo.FileName = "ChromeDriverUpdateModule.exe"; // 파일수정
                proc.StartInfo.Arguments = System.Windows.Forms.Application.StartupPath;
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                proc.Start();
                //System.Windows.Forms.Application.ExitThread();
                //Environment.Exit(0);
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(this, e.ToString());
            }
        }
        private void AutoTethering()
        {
            if (cb_Tethering.Checked)
            {
                string DeviceID = string.Empty;
                if (!ADBController.Adb_IsDevice())
                {
                    MessageBoxEx.Show(this, "작업을 위해 연결된 휴대기기가 없습니다. 우선 기기를 연결해주세요.");
                    Log("작업을 위해 연결된 휴대기기가 없습니다. 우선 기기를 연결해주세요.");
                    return;
                }
                int Device_cnt = 0;
                for (int devicei = 0; devicei < ADBController.Adb_Shell("adb devices").Split('\n').Length; devicei++)
                {
                    var item = ADBController.Adb_Shell("adb devices").Split('\n')[devicei].Replace(">", "").Trim();
                    //AddLog(item.Trim());
                    if (!item.Contains('\t')) continue;
                    if (item.Split('\t')[1].Trim().Equals("device"))
                        DeviceID = item.Trim().Split('\t')[0].Trim();
                    Device_cnt++;
                }
                if (Device_cnt > 1)
                {
                    MessageBoxEx.Show(this, "휴대기기는 한개만 연결해주세요.");
                    Log("휴대기기는 한개만 연결해주세요.");
                    return;
                }
                if (DeviceID.Equals(string.Empty))
                {
                    MessageBoxEx.Show(this, "연결된 휴대기기의 USB디버깅 모드를 허용해주세요.");
                    Log("연결된 휴대기기의 USB디버깅 모드를 허용해주세요.");
                    return;
                }
                ADBController.Adb_Shell("adb -s " + DeviceID + " shell svc data disable");
                ADBController.Adb_Shell("adb -s " + DeviceID + " shell svc data enable");
                //adb shell service call connectivity 40 i32 1 //<<최신 삼성 소프트 웨어 40 이 변경됨
                Log("아이피 변경으로 2분간 잠시 대기하겠습니다");
                Thread.Sleep(120000);
            }
        }
        protected string LoginCheck(CookieContainer Cookie)
        {
            try
            {
                string RET = WebController.GET("https://nid.naver.com/user2/help/myInfo?lang=ko_KR", "https://nid.naver.com/user2/help/myInfo.nhn?lang=ko_KR", Cookie, Encoding.UTF8);
                if (RET.Trim().Equals(string.Empty))
                {
                    return string.Empty;
                }
                HtmlAgilityPack.HtmlDocument DOC = new HtmlAgilityPack.HtmlDocument();
                DOC.LoadHtml(RET);
                if (DOC.DocumentNode.SelectSingleNode("//title") == null) return string.Empty;
                if (DOC.DocumentNode.SelectSingleNode("//title").InnerText.Trim().Equals("네이버 내정보")) return "OK";
                //string ID = RET.strSplit("{ \"loginId\":\"")[1].Split('\"')[0].Trim();
                //if (!ID.Equals(string.Empty)) return ID;
                else return string.Empty;
            }
            catch (ThreadAbortException) { return string.Empty; }
            catch (Exception ex)
            {
                Log("서버로부터 [ 네이버로그인상태 ] 체크 ::" + ex.ToString());
                return string.Empty;
            }
        }
       
        private void Write_Reply(int value)
        {
            bool IsUsingMonitorID = this.chk_MonitorID.Checked;
            Main_thr = new Thread(new ThreadStart(delegate
            {
                try
                {
                    while (true)
                    {
                        List<string> ExceptIDS = new List<string>();
                        foreach (ListViewItem lvi in lv_ExceptID.Items) ExceptIDS.Add(lvi.SubItems[2].Text);

                        List<string> MonitorIDS = new List<string>();
                        if (IsUsingMonitorID)
                        {
                            string MonitorID = this.tb_MonitorID.Text.Replace("모니터링할 아이디를 입력해주세요 , (쉼표) 로 구분합니다.", "");
                            if (MonitorID.Contains(','))
                            {
                                foreach (var word in MonitorID.Split(','))
                                {
                                    if (word.Trim().Equals(string.Empty)) continue;
                                    MonitorIDS.Add(word);
                                }
                            }
                            else MonitorIDS.Add(MonitorID);
                        }



                        if (value == 0)
                        {
                            for (int i = 0; i < lv_work_WorkList.CheckedItems.Count; i++)
                            {
                                Set_Status((i + 1) + " 번째 댓글 작업중");
                                lv_work_WorkList.CheckedItems[i].SubItems[5].Text = "작업중";
                                CookieContainer cookie = (lv_work_WorkList.CheckedItems[i].Tag as List<object>)[0] as CookieContainer;
                                string stiker = string.Empty; string image = string.Empty;
                                Dictionary<string, string> imagePath = new Dictionary<string, string>();
                                string clubid = ((lv_work_WorkList.CheckedItems[i].Tag as List<object>)[1] as List<object>)[1].ToString().Trim();
                                string cafeurl = ((lv_work_WorkList.CheckedItems[i].Tag as List<object>)[4]).ToString().Trim();
                                string menuid = (lv_work_WorkList.CheckedItems[i].Tag as List<object>)[2] as string;
                                string ID = lv_work_WorkList.CheckedItems[i].SubItems[4].Text.Trim();
                                int ID_INDEX = -1;
                                int cnt = 0;
                                for (int index = 0; index < lv_work_IdList.Items.Count; index++)
                                {
                                    if (lv_work_IdList.Items[index].SubItems[1].Text.Trim().Equals(lv_work_WorkList.CheckedItems[i].SubItems[4].Text.Trim()))
                                    {
                                        //MessageBox.Show("CHK");
                                        ID_INDEX = cnt;
                                        break;
                                    }
                                    if (lv_work_IdList.Items[i].SubItems[3].Text.Equals("O"))
                                    {
                                        //MessageBox.Show(cnt.ToString());
                                        cnt++;
                                    }
                                }
                                string op = (lv_work_WorkList.CheckedItems[i].Tag as List<object>)[5] as string;
                                string sel_page_First = string.Empty;
                                string sel_page_Last = string.Empty;

                                if (op.Equals("4") || op.Equals("5"))
                                {
                                    if ((lv_work_WorkList.CheckedItems[i].Tag as List<object>)[9] != null) sel_page_First = ((lv_work_WorkList.CheckedItems[i].Tag as List<object>)[9] as string).Split('|')[0].Trim();
                                    if ((lv_work_WorkList.CheckedItems[i].Tag as List<object>)[9] != null) sel_page_Last = ((lv_work_WorkList.CheckedItems[i].Tag as List<object>)[9] as string).Split('|')[1].Trim();
                                }
                                string articleid = string.Empty;
                                string userid = (lv_work_WorkList.CheckedItems[i].Tag as List<object>)[8] as string;
                                string delay = (lv_work_WorkList.CheckedItems[i].Tag as List<object>)[7] as string;
                                int Delayy = int.Parse(delay) * 1000;
                                Delayy = new Random().Next((Delayy - (Delayy / 3)), (Delayy + (Delayy / 3)));
                                List<string> keywords = (lv_work_WorkList.CheckedItems[i].Tag as List<object>)[6] as List<string>;
                                //MessageBox.Show(keywords.Count.ToString());
                                Dictionary<string, string> map = new Dictionary<string, string>();
                                string result = string.Empty;
                                //&userDisplay=10 몇개씩볼것인지
                                Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
                                    "] 게시판 [" + lv_work_WorkList.CheckedItems[i].SubItems[4].Text.Trim() + "] 아이디로 작업을 시작합니다.");

                                map.Add("clubid", clubid);
                                map.Add("articleid", articleid);
                                map.Add("page", "");
                                map.Add("menuid", "");
                                map.Add("showCafeHome", "");
                                map.Add("stickerId", "");
                                map.Add("imagePath", "");
                                map.Add("imageFileName", "");
                                map.Add("imageWidth", "");
                                map.Add("imageHeight", "");
                                map.Add("content", "");
                                Thread.Sleep(5000);
                                if (LoginCheck(cookie).Equals(string.Empty))
                                {
                                    Log("[ 네이버 로그인상태 ] 응답을 가져올 수 없습니다. - 이용제재 또는 로그인세션만료로 다음아이디 작업을 진행합니다.");
                                    continue;
                                }

                                if (op.Equals("1"))
                                {
                                    int set_cnt = 1;
                                    while (true)
                                    {
                                        //
                                        bool IsUsing_UnLogin = false;
                                        //result = WebController.GET("https://m.cafe.naver.com/ArticleList.nhn?search.clubid=" + clubid + "&search.menuid=" + menuid + "&search.boardtype=L&search.page=" + set_cnt, cafeurl, cookie, Encoding.UTF8);
                                        //https://m.cafe.naver.com/ArticleHeadView.nhn?clubid=20619062&menuid=6
                                        var RET_HeadView = WebController.GET("https://m.cafe.naver.com/ArticleHeadView.nhn?clubid=" + clubid + "&menuid=" + menuid, null, cookie, Encoding.UTF8);
                                        JObject JOBJ_HeadView = JObject.Parse(RET_HeadView);
                                        string URL = "https://apis.naver.com/cafe-web/cafe2/ArticleList.json?search.clubid=" + clubid +
                                            "&search.queryType=lastArticle&search.menuid=" + menuid + "&search.page=" + set_cnt +
                                            "&search.replylistorder=&search.firstArticleInReply=false";
                                        if (JOBJ_HeadView["result"]["boardtype"].ToString().Equals("M") || JOBJ_HeadView["result"]["boardtype"].ToString().Equals(" "))
                                        {
                                            URL = "https://m.cafe.naver.com/MemoListAjax.nhn?search.clubid=" + clubid + "&search.menuid=" + menuid + "&search.page=" + set_cnt + "&search.perPage=20";
                                        }
                                        if (URL.Contains("https://apis.naver.com/cafe-web/cafe2/") && menuid == "0")
                                        {
                                            URL = "https://apis.naver.com/cafe-web/cafe2/ArticleList.json?search.clubid=" + clubid +
                                            "&search.queryType=lastArticle&search.menuid=&search.page=" + set_cnt +
                                            "&search.replylistorder=&search.firstArticleInReply=false";
                                        }
                                        result = WebController.GET(URL, cafeurl, cookie, Encoding.UTF8);
                                        JObject JOBJ_ArticleList = new JObject();
                                        if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?"))
                                        {
                                            JOBJ_ArticleList.Add("message", new JObject());
                                            (JOBJ_ArticleList["message"] as JObject).Add(new JProperty("status", "200"));
                                            (JOBJ_ArticleList["message"] as JObject).Add(new JProperty("result", new JObject()));
                                            ((JOBJ_ArticleList["message"] as JObject)["result"] as JObject).Add(new JProperty("articleList", null));

                                            var JARY_articleList = new JArray();
                                            HtmlAgilityPack.HtmlDocument DOC_ArticleList = new HtmlAgilityPack.HtmlDocument();
                                            DOC_ArticleList.LoadHtml(result);
                                            if (DOC_ArticleList.DocumentNode.SelectSingleNode("//li[@class='u_cbox_comment']") != null)  //<li class="u_cbox_comment">
                                            {
                                                foreach (var NODE in DOC_ArticleList.DocumentNode.SelectNodes("//li[@class='u_cbox_comment']"))
                                                {
                                                    var JOBJ_DATA = new JObject();
                                                    HtmlAgilityPack.HtmlDocument DOC_Inner = new HtmlAgilityPack.HtmlDocument();
                                                    DOC_Inner.LoadHtml(NODE.InnerHtml);
                                                    var articlenum = DOC_Inner.DocumentNode.SelectSingleNode("//a[contains(@class,'u_cbox_btn_reply _click')]").Attributes["class"].Value;
                                                    articlenum = articlenum.Split('|')[4].Trim().Split(')')[0].Trim();
                                                    JOBJ_DATA.Add("subject", DOC_Inner.DocumentNode.SelectSingleNode("//span[@class='u_cbox_contents']").InnerText.Trim());
                                                    JOBJ_DATA.Add("articleId", articlenum);
                                                    JOBJ_DATA.Add("writerId", DOC_Inner.DocumentNode.SelectSingleNode("//a[contains(@class,'u_cbox_name _click(')]").Attributes["class"].Value.Split('|')[3].Trim());
                                                    JARY_articleList.Add(JOBJ_DATA);
                                                }
                                            }
                                            (JOBJ_ArticleList["message"]["result"] as JObject)["articleList"] = JARY_articleList;
                                        }
                                        else JOBJ_ArticleList = JObject.Parse(result);
                                        if (!JOBJ_ArticleList["message"]["status"].ToString().Equals("200"))
                                        {
                                            Log("카페 운영진이 삭제했거나 없는 게시판입니다.");
                                            break;
                                        }

                                        foreach (var JTKN_articleList in JOBJ_ArticleList["message"]["result"]["articleList"] as JArray)
                                        {
                                            map["stickerId"] = string.Empty;
                                            map["imagePath"] = string.Empty;
                                            map["imageFileName"] = string.Empty;
                                            map["imageWidth"] = string.Empty;
                                            map["imageHeight"] = string.Empty;
                                            map["content"] = string.Empty;
                                            if (ExceptIDS.Contains(JTKN_articleList["writerId"].ToString()))
                                            {
                                                Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
             "] 게시판 " + set_cnt + " 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글 [ " + JTKN_articleList["writerId"].ToString() + " ]중복아이디 발견으로 제외");
                                                continue;
                                            }
                                            //Log(JTKN_articleList["writerId"].ToString());
                                            if (IsUsingMonitorID && !MonitorIDS.Contains(JTKN_articleList["writerId"].ToString()))
                                            {
                                                continue;
                                            }
                                            if (keywords.Count > 0)
                                            {
                                                if (JTKN_articleList["subject"].ToString().ContainsAny(keywords, StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
                 "] 게시판 " + set_cnt + " 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글에 작성중");
                                                    articleid = JTKN_articleList["articleId"].ToString();
                                                    map["articleid"] = articleid;
                                                    Random rd = new Random();
                                                    int rand_cnt = rd.Next(0, lv_work_ContentList.Items.Count -1 );
                                                    
                                                    if (chk_IdReplyMatch.Checked) rand_cnt = ID_INDEX;
                                                    string content = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[0];
                                                    try { stiker = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[1]; } catch { }
                                                    try { image = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[2]; } catch { }
                                                    if (!stiker.Equals(string.Empty)) map["stickerId"] = stiker;
                                                    else map["stickerId"] = string.Empty;
                                                    map["content"] = WebUtility.UrlEncode(content);
                                                    if (!image.Trim().Equals(string.Empty))
                                                    {
                                                        imagePath = Send_Image(null, cookie, clubid, articleid, image.Trim(), userid);
                                                        if (imagePath != null)
                                                        {
                                                            map["imagePath"] = WebUtility.UrlEncode(imagePath["path"]);
                                                            map["imageFileName"] = WebUtility.UrlEncode(imagePath["fileName"]);
                                                            map["imageWidth"] = WebUtility.UrlEncode(imagePath["width"]);
                                                            map["imageHeight"] = WebUtility.UrlEncode(imagePath["height"]);
                                                        }
                                                        else Log("해당 댓글 이미지 업로드중 문제가 발생하여 이미지를 생략하였습니다.");
                                                    }
                                                    else
                                                    {
                                                        map["imagePath"] = map["imageFileName"] = map["imageWidth"] = map["imageHeight"] = string.Empty;
                                                    }
                                                    if (chk_Overlap.Checked)
                                                    {
                                                        if (REPLY_OVERLAP(clubid, menuid, articleid, ID, string.Empty, cookie)) continue;
                                                    }
                                                    string CreateFlag = "실패";
                                                    if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?")) CreateFlag = reply_write(map, content, articleid, cookie, clubid, 1);
                                                    else CreateFlag = reply_write(map, content, articleid, cookie, clubid);

                                                    //https://cafe.naver.com/coreppp/718
                                                    if (CreateFlag.Equals("성공")) Log("[ " + cafeurl + "/" + articleid + " ] - 댓글작성완료");
                                                    else if (CreateFlag.Equals("제재")) { IsUsing_UnLogin = true; break; }
                                                    else Log("작성실패");
                                                    Thread.Sleep(Delayy);
                                                }
                                            }
                                            else
                                            {
                                                Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
        "] 게시판 " + set_cnt + " 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글에 작성중");
                                                articleid = JTKN_articleList["articleId"].ToString();
                                                map["articleid"] = articleid;
                                                Random rd = new Random();
                                                int rand_cnt = rd.Next(0, lv_work_ContentList.Items.Count -1);
                                                if (chk_IdReplyMatch.Checked) rand_cnt = ID_INDEX;
                                                //Log(lv_work_ContentList.Items[rand_cnt].ToString());
                                                //MessageBox.Show(ID_INDEX.ToString());
                                                string content = (lv_work_ContentList.Items[rand_cnt].Tag as List<string>)[0];
                                                try { stiker = (lv_work_ContentList.Items[rand_cnt].Tag as List<string>)[1]; } catch { }
                                                try { image = (lv_work_ContentList.Items[rand_cnt].Tag as List<string>)[2]; } catch { }
                                                if (!stiker.Equals(string.Empty)) map["stickerId"] = stiker;
                                                else map["stickerId"] = string.Empty;
                                                map["content"] = WebUtility.UrlEncode(content);
                                                if (!image.Trim().Equals(string.Empty))
                                                {
                                                    imagePath = Send_Image(null, cookie, clubid, articleid, image.Trim(), userid);
                                                    if (imagePath != null)
                                                    {
                                                        map["imagePath"] = WebUtility.UrlEncode(imagePath["path"]);
                                                        map["imageFileName"] = WebUtility.UrlEncode(imagePath["fileName"]);
                                                        map["imageWidth"] = WebUtility.UrlEncode(imagePath["width"]);
                                                        map["imageHeight"] = WebUtility.UrlEncode(imagePath["height"]);
                                                    }
                                                    else Log("해당 댓글 이미지 업로드중 문제가 발생하여 이미지를 생략하였습니다.");
                                                }
                                                else
                                                {
                                                    map["imagePath"] = map["imageFileName"] = map["imageWidth"] = map["imageHeight"] = string.Empty;
                                                }
                                                if (chk_Overlap.Checked)
                                                {
                                                    if (REPLY_OVERLAP(clubid, menuid, articleid, ID, string.Empty, cookie)) continue;
                                                }
                                                string CreateFlag = "실패";
                                                if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?")) CreateFlag = reply_write(map, content, articleid, cookie, clubid, 1);
                                                else CreateFlag = reply_write(map, content, articleid, cookie, clubid);

                                                if (CreateFlag.Equals("성공")) Log("[ " + cafeurl + "/" + articleid + " ] - 댓글작성완료");
                                                else if (CreateFlag.Equals("제재")) { IsUsing_UnLogin = true; break; }
                                                else Log("작성실패");
                                                Thread.Sleep(Delayy);
                                            }
                                        }
                                        set_cnt++;
                                        if (IsUsing_UnLogin) break;
                                    }
                                }
                                else if (op.Equals("2"))
                                {
                                    var RET_HeadView = WebController.GET("https://m.cafe.naver.com/ArticleHeadView.nhn?clubid=" + clubid + "&menuid=" + menuid, null, cookie, Encoding.UTF8);
                                    JObject JOBJ_HeadView = JObject.Parse(RET_HeadView);
                                    string URL = "https://apis.naver.com/cafe-web/cafe2/ArticleList.json?search.clubid=" + clubid +
                                    "&search.queryType=lastArticle&search.menuid=" + menuid + "&search.page=1&" +
                                    "search.replylistorder=&search.firstArticleInReply=false";
                                    if (JOBJ_HeadView["result"]["boardtype"].ToString().Equals("M") || JOBJ_HeadView["result"]["boardtype"].ToString().Equals(" "))
                                    {
                                        URL = "https://m.cafe.naver.com/MemoListAjax.nhn?search.clubid=" + clubid + "&search.menuid=" + menuid + "&search.page=1&search.perPage=20";
                                    }
                                    if (URL.Contains("https://apis.naver.com/cafe-web/cafe2/") && menuid == "0")
                                    {
                                        URL = "https://apis.naver.com/cafe-web/cafe2/ArticleList.json?search.clubid=" + clubid +
                                      "&search.queryType=lastArticle&search.menuid=&search.page=1&" +
                                      "search.replylistorder=&search.firstArticleInReply=false";
                                    }
                                    result = WebController.GET(URL, cafeurl, cookie, Encoding.UTF8);
                                    JObject JOBJ_ArticleList = new JObject();
                                    if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?"))
                                    {
                                        JOBJ_ArticleList.Add("message", new JObject());
                                        (JOBJ_ArticleList["message"] as JObject).Add(new JProperty("status", "200"));
                                        (JOBJ_ArticleList["message"] as JObject).Add(new JProperty("result", new JObject()));
                                        ((JOBJ_ArticleList["message"] as JObject)["result"] as JObject).Add(new JProperty("articleList", null));

                                        var JARY_articleList = new JArray();
                                        HtmlAgilityPack.HtmlDocument DOC_ArticleList = new HtmlAgilityPack.HtmlDocument();
                                        DOC_ArticleList.LoadHtml(result);
                                        if (DOC_ArticleList.DocumentNode.SelectSingleNode("//li[@class='u_cbox_comment']") != null)  //<li class="u_cbox_comment">
                                        {
                                            foreach (var NODE in DOC_ArticleList.DocumentNode.SelectNodes("//li[@class='u_cbox_comment']"))
                                            {
                                                var JOBJ_DATA = new JObject();
                                                HtmlAgilityPack.HtmlDocument DOC_Inner = new HtmlAgilityPack.HtmlDocument();
                                                DOC_Inner.LoadHtml(NODE.InnerHtml);
                                                var articlenum = DOC_Inner.DocumentNode.SelectSingleNode("//a[contains(@class,'u_cbox_btn_reply _click')]").Attributes["class"].Value;
                                                articlenum = articlenum.Split('|')[4].Trim().Split(')')[0].Trim();
                                                JOBJ_DATA.Add("subject", DOC_Inner.DocumentNode.SelectSingleNode("//span[@class='u_cbox_contents']").InnerText.Trim());
                                                JOBJ_DATA.Add("articleId", articlenum);
                                                JOBJ_DATA.Add("writerId", DOC_Inner.DocumentNode.SelectSingleNode("//a[contains(@class,'u_cbox_name _click(')]").Attributes["class"].Value.Split('|')[3].Trim());
                                                JARY_articleList.Add(JOBJ_DATA);
                                            }
                                        }
                                        (JOBJ_ArticleList["message"]["result"] as JObject)["articleList"] = JARY_articleList;
                                    }
                                    else JOBJ_ArticleList = JObject.Parse(result);
                                    if (!JOBJ_ArticleList["message"]["status"].ToString().Equals("200"))
                                    {
                                        Log("카페 운영진이 삭제했거나 없는 게시판입니다.");
                                        break;
                                    }

                                    foreach (var JTKN_articleList in JOBJ_ArticleList["message"]["result"]["articleList"] as JArray)
                                    {
                                        map["stickerId"] = string.Empty;
                                        map["imagePath"] = string.Empty;
                                        map["imageFileName"] = string.Empty;
                                        map["imageWidth"] = string.Empty;
                                        map["imageHeight"] = string.Empty;
                                        map["content"] = string.Empty;
                                        if (ExceptIDS.Contains(JTKN_articleList["writerId"].ToString()))
                                        {
                                            Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
       "] 게시판 1 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글 [ " + JTKN_articleList["writerId"].ToString() + " ]중복아이디 발견으로 제외");
                                            continue;
                                        }
                                        if (IsUsingMonitorID && !MonitorIDS.Contains(JTKN_articleList["writerId"].ToString()))
                                        {
                                            continue;
                                        }
                                        if (keywords.Count > 0)
                                        {
                                            if (JTKN_articleList["subject"].ToString().ContainsAny(keywords, StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
         "] 게시판 1 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글에 작성중");
                                                articleid = JTKN_articleList["articleId"].ToString();
                                                map["articleid"] = articleid;
                                                Random rd = new Random();
                                                int rand_cnt = rd.Next(0, lv_work_ContentList.Items.Count);
                                                if (chk_IdReplyMatch.Checked) rand_cnt = ID_INDEX;
                                                string content = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[0];
                                                try { stiker = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[1]; } catch { }
                                                try { image = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[2]; } catch { }
                                                if (!stiker.Equals(string.Empty)) map["stickerId"] = stiker;
                                                else map["stickerId"] = string.Empty;
                                                map["content"] = WebUtility.UrlEncode(content);
                                                if (!image.Trim().Equals(string.Empty))
                                                {
                                                    imagePath = Send_Image(null, cookie, clubid, articleid, image.Trim(), userid);
                                                    if (imagePath != null)
                                                    {
                                                        map["imagePath"] = WebUtility.UrlEncode(imagePath["path"]);
                                                        map["imageFileName"] = WebUtility.UrlEncode(imagePath["fileName"]);
                                                        map["imageWidth"] = WebUtility.UrlEncode(imagePath["width"]);
                                                        map["imageHeight"] = WebUtility.UrlEncode(imagePath["height"]);
                                                    }
                                                    else Log("해당 댓글 이미지 업로드중 문제가 발생하여 이미지를 생략하였습니다.");
                                                }
                                                else
                                                {
                                                    map["imagePath"] = map["imageFileName"] = map["imageWidth"] = map["imageHeight"] = string.Empty;
                                                }
                                                if (chk_Overlap.Checked)
                                                {
                                                    if (REPLY_OVERLAP(clubid, menuid, articleid, ID, string.Empty, cookie)) continue;
                                                }
                                                string CreateFlag = "실패";
                                                if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?")) CreateFlag = reply_write(map, content, articleid, cookie, clubid, 1);
                                                else CreateFlag = reply_write(map, content, articleid, cookie, clubid);

                                                if (CreateFlag.Equals("성공")) Log("[ " + cafeurl + "/" + articleid + " ] - 댓글작성완료");
                                                else if (CreateFlag.Equals("제재")) { break; }
                                                else Log("작성실패");
                                                Thread.Sleep(Delayy);
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
        "] 게시판 1 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글에 작성중");
                                            articleid = JTKN_articleList["articleId"].ToString();
                                            map["articleid"] = articleid;
                                            Random rd = new Random();
                                            int rand_cnt = rd.Next(0, lv_work_ContentList.Items.Count -1);
                                            if (chk_IdReplyMatch.Checked) rand_cnt = ID_INDEX;
                                            string content = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[0];
                                            try { stiker = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[1]; } catch { }
                                            try { image = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[2]; } catch { }
                                            if (!stiker.Equals(string.Empty)) map["stickerId"] = stiker;
                                            else map["stickerId"] = string.Empty;
                                            map["content"] = WebUtility.UrlEncode(content);
                                            if (!image.Trim().Equals(string.Empty))
                                            {
                                                imagePath = Send_Image(null, cookie, clubid, articleid, image.Trim(), userid);
                                                if (imagePath != null)
                                                {
                                                    map["imagePath"] = WebUtility.UrlEncode(imagePath["path"]);
                                                    map["imageFileName"] = WebUtility.UrlEncode(imagePath["fileName"]);
                                                    map["imageWidth"] = WebUtility.UrlEncode(imagePath["width"]);
                                                    map["imageHeight"] = WebUtility.UrlEncode(imagePath["height"]);
                                                }
                                                else Log("해당 댓글 이미지 업로드중 문제가 발생하여 이미지를 생략하였습니다.");
                                            }
                                            else
                                            {
                                                map["imagePath"] = map["imageFileName"] = map["imageWidth"] = map["imageHeight"] = string.Empty;
                                            }
                                            if (chk_Overlap.Checked)
                                            {
                                                if (REPLY_OVERLAP(clubid, menuid, articleid, ID, string.Empty, cookie)) continue;
                                            }
                                            string CreateFlag = "실패";
                                            if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?")) CreateFlag = reply_write(map, content, articleid, cookie, clubid, 1);
                                            else CreateFlag = reply_write(map, content, articleid, cookie, clubid);

                                            if (CreateFlag.Equals("성공")) Log("[ " + cafeurl + "/" + articleid + " ] - 댓글작성완료");
                                            else if (CreateFlag.Equals("제재")) { break; }
                                            else Log("작성실패");
                                            Thread.Sleep(Delayy);
                                            break;
                                        }
                                    }
                                }
                                else if (op.Equals("3"))
                                {
                                    var RET_HeadView = WebController.GET("https://m.cafe.naver.com/ArticleHeadView.nhn?clubid=" + clubid + "&menuid=" + menuid, null, cookie, Encoding.UTF8);
                                    JObject JOBJ_HeadView = JObject.Parse(RET_HeadView);
                                    string URL = "https://apis.naver.com/cafe-web/cafe2/ArticleList.json?search.clubid=" + clubid +
                                         "&search.queryType=lastArticle&search.menuid=" + menuid + "&search.page=1&" +
                                         "search.replylistorder=&search.firstArticleInReply=false";
                                    if (JOBJ_HeadView["result"]["boardtype"].ToString().Equals("M") || JOBJ_HeadView["result"]["boardtype"].ToString().Equals(" "))
                                    {
                                        URL = "https://m.cafe.naver.com/MemoListAjax.nhn?search.clubid=" + clubid + "&search.menuid=" + menuid + "&search.page=1&search.perPage=20";
                                    }
                                    if (URL.Contains("https://apis.naver.com/cafe-web/cafe2/") && menuid == "0")
                                    {
                                        URL = "https://apis.naver.com/cafe-web/cafe2/ArticleList.json?search.clubid=" + clubid +
                                                     "&search.queryType=lastArticle&search.menuid=&search.page=1&" +
                                                     "search.replylistorder=&search.firstArticleInReply=false";
                                    }
                                    result = WebController.GET(URL, cafeurl, cookie, Encoding.UTF8);
                                    JObject JOBJ_ArticleList = new JObject();
                                    if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?"))
                                    {
                                        JOBJ_ArticleList.Add("message", new JObject());
                                        (JOBJ_ArticleList["message"] as JObject).Add(new JProperty("status", "200"));
                                        (JOBJ_ArticleList["message"] as JObject).Add(new JProperty("result", new JObject()));
                                        ((JOBJ_ArticleList["message"] as JObject)["result"] as JObject).Add(new JProperty("articleList", null));

                                        var JARY_articleList = new JArray();
                                        HtmlAgilityPack.HtmlDocument DOC_ArticleList = new HtmlAgilityPack.HtmlDocument();
                                        DOC_ArticleList.LoadHtml(result);
                                        if (DOC_ArticleList.DocumentNode.SelectSingleNode("//li[@class='u_cbox_comment']") != null)  //<li class="u_cbox_comment">
                                        {
                                            foreach (var NODE in DOC_ArticleList.DocumentNode.SelectNodes("//li[@class='u_cbox_comment']"))
                                            {
                                                var JOBJ_DATA = new JObject();
                                                HtmlAgilityPack.HtmlDocument DOC_Inner = new HtmlAgilityPack.HtmlDocument();
                                                DOC_Inner.LoadHtml(NODE.InnerHtml);
                                                var articlenum = DOC_Inner.DocumentNode.SelectSingleNode("//a[contains(@class,'u_cbox_btn_reply _click')]").Attributes["class"].Value;
                                                articlenum = articlenum.Split('|')[4].Trim().Split(')')[0].Trim();
                                                JOBJ_DATA.Add("subject", DOC_Inner.DocumentNode.SelectSingleNode("//span[@class='u_cbox_contents']").InnerText.Trim());
                                                JOBJ_DATA.Add("articleId", articlenum);
                                                JOBJ_DATA.Add("writerId", DOC_Inner.DocumentNode.SelectSingleNode("//a[contains(@class,'u_cbox_name _click(')]").Attributes["class"].Value.Split('|')[3].Trim());
                                                JARY_articleList.Add(JOBJ_DATA);
                                            }
                                        }
                                        (JOBJ_ArticleList["message"]["result"] as JObject)["articleList"] = JARY_articleList;
                                    }
                                    else JOBJ_ArticleList = JObject.Parse(result);
                                    if (!JOBJ_ArticleList["message"]["status"].ToString().Equals("200"))
                                    {
                                        Log("카페 운영진이 삭제했거나 없는 게시판입니다.");
                                        break;
                                    }
                                    bool IsUsing_UnLogin = false;
                                    foreach (var JTKN_articleList in JOBJ_ArticleList["message"]["result"]["articleList"] as JArray)
                                    {
                                        map["stickerId"] = string.Empty;
                                        map["imagePath"] = string.Empty;
                                        map["imageFileName"] = string.Empty;
                                        map["imageWidth"] = string.Empty;
                                        map["imageHeight"] = string.Empty;
                                        map["content"] = string.Empty;
                                        if (ExceptIDS.Contains(JTKN_articleList["writerId"].ToString()))
                                        {
                                            Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
       "] 게시판 1 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글 [ " + JTKN_articleList["writerId"].ToString() + " ]중복아이디 발견으로 제외");
                                            continue;
                                        }
                                        if (IsUsingMonitorID && !MonitorIDS.Contains(JTKN_articleList["writerId"].ToString()))
                                        {
                                            continue;
                                        }
                                        if (keywords.Count > 0)
                                        {
                                            if (JTKN_articleList["subject"].ToString().ContainsAny(keywords, StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
        "] 게시판 1 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글에 작성중");
                                                articleid = JTKN_articleList["articleId"].ToString();
                                                map["articleid"] = articleid;
                                                Random rd = new Random();
                                                int rand_cnt = rd.Next(0, lv_work_ContentList.Items.Count);
                                                if (chk_IdReplyMatch.Checked) rand_cnt = ID_INDEX;
                                                string content = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[0];
                                                try { stiker = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[1]; } catch { }
                                                try { image = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[2]; } catch { }
                                                if (!stiker.Equals(string.Empty)) map["stickerId"] = stiker;
                                                else map["stickerId"] = string.Empty;
                                                map["content"] = WebUtility.UrlEncode(content);
                                                if (!image.Trim().Equals(string.Empty))
                                                {
                                                    imagePath = Send_Image(null, cookie, clubid, articleid, image.Trim(), userid);
                                                    if (imagePath != null)
                                                    {
                                                        map["imagePath"] = WebUtility.UrlEncode(imagePath["path"]);
                                                        map["imageFileName"] = WebUtility.UrlEncode(imagePath["fileName"]);
                                                        map["imageWidth"] = WebUtility.UrlEncode(imagePath["width"]);
                                                        map["imageHeight"] = WebUtility.UrlEncode(imagePath["height"]);
                                                    }
                                                    else Log("해당 댓글 이미지 업로드중 문제가 발생하여 이미지를 생략하였습니다.");
                                                }
                                                else
                                                {
                                                    map["imagePath"] = map["imageFileName"] = map["imageWidth"] = map["imageHeight"] = string.Empty;
                                                }
                                                if (chk_Overlap.Checked)
                                                {
                                                    if (REPLY_OVERLAP(clubid, menuid, articleid, ID, string.Empty, cookie)) continue;
                                                }
                                                string CreateFlag = "실패";
                                                if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?")) CreateFlag = reply_write(map, content, articleid, cookie, clubid, 1);
                                                else CreateFlag = reply_write(map, content, articleid, cookie, clubid);

                                                if (CreateFlag.Equals("성공")) Log("[ " + cafeurl + "/" + articleid + " ] - 댓글작성완료");
                                                else if (CreateFlag.Equals("제재")) { IsUsing_UnLogin = true; break; }
                                                else Log("작성실패");
                                                Thread.Sleep(Delayy);
                                            }
                                        }
                                        else
                                        {
                                            Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
        "] 게시판 1 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글에 작성중");
                                            articleid = JTKN_articleList["articleId"].ToString();
                                            map["articleid"] = articleid;
                                            Random rd = new Random();
                                            int rand_cnt = rd.Next(0, lv_work_ContentList.Items.Count);
                                            if (chk_IdReplyMatch.Checked) rand_cnt = ID_INDEX;
                                            string content = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[0];
                                            try { stiker = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[1]; } catch { }
                                            try { image = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[2]; } catch { }
                                            if (!stiker.Equals(string.Empty)) map["stickerId"] = stiker;
                                            else map["stickerId"] = string.Empty;
                                            map["content"] = WebUtility.UrlEncode(content);
                                            if (!image.Trim().Equals(string.Empty))
                                            {
                                                imagePath = Send_Image(null, cookie, clubid, articleid, image.Trim(), userid);
                                                if (imagePath != null)
                                                {
                                                    map["imagePath"] = WebUtility.UrlEncode(imagePath["path"]);
                                                    map["imageFileName"] = WebUtility.UrlEncode(imagePath["fileName"]);
                                                    map["imageWidth"] = WebUtility.UrlEncode(imagePath["width"]);
                                                    map["imageHeight"] = WebUtility.UrlEncode(imagePath["height"]);
                                                }
                                                else Log("해당 댓글 이미지 업로드중 문제가 발생하여 이미지를 생략하였습니다.");
                                            }
                                            else
                                            {
                                                map["imagePath"] = map["imageFileName"] = map["imageWidth"] = map["imageHeight"] = string.Empty;
                                            }
                                            if (chk_Overlap.Checked)
                                            {
                                                if (REPLY_OVERLAP(clubid, menuid, articleid, ID, string.Empty, cookie)) continue;
                                            }
                                            string CreateFlag = "실패";
                                            if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?")) CreateFlag = reply_write(map, content, articleid, cookie, clubid, 1);
                                            else CreateFlag = reply_write(map, content, articleid, cookie, clubid);

                                            if (CreateFlag.Equals("성공")) Log("[ " + cafeurl + "/" + articleid + " ] - 댓글작성완료");
                                            else if (CreateFlag.Equals("제재")) { IsUsing_UnLogin = true; break; }
                                            else Log("작성실패");
                                            Thread.Sleep(Delayy);
                                        }
                                    }
                                    if (IsUsing_UnLogin) break;
                                }
                                else if (op.Equals("4"))
                                {
                                    for (int set = int.Parse(sel_page_First); set <= int.Parse(sel_page_Last); set++)
                                    {
                                        map["stickerId"] = string.Empty;
                                        map["imagePath"] = string.Empty;
                                        map["imageFileName"] = string.Empty;
                                        map["imageWidth"] = string.Empty;
                                        map["imageHeight"] = string.Empty;
                                        map["content"] = string.Empty;

                                        var RET_HeadView = WebController.GET("https://m.cafe.naver.com/ArticleHeadView.nhn?clubid=" + clubid + "&menuid=" + menuid, null, cookie, Encoding.UTF8);
                                        JObject JOBJ_HeadView = JObject.Parse(RET_HeadView);
                                        string URL = "https://apis.naver.com/cafe-web/cafe2/ArticleList.json?search.clubid=" + clubid +
                                            "&search.queryType=lastArticle&search.menuid=" + menuid + "&search.page=" + set +
                                            "&search.replylistorder=&search.firstArticleInReply=false";
                                        if (JOBJ_HeadView["result"]["boardtype"].ToString().Equals("M") || JOBJ_HeadView["result"]["boardtype"].ToString().Equals(" "))
                                        {
                                            URL = "https://m.cafe.naver.com/MemoListAjax.nhn?search.clubid=" + clubid + "&search.menuid=" + menuid + "&search.page=" + set + "&search.perPage=20";
                                        }
                                        if (URL.Contains("https://apis.naver.com/cafe-web/cafe2/") && menuid == "0")
                                        {
                                            URL = "https://apis.naver.com/cafe-web/cafe2/ArticleList.json?search.clubid=" + clubid +
                                              "&search.queryType=lastArticle&search.menuid=&search.page=" + set +
                                              "&search.replylistorder=&search.firstArticleInReply=false";
                                        }
                                        result = WebController.GET(URL, cafeurl, cookie, Encoding.UTF8);
                                        JObject JOBJ_ArticleList = new JObject();
                                        if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?"))
                                        {
                                            JOBJ_ArticleList.Add("message", new JObject());
                                            (JOBJ_ArticleList["message"] as JObject).Add(new JProperty("status", "200"));
                                            (JOBJ_ArticleList["message"] as JObject).Add(new JProperty("result", new JObject()));
                                            ((JOBJ_ArticleList["message"] as JObject)["result"] as JObject).Add(new JProperty("articleList", null));

                                            var JARY_articleList = new JArray();
                                            HtmlAgilityPack.HtmlDocument DOC_ArticleList = new HtmlAgilityPack.HtmlDocument();
                                            DOC_ArticleList.LoadHtml(result);
                                            if (DOC_ArticleList.DocumentNode.SelectSingleNode("//li[@class='u_cbox_comment']") != null)  //<li class="u_cbox_comment">
                                            {
                                                foreach (var NODE in DOC_ArticleList.DocumentNode.SelectNodes("//li[@class='u_cbox_comment']"))
                                                {
                                                    var JOBJ_DATA = new JObject();
                                                    HtmlAgilityPack.HtmlDocument DOC_Inner = new HtmlAgilityPack.HtmlDocument();
                                                    DOC_Inner.LoadHtml(NODE.InnerHtml);
                                                    var articlenum = DOC_Inner.DocumentNode.SelectSingleNode("//a[contains(@class,'u_cbox_btn_reply _click')]").Attributes["class"].Value;
                                                    articlenum = articlenum.Split('|')[4].Trim().Split(')')[0].Trim();
                                                    JOBJ_DATA.Add("subject", DOC_Inner.DocumentNode.SelectSingleNode("//span[@class='u_cbox_contents']").InnerText.Trim());
                                                    JOBJ_DATA.Add("articleId", articlenum);
                                                    JOBJ_DATA.Add("writerId", DOC_Inner.DocumentNode.SelectSingleNode("//a[contains(@class,'u_cbox_name _click(')]").Attributes["class"].Value.Split('|')[3].Trim());
                                                    JARY_articleList.Add(JOBJ_DATA);
                                                }
                                            }
                                            (JOBJ_ArticleList["message"]["result"] as JObject)["articleList"] = JARY_articleList;
                                        }
                                        else JOBJ_ArticleList = JObject.Parse(result);
                                        if (!JOBJ_ArticleList["message"]["status"].ToString().Equals("200"))
                                        {
                                            Log("카페 운영진이 삭제했거나 없는 게시판입니다.");
                                            break;
                                        }

                                        foreach (var JTKN_articleList in JOBJ_ArticleList["message"]["result"]["articleList"] as JArray)
                                        {
                                            if (ExceptIDS.Contains(JTKN_articleList["writerId"].ToString()))
                                            {
                                                Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
               "] 게시판 " + set + " 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글 [ " + JTKN_articleList["writerId"].ToString() + " ]중복아이디 발견으로 제외");
                                                continue;
                                            }
                                            if (IsUsingMonitorID && !MonitorIDS.Contains(JTKN_articleList["writerId"].ToString()))
                                            {
                                                continue;
                                            }
                                            if (keywords.Count > 0)
                                            {
                                                if (JTKN_articleList["subject"].ToString().ContainsAny(keywords, StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
                   "] 게시판 " + set + " 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글에 작성중");
                                                    articleid = JTKN_articleList["articleId"].ToString();
                                                    map["articleid"] = articleid;
                                                    Random rd = new Random();
                                                    int rand_cnt = rd.Next(0, lv_work_ContentList.Items.Count);
                                                    if (chk_IdReplyMatch.Checked) rand_cnt = ID_INDEX;
                                                    string content = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[0];
                                                    try { stiker = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[1]; } catch { }
                                                    try { image = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[2]; } catch { }
                                                    if (!stiker.Equals(string.Empty)) map["stickerId"] = stiker;
                                                    else map["stickerId"] = string.Empty;
                                                    map["content"] = WebUtility.UrlEncode(content);
                                                    if (!image.Trim().Equals(string.Empty))
                                                    {
                                                        imagePath = Send_Image(null, cookie, clubid, articleid, image.Trim(), userid);
                                                        if (imagePath != null)
                                                        {
                                                            map["imagePath"] = WebUtility.UrlEncode(imagePath["path"]);
                                                            map["imageFileName"] = WebUtility.UrlEncode(imagePath["fileName"]);
                                                            map["imageWidth"] = WebUtility.UrlEncode(imagePath["width"]);
                                                            map["imageHeight"] = WebUtility.UrlEncode(imagePath["height"]);
                                                        }
                                                        else Log("해당 댓글 이미지 업로드중 문제가 발생하여 이미지를 생략하였습니다.");
                                                    }
                                                    else
                                                    {
                                                        map["imagePath"] = map["imageFileName"] = map["imageWidth"] = map["imageHeight"] = string.Empty;
                                                    }
                                                    if (chk_Overlap.Checked)
                                                    {
                                                        if (REPLY_OVERLAP(clubid, menuid, articleid, ID, string.Empty, cookie)) continue;
                                                    }
                                                    string CreateFlag = "실패";
                                                    if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?")) CreateFlag = reply_write(map, content, articleid, cookie, clubid, 1);
                                                    else CreateFlag = reply_write(map, content, articleid, cookie, clubid);

                                                    if (CreateFlag.Equals("성공")) Log("[ " + cafeurl + "/" + articleid + " ] - 댓글작성완료");
                                                    else if (CreateFlag.Equals("제재")) { break; }
                                                    else Log("작성실패");
                                                    Thread.Sleep(Delayy);
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
                  "] 게시판 " + set + " 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글에 작성중");
                                                articleid = JTKN_articleList["articleId"].ToString();
                                                map["articleid"] = articleid;
                                                Random rd = new Random();
                                                int rand_cnt = rd.Next(0, lv_work_ContentList.Items.Count);
                                                if (chk_IdReplyMatch.Checked) rand_cnt = ID_INDEX;
                                                string content = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[0];
                                                try { stiker = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[1]; } catch { }
                                                try { image = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[2]; } catch { }
                                                if (!stiker.Equals(string.Empty)) map["stickerId"] = stiker;
                                                else map["stickerId"] = string.Empty;
                                                map["content"] = WebUtility.UrlEncode(content);
                                                if (!image.Trim().Equals(string.Empty))
                                                {
                                                    imagePath = Send_Image(null, cookie, clubid, articleid, image.Trim(), userid);
                                                    if (imagePath != null)
                                                    {
                                                        map["imagePath"] = WebUtility.UrlEncode(imagePath["path"]);
                                                        map["imageFileName"] = WebUtility.UrlEncode(imagePath["fileName"]);
                                                        map["imageWidth"] = WebUtility.UrlEncode(imagePath["width"]);
                                                        map["imageHeight"] = WebUtility.UrlEncode(imagePath["height"]);
                                                    }
                                                    else Log("해당 댓글 이미지 업로드중 문제가 발생하여 이미지를 생략하였습니다.");
                                                }
                                                else
                                                {
                                                    map["imagePath"] = map["imageFileName"] = map["imageWidth"] = map["imageHeight"] = string.Empty;
                                                }
                                                if (chk_Overlap.Checked)
                                                {
                                                    if (REPLY_OVERLAP(clubid, menuid, articleid, ID, string.Empty, cookie)) continue;
                                                }
                                                string CreateFlag = "실패";
                                                if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?")) CreateFlag = reply_write(map, content, articleid, cookie, clubid, 1);
                                                else CreateFlag = reply_write(map, content, articleid, cookie, clubid);

                                                if (CreateFlag.Equals("성공")) Log("[ " + cafeurl + "/" + articleid + " ] - 댓글작성완료");
                                                else if (CreateFlag.Equals("제재")) { break; }
                                                else Log("작성실패");
                                                Thread.Sleep(Delayy);
                                                break;
                                            }
                                        }
                                    }
                                }
                                else if (op.Equals("5"))
                                {
                                    for (int set = int.Parse(sel_page_First); set <= int.Parse(sel_page_Last); set++)
                                    {

                                        map["stickerId"] = string.Empty;
                                        map["imagePath"] = string.Empty;
                                        map["imageFileName"] = string.Empty;
                                        map["imageWidth"] = string.Empty;
                                        map["imageHeight"] = string.Empty;
                                        map["content"] = string.Empty;
                                        var RET_HeadView = WebController.GET("https://m.cafe.naver.com/ArticleHeadView.nhn?clubid=" + clubid + "&menuid=" + menuid, null, cookie, Encoding.UTF8);
                                        JObject JOBJ_HeadView = JObject.Parse(RET_HeadView);
                                        string URL = "https://apis.naver.com/cafe-web/cafe2/ArticleList.json?search.clubid=" + clubid +
                                          "&search.queryType=lastArticle&search.menuid=" + menuid + "&search.page=" + set +
                                          "&search.replylistorder=&search.firstArticleInReply=false";
                                        if (JOBJ_HeadView["result"]["boardtype"].ToString().Equals("M") || JOBJ_HeadView["result"]["boardtype"].ToString().Equals(" "))
                                        {
                                            URL = "https://m.cafe.naver.com/MemoListAjax.nhn?search.clubid=" + clubid + "&search.menuid=" + menuid + "&search.page=" + set + "&search.perPage=20";
                                        }
                                        if (URL.Contains("https://apis.naver.com/cafe-web/cafe2/") && menuid == "0")
                                        {
                                            URL = "https://apis.naver.com/cafe-web/cafe2/ArticleList.json?search.clubid=" + clubid +
                                             "&search.queryType=lastArticle&search.menuid=&search.page=" + set +
                                             "&search.replylistorder=&search.firstArticleInReply=false";
                                        }
                                        result = WebController.GET(URL, cafeurl, cookie, Encoding.UTF8);
                                        JObject JOBJ_ArticleList = new JObject();
                                        if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?"))
                                        {
                                            JOBJ_ArticleList.Add("message", new JObject());
                                            (JOBJ_ArticleList["message"] as JObject).Add(new JProperty("status", "200"));
                                            (JOBJ_ArticleList["message"] as JObject).Add(new JProperty("result", new JObject()));
                                            ((JOBJ_ArticleList["message"] as JObject)["result"] as JObject).Add(new JProperty("articleList", null));

                                            var JARY_articleList = new JArray();
                                            HtmlAgilityPack.HtmlDocument DOC_ArticleList = new HtmlAgilityPack.HtmlDocument();
                                            DOC_ArticleList.LoadHtml(result);
                                            if (DOC_ArticleList.DocumentNode.SelectSingleNode("//li[@class='u_cbox_comment']") != null)  //<li class="u_cbox_comment">
                                            {
                                                foreach (var NODE in DOC_ArticleList.DocumentNode.SelectNodes("//li[@class='u_cbox_comment']"))
                                                {
                                                    var JOBJ_DATA = new JObject();
                                                    HtmlAgilityPack.HtmlDocument DOC_Inner = new HtmlAgilityPack.HtmlDocument();
                                                    DOC_Inner.LoadHtml(NODE.InnerHtml);
                                                    var articlenum = DOC_Inner.DocumentNode.SelectSingleNode("//a[contains(@class,'u_cbox_btn_reply _click')]").Attributes["class"].Value;
                                                    articlenum = articlenum.Split('|')[4].Trim().Split(')')[0].Trim();
                                                    JOBJ_DATA.Add("subject", DOC_Inner.DocumentNode.SelectSingleNode("//span[@class='u_cbox_contents']").InnerText.Trim());
                                                    JOBJ_DATA.Add("articleId", articlenum);
                                                    JOBJ_DATA.Add("writerId", DOC_Inner.DocumentNode.SelectSingleNode("//a[contains(@class,'u_cbox_name _click(')]").Attributes["class"].Value.Split('|')[3].Trim());
                                                    JARY_articleList.Add(JOBJ_DATA);
                                                }
                                            }
                                            (JOBJ_ArticleList["message"]["result"] as JObject)["articleList"] = JARY_articleList;
                                        }
                                        else JOBJ_ArticleList = JObject.Parse(result);
                                        if (!JOBJ_ArticleList["message"]["status"].ToString().Equals("200"))
                                        {
                                            Log("카페 운영진이 삭제했거나 없는 게시판입니다.");
                                            break;
                                        }


                                        foreach (var JTKN_articleList in JOBJ_ArticleList["message"]["result"]["articleList"] as JArray)
                                        {
                                            if (keywords.Count > 0)
                                            {
                                                if (ExceptIDS.Contains(JTKN_articleList["writerId"].ToString()))
                                                {
                                                    Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
                  "] 게시판 " + set + " 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글 [ " + JTKN_articleList["writerId"].ToString() + " ]중복아이디 발견으로 제외");
                                                    continue;
                                                }
                                                if (IsUsingMonitorID && !MonitorIDS.Contains(JTKN_articleList["writerId"].ToString()))
                                                {
                                                    continue;
                                                }
                                                if (JTKN_articleList["subject"].ToString().ContainsAny(keywords, StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
                   "] 게시판 " + set + " 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글에 작성중");
                                                    articleid = JTKN_articleList["articleId"].ToString();
                                                    map["articleid"] = articleid;
                                                    Random rd = new Random();
                                                    image = stiker = string.Empty;
                                                    int rand_cnt = rd.Next(0, lv_work_ContentList.Items.Count);
                                                    if (chk_IdReplyMatch.Checked) rand_cnt = ID_INDEX;
                                                    string content = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[0];
                                                    try { stiker = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[1]; } catch { }
                                                    try { image = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[2]; } catch { }
                                                    if (!stiker.Equals(string.Empty)) map["stickerId"] = stiker;
                                                    else map["stickerId"] = string.Empty;
                                                    map["content"] = WebUtility.UrlEncode(content);
                                                    if (!image.Trim().Equals(string.Empty))
                                                    {
                                                        imagePath = Send_Image(null, cookie, clubid, articleid, image.Trim(), userid);
                                                        if (imagePath != null)
                                                        {
                                                            map["imagePath"] = WebUtility.UrlEncode(imagePath["path"]);
                                                            map["imageFileName"] = WebUtility.UrlEncode(imagePath["fileName"]);
                                                            map["imageWidth"] = WebUtility.UrlEncode(imagePath["width"]);
                                                            map["imageHeight"] = WebUtility.UrlEncode(imagePath["height"]);
                                                        }
                                                        else Log("해당 댓글 이미지 업로드중 문제가 발생하여 이미지를 생략하였습니다.");
                                                    }
                                                    else
                                                    {
                                                        map["imagePath"] = map["imageFileName"] = map["imageWidth"] = map["imageHeight"] = string.Empty;
                                                    }
                                                    if (chk_Overlap.Checked)
                                                    {
                                                        if (REPLY_OVERLAP(clubid, menuid, articleid, ID, string.Empty, cookie)) continue;
                                                    }
                                                    string CreateFlag = "실패";
                                                    if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?")) CreateFlag = reply_write(map, content, articleid, cookie, clubid, 1);
                                                    else CreateFlag = reply_write(map, content, articleid, cookie, clubid);

                                                    if (CreateFlag.Equals("성공")) Log("[ " + cafeurl + "/" + articleid + " ] - 댓글작성완료");
                                                    else if (CreateFlag.Equals("제재")) { break; }
                                                    else Log("작성실패");
                                                    Thread.Sleep(Delayy);
                                                }
                                            }
                                            else
                                            {
                                                Log((i + 1) + "번째 [" + lv_work_WorkList.CheckedItems[i].SubItems[2].Text.Trim() + "] 카페" + " [" + lv_work_WorkList.CheckedItems[i].SubItems[3].Text.Trim() +
                "] 게시판 " + set + " 페이지[" + JTKN_articleList["subject"].ToString() + "] 게시글에 작성중");
                                                articleid = JTKN_articleList["articleId"].ToString();
                                                map["articleid"] = articleid;
                                                Random rd = new Random();
                                                int rand_cnt = rd.Next(0, lv_work_ContentList.Items.Count);
                                                if (chk_IdReplyMatch.Checked) rand_cnt = ID_INDEX;
                                                image = stiker = string.Empty;
                                                string content = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[0];
                                                try { stiker = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[1]; } catch { }
                                                try { image = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[2]; } catch { }
                                                if (!stiker.Equals(string.Empty)) map["stickerId"] = stiker;
                                                map["content"] = WebUtility.UrlEncode(content);
                                                if (!image.Trim().Equals(string.Empty))
                                                {
                                                    imagePath = Send_Image(null, cookie, clubid, articleid, image.Trim(), userid);
                                                    if (imagePath != null)
                                                    {
                                                        map["imagePath"] = WebUtility.UrlEncode(imagePath["path"]);
                                                        map["imageFileName"] = WebUtility.UrlEncode(imagePath["fileName"]);
                                                        map["imageWidth"] = WebUtility.UrlEncode(imagePath["width"]);
                                                        map["imageHeight"] = WebUtility.UrlEncode(imagePath["height"]);
                                                    }
                                                    else Log("해당 댓글 이미지 업로드중 문제가 발생하여 이미지를 생략하였습니다.");
                                                }
                                                else
                                                {
                                                    map["imagePath"] = map["imageFileName"] = map["imageWidth"] = map["imageHeight"] = string.Empty;
                                                }
                                                if (chk_Overlap.Checked)
                                                {
                                                    if (REPLY_OVERLAP(clubid, menuid, articleid, ID, string.Empty, cookie)) continue;
                                                }
                                                string CreateFlag = "실패";
                                                if (URL.Contains("m.cafe.naver.com/MemoListAjax.nhn?")) CreateFlag = reply_write(map, content, articleid, cookie, clubid, 1);
                                                else CreateFlag = reply_write(map, content, articleid, cookie, clubid);

                                                if (CreateFlag.Equals("성공")) Log("[ " + cafeurl + "/" + articleid + " ] - 댓글작성완료");
                                                else if (CreateFlag.Equals("제재")) { break; }
                                                else Log("작성실패");
                                                Thread.Sleep(Delayy);
                                            }
                                        }
                                    }
                                }
                                lv_work_WorkList.CheckedItems[i].SubItems[5].Text = "완료";
                            }
                        }
                        else if (value == 1)
                        {
                            List<ListViewItem> LIST_URL = new List<ListViewItem>();
                            for (int i = 0; i < lv_URL.CheckedItems.Count; i++) LIST_URL.Add(lv_URL.CheckedItems[i]);

                            List<ListViewItem> LIST_ID = new List<ListViewItem>();
                            for (int i = 0; i < lv_work_IdList.CheckedItems.Count; i++) LIST_ID.Add(lv_work_IdList.CheckedItems[i]);
                            if (LIST_ID.Count < 1)
                            {
                                MessageBox.Show("작업을 위해 선택된 아이디가 없습니다 작업할 아이디를 선택(체크)해주세요.");
                                return;
                            }
                            int workcnt = 0;

                            string delay = fc_DelayTime.Text.Trim();
                            int Delayy = int.Parse(delay) * 1000;
                            Delayy = new Random().Next((Delayy - (Delayy / 3)), (Delayy + (Delayy / 3)));

                            foreach (ListViewItem LVI_URL in LIST_URL)
                            {
                                try
                                {
                                    Set_Status((workcnt + 1) + " 번째 댓글 작업중");
                                    LVI_URL.SubItems[2].Text = "작업중";
                                    string URL = LVI_URL.SubItems[1].Text.Trim();
                                    if (!URL.Contains("m.cafe.naver.com/")) URL = URL.Replace("cafe.naver.com/", "m.cafe.naver.com/").Trim();
                                    //https://cafe.naver.com/43936/576078
                                    //https://cafe.naver.com/coreplanett/508
                                    //https://cafe.naver.com/ma9guide/1181442
                                    foreach (ListViewItem LVI_ID in LIST_ID)
                                    {
                                        JObject JOBJ_RET = new JObject();
                                        CookieContainer cookie = (LVI_ID.Tag as List<object>)[1] as CookieContainer;


                                        string stiker = string.Empty; string image = string.Empty;
                                        string CafeName = URL.Replace("https://", string.Empty).Replace("http://", string.Empty).Replace("m.cafe.naver.com/", string.Empty).Split('/')[0].Trim();
                                        Dictionary<string, string> imagePath = new Dictionary<string, string>();
                                        string clubid = string.Empty;
                                        string articleid = URL.Replace("https://", string.Empty).Replace("http://", string.Empty).Replace("m.cafe.naver.com/", string.Empty).Split('/')[1].Trim();
                                        int ID_INDEX = LVI_ID.Index;
                                        string userid = LVI_ID.SubItems[1].Text;
                                        string RET_CafeGetInfo = WebController.GET("https://apis.naver.com/cafe-web/cafe2/CafeGateInfo.json?cluburl=" + CafeName, URL, cookie, Encoding.UTF8);
                                        //Log(RET_CafeGetInfo);
                                        //MessageBox.Show("chk");
                                        clubid = RET_CafeGetInfo.strSplit(":{\"cafeId\":")[1].Split(',')[0].Trim();
                                        Log(userid + "작업중");
                                        // Log("https://apis.naver.com/cafe-web/cafe-articleapi/cafes/" + clubid + "/articles/" + articleid);
                                        // MessageBox.Show(clubid + " | " + articleid);

                                        string RET_URL = WebController.GET("https://apis.naver.com/cafe-web/cafe-articleapi/cafes/" + clubid + "/articles/" + articleid, URL, cookie, Encoding.UTF8);
                                        //Log(RET_URL);
                                        //MessageBox.Show("chker2");
                                        if (RET_URL.Trim().Equals(string.Empty))
                                        {
                                            Log("[ " + LVI_URL.SubItems[1].Text.Trim() + " ] URL은 잘못된 URL입니다.");
                                            continue;
                                        }
                                        JOBJ_RET = JObject.Parse(RET_URL);
                                        if (JOBJ_RET["errorCode"] != null)
                                        {
                                            Log("[ " + LVI_URL.SubItems[1].Text.Trim() + " ] URL에 대한 [ " + userid + " ]아이디가 글을 볼 수 있는 권한이 없습니다.");
                                            continue;
                                        }

                                        clubid = JOBJ_RET["cafeId"].ToString();
                                        articleid = JOBJ_RET["articleId"].ToString();
                                        Dictionary<string, string> map = new Dictionary<string, string>();
                                        map.Add("clubid", clubid);
                                        map.Add("articleid", articleid);
                                        map.Add("page", "");
                                        map.Add("menuid", "");
                                        map.Add("showCafeHome", "");
                                        map.Add("stickerId", "");
                                        map.Add("imagePath", "");
                                        map.Add("imageFileName", "");
                                        map.Add("imageWidth", "");
                                        map.Add("imageHeight", "");
                                        map.Add("content", "");
                                        // MessageBox.Show("subchk");
                                        Random rd = new Random();
                                        int rand_cnt = rd.Next(0, lv_work_ContentList.Items.Count);
                                        if (chk_IdReplyMatch.Checked) rand_cnt = ID_INDEX;
                                        string content = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[0];
                                        try { stiker = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[1]; } catch { }
                                        try { image = (lv_work_ContentList.Items[rand_cnt].Tag as List<String>)[2]; } catch { }
                                        if (!stiker.Equals(string.Empty)) map["stickerId"] = stiker;
                                        map["content"] = WebUtility.UrlEncode(content);
                                        if (!image.Trim().Equals(string.Empty))
                                        {
                                            imagePath = Send_Image(null, cookie, clubid, articleid, image.Trim(), userid);
                                            if (imagePath != null)
                                            {
                                                map["imagePath"] = WebUtility.UrlEncode(imagePath["path"]);
                                                map["imageFileName"] = WebUtility.UrlEncode(imagePath["fileName"]);
                                                map["imageWidth"] = WebUtility.UrlEncode(imagePath["width"]);
                                                map["imageHeight"] = WebUtility.UrlEncode(imagePath["height"]);
                                            }
                                            else Log("해당 댓글 이미지 업로드중 문제가 발생하여 이미지를 생략하였습니다.");
                                        }
                                        // MessageBox.Show("subchk2");
                                        //if (chk_Overlap.Checked)
                                        //{
                                        //    if (REPLY_OVERLAP(clubid, menuid, articleid, userid, string.Empty)) continue;
                                        //}
                                        if (reply_write(map, content, articleid, cookie, clubid).Equals("성공"))
                                            Log("[ https://cafe.naver.com/ArticleRead?clubid=" + clubid + "&articleid=" + articleid + " ] - 댓글작성완료");
                                        //Log("[ https://cafe.naver.com/CommentView.nhn?search.clubid=" + clubid + "&search.articleid=" + articleid + " ] [ "+ userid + " ] - 아이디 댓글작성완료");
                                        else Log("작성실패");
                                        Thread.Sleep(Delayy);
                                    }
                                }
                                catch (ThreadAbortException)
                                {
                                    return;
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.ToString());
                                }

                            }
                            Log("게시글주소 기준 작업완료");
                        }
                        if (!chk_while.Checked) break;
                    }
                    Set_Status("※ 작업 대기중 - 마지막 작업 [ 게시글작성 ]");
                }
                catch(Exception ex) { MessageBox.Show(ex.ToString()); }
            }));
            Main_thr.IsBackground = true;
            Main_thr.Start();
        }
        private string reply_write(Dictionary<string, string> map, string content, string articleid, CookieContainer cookie, string clubid,object memo = null)
        {

            string ret = string.Empty;
            string result = string.Empty;
            try
            {
               // MessageBox.Show("chk");
                ChangedNICK(clubid, cookie);
               // MessageBox.Show("chk2");
                AutoTethering();
                //MessageBox.Show("chk3");

                string createurl = "https://m.cafe.naver.com/CommentPost.nhn?m=write";
                if (memo != null) createurl = "https://cafe.naver.com/MemoCommentPost.nhn";
                string refererurl = "https://m.cafe.naver.com/CommentView.nhn?search.clubid=" + clubid + "&search.articleid=" + articleid + "&commentCursorOn=true";
                if (memo != null) {
                    if(!map.ContainsKey("m")) map.Add("m", "write");
                    map["content"] = content;
                    refererurl = "https://cafe.naver.com/MemoList.nhn?search.clubid=" + clubid + "&search.menuid=&viewType=pc";
                }
                String body = "";
                if (map != null)
                {
                    foreach (string key in map.Keys)
                        body += key + "=" + map[key] + "&";
                }
                //Log(body);
                //HttpWebRequest request = null;
                WebResponse response = null;

                byte[] data = null;
                /*if (memo == null) */data = Encoding.Default.GetBytes(body);
                //else data = Encoding.UTF8.GetBytes(body);

                HttpWebRequest request = WebRequest.Create(createurl) as HttpWebRequest;
                
                request.Method = "POST";
                request.Referer = refererurl;
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                request.CookieContainer = cookie;
                request.Timeout = 180000;
                if (cb_Tethering.Checked) request.KeepAlive = true;
                //   MessageBox.Show("1");
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                void CallbackMethod(IAsyncResult resulter)
                {
                    response = request.EndGetResponse(resulter);
                }

                request.BeginGetResponse(new AsyncCallback(CallbackMethod), null);
                // MessageBox.Show("chk4");

                Stream ststream = response.GetResponseStream();
                StreamReader reader = new StreamReader(ststream, Encoding.UTF8, true);  //이렇게 하면 안깨짐 
                result = reader.ReadToEnd();
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Log("자세한 에러내용은 다음과 같습니다. ----\r\n" + ex.Message);
                return "실패";
            }
            //Log(result);
            try
            {
                HtmlAgilityPack.HtmlDocument result_doc = new HtmlAgilityPack.HtmlDocument();
                result_doc.LoadHtml(result);
                //현재 활동정지 상태입니다.활동 정지가 해제되기 전까지해당 카페에서 글쓰기와 수정,공유하기, 네이버 MY구독 추가 등카페 활동이 불가합니다.
                string RET = result_doc.DocumentNode.SelectSingleNode("//div[@class='error_content_body']").InnerText.Trim();
                Log(RET);
                if (RET.Equals("현재 활동정지 상태입니다.활동 정지가 해제되기 전까지해당 카페에서 글쓰기와 수정,공유하기, 네이버 MY구독 추가 등카페 활동이 불가합니다.")) ret = "제재";
                else ret = "실패";
            }
            catch
            {
                ret = "성공";
            }
            return ret;
        }
        private void ChangedNICK(string get_clubid,CookieContainer cookie)
        {
            if (cb_ChangeNick.Checked)
            {
                try
                {
                    while (true)
                    {
                        string Rand_NICK = ConstNICK.LIST_NICK[new Random().Next(0, ConstNICK.LIST_NICK.Count)].Trim();
                        Dictionary<string, string> DIC_NickCHK_Parameters = new Dictionary<string, string>();
                        DIC_NickCHK_Parameters.Add("cafeId", get_clubid);
                        DIC_NickCHK_Parameters.Add("nickname", System.Web.HttpUtility.UrlEncode(Rand_NICK));
                        string RET_NICK = WebController.POST(DIC_NickCHK_Parameters, "https://m.cafe.naver.com/NicknameValidation.nhn",
                            "https://m.cafe.naver.com/CafeMemberProfileModify.nhn?cafeId=" + get_clubid, cookie, Encoding.UTF8);
                        if (RET_NICK.Trim().Equals(string.Empty)) Log("이미 사용중인 닉네임이 있으므로 5초후 재시도합니다.");
                        JObject JOBJ_NICK = JObject.Parse(RET_NICK);
                        if (JOBJ_NICK["message"]["status"].ToString().Equals("200"))
                        {
                            DIC_NickCHK_Parameters.Clear();
                            DIC_NickCHK_Parameters.Add("cafeId", get_clubid);
                            DIC_NickCHK_Parameters.Add("profileImageUrl", string.Empty);
                            DIC_NickCHK_Parameters.Add("cafeMemberProfileImageUrl", string.Empty);
                            DIC_NickCHK_Parameters.Add("profileImageType", "0");
                            DIC_NickCHK_Parameters.Add("nickname", System.Web.HttpUtility.UrlEncode(Rand_NICK));
                            DIC_NickCHK_Parameters.Add("allowShowSexAndAge", "true");
                            DIC_NickCHK_Parameters.Add("allowPopularMember", "true");
                            DIC_NickCHK_Parameters.Add("allowShowBlog", "true");
                            RET_NICK = WebController.POST(DIC_NickCHK_Parameters, "https://m.cafe.naver.com/CafeMemberProfileSave.nhn",
                            "https://m.cafe.naver.com/CafeMemberProfileModify.nhn?cafeId=" + get_clubid, cookie, Encoding.UTF8);
                            Log("닉네임 변경성공 - [ " + Rand_NICK + " ]");
                            break;
                        }
                        Thread.Sleep(5000);
                    }
                    //else Log("닉네임 변경성공 - [ " + "가온" + " ]");
                }
                catch (ThreadAbortException) { return; }
                catch (JsonReaderException)
                {
                    Log("닉네임 변경에 실패하였습니다.");
                }
                catch (Exception)
                {
                    Log("닉네임 변경에 실패하였습니다...");
                }
            }
        }
        private Dictionary<string, string> Send_Image(Dictionary<string, string> map, CookieContainer cookie, string clubid, string articleid, string image, string userid)
        {
            string result = string.Empty;
            string imagepath = string.Empty;
            string filename = string.Empty;
            string width = string.Empty;
            string height = string.Empty;
            Dictionary<String, string> ret_map = new Dictionary<string, string>();
            try
            {
                string body = "";

                if (map != null)
                {
                    foreach (string key in map.Keys)
                        body += key + "=" + map[key] + "&";
                }

                HttpWebRequest request = null;
                HttpWebResponse response = null;
                var data = Encoding.Default.GetBytes(body);
                request = WebRequest.Create("https://m.cafe.naver.com/PhotoInfraSessionKey.nhn") as HttpWebRequest;
                request.Method = "POST";
                request.Referer = "https://m.cafe.naver.com/CommentView.nhn?search.clubid=" + clubid + "&search.articleid=" + articleid + "&commentcursoron=true";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                request.CookieContainer = cookie;
                request.KeepAlive = true;
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode.ToString() == "OK")
                {
                    Stream stream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                    result = reader.ReadToEnd();//<<
                }
                else
                {
                    Log("호스트에서 작성중 승인을 받지 못하였습니다.");
                    //main_cnt--;
                    //continue;
                }

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Log("자세한 에러내용은 다음과 같습니다. ----\r\n" + ex.Message);
            }
            string code = string.Empty;
            JObject jobj = new JObject();
            try
            {
                jobj = JObject.Parse(result);
            }
            catch (Exception ex)
            {
                return null;
            }
            if (jobj["message"]["error"]["code"].ToString().Equals(string.Empty)) code = jobj["message"]["result"].ToString();
            else return null;



            if (!image.Equals(string.Empty))
            {
                NameValueCollection nyc = new NameValueCollection();
                NameValueCollection files = new NameValueCollection();
                files.Add("image", image);
                //https://cafe.upphoto.naver.com/MjAxODEyMDQxMTM3NDUHMTU0Mzg5MjAyNDIwOQdjYWZlMgd0eXBlMDcwNQcwBzIHMDJkZTIwNGUxNmZiZTUzMTViYWE4YmY2ZDFlN2FjNjM/simpleUpload/0?userId=type0705&extractExif=false&extractAnimatedCnt=false&autorotate=true
                result = sendHttpRequest("https://cafe.upphoto.naver.com/" + code + "/simpleUpload/0?userId=" + userid + "&extractExif=false&extractAnimatedCnt=false&autorotate=true", nyc, files);


                if (!result.Equals(string.Empty))
                {
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(result);
                    imagepath = doc.DocumentNode.SelectSingleNode("//path").InnerText.Trim();
                    filename = doc.DocumentNode.SelectSingleNode("//filename").InnerText.Trim();
                    width = doc.DocumentNode.SelectSingleNode("//width").InnerText.Trim();
                    height = doc.DocumentNode.SelectSingleNode("//height").InnerText.Trim();

                    ret_map.Add("path", imagepath + '/');
                    ret_map.Add("fileName", filename);
                    ret_map.Add("width", width);
                    ret_map.Add("height", height);
                }
                else Log("이미지 업로드실패");

                // Log(result);
            }
            if (!imagepath.Equals(string.Empty))
            {
                return ret_map;
            }
            else return null;
        }
        private static string sendHttpRequest(string url, NameValueCollection values, NameValueCollection files = null)
        {
            string result = "";
            try
            {
                string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
                byte[] boundaryBytes = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "\r\n");
                byte[] trailer = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
                byte[] boundaryBytesF = System.Text.Encoding.ASCII.GetBytes("--" + boundary + "\r\n");

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "multipart/form-data; boundary=" + boundary;
                request.Method = "POST";
                request.KeepAlive = true;
                request.Credentials = System.Net.CredentialCache.DefaultCredentials;

                Stream requestStream = request.GetRequestStream();

                if (files != null)
                {
                    foreach (string key in files.Keys)
                    {
                        if (File.Exists(files[key]))
                        {
                            int bytesRead = -1;
                            byte[] buffer = new byte[1024];
                            byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n", key, files[key]));
                            requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                            requestStream.Write(formItemBytes, 0, formItemBytes.Length);
                            using (FileStream fileStream = new FileStream(files[key], FileMode.Open, FileAccess.Read))
                            {
                                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                                {
                                    requestStream.Write(buffer, 0, bytesRead);
                                }
                                fileStream.Close();
                            }
                        }
                    }
                }
                requestStream.Write(trailer, 0, trailer.Length);
                foreach (string key in values.Keys)
                {
                    byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}", key, values[key]));
                    requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                    requestStream.Write(formItemBytes, 0, formItemBytes.Length);
                }
                requestStream.Close();
                using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                };
            }
            catch (Exception e)
            {
                return string.Empty;
            }
            return result;
        }

        private void pb_work_Start_Click(object sender, EventArgs e)
        {
            try
            {
                if (LoginMember.Memcode == "-1")
                {
                    MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                    return;
                }
                if (!LoginMember.Payment)
                {
                    MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                    return;
                }
                if (lv_work_WorkList.CheckedItems.Count < 1)
                {
                    MessageBoxEx.Show(this, "[작업설정]의 3개의 작업대상을 선택한 후 [게시판 목록] 의 [선택]버튼을 눌러 추가한 후 추가된 항목을[체크] 해주세요.");
                    return;
                }
                if (Time_thr != null && Time_thr.IsAlive)
                {
                    if (MessageBoxEx.Show(this, "현재 예약 대기중인 스케줄이 있습니다. 이전 예약을 취소하고 현재 작업을 진행하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        Time_thr.Abort();
                    else return;
                }
                if (Main_thr != null && Main_thr.IsAlive)
                {
                    if (MessageBoxEx.Show(this, "현재 진행중인 작업이 있습니다. 이전작업을 취소하고 현재작업을 진행하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        Main_thr.Abort();
                    else return;
                }
                if (chk_IdReplyMatch.Checked)
                {
                    int CertCnt = 0;
                    //if (lv_work_IdList.Items[i].SubItems[3].Text.Equals("O"))
                    for (int i = 0; i < lv_work_IdList.Items.Count; i++) if (lv_work_IdList.Items[i].SubItems[3].Text.Equals("O")) CertCnt++;
                    if (CertCnt > lv_work_ContentList.Items.Count)
                    {
                        MessageBoxEx.Show(this, "아이디와 댓글 매칭기능이 설정 되어 있으나 검증 받은 아이디개수와 댓글 개수가 서로 다릅니다.\r\n" +
                            "검증받으신 아이디와 댓글 개수를 일치시켜주세요");
                        return;
                    }
                }
                Write_Reply(0);
            }
            catch (Exception ex) {/* MessageBox.Show(ex.ToString());*/ }    
        }

        private bool REPLY_OVERLAP(string clubid,string menuid, string articleid,string targetid, string targetComment, CookieContainer cookie)
        {
          //  string chk = string.Empty;
            try
            {
                //MessageBox.Show("chk");
                bool IsOverLap = false;
                Dictionary<string, string> DIC_CommentView_Para = new Dictionary<string, string>();
                DIC_CommentView_Para.Add("search.page", "1");
                DIC_CommentView_Para.Add("search.orderby", "undefined");
                string RET_CommentView = WebController.POST(DIC_CommentView_Para, "https://cafe.naver.com/CommentView.nhn?search.clubid=" + clubid + "&search.menuid=" + menuid + "&search.articleid=" + articleid + "&search.lastpageview=true&lcs=Y",
                    "https://cafe.naver.com/ArticleRead.nhn?articleid=" + articleid + "&clubid=" + clubid, cookie, Encoding.UTF8);
                //Log("RET_CommentView::"+RET_CommentView);
                if (!RET_CommentView.Trim().Equals(string.Empty))
                {
                    JObject JOBJ_CommentView = null;
                    //  chk = RET_CommentView;
                    try
                    {
                        JOBJ_CommentView = JObject.Parse(RET_CommentView);
                        foreach (var JTKN_CommentView in JOBJ_CommentView["result"]["list"] as JArray)
                        {
                            string id = JTKN_CommentView["writerid"].ToString();
                            string content = JTKN_CommentView["content"].ToString().Replace("<br />", "\r\n");
                            if (id.Equals(targetid))
                            {
                                Log("동일 [ " + id + " ]아이디 중복검색됨 작업생략 딜레이30초");
                                Thread.Sleep(30000);
                                IsOverLap = true;
                                break;
                            }
                            if (content.Equals(targetComment))
                            {
                                Log("동일 [ " + content + " ]내용 중복검색됨 작업생략 딜레이30초");
                                Thread.Sleep(30000);
                                IsOverLap = true;
                                break;
                            }
                        }
                    }
                    catch(JsonReaderException)
                    {
                        Log("작업생략 딜레이30초");
                        Thread.Sleep(30000);
                        IsOverLap = true;
                    }
                }
                return IsOverLap;
            }
            catch (ThreadAbortException) { return false; }
            catch(Exception ex)
            {
             //   Log(chk);
               // MessageBox.Show("chk");
                Log("댓글중복체크::" + ex.ToString());
                return false;
            }
           
        }
        private void tb_work_Id_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("아이디")) tb.Text = "";
        }

        private void tb_work_Id_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("아이디") || tb.Text.Equals("")) tb.Text = "아이디";
        }

        private void cb_work_IdList_All_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            Thread chk_thr = new Thread(new ThreadStart(delegate
            {
                if (cb.Checked)
                {
                    cb.Margin = new Padding(-1, -1, 0, 0);
                    cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
                    lv_work_IdList.BeginUpdate();
                    int cnt = 0;
                    for (int i = 0; i < lv_work_IdList.Items.Count; i++)
                    {
                        cnt++;
                        lv_work_IdList.Items[i].Checked = true;
                        if (cnt % 100 == 0)
                        {
                            lv_work_IdList.EndUpdate();
                            lv_work_IdList.Refresh();
                            lv_work_IdList.BeginUpdate();
                        }
                    }
                    lv_work_IdList.EndUpdate();
                }
                else
                {
                    cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));
                    for (int i = 0; i < lv_work_IdList.Items.Count; i++)
                        lv_work_IdList.Items[i].Checked = false;
                }
                lv_work_IdList.EndUpdate();
            }));
            chk_thr.IsBackground = true;
            chk_thr.Start();
        }

        private void lv_work_IdList_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lv_work_IdList_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            if (e.Header == columnHeader4)
            {
                var imageRect = new Rectangle(e.Bounds.X + 23, e.Bounds.Y + 2, 10, 10);
                if (e.Item.SubItems[3].Text == "O")
                {
                    e.Graphics.DrawImage(((System.Drawing.Image)(Properties.Resources.icon_ok)), imageRect);
                }
                else
                {
                    e.Graphics.DrawImage(((System.Drawing.Image)(Properties.Resources.icon_not)), imageRect);
                }
                return;
            }
            e.DrawDefault = true;
        }

        private void pb_work_IdSelDel_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (lv_work_IdList.CheckedItems.Count == 0)
            {
                MessageBoxEx.Show(this, "선택된 항목이 없습니다.");
                return;
            }
            if (MessageBoxEx.Show(this, "선택하신 항목이 삭제 됩니다.\r계속 하시겠습니까?", "항목 삭제", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                for (int i = lv_work_IdList.CheckedItems.Count - 1; i >= 0; i--)
                    lv_work_IdList.Items.Remove(lv_work_IdList.CheckedItems[i]);
            }
            if (lv_work_IdList.Items.Count < 1)
            {
                cb_work_IdList_All.Checked = false;
            }
            SavedFormData_server();
        }

        private void pb_work_ContentList_Add_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (Time_thr != null && Time_thr.IsAlive)
            {
                if (MessageBoxEx.Show(this, "현재 예약 대기중인 스케줄이 있습니다. 추가시 작업에 현재 댓글이 추가되어 작업이 진행됩니다. 계속하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes) { }
                else return;
            }
            if (Main_thr != null && Main_thr.IsAlive)
            {
                if (MessageBoxEx.Show(this, "현재 진행중인 작업이 있습니다. 추가시 작업에 현재 댓글이 추가되어 작업이 진행됩니다. 계속하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes) { }
                else return;
            }
            pn_Reply.Visible = true;
            pn_Reply.Location = new Point(347, 47);
            tb_Reply_write.Size = new Size(274, 331);
            pn_Reply.Controls.RemoveByKey("image");
            pb_reply_clear.Visible = false;
            bt_reply_stiker.Tag = null;
            tb_Reply_write.Text = "댓글을 남겨보세요.";
            tb_Reply_write.ForeColor = Color.Gray;
            bt_reply_image.Visible = bt_reply_stiker.Visible = tb_Reply_write.Visible = true;
            bt_reply_image.Tag = null;
            pb_reply_clear.Location = new Point(244, 3);
            tb_Reply_write.Focus();
            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("sticker_pn"))
                {
                    pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                    break;
                }
            }
            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("sticker_pb"))
                {
                    pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                    break;
                }
            }
            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("image_pb"))
                {
                    pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                    break;
                }
            }
        }



        private void tb_Reply_write_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("댓글을 남겨보세요."))
            {
                tb.Text = "";
                tb.ForeColor = Color.Black;
            }
        }

        private void tb_Reply_write_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("댓글을 남겨보세요.") || tb.Text.Equals(""))
            {
                tb.ForeColor = Color.Gray;
                tb.Text = "댓글을 남겨보세요.";
            }
        }

        private void pb_Reply_ok_Click(object sender, EventArgs e)
        {
            //if (tb_Reply_write.Text.Equals("댓글을 남겨보세요.") || tb_Reply_write.Text.Equals(""))
            //{
            //    MessageBox.Show("댓글을 입력해주세요.");
            //    return;
            //}

            List<string> total = new List<string>();
            total.Add(tb_Reply_write.Text.Trim());
            total.Add("");
            total.Add("");

            ListViewItem lvi = new ListViewItem();
            lvi.SubItems.Add(tb_Reply_write.Text.Trim());
            if (bt_reply_stiker.Tag != null)
            {
                total[1] = bt_reply_stiker.Tag.ToString();
                lvi.SubItems.Add("O");
            }
            else lvi.SubItems.Add("X");
            if (bt_reply_image.Tag != null)
            {
                total[2] = bt_reply_image.Tag.ToString();
                lvi.SubItems.Add("O");
            }
            else lvi.SubItems.Add("X");
            lvi.Tag = total;
            lv_work_ContentList.Items.Add(lvi);
            lv_work_ContentList.EnsureVisible(lv_work_ContentList.Items.Count - 1);
            pn_Reply.Visible = false;

            SavedFormData_server();
        }

        private void pb_Reply_cancle_Click(object sender, EventArgs e)
        {
            if (!(tb_Reply_write.Text.Equals("댓글을 남겨보세요.") || tb_Reply_write.Text.Trim().Equals(string.Empty) || bt_reply_stiker.Tag != null || bt_reply_image.Tag != null))
            {
                if (MessageBox.Show("작성중인 내용이 있습니다. 취소 하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    pn_Reply.Visible = false;
            }
            else pn_Reply.Visible = false;
        }
        bool keyword_notice_chk = false;
        private void pb_keyword_Write_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (!keyword_notice_chk) {
                MessageBox.Show("키워드입력은 입력하신 키워드가 게시글 제목에 포함된 게시글에만 댓글을 작성하는 기능입니다.\r\n" +
                    "예) 키워드 \"사업\"입력 시 \"사업아이템 추천드립니다\"라는 게시글에 댓글을 작성하게 되는 기능입니다.");
                keyword_notice_chk = true;
            }
            
            pn_Keyword_Write.Location = new Point(347, 212);
            pn_Keyword_Write.Visible = true;
            tb_Keyword_word.Text = "";
            tb_Keyword_word.Focus();
        }

        private void pb_keyword_Add_Click_1(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (tb_Keyword_word.Text.Equals("이곳에 키워드를 입력해주세요.") || tb_Keyword_word.Text.Equals(""))
            {
                MessageBox.Show("작업을 진행할 키워드를 입력해주세요.");
                return;
            }
            ListViewItem lvi = new ListViewItem("");
            lvi.SubItems.Add((tb_Keyword_word.Text.Trim()));
            lv_Keyword_data.Items.Add(lvi);
            tb_Keyword_word.Text = "이곳에 키워드를 입력해주세요.";
            tb_Keyword_word.ForeColor = Color.Gray;
            lv_Keyword_data.EnsureVisible(lv_Keyword_data.Items.Count - 1);
            pn_Keyword_Write.Visible = false;
            tb_Keyword_word.Focus();
            SavedFormData_server();
        }

        private void tb_Keyword_word_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("이곳에 키워드를 입력해주세요."))
            {
                tb.ForeColor = Color.White;
                tb.Text = "";
            }
        }

        private void tb_Keyword_word_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("이곳에 키워드를 입력해주세요.") || tb.Text.Equals(""))
            {
                tb.ForeColor = Color.Gray;
                tb.Text = "이곳에 키워드를 입력해주세요.";
            }
        }

        private void tb_Keyword_cnt_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("개수"))
            {
                tb.ForeColor = Color.White;
                tb.Text = "";
            }
        }

        private void tb_Keyword_cnt_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("개수") || tb.Text.Equals(""))
            {
                tb.ForeColor = Color.Gray;
                tb.Text = "개수";
            }
        }

        private void pb_work_IdList_Sel_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (fc_IdList.Text == string.Empty)
            {
                MessageBoxEx.Show(this, "검증을 받은 아이디를 선택해주세요");
                return;
            }
            Log("아이디 선택 : " + fc_IdList.Text);
            if (MessageBoxEx.Show(this, "선택하신 아이디가 " + fc_IdList.Text + " 가 맞습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                fc_CafeURL.Items.Clear();
                fc_CafeMenuList.Items.Clear();
                fc_CafeURL.Text = "";
                fc_CafeMenuList.Text = "";
                List<string> Cafe_List = ((lv_work_IdList.Items[lv_work_IdList.FindItemWithText(fc_IdList.Text).Index].Tag as List<object>)[0] as List<string>);

                fc_IdList.Tag = ((lv_work_IdList.Items[lv_work_IdList.FindItemWithText(fc_IdList.Text).Index].Tag as List<object>)[1] as CookieContainer);
                foreach (var _item in Cafe_List) fc_CafeURL.Items.Add(_item.Trim());
            }
        }

        private void pb_work_CafeURL_Search_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (fc_IdList.Text.Equals(string.Empty))
            {
                MessageBoxEx.Show(this, "검증받으신 아이디를 선택해주세요.");
                return;
            }
            if (fc_CafeURL.Text == string.Empty)
            {
                MessageBoxEx.Show(this, "카페주소가 빈칸입니다 다시확인 해주세요.");
                return;
            }
            Log("카페주소입력 : " + fc_CafeURL.Text.Trim());

            if (MessageBoxEx.Show(this, fc_CafeURL.Text + " 카페의 게시글 목록을 불러오시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                fc_CafeMenuList.Items.Clear();
                fc_CafeMenuList.Text = "";
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                CookieContainer cookie = ((lv_work_IdList.Items[lv_work_IdList.FindItemWithText(fc_IdList.Text).Index].Tag as List<object>)[1] as CookieContainer);

                if (fc_CafeURL.Text.Contains('▶')) doc.LoadHtml(WebController.GET(fc_CafeURL.Text.Split('▶')[1].Trim(), "", cookie, Encoding.Default));
                else doc.LoadHtml(WebController.GET(fc_CafeURL.Text.Trim(), "", cookie, Encoding.Default));

                if (doc.Text.Trim().Equals(string.Empty)) return;
                try
                {
                    if (doc.DocumentNode.SelectSingleNode("//p[@class='desc2']").InnerText.Contains("제한"))
                    {
                        MessageBoxEx.Show(this, doc.DocumentNode.SelectSingleNode("//p[@class='desc2']").InnerText.Trim());
                        return;
                    }
                }
                catch
                {

                }
                try
                {
                    if ((doc.DocumentNode.SelectSingleNode("//script[@language='JavaScript']").InnerHtml.Contains("휴면카페입니다.")))
                    {
                        MessageBoxEx.Show(this, "휴면카페입니다. \n휴면 상태에서는 카페 댓글쓰기가 제한됩니다.");
                        return;
                    }
                }
                catch
                {

                }
                string cafe_name = string.Empty;
                try { cafe_name = doc.DocumentNode.SelectSingleNode("//title").InnerText.Split(':')[0].Trim(); }
                catch { cafe_name = fc_CafeURL.Text.Trim(); }
                string clubid = doc.DocumentNode.SelectSingleNode("//script").InnerHtml.strSplit("var g_sClubId = " + '"')[1].Split('"')[0];
                string result = WebController.GET(fc_CafeURL.Text.Trim(), fc_CafeURL.Text.Trim(), cookie, Encoding.Default);
                doc.LoadHtml(result);
                doc.LoadHtml(doc.DocumentNode.SelectSingleNode("//div[@class='box-g-m']").InnerHtml);
                            
                //https://m.cafe.naver.com/MenuListAjax.nhn <<여기로해볼것
                List<object> total = new List<object>();
                List<string> CafeContentList_idx = new List<string>();

                    foreach (var _item in doc.DocumentNode.SelectNodes("//li"))
                    {
                        var innerdoc = new HtmlAgilityPack.HtmlDocument();
                        innerdoc.LoadHtml(_item.InnerHtml);
                        //Log(innerdoc.Text);
                        if (!(innerdoc.DocumentNode.SelectSingleNode("//a").Attributes["id"].Value.Contains("-") ||
                            innerdoc.DocumentNode.SelectSingleNode("//a").Attributes["id"].Value.Equals("0")))
                        {
                            try
                            {
                                int.Parse(innerdoc.DocumentNode.SelectSingleNode("//a").Attributes["id"].Value.Replace("menuLink", "").Trim());
                                fc_CafeMenuList.Items.Add(innerdoc.DocumentNode.SelectSingleNode("//a").InnerText.Trim());
                                CafeContentList_idx.Add(innerdoc.DocumentNode.SelectSingleNode("//a").Attributes["id"].Value.Replace("menuLink", "").Trim());
                            }
                            catch (Exception me)
                            {
                                continue;
                            }
                        }
                    }
        
               

                total.Add(cafe_name);
                total.Add(clubid);
                total.Add(CafeContentList_idx);
                total.Add("");
                fc_CafeURL.Tag = fc_CafeURL.Text.Trim();
                fc_CafeMenuList.Tag = total;
                fc_CafeMenuList.SelectedIndex = 0;

            }
        }

        private void pb_work_CafeContentList_Sel_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (fc_IdList.Text.Equals(string.Empty))
            {
                MessageBoxEx.Show(this, "아이디를 선택해주세요.");
                return;
            }
            if (fc_CafeURL.Text.Equals(string.Empty))
            {
                MessageBoxEx.Show(this, "카페주소를 입력하거나 선택해주세요.");
                return;
            }
            if (lv_work_ContentList.Items.Count < 1)
            {
                MessageBoxEx.Show(this, "댓글을 최소 1개 이상 작성해주세요");
                return;
            }
            try { int.Parse(fc_DelayTime.Text); }
            catch
            {
                MessageBox.Show("[작업설정]의 딜레이는 숫자(초)만 입력해주세요");
                return;
            }
            if (!(chk_Allpage_All.Checked || chk_1page_one.Checked || chk_1page_all.Checked || chk_Selpage_one.Checked || chk_Selpage_All.Checked))
            {
                if (MessageBox.Show("옵션을 선택하지 않을경우 \"모든페이지 모두 댓글작성\"이 기본옵션으로 설정됩니다. 계속하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    chk_Allpage_All.Checked = true;
                else return;
            }
            try
            {
                fc_CafeMenuList.Tag.ToString();
            }
            catch
            {
                MessageBox.Show("[작업설정]에서 [카페주소입력]란에 카페주소를 선택한 후 [선택]버튼을 눌러주세요.");
                return;
            }
            if (chk_Selpage_one.Checked || chk_Selpage_All.Checked)
            {
                try
                {
                    int.Parse(fc_Selpage_one_1.Text.Trim()); int.Parse(fc_Selpage_one_2.Text.Trim()); int.Parse(fc_Selpage_All_1.Text.Trim()); int.Parse(fc_Selpage_All_2.Text.Trim());
                }
                catch
                {
                    MessageBox.Show("우측하단 [작업설정]의 선택 페이지부분은 숫자만 입력해주세요");
                    return;
                }

                if ((int.Parse(fc_Selpage_one_1.Text.Trim()) > int.Parse(fc_Selpage_one_2.Text.Trim())) || (int.Parse(fc_Selpage_All_1.Text.Trim()) > int.Parse(fc_Selpage_All_2.Text.Trim())))
                {
                    MessageBox.Show("[작업설정]의 시작페이지는 마지막페이지보다 클 수 없습니다. 시작페이지를 마지막 페이지보다 낮게 설정해주세요.");
                    return;
                }
            }
            if (Time_thr != null && Time_thr.IsAlive)
            {
                if (MessageBoxEx.Show(this, "현재 예약 대기중인 스케줄이 있습니다. 추가는 현재 작업대기중인 작업목록에 영향을 끼칠 수 있습니다. 계속하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes) { }
                else return;
            }
            if (Main_thr != null && Main_thr.IsAlive)
            {
                if (MessageBoxEx.Show(this, "현재 진행중인 작업이 있습니다. 추가는 현재 작업중인 작업목록에 영향을 끼칠 수 있습니다. 계속하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes) { }
                else return;
            }
            string op = string.Empty; string op_status = string.Empty;
            if (chk_Allpage_All.Checked) { op = "1"; op_status = "모든페이지 모두 댓글 작성"; }
            if (chk_1page_one.Checked) { op = "2"; op_status = "1페이지 글에 1개만 댓글작성"; }
            if (chk_1page_all.Checked) { op = "3"; op_status = "1페이지 게시글에 모두 댓글작성"; }
            if (chk_Selpage_one.Checked) { op = "4"; op_status = "선택 페이지 1개씩 댓글 작성"; }
            if (chk_Selpage_All.Checked) { op = "5"; op_status = "선택 페이지 모두 댓글 작성"; }

            List<string> keywords = new List<string>();
            string keywords_get = string.Empty;
            for (int i = 0; i < lv_Keyword_data.Items.Count; i++)
            {
                keywords.Add(lv_Keyword_data.Items[i].SubItems[1].Text.Trim());
                keywords_get += lv_Keyword_data.Items[i].SubItems[1].Text.Trim() + ',';
            }
            if (MessageBoxEx.Show(this, "아이디 : " + fc_IdList.Text + "\r\n" +
                "카페주소 : " + fc_CafeURL.Text + "\r\n" +
                "카페이름 : " + (fc_CafeMenuList.Tag as List<object>)[0].ToString() + "\r\n" +
                "게시판 : " + fc_CafeMenuList.Text + "\r\n" +
                "옵션 : " + op_status + "\r\n" +
                "딜레이 : " + fc_DelayTime.Text.Trim() + "초\r\n" +
                "키워드 : " + keywords_get + "\r\n" +
                "을(를) 작업목록에 추가하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ListViewItem lvi = new ListViewItem("");
                lvi.SubItems.Add((lv_work_WorkList.Items.Count + 1).ToString());
                lvi.SubItems.Add((fc_CafeMenuList.Tag as List<object>)[0].ToString());
                lvi.SubItems.Add(fc_CafeMenuList.Text);
                lvi.SubItems.Add(fc_IdList.Text);
                lvi.SubItems.Add("대기");
                List<object> obj_list = new List<object>();
                //MessageBoxEx.Show(this,.SelectedIndex.ToString());

                obj_list.Add(fc_IdList.Tag); // cookiecontainer
                obj_list.Add(fc_CafeMenuList.Tag); // list<object>  형식 [0] string 카페이름 , [1] string 작성페이지,  list<String>메뉴목록
                obj_list.Add(((fc_CafeMenuList.Tag as List<object>)[2] as List<string>)[fc_CafeMenuList.SelectedIndex].Trim()); // menuid (string)
                obj_list.Add(""); // 작업게시글
                obj_list.Add(fc_CafeURL.Tag); // string cafeURL 리퍼러값에 사용
                obj_list.Add(op); // option 4중 택 1
                obj_list.Add(keywords); // 키워드 dic<str,int>형태
                obj_list.Add(fc_DelayTime.Text.Trim()); // 딜레이타임(초)
                obj_list.Add(fc_IdList.Text.Trim()); // userid
                if (op.Equals("4")) obj_list.Add(fc_Selpage_one_1.Text.Trim() + "|" + fc_Selpage_one_2.Text.Trim()); // 선택페이지에 댓글하나씩
                if (op.Equals("5")) obj_list.Add(fc_Selpage_All_1.Text.Trim() + "|" + fc_Selpage_All_2.Text.Trim()); // 선택페이지에 모두댓글
                lvi.Tag = obj_list;
                lv_work_WorkList.Items.Add(lvi);

                Log("게시글선택---\r\n" +
                    "아이디 : " + fc_IdList.Text + "\r\n" +
                "카페주소 : " + fc_CafeURL.Text + "\r\n" +
                "카페이름 : " + (fc_CafeMenuList.Tag as List<object>)[0].ToString() + "\r\n" +
                "게시판 : " + fc_CafeMenuList.Text + "\r\n" +
                "옵션 : " + op_status + "\r\n" +
                "딜레이 : " + fc_DelayTime.Text.Trim() + "초\r\n" +
                "키워드 : " + keywords_get +
                "(을)를 작업목록에 추가");
                Set_Status("※ 작업 대기중 - 마지막 작업 [ 작업목록 추가 ]");
                lv_work_WorkList.EnsureVisible(lv_work_WorkList.Items.Count - 1);
                Log("게시판목록 : " + fc_CafeMenuList.Text.Trim());
            }
        }

        private void _Click(object sender, EventArgs e)
        {
            //if (LoginMember.Memcode == "-1")
            //{
            //    MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
            //    return;
            //}
            //if (!LoginMember.Payment)
            //{
            //    MessageBoxEx.Show(this, "결제 후 이용해 주세요");
            //    return;
            //}
            FlatCombo fc = (FlatCombo)sender;
            fc.Items.Clear();
            for (int i = 0; i < lv_work_ContentList.Items.Count; i++)
            {
                fc.Items.Add(lv_work_ContentList.Items[i].SubItems[1].Text);
            }
            if (fc.Items.Count < 1)
            {
                lv_work_ContentList.Focus();
                MessageBoxEx.Show(this, "[게시글 등록]란에서 게시글 작성 후 추가를 해주셔야 목록이 추가됩니다.");
            }
        }

        private void _KeyDown(object sender, KeyEventArgs e)
        {
            if (!e.KeyCode.ToString().Equals("Return"))
                e.SuppressKeyPress = true;
        }

        private void pb_work_ContentList_Sel_Click(object sender, EventArgs e)
        {
            Content_List_Sel();
        }

        private void _KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                Content_List_Sel();
        }
        private void Content_List_Sel()
        {
            //if (LoginMember.Memcode == "-1")
            //{
            //    MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
            //    return;
            //}
            //if (!LoginMember.Payment)
            //{
            //    MessageBoxEx.Show(this, "결제 후 이용해 주세요");
            //    return;
            //}

        }

        private void chk_Allpage_All_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                chk_1page_one.Checked = false;
                chk_1page_all.Checked = false;
                chk_Selpage_one.Checked = false;
                chk_Selpage_All.Checked = false;
            }
        }

        private void chk_1page_one_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                chk_Allpage_All.Checked = false;
                chk_1page_all.Checked = false;
                chk_Selpage_one.Checked = false;
                chk_Selpage_All.Checked = false;
            }
        }

        private void chk_1page_all_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                chk_Allpage_All.Checked = false;
                chk_1page_one.Checked = false;
                chk_Selpage_one.Checked = false;
                chk_Selpage_All.Checked = false;
            }
        }

        private void chk_Selpage_one_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                chk_Allpage_All.Checked = false;
                chk_1page_all.Checked = false;
                chk_1page_one.Checked = false;
                chk_Selpage_All.Checked = false;
            }
        }

        private void chk_Selpage_All_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                chk_Allpage_All.Checked = false;
                chk_1page_all.Checked = false;
                chk_Selpage_one.Checked = false;
                chk_Allpage_All.Checked = false;
            }
        }

        private void pn_main_logo_Click(object sender, EventArgs e)
        {
        }

        private void tb_delay_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (e.KeyChar == '\r')
            {
                try
                {
                    int.Parse(tb.Text);
                    tb.Tag = tb.Text;
                    MessageBox.Show("적용이 완료되었습니다.");
                }
                catch
                {
                    MessageBox.Show("숫자만 입력해주세요");
                    tb.Text = "30";
                    tb.Tag = "30";
                }
            }
        }

        private void bt_reply_image_Click(object sender, EventArgs e)
        {
            MessageBox.Show("JPG, GIF, PNG, BMP 이미지 파일을 올릴 수 있습니다.\r\n" +
                         "한 장당 10MB, 1회 50MB까지 올리기 가능합니다.\r\n\r\n" +
                         "저작권자 등 원 권리자의 허락을 받지 않은 이미지의 수정 및 저작권 등\r\n" +
                         "다른 사람의 권리 침해, 명예를 훼손하는 이미지는\r\n" +
                         "이용약관 및 관련 법률에 의해 제재를 받으실 수 있습니다.\r\n");
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "이미지파일 (*.jpg, *.gif, *.png, *.bmp) | *.jpg; *.gif; *.png; *.bmp";
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {

                if (ofd.FileName.Equals(string.Empty))
                {
                    return;
                }
                if (!File.Exists(ofd.FileName))
                {
                    MessageBox.Show("대상 파일이 있는지 확인해주세요.", "확인");
                    return;
                }
                Button bt = (Button)sender;
                bt.Tag = ofd.FileName;

                tb_Reply_write.Size = new Size(274, 166);//274, 331
                PictureBox pb = new PictureBox();

                var bytes = System.IO.File.ReadAllBytes(ofd.FileName);
                var ms = new System.IO.MemoryStream(bytes);
                pb.Image = Image.FromStream(ms);
                pb.SizeMode = PictureBoxSizeMode.Zoom;
                pb.Location = new Point(0, 166);
                pb.Size = new Size(274, 165);
                pb.BackColor = Color.FromArgb(245, 245, 245);
                pb.Name = "image_pb";

                PictureBox close_owner_pb = new PictureBox();
                close_owner_pb.Size = new Size(15, 15);
                close_owner_pb.Name = "close_owner_pb";
                close_owner_pb.Location = new Point(259, 0);
                close_owner_pb.BackgroundImage = Properties.Resources.posts_btn_close_on;
                close_owner_pb.Click += Close_owner_pb_Click2;
                close_owner_pb.Cursor = Cursors.Hand;
                pb.Controls.Add(close_owner_pb);

                for (int i = 0; i < pn_Reply.Controls.Count; i++)
                {
                    if (pn_Reply.Controls[i].Name.Equals("sticker_pn"))
                    {
                        pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                        break;
                    }
                }
                for (int i = 0; i < pn_Reply.Controls.Count; i++)
                {
                    if (pn_Reply.Controls[i].Name.Equals("sticker_pb"))
                    {
                        pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                        break;
                    }
                }
                for (int i = 0; i < pn_Reply.Controls.Count; i++)
                {
                    if (pn_Reply.Controls[i].Name.Equals("image_pb"))
                    {
                        pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                        break;
                    }
                }
                pn_Reply.Controls.Add(pb);
                bt_reply_stiker.Tag = null;
            }
        }

        private void Close_owner_pb_Click2(object sender, EventArgs e)
        {
            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("image_pb"))
                {
                    pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                    break;
                }
            }
            tb_Reply_write.Size = new Size(274, 331);
            bt_reply_image.Tag = null;
        }

        private void bt_reply_stiker_Click(object sender, EventArgs e)
        {
            if (load_thr != null && load_thr.IsAlive)
            {
                load_thr.Abort();
            }
            int chk_cnt = 0;
            for (int i = 0; i < pn_Reply.Controls.Count - 1; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("sticker_pb"))
                {
                    chk_cnt++;
                    pn_Reply.Controls[i].Visible = false;
                }
            }
            if (chk_cnt == 0)
            {
                pn_Reply.Controls[pn_Reply.Controls.Count - 1].Visible = false;
            }
            pb_reply_clear.Visible = true;
            pb_reply_clear.Location = new Point(256, 3);
            load_thr = new Thread(new ThreadStart(delegate
            {
                tb_Reply_write.Visible = false;
                bt_reply_stiker.Visible = false;
                bt_reply_image.Visible = false;
                Panel pn = new Panel();
                pn.Size = new Size(274, 331);
                pn.BackColor = Color.FromArgb(245, 245, 245);
                pn.Location = new Point(0, 0);
                pn.Name = "sticker_pn";
                Panel menu_pn = new Panel();
                menu_pn.Size = new Size(274, 30);
                menu_pn.Location = new Point(0, 0);
                menu_pn.BackColor = Color.FromArgb(245, 245, 245);
                menu_pn.Name = "menu_pn";
                menu_pn.Paint += Menu_pn_Paint;
                Panel contents_pn = new Panel();
                contents_pn.Size = new Size(274, 301);
                contents_pn.Location = new Point(0, 30);
                contents_pn.BackColor = Color.FromArgb(245, 245, 245);
                contents_pn.Name = "contents_pn";
                contents_pn.AutoScroll = true;
                Invoke(new Action(delegate { pn.Controls.Add(menu_pn); }));
                Invoke(new Action(delegate { pn.Controls.Add(contents_pn); }));
                Invoke(new Action(delegate { pn_Reply.Controls.Add(pn); }));

                string result = WebController.GET("https://cafe.naver.com/gfmarket_sticker/StickerPackListAsync.nhn", "", null, Encoding.UTF8);
                var jobj = JObject.Parse(result);
            
                int count = int.Parse(jobj["info"]["count"].ToString().Replace(",", "").Trim());
                string domain = jobj["info"]["domain"].ToString().Trim() + '/';
                string tabOnName = jobj["info"]["tabOnName"].ToString().Trim();
                string tabOffName = jobj["info"]["tabOffName"].ToString().Trim();
                string previewName = jobj["info"]["previewName"].ToString().Trim();
                List<string> list = new List<string>();

                foreach (var _item in jobj["list"] as JArray)
                {
                    JObject inner_jobj = _item as JObject;
                    list.Add(inner_jobj["packCode"].ToString());
                    PictureBox pb = new PictureBox();
                    pb.Size = new Size(34, 29);
                    pb.Load(domain + inner_jobj["packCode"].ToString() + '/' + tabOffName);
                    List<string> on_off_list = new List<string>();
                    on_off_list.Add("N");
                    on_off_list.Add(domain + inner_jobj["packCode"].ToString() + '/' + tabOnName);
                    on_off_list.Add(domain + inner_jobj["packCode"].ToString() + '/' + tabOffName);
                    pb.Tag = on_off_list;
                    pb.Name = domain + inner_jobj["packCode"].ToString() + '/' + tabOffName;
                    pb.MouseClick += Pb_MouseClick;
                    pb.MouseDown += Pb_MouseDown;
                    pb.MouseEnter += Pb_MouseEnter;
                    pb.MouseLeave += Pb_MouseLeave;
                    pb.MouseUp += Pb_MouseUp;
                    pb.Paint += Pb_Paint;
                    pb.Cursor = Cursors.Hand;
                    if (menu_pn.Controls.Count == 0)
                        pb.Location = new Point(0, 1);
                    else pb.Location = new Point(menu_pn.Controls[0].Location.X + (menu_pn.Controls.Count * 43), 1);
                    Invoke(new Action(delegate { menu_pn.Controls.Add(pb); }));
                }

                Dictionary<string, string> map = new Dictionary<string, string>();
                //MessageBox.Show((menu_pn.Controls[0].Tag as List<string>)[1]);
                //return;
                string packCode = (menu_pn.Controls[0].Tag as List<string>)[1].Split('/')[3].Split('/')[0].Trim();
                map.Add("packCode", packCode);
                result = WebController.POST(map, "https://cafe.naver.com/gfmarket_sticker/StickerListAsync.nhn", "", null, Encoding.UTF8);
                jobj = JObject.Parse(result);
                //Log(jobj.ToString());
                foreach (var _item in jobj["list"] as JArray)
                {
                    var inner_jobj = _item as JObject;
                    PictureBox pb = new PictureBox();
                    pb.Size = new Size(85, 85);
                    pb.SizeMode = PictureBoxSizeMode.Zoom;
                    pb.BackColor = Color.FromArgb(245, 245, 245);
                    pb.Load(inner_jobj["originalUrl"].ToString() + '?' + inner_jobj["type"].ToString());
                    List<String> tag = new List<string>();
                    tag.Add("N");
                    tag.Add(inner_jobj["stickerCode"].ToString().Trim() + '-'+ inner_jobj["imageWidth"].ToString().Trim() + '-' + inner_jobj["imageHeight"].ToString().Trim());
                    pb.Tag = tag;
                    pb.Paint += Pb_Paint1;
                    pb.Cursor = Cursors.Hand;
                    pb.Click += Pb_Click;
                    pb.DoubleClick += Pb_DoubleClick;
                    if (contents_pn.Controls.Count == 0) pb.Location = new Point(0, 0);
                    else
                    {
                        int location_X = contents_pn.Controls[contents_pn.Controls.Count - 1].Right;
                        if ((contents_pn.Controls.Count % 3 == 0)) location_X = 0;
                        pb.Location = new Point(location_X, contents_pn.Controls[0].Location.Y + ((contents_pn.Controls.Count / 3) * 85));
                    }
                    Invoke(new Action(delegate { contents_pn.Controls.Add(pb); }));
                }
                pn_Reply.Controls[pn_Reply.Controls.Count - 1].BringToFront();
                pb_reply_clear.BringToFront();
            }));
            load_thr.IsBackground = true;
            load_thr.Start();
        }

        private void Pb_DoubleClick(object sender, EventArgs e)
        {
            if (load_thr != null && load_thr.IsAlive)
            {
                load_thr.Abort();
            }
            tb_Reply_write.Size = new Size(274, 166);//274, 331
            PictureBox pb = (PictureBox)sender;
            PictureBox owner_pb = new PictureBox();
            owner_pb.ImageLocation = pb.ImageLocation;
            owner_pb.SizeMode = PictureBoxSizeMode.Zoom;
            owner_pb.Location = new Point(0, 166);
            owner_pb.Size = new Size(274, 165);
            owner_pb.Tag = (pb.Tag as List<string>)[1];
            owner_pb.BackColor = Color.WhiteSmoke;
            owner_pb.Name = "sticker_pb";

            PictureBox close_owner_pb = new PictureBox();
            close_owner_pb.Size = new Size(15, 15);
            close_owner_pb.Name = "close_owner_pb";
            close_owner_pb.Location = new Point(259, 0);
            close_owner_pb.BackgroundImage = Properties.Resources.posts_btn_close_on;
            close_owner_pb.Click += Close_owner_pb_Click;
            close_owner_pb.Cursor = Cursors.Hand;
            owner_pb.Controls.Add(close_owner_pb);
            //MessageBox.Show(pn_Reply.Controls[pn_Reply.Controls.Count - 1].ToString());

            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("sticker_pn"))
                {
                    pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                    break;
                }
            }
            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("sticker_pb"))
                {
                    pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                    break;
                }
            }
            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("image_pb"))
                {
                    pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                    break;
                }
            }
            pb_reply_clear.Visible = false;
            pb_reply_clear.Location = new Point(244, 3);
            pn_Reply.Controls.Add(owner_pb);
            bt_reply_stiker.Tag = (pb.Tag as List<string>)[1];
            bt_reply_image.Tag = null;
            pb.Dispose();
            owner_pb.Refresh();
            bt_reply_stiker.Visible = bt_reply_image.Visible = tb_Reply_write.Visible = true;
        }

        private void Close_owner_pb_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("sticker_pb"))
                {
                    pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                    break;
                }
            }
            bt_reply_stiker.Tag = null;
            tb_Reply_write.Size = new Size(274, 331);
        }

        private void Pb_Click(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            //MessageBox.Show(pn_Reply.Controls[pn_Reply.Controls.Count - 1].Controls.Count.ToString());
            //Panel contents_pn = pn_Reply.Controls[pn_Reply.Controls.Count - 1].Controls[0] as Panel;
            if ((pb.Tag as List<string>)[0].Equals("N"))
            {
                pb.BackColor = Color.DarkGray;
                (pb.Tag as List<string>)[0] = "Y";

            }
            else
            {
                (pb.Tag as List<string>)[0] = "N";
                pb.BackColor = Color.FromArgb(245, 245, 245);
                //ControlPaint.DrawBorder(pb.CreateGraphics(), pb.ClientRectangle, Color.DarkSlateGray, ButtonBorderStyle.Dotted);
            }
        }

        private void Pb_Paint1(object sender, PaintEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            ControlPaint.DrawBorder(e.Graphics, pb.ClientRectangle, Color.DarkSlateGray, ButtonBorderStyle.Dotted);
        }

        private void Pb_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Menu_pn_Paint(object sender, PaintEventArgs e)
        {
            Panel pb = (Panel)sender;
            ControlPaint.DrawBorder(e.Graphics, pb.ClientRectangle, Color.DarkSlateGray, ButtonBorderStyle.Dotted);
        }

        private void Pb_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.Load((pb.Tag as List<string>)[1]);
        }

        private void Pb_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            //if(pn_Reply.Controls[].ContainsKey("menu_pn"))
            pb.Load((pb.Tag as List<string>)[2]);
        }

        private void Pb_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.Load((pb.Tag as List<string>)[1]);
        }

        private void Pb_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.Load((pb.Tag as List<string>)[1]);
        }

        private void Pb_MouseClick(object sender, MouseEventArgs e)
        {
            if (load_thr != null && load_thr.IsAlive)
            {
                MessageBox.Show("스티커를 로드중입니다. 잠시만 기다려주세요");
                return;
            }
            load_thr = new Thread(new ThreadStart(delegate
            {
                PictureBox pb = (PictureBox)sender;
                Dictionary<string, string> map = new Dictionary<string, string>();
                Panel contents_pn = null;
                for (int i = 0; i < pn_Reply.Controls.Count; i++)
                    if (pn_Reply.Controls[i].Name.Equals("sticker_pn"))
                        for (int j = 0; j < pn_Reply.Controls[i].Controls.Count; j++)
                            if (pn_Reply.Controls[i].Controls[j].Name.Equals("contents_pn"))
                            {
                                contents_pn = pn_Reply.Controls[i].Controls[j] as Panel;
                                break;
                            }

                Invoke(new Action(delegate { contents_pn.Controls.Clear(); }));
                string packCode = ((pb.Tag as List<String>)[1]).Split('/')[3].Split('/')[0].Trim();
                map.Add("packCode", packCode);
                string result = WebController.POST(map, "https://cafe.naver.com/gfmarket_sticker/StickerListAsync.nhn", "", null, Encoding.UTF8);
                var jobj = JObject.Parse(result);
                //Log(jobj.ToString());
                foreach (var _item in jobj["list"] as JArray)
                {
                    var inner_jobj = _item as JObject;
                    PictureBox inner_pb = new PictureBox();
                    inner_pb.Size = new Size(85, 85);
                    inner_pb.SizeMode = PictureBoxSizeMode.Zoom;
                    inner_pb.BackColor = Color.FromArgb(245, 245, 245);
                    inner_pb.Load(inner_jobj["originalUrl"].ToString() + '?' + inner_jobj["type"].ToString());
                    List<string> pack = new List<string>();
                    pack.Add("N");
                    pack.Add(inner_jobj["stickerCode"].ToString().Trim() + '-' + inner_jobj["imageWidth"].ToString().Trim() + '-' + inner_jobj["imageHeight"].ToString().Trim());
                    inner_pb.Tag = pack;
                    inner_pb.Click += Inner_pb_Click;
                    inner_pb.Paint += Inner_pb_Paint;
                    inner_pb.Cursor = Cursors.Hand;
                    inner_pb.DoubleClick += Inner_pb_DoubleClick;
                    if (contents_pn.Controls.Count == 0) inner_pb.Location = new Point(1, 0);
                    else
                    {
                        int location_X = contents_pn.Controls[contents_pn.Controls.Count - 1].Right;
                        if ((contents_pn.Controls.Count % 3 == 0)) location_X = 1;
                        inner_pb.Location = new Point(location_X, contents_pn.Controls[0].Location.Y + ((contents_pn.Controls.Count / 3) * 85));
                    }
                    Invoke(new Action(delegate { contents_pn.Controls.Add(inner_pb); }));
                }
            }));
            load_thr.IsBackground = true;
            load_thr.Start();
        }

        private void Inner_pb_Click(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            if ((pb.Tag as List<String>)[0].Equals("N"))
            {
                pb.BackColor = Color.DarkGray;
                (pb.Tag as List<String>)[0] = "Y";
            }
            else
            {
                (pb.Tag as List<String>)[0] = "N";
                pb.BackColor = Color.FromArgb(245, 245, 245);
            }
        }

        private void Inner_pb_DoubleClick(object sender, EventArgs e)
        {
            if (load_thr != null && load_thr.IsAlive)
            {
                load_thr.Abort();
            }
            tb_Reply_write.Size = new Size(274, 166);//274, 331
            PictureBox pb = (PictureBox)sender;
            PictureBox owner_pb = new PictureBox();
            owner_pb.ImageLocation = pb.ImageLocation;
            owner_pb.SizeMode = PictureBoxSizeMode.Zoom;
            owner_pb.Location = new Point(0, 166);
            owner_pb.Size = new Size(274, 165);
            //owner_pb.BackColor = Color.FromArgb(245, 245, 245);
            owner_pb.BackColor = Color.WhiteSmoke;
            owner_pb.Name = "sticker_pb";
            owner_pb.Tag = (pb.Tag as List<String>)[1];

            PictureBox close_owner_pb = new PictureBox();
            close_owner_pb.Size = new Size(15, 15);
            close_owner_pb.Name = "close_owner_pb";
            close_owner_pb.Location = new Point(259, 0);
            close_owner_pb.BackgroundImage = Properties.Resources.posts_btn_close_on;
            close_owner_pb.Click += Close_owner_pb_Click1;
            close_owner_pb.Cursor = Cursors.Hand;
            owner_pb.Controls.Add(close_owner_pb);

            //MessageBox.Show(pn_Reply.Controls[pn_Reply.Controls.Count - 1].ToString());

            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("sticker_pn"))
                {
                    pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                    break;
                }
            }
            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("sticker_pb"))
                {
                    pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                    break;
                }
            }
            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("image_pb"))
                {
                    pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                    break;
                }
            }
            pb_reply_clear.Visible = false;
            pb_reply_clear.Location = new Point(244, 3);
            pn_Reply.Controls.Add(owner_pb);
            bt_reply_stiker.Tag = (pb.Tag as List<string>)[1];
            bt_reply_image.Tag = null;
            pb.Dispose();
            owner_pb.Refresh();
            bt_reply_stiker.Visible = bt_reply_image.Visible = tb_Reply_write.Visible = true;
        }

        private void Close_owner_pb_Click1(object sender, EventArgs e)
        {
            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("sticker_pb"))
                {
                    pn_Reply.Controls.Remove(pn_Reply.Controls[i]);
                    break;
                }
            }
            tb_Reply_write.Size = new Size(274, 331);
            bt_reply_stiker.Tag = null;
        }

        private void Inner_pb_Paint(object sender, PaintEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            ControlPaint.DrawBorder(e.Graphics, pb.ClientRectangle, Color.DarkSlateGray, ButtonBorderStyle.Dotted);
        }

        private void pb_reply_clear_Click(object sender, EventArgs e)
        {
            if (load_thr != null && load_thr.IsAlive)
            {
                load_thr.Abort();
                pn_Reply.Controls.RemoveAt(pn_Reply.Controls.Count - 1);
                bt_reply_stiker.Visible = bt_reply_image.Visible = tb_Reply_write.Visible = true;
                pn_Reply.Controls[pn_Reply.Controls.Count - 1].Visible = true;
                return;
            }
            for (int i = 0; i < pn_Reply.Controls.Count; i++)
            {
                if (pn_Reply.Controls[i].Name.Equals("sticker_pn"))
                {
                    pb_reply_clear.Location = new Point(244, 3);
                    pn_Reply.Controls.RemoveAt(i);
                    tb_Reply_write.Size = new Size(274, 331);
                    bt_reply_stiker.Visible = bt_reply_image.Visible = tb_Reply_write.Visible = true;
                    break;
                }
            }
            pn_Reply.Refresh();
        }

        private void fc_CafeMenuList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //fc_CafeMenuList.Tag
        }

        private void cb_work_ContentList_All_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            Thread chk_thr = new Thread(new ThreadStart(delegate
            {
                if (cb.Checked)
                {
                    cb.Margin = new Padding(-1, -1, 0, 0);
                    cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
                    lv_work_ContentList.BeginUpdate();
                    int cnt = 0;
                    for (int i = 0; i < lv_work_ContentList.Items.Count; i++)
                    {
                        cnt++;
                        lv_work_ContentList.Items[i].Checked = true;
                        if (cnt % 100 == 0)
                        {
                            lv_work_ContentList.EndUpdate();
                            lv_work_ContentList.Refresh();
                            lv_work_ContentList.BeginUpdate();
                        }
                    }
                    lv_work_ContentList.EndUpdate();
                }
                else
                {
                    cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));
                    for (int i = 0; i < lv_work_ContentList.Items.Count; i++)
                        lv_work_ContentList.Items[i].Checked = false;
                }
                lv_work_ContentList.EndUpdate();
            }));
            chk_thr.IsBackground = true;
            chk_thr.Start();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            Thread chk_thr = new Thread(new ThreadStart(delegate
            {
                if (cb.Checked)
                {
                    cb.Margin = new Padding(-1, -1, 0, 0);
                    cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
                    lv_Keyword_data.BeginUpdate();
                    int cnt = 0;
                    for (int i = 0; i < lv_Keyword_data.Items.Count; i++)
                    {
                        cnt++;
                        lv_Keyword_data.Items[i].Checked = true;
                        if (cnt % 100 == 0)
                        {
                            lv_Keyword_data.EndUpdate();
                            lv_Keyword_data.Refresh();
                            lv_Keyword_data.BeginUpdate();
                        }
                    }
                    lv_Keyword_data.EndUpdate();
                }
                else
                {
                    cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));
                    for (int i = 0; i < lv_Keyword_data.Items.Count; i++)
                        lv_Keyword_data.Items[i].Checked = false;
                }
                lv_Keyword_data.EndUpdate();
            }));
            chk_thr.IsBackground = true;
            chk_thr.Start();
        }

        private void cb_work_WorktList_All_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            Thread chk_thr = new Thread(new ThreadStart(delegate
            {
                if (cb.Checked)
                {
                    cb.Margin = new Padding(-1, -1, 0, 0);
                    cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
                    lv_work_WorkList.BeginUpdate();
                    int cnt = 0;
                    for (int i = 0; i < lv_work_WorkList.Items.Count; i++)
                    {
                        cnt++;
                        lv_work_WorkList.Items[i].Checked = true;
                        if (cnt % 100 == 0)
                        {
                            lv_work_WorkList.EndUpdate();
                            lv_work_WorkList.Refresh();
                            lv_work_WorkList.BeginUpdate();
                        }
                    }
                    lv_work_WorkList.EndUpdate();
                }
                else
                {
                    cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));
                    for (int i = 0; i < lv_work_WorkList.Items.Count; i++)
                        lv_work_WorkList.Items[i].Checked = false;
                }
                lv_work_WorkList.EndUpdate();
            }));
            chk_thr.IsBackground = true;
            chk_thr.Start();
        }

        private void pb_work_ContentSelDel_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (lv_work_ContentList.CheckedItems.Count == 0)
            {
                MessageBoxEx.Show(this, "선택된 항목이 없습니다.");
                return;
            }
            if (Time_thr != null && Time_thr.IsAlive)
            {
                if (MessageBoxEx.Show(this, "현재 예약 대기중인 스케줄이 있습니다. 삭제시 작업에 삭제된 댓글은 작업이 되지않습니다 계속하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes) { }
                else return;
            }
            if (Main_thr != null && Main_thr.IsAlive)
            {
                if (MessageBoxEx.Show(this, "현재 진행중인 작업이 있습니다. 삭제시 작업에 삭제된 댓글은 작업이 되지않습니다 계속하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes) { }
                else return;
            }
            if (MessageBoxEx.Show(this, "선택하신 항목이 삭제 됩니다.\r계속 하시겠습니까?", "항목 삭제", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                for (int i = lv_work_ContentList.CheckedItems.Count - 1; i >= 0; i--)
                {
                    if ((Main_thr != null && Main_thr.IsAlive) || Time_thr != null && Time_thr.IsAlive)
                    {
                        if (lv_work_ContentList.Items.Count == 1)
                        {
                            MessageBox.Show("작업이 진행또는 예약대기중입니다. 전체삭제를 하실경우 작업을 진행할 댓글이 없어 작업이 종료됩니다. 최소 1개이상의 댓글을 유지해주세요.\r\n삭제를 원할시 우선 작업중지 후 이용해주세요.");
                            return;
                        }
                    }
                    lv_work_ContentList.Items.Remove(lv_work_ContentList.CheckedItems[i]);
                }
            }
            if (lv_work_ContentList.Items.Count < 1)
            {
                cb_work_WorktList_All.Checked = false;
            }
            SavedFormData_server();
        }

        private void pb_work_KeywordSelDel_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (lv_Keyword_data.CheckedItems.Count == 0)
            {
                MessageBoxEx.Show(this, "선택된 항목이 없습니다.");
                return;
            }
            if (MessageBoxEx.Show(this, "선택하신 항목이 삭제 됩니다.\r계속 하시겠습니까?", "항목 삭제", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                for (int i = lv_Keyword_data.CheckedItems.Count - 1; i >= 0; i--)
                    lv_Keyword_data.Items.Remove(lv_Keyword_data.CheckedItems[i]);
            }
            if (lv_Keyword_data.Items.Count < 1)
            {
                checkBox1.Checked = false;
            }
            SavedFormData_server();
        }

        private void tb_work_datetime_Click(object sender, EventArgs e)
        {
            if (mc_work_crd.Visible)
            {
                mc_work_crd.Visible = false;
            }
            else
            {
                mc_work_crd.Location = new Point(662, 189);
                mc_work_crd.Visible = true;
                mc_work_crd.Focus();
            }
        }

        private void pb_work_datetime_Click(object sender, EventArgs e)
        {
            if (mc_work_crd.Visible)
            {
                mc_work_crd.Visible = false;
            }
            else
            {
                mc_work_crd.Location = new Point(662, 189);
                mc_work_crd.Visible = true;
                mc_work_crd.Focus();
            }
        }

        private void mc_work_crd_DateSelected(object sender, DateRangeEventArgs e)
        {
            try
            {
                tb_work_datetime.Text = (mc_work_crd.SelectionRange.Start.ToString("yyyy.MM.dd"));
                mc_work_crd.Visible = false;
            }
            catch
            {
                MessageBoxEx.Show(this, "정상적으로 다시 선택해주세요.");
                return;
            }
        }

        private void mc_work_crd_Leave(object sender, EventArgs e)
        {
            mc_work_crd.Visible = false;
        }

        private void tb_work_datetime_HH_Enter(object sender, EventArgs e)
        {
            pb_Hup.Focus();
        }

        private void pb_Hup_Click(object sender, EventArgs e)
        {
            try
            {
                int realtime = int.Parse(tb_work_datetime_HH.Text);
                tb_work_datetime_HH.Text = (++realtime).ToString();
                if (realtime > 23)
                    tb_work_datetime_HH.Text = "0";

            }
            catch
            {
                MessageBoxEx.Show(this, "숫자만 입력해주세요.");
            }
        }

        private void pb_Hdown_Click(object sender, EventArgs e)
        {
            try
            {
                int realtime = int.Parse(tb_work_datetime_HH.Text);
                tb_work_datetime_HH.Text = (--realtime).ToString();
                if (realtime < 0)
                    tb_work_datetime_HH.Text = "23";

            }
            catch
            {
                MessageBoxEx.Show(this, "숫자만 입력해주세요.");
            }
        }

        private void tb_work_datetime_mm_Enter(object sender, EventArgs e)
        {
            pb_secup.Focus();
        }

        private void pb_secup_Click(object sender, EventArgs e)
        {
            try
            {
                int realtime = int.Parse(tb_work_datetime_mm.Text);
                tb_work_datetime_mm.Text = (++realtime).ToString();
                if (realtime > 59)
                    tb_work_datetime_mm.Text = "0";

            }
            catch
            {
                MessageBoxEx.Show(this, "숫자만 입력해주세요.");
            }
        }

        private void pb_secdown_Click(object sender, EventArgs e)
        {
            try
            {
                int realtime = int.Parse(tb_work_datetime_mm.Text);
                tb_work_datetime_mm.Text = (--realtime).ToString();
                if (realtime < 0)
                    tb_work_datetime_mm.Text = "59";

            }
            catch
            {
                MessageBoxEx.Show(this, "숫자만 입력해주세요.");
            }
        }

        private void pb_work_dateset_sel_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (lv_work_WorkList.CheckedItems.Count < 1)
            {
                MessageBoxEx.Show(this, "선택된 항목이 없습니다.\r\n[작업설정]의 3개의 작업대상을 설정하여 [게시판 목록] 의 [선택]버튼을 눌러 추가한 후 추가된 항목을[체크] 해주세요.");
                return;
            }
            if (chk_IdReplyMatch.Checked)
            {
                int CertCnt = 0;
                //if (lv_work_IdList.Items[i].SubItems[3].Text.Equals("O"))
                for (int i = 0; i < lv_work_IdList.Items.Count; i++) if (lv_work_IdList.Items[i].SubItems[3].Text.Equals("O")) CertCnt++;
                if (CertCnt > lv_work_ContentList.Items.Count)
                {
                    MessageBoxEx.Show(this, "아이디와 댓글 매칭기능이 설정 되어 있으나 검증 받은 아이디개수와 댓글 개수가 서로 다릅니다.\r\n" +
                        "검증받으신 아이디와 댓글 개수를 일치시켜주세요");
                    return;
                }
            }

            DateTime set_date = new DateTime();
            try
            {
                set_date = DateTime.Parse(tb_work_datetime.Text + " " + tb_work_datetime_HH.Text + ":" + tb_work_datetime_mm.Text);
                TimeSpan t3 = set_date.Subtract(DateTime.Now);
                TimeSpan t2 = DateTime.Now.Subtract(DateTime.Now);
                if (t3.CompareTo(t2) < 0)
                {
                    MessageBoxEx.Show(this, "현재 시간 이후로 설정해주세요. 시간은 00:00~23:59 까지입니다.\r\n" +
                        "현재시간은 [ " + DateTime.Now.ToString("yyyy.MM.dd HH:mm") + " ]입니다.");
                    return;
                }
            }
            catch (FormatException)
            {
                MessageBoxEx.Show(this, "유효한 날짜만 입력해주세요.\r\n" +
                    "ex)" + DateTime.Now.ToString("yyyy.MM.dd HH:mm") + "과 같은 형식으로 입력해주세요.\r\n" +
                    "시간은 00:00~23:59까지 입력 가능합니다.");
                return;
            }
            if (Main_thr != null && Main_thr.IsAlive)
            {
                if (MessageBoxEx.Show(this, "현재 진행중인 작업이 있습니다. 현재 작업을 취소하고 예약을 진행하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    Main_thr.Abort();
                else return;
            }
            if (Time_thr != null && Time_thr.IsAlive)
            {
                if (MessageBoxEx.Show(this, "현재 예약 대기중인 스케줄이 있습니다. 이전 예약을 취소하고 현재 예약을 진행하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    Time_thr.Abort();
                else return;
            }
            if (MessageBoxEx.Show(this, "총 " + lv_work_WorkList.CheckedItems.Count + " 개의 항목을 예약하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.No) return;

            Log("작업 예약등록 [ " + set_date.ToString("yyyy.MM.dd HH:mm") + " ]");
            Time_thr = new Thread(new ThreadStart(delegate
            {
                bool state = false;
                while (true)
                {
                    //MessageBoxEx.Show(this,pn_work_AccountList.Controls[idxs[i]].Controls[8].Controls[0].Text);
                    DateTime end = new DateTime();
                    end = set_date;
                    TimeSpan t3 = end.Subtract(DateTime.Now);
                    TimeSpan t2 = DateTime.Now.Subtract(DateTime.Now);
                    if (t3.CompareTo(t2) > 0)
                    {
                        string time = ((t3.Days * 24) + t3.Hours) + ":" + t3.Minutes + ":" + t3.Seconds;
                        state = false;
                        Set_Status("작업 대기중 - [ 예약대기 : " + time + " ]");
                    }
                    else
                    {
                        //    //Thread.Sleep(int.Parse(pn_work_AccountList.Controls[num].Controls[9].Controls[0].Text.Split(':')[2]) * 1000);//<<
                        try
                        {
                            /*MessageBoxEx.Show(this,"멈추는쪽" + constcnt.ToString());*/
                            state = false;
                            Time_thr.Abort();
                        }
                        catch (Exception)
                        {
                            Write_Reply(0);
                            continue;
                        }
                        Write_Reply(0);
                        state = false;
                        break;
                    }
                    Thread.Sleep(1500);
                    state = false;
                }
            }));
            Time_thr.IsBackground = true;
            Time_thr.Start();
        }

        private void pb_work_Stop_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            try
            {
                if (Time_thr != null && Time_thr.IsAlive)
                {
                    if (MessageBoxEx.Show(this, "현재 예약 대기중인 스케줄이 있습니다. 예약을 취소하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Time_thr.Abort();
                        Log("작업이 중지되었습니다.");
                        MessageBoxEx.Show(this, "작업이 중지되었습니다.");
                        Set_Status("※ 작업 대기중");
                    }
                }
                if (Main_thr != null && Main_thr.IsAlive)
                {
                    if (MessageBoxEx.Show(this, "현재 진행중인 작업이 있습니다. 작업을 취소하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Main_thr.Abort();
                        Set_Status("※ 작업 대기중");
                        Log("작업이 중지되었습니다.");
                        MessageBoxEx.Show(this, "작업이 중지되었습니다.");
                    }
                }
            }
            catch
            {
                MessageBoxEx.Show(this, "잠시 후 다시 시도해주세요.");
            }
        }

        private void pb_work_WorkList_Seldel_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (lv_work_WorkList.CheckedItems.Count == 0)
            {
                MessageBoxEx.Show(this, "선택된 항목이 없습니다.");
                return;
            }
            if (Time_thr != null && Time_thr.IsAlive)
            {
                if (MessageBoxEx.Show(this, "현재 예약 대기중인 스케줄이 있습니다. 삭제는 현재 작업대기중인 작업목록에 영향을 끼칠 수 있습니다. 계속하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes) { }
                else return;
            }
            if (Main_thr != null && Main_thr.IsAlive)
            {
                if (MessageBoxEx.Show(this, "현재 진행중인 작업이 있습니다. 삭제는 현재 작업중인 작업목록에 영향을 끼칠 수 있습니다. 계속하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes) { }
                else return;
            }
            if (MessageBoxEx.Show(this, "선택하신 항목이 삭제 됩니다.\r계속 하시겠습니까?", "항목 삭제", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                for (int i = lv_work_WorkList.CheckedItems.Count - 1; i >= 0; i--)
                    lv_work_WorkList.Items.Remove(lv_work_WorkList.CheckedItems[i]);
            }
        }

        private void lv_work_WorkList_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (Time_thr != null && Time_thr.IsAlive)
            {
                if (MessageBoxEx.Show(this, "현재 예약 대기중인 스케줄이 있습니다. 체크를 하실경우 현재 작업대기중인 작업목록에 영향을 끼칠 수 있습니다. 계속하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes) { }
                else return;
            }
            if (Main_thr != null && Main_thr.IsAlive)
            {
                if (MessageBoxEx.Show(this, "현재 진행중인 작업이 있습니다. 체크를 하실경우 현재 작업중인 작업목록에 영향을 끼칠 수 있습니다. 계속하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes) { }
                else return;
            }
            if (lv_work_WorkList.CheckedItems.Count < 1)
            {
                if (Time_thr != null && Time_thr.IsAlive)
                {
                    Time_thr.Abort();
                    Set_Status("※ 작업 대기중");
                    MessageBoxEx.Show(this, "선택된 작업이 없어 작업을 중지하였습니다.");
                    Log("선택된 작업이 없어 작업을 중지하였습니다.");
                }
                if (Main_thr != null && Main_thr.IsAlive)
                {
                    Main_thr.Abort();
                    Set_Status("※ 작업 대기중");
                    MessageBoxEx.Show(this, "선택된 작업이 없어 작업을 중지하였습니다.");
                    Log("선택된 작업이 없어 작업을 중지하였습니다.");
                }
            }
        }

        private void lv_work_WorkList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pb_work_KeywordEdit_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (lv_Keyword_data.CheckedItems.Count < 1)
            {
                MessageBox.Show("편집할 항목을 선택해주세요.");
                return;
            }
            if (lv_Keyword_data.CheckedItems.Count > 1)
            {
                MessageBox.Show("편집할 항목을 하나만 선택해주세요.");
                return;
            }
            string val = string.Empty;
            ConstMethod.InputBox("키워드 변경", "변경하실 키워드를 입력해주세요.", ref val);
            if (val.Equals(string.Empty))
            {
                MessageBox.Show("키워드를 입력해주세요.");
                return;
            }
            lv_Keyword_data.CheckedItems[0].SubItems[1].Text = val;
            SavedFormData_server();
        }

        private void pb_delay_Click(object sender, EventArgs e)
        {
            //ComboBox cb = new ComboBox();
            //for (int i = 1; i <= 60; i++)
            //    cb.Items.Add(i);
            //cb.Controls.Add()
        }
        private void CheckVer()
        {
            lb_header_ver.Text = "Ver." + ver;
            JObject jObject = null;
            try
            {
                jObject = WebController.ServerSender(url + URL + "/lib/control.siso",
                    string.Format("dbControl=getStartCheck&CONNECTCODE=PC&siteUrl=" + URL + "&CLASS=" + Classname));
                refreshTime = (string)jObject["refreshTime"];
            }
            catch
            {
                return;
            }

            if (!((string)jObject["ver"]).Equals(ver))
            {

                if (MessageBoxEx.Show(this, "최신 버전이 존재합니다.업그레이드 하시겠습니까?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    try
                    {
                        Process proc = new Process();
                        proc.StartInfo.FileName = Productname + "UpdateModule.exe"; // 파일수정
                        proc.StartInfo.Arguments = System.Windows.Forms.Application.StartupPath;
                        proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                        proc.Start();
                        System.Windows.Forms.Application.ExitThread();
                        Environment.Exit(0);
                    }
                    catch (Exception e)
                    {
                        MessageBoxEx.Show(this, e.ToString());
                    }
                }


            }
        }
        public void GetNotice()
        {
            JObject jObject = WebController.ServerSender(url + URL + "/lib/control.siso",
                string.Format("dbControl=getNotice5&CONNECTCODE=PC&siteUrl=" + URL + "&CLASS=" + Classname));

            if (((string)jObject["result"]).Equals("Y"))
            {
                try
                {
                    //lb_Main_notice.Text = StringTransfer((string)jObject["data"][0]["b_title"],15);
                    string notice = (string)jObject["data"][0]["b_title"];
                    //notice = GetShortString(notice, 55, "...");
                    lb_header_notice.Text = notice;
                    noticeUrl = (string)jObject["data"][0]["href"];
                }
                catch { }
            }
        }
        private void pb_login_login_Click(object sender, EventArgs e)
        {
            Login();
        }
        private void Login()
        {
            if (tb_login_id.Text.Equals(""))
            {
                MessageBoxEx.Show(this, "아이디를 입력해주세요");
                return;
            }
            if (tb_login_pw.Text.Equals(""))
            {
                MessageBoxEx.Show(this, "비밀번호를 입력해주세요");
                return;
            }
            if (LoginMember.Login(tb_login_id.Text, tb_login_pw.Text))
            {
                pn_login.Visible = false;
                pn_Slogin.Location = pn_login.Location;
                pn_Slogin.Visible = true;

                lb_SLogin_Day.Text = "정식버전";
                //lb_SLogin_Day.Location = new System.Drawing.Point(pb_Slogin_Day.Right - lb_SLogin_Day.Width - 5, 8);
                lb_SLogin_Day2.Location = new System.Drawing.Point(lb_SLogin_Day.Left - lb_SLogin_Day2.Width, 8);
                lb_SLogin_Hello.Location = new System.Drawing.Point(pb_Slogin_Day.Left - lb_SLogin_Hello.Width - 10, 8);

                lb_SLogin_Id.Text = tb_login_id.Text;
                lb_SLogin_Id.Location = new System.Drawing.Point(lb_SLogin_Hello.Left - lb_SLogin_Id.Width, 8);
                lb_SLogin_ID2.Location = new System.Drawing.Point(lb_SLogin_Id.Left - lb_SLogin_ID2.Width, 8);
                lb_SLogin_Hard.Text = "[" + LoginMember.hdd() + "]";
                lb_SLogin_Hard.Location = new System.Drawing.Point(lb_SLogin_ID2.Left - lb_SLogin_Hard.Width, 8);
                lb_SLogin_Hard2.Location = new System.Drawing.Point(lb_SLogin_Hard.Left - lb_SLogin_Hard2.Width, 8);
                if (LoginMember.RemainDate == "-1")
                {
                    pb_Slogin_Day.BackgroundImage = Properties.Resources.btn_payment;
                    lb_SLogin_Day.Visible = lb_SLogin_Day2.Visible = false;
                }
                if (LoginMember.Payment)
                {
                    CheckPayment = new Thread(new ThreadStart(PayCheck));
                    CheckPayment.IsBackground = true;
                    CheckPayment.Start();
                }
                Log("NCafeCommenter 로그인 성공");

                LoadFormData_server();
            }
        }
        public void PayCheck()
        {
            Newtonsoft.Json.Linq.JObject mapList = null;
            try
            {
                while (true)
                {
                    mapList = WebController.ServerSender(Form1.url + Form1.URL + "/lib/control.siso",
                        string.Format("dbControl=memberPayCk&_APP_MEM_IDX={0}&CLASS={1}&CONNECTCODE=PC&siteUrl=" + Form1.URL + "&m_lite_is=Y", LoginMember.Memcode, Classname));
                    //MessageBoxEx.Show(this,mapList.ToString());
                    if (((string)mapList["result"]).Equals("Y")) { }
                    else if (((string)mapList["result"]).Equals("MONTHUSEPAY"))
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            LoginMember.Payment = false;
                            MonthlyPay mp = new MonthlyPay((string)mapList["PAYCODE"], (string)mapList["PRICE"]);

                            mp.Show();
                            mp.Top = (this.Top + (this.Height / 2)) - mp.Height / 2;
                            mp.Left = (this.Left + (this.Width / 2)) - mp.Width / 2;
                        });
                    }
                    else if (((string)mapList["result"]).Equals("PAY"))
                        LoginMember.Payment = false;

                    Thread.Sleep(int.Parse(refreshTime));
                }
            }
            catch (Exception ex) { MessageBoxEx.Show(this, ex.ToString()); }
        }
        private void fc_IdList_SelectedIndexChanged(object sender, EventArgs e)
        {
            fc_CafeURL.Items.Clear();
            fc_CafeMenuList.Items.Clear();
            fc_CafeURL.Text = fc_CafeMenuList.Text = "";
        }

        private void fc_IdList_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                pb_work_IdList_Sel.Focus();
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                pb_work_IdList_Sel.Focus();
                return;
            }
            FlatCombo fc = (FlatCombo)sender;

            if (fc.Items.Count < 1)
            {
                tb_work_Id.Focus();
                MessageBoxEx.Show(this, "좌측 상단[아이디 입력]부분에서 아이디와 비밀번호를 입력 후\r\n[추가]하셔서 [아이디검증]이 완료된 아이디만 목록에 출력됩니다.");
                return;
            }
            fc.Items.Clear();
            for (int i = 0; i < lv_work_IdList.Items.Count; i++)
                if (lv_work_IdList.Items[i].SubItems[3].Text.Equals("O"))
                    fc.Items.Add(lv_work_IdList.Items[i].SubItems[1].Text);
        }

        private void fc_IdList_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        private void fc_CafeURL_SelectedIndexChanged(object sender, EventArgs e)
        {
            fc_CafeMenuList.Items.Clear();
            fc_CafeMenuList.Text = "";
        }

        private void fc_CafeURL_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            FlatCombo fc = (FlatCombo)sender;
            if (fc.Items.Count < 1)
            {
                fc_IdList.Focus();
                MessageBoxEx.Show(this, "검증받은 아이디를 선택하신 후 [선택]버튼을 누르셔야 목록이 출력됩니다.");
                return;
            }
        }

        private void fc_CafeMenuList_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            FlatCombo fc = (FlatCombo)sender;
            if (fc.Items.Count < 1)
            {
                pb_work_CafeContentList_Sel.Focus();
                MessageBoxEx.Show(this, "원하는 카페리스트를 선택하신 후 [선택]버튼을 누르셔야 목록이 출력됩니다.");
                return;
            }
        }

        private void fc_CafeMenuList_KeyDown_1(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        private void pb_login_reg_Click(object sender, EventArgs e)
        {
            Register rg = new Register();
            rg.ShowDialog();
        }

        private void pb_Slogin_Day_Click(object sender, EventArgs e)
        {
            if (LoginMember.RemainDate == "-1")
            {
                Charge ch = new Charge();
                ch.ShowDialog();
            }
        }

        private void tb_login_pw_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                Login();
            }
        }

        private void pb_work_Log_Save_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (!tb_work_log.Text.Equals(string.Empty))
            {
                Clipboard.SetText(tb_work_log.Text);
                MessageBoxEx.Show(this, "로그가 성공적으로 복사되었습니다.");
            }
            else MessageBoxEx.Show(this, "복사할 로그가 없습니다.");
        }

        private void tb_Keyword_word_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (e.KeyChar == '\r')
            {
                if (tb_Keyword_word.Text.Equals("이곳에 키워드를 입력해주세요.") || tb_Keyword_word.Text.Equals(""))
                {
                    MessageBox.Show("작업을 진행할 키워드를 입력해주세요.");
                    return;
                }
                ListViewItem lvi = new ListViewItem("");
                lvi.SubItems.Add((tb_Keyword_word.Text.Trim()));
                lv_Keyword_data.Items.Add(lvi);
                tb_Keyword_word.Text = "";
                tb_Keyword_word.ForeColor = Color.Gray;
                lv_Keyword_data.EnsureVisible(lv_Keyword_data.Items.Count - 1);
                pn_Keyword_Write.Visible = false;
                tb_Keyword_word.Focus();
                SavedFormData_server();
            }
        }
        List<string> j_idx = new List<string>();
        public void SavedFormData_server()
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (j_idx.Count > 1)
            {
                for (int main_cnt = j_idx.Count - 1; main_cnt >= 0; main_cnt--)
                {
                    /*삭제하기*/
                    JObject resulter = WebController.ServerSender(url + URL + "/lib/control.siso",
               string.Format(
               "siteUrl={0}" +
               "&CONNECTCODE={1}" +
               "&CLASS={2}" +
               "&_APP_MEM_IDX={3}" +
               "&dbControl={4}" +
               "&CODE={5}",
               URL,
               "PC",
               Classname,
               LoginMember.Memcode,
               "setJsonDelete",
               j_idx[main_cnt]));
                    if (resulter.ToString().Equals(string.Empty)) { main_cnt--; continue; };
                    if (resulter["result"].ToString().Equals("Y"))
                        j_idx.RemoveAt(main_cnt);
                }
            }

            string form_data = string.Empty;
            JObject init_jobj = new JObject();
            JObject set_obj = new JObject();

            init_jobj.Add("datas", set_obj);
            var jobj = new JObject();
            var idlist_jary = new JArray();
            for (int i = 0; i < lv_work_IdList.Items.Count; i++)
            {
                var data_obj = new JObject();
                data_obj.Add("id", lv_work_IdList.Items[i].SubItems[1].Text);
                data_obj.Add("pw", lv_work_IdList.Items[i].SubItems[2].Text);
                idlist_jary.Add(data_obj);
            }

            var contentlist_jary = new JArray();
            for (int i = 0; i < lv_work_ContentList.Items.Count; i++)
            {
                var data_obj = new JObject();
                data_obj.Add("contents", (lv_work_ContentList.Items[i].Tag as List<string>)[0]);
                data_obj.Add("sticker", (lv_work_ContentList.Items[i].Tag as List<string>)[1]);
                data_obj.Add("image", (lv_work_ContentList.Items[i].Tag as List<string>)[2]);
                contentlist_jary.Add(data_obj);
            }

            var keyword_jary = new JArray();
            for (int i = 0; i < lv_Keyword_data.Items.Count; i++)
            {
                var data_obj = new JObject();
                data_obj.Add("keyword", lv_Keyword_data.Items[i].SubItems[1].Text.Trim());
                keyword_jary.Add(data_obj);
            }

            jobj.Add("idx", DateTime.Now.Ticks.ToString("x"));
            jobj.Add("idlist", idlist_jary);
            jobj.Add("contentlist", contentlist_jary);
            jobj.Add("keywordlist", keyword_jary);

            (init_jobj).Remove("datas");
            (init_jobj).Add("datas", jobj);

            /*저장쪽 최초 데이터가 0 일때*/
            if (j_idx.Count < 1)
            {
                while (true)
                {
                    JObject result = WebController.ServerSender(url + URL + "/lib/control.siso",
                    string.Format(
                        "siteUrl={0}" +
                        "&CONNECTCODE={1}" +
                        "&CLASS={2}" +
                        "&_APP_MEM_IDX={3}" +
                        "&dbControl={4}" +
                        "&j_class={5}" +
                        "&j_user_idx={6}" +
                        "&j_json={7}",
                        URL,
                        "PC",
                        Classname,
                        LoginMember.Memcode,
                        "setJsonRegi",
                        "ncafecommenter_form",
                        LoginMember.Memcode,
                        EncodingHelper.Base64Encoding(init_jobj.ToString())));

                    if (result.ToString().Equals(string.Empty)) continue;

                    if (result["result"].ToString().Equals("Y"))
                    {
                        //Log("데이터 저장 성공"); 
                        break;
                    }
                    else continue;
                }
            }
            else if (j_idx.Count == 1) // 딱한개 수정해야할 때
            {
                while (true)
                {
                    //MessageBox.Show("최초 데이터 1");
                    JObject result = WebController.ServerSender(url + URL + "/lib/control.siso",
                    string.Format(
                        "siteUrl={0}" +
                        "&CONNECTCODE={1}" +
                        "&CLASS={2}" +
                        "&_APP_MEM_IDX={3}" +
                        "&dbControl={4}" +
                        "&j_class={5}" +
                        "&j_user_idx={6}" +
                        "&j_json={7}" +
                        "&CODE={8}",
                        URL,
                        "PC",
                        Classname,
                        LoginMember.Memcode,
                        "setJsonEdit",
                        "ncafecommenter_form",
                        LoginMember.Memcode,
                        EncodingHelper.Base64Encoding(init_jobj.ToString()),
                        j_idx[0]));

                    if (result.ToString().Equals(string.Empty)) continue;

                    if (result["result"].ToString().Equals("Y"))
                    {
                        //Log("데이터 수정 성공"); 
                        break;
                    }
                    else continue;
                }
            }
        }

        public void LoadFormData_server()
        {
            bool flag = false;
            while (true)
            {
                JObject result = WebController.ServerSender(url + URL + "/lib/control.siso",
          string.Format(
          "siteUrl={0}" +
          "&CONNECTCODE={1}" +
          "&CLASS={2}" +
          "&_APP_MEM_IDX={3}" +
          "&dbControl={4}" +
          "&j_class={5}" +
          "&j_user_idx={6}",
          URL,
          "PC",
          Classname,
          LoginMember.Memcode,
          "setJsonList",
          "ncafecommenter_form",
          LoginMember.Memcode));


                if (result.ToString().Equals(string.Empty)) continue;
                else flag = true;
                if (result["result"].ToString().Equals("Y"))
                {
                    lv_work_IdList.Items.Clear();
                    lv_work_ContentList.Items.Clear();
                    var server_jobj = JObject.Parse(EncodingHelper.Base64Decoding(((result["data"] as JArray)[0] as JObject)["j_json"].ToString().Replace(" ", "+")));
                    
                    
                    //Log(server_jobj.ToString());
                    if (result["data"] != null)
                    {
                        foreach (var _item in result["data"] as JArray)
                            j_idx.Add(_item["j_idx"].ToString());
                    }
                    if (server_jobj["datas"]["idlist"] != null)
                    {
                        foreach (var _item in server_jobj["datas"]["idlist"] as JArray)
                        {
                            ListViewItem lvi = new ListViewItem();
                            lvi.SubItems.Add(_item["id"].ToString());
                            lvi.SubItems.Add(_item["pw"].ToString());
                            lvi.SubItems.Add("X");
                            lv_work_IdList.Items.Add(lvi);
                            lv_work_IdList.EnsureVisible(lv_work_IdList.Items.Count - 1);
                        }
                    }
                    if (server_jobj["datas"]["contentlist"] != null)
                    {
                        foreach (var _item in server_jobj["datas"]["contentlist"] as JArray)
                        {
                            ListViewItem lvi = new ListViewItem();
                            lvi.SubItems.Add(_item["contents"].ToString());
                            if (!_item["sticker"].ToString().Equals(string.Empty)) lvi.SubItems.Add("O");
                            else lvi.SubItems.Add("X");
                            if (!_item["image"].ToString().Equals(string.Empty))
                            {
                                if (File.Exists(_item["image"].ToString().Trim()))
                                {
                                    lvi.SubItems.Add("O");
                                }
                                else lvi.SubItems.Add("X");
                            }
                            else lvi.SubItems.Add("X");
                            List<string> tag = new List<string>();
                            tag.Add(_item["contents"].ToString());
                            tag.Add(_item["sticker"].ToString().Trim());
                            tag.Add(_item["image"].ToString().Trim());

                            lvi.Tag = tag;
                            lv_work_ContentList.Items.Add(lvi);
                            lv_work_ContentList.EnsureVisible(lv_work_ContentList.Items.Count - 1);
                        }
                    }
                    if (server_jobj["datas"]["keywordlist"] != null)
                    {
                        foreach (var _item in server_jobj["datas"]["keywordlist"] as JArray)
                        {
                            ListViewItem lvi = new ListViewItem();
                            lvi.SubItems.Add(_item["keyword"].ToString());
                            lv_Keyword_data.Items.Add(lvi);
                            lv_Keyword_data.EnsureVisible(lv_Keyword_data.Items.Count - 1);
                        }
                    }
                    lv_work_IdList.Refresh();
                    lv_work_ContentList.Refresh();
                    lv_Keyword_data.Refresh();
                }
                if (flag) { Log("데이터 로드 성공"); break; }
            }
        }

        private void lb_header_notice_Click(object sender, EventArgs e)
        {
            try { Process.Start(noticeUrl); }
            catch { }
        }

        private void pb_header_mini_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_header_mini_on;
        }

        private void pb_header_mini_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_header_mini_on;
        }

        private void pb_header_mini_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_header_mini;
        }

        private void pb_header_mini_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_header_mini;
        }

        private void pb_header_close_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_header_close_on;
        }

        private void pb_header_close_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_header_close_on;
        }

        private void pb_header_close_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_header_close;
        }

        private void pb_header_close_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_header_close;
        }

        private void pb_login_login_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_login_on;
        }

        private void pb_login_login_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_login_over;
        }

        private void pb_login_login_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_login;
        }

        private void pb_login_login_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_login_over;
        }

        private void pb_login_reg_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_join_on;
        }

        private void pb_login_reg_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_join_over;
        }

        private void pb_login_reg_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_join;
        }

        private void pb_login_reg_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_join;
        }

        private void tb_work_IdAdd_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add_on;
        }

        private void tb_work_IdAdd_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add_over;
        }

        private void tb_work_IdAdd_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add;
        }

        private void tb_work_IdAdd_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add_over;
        }

        private void pb_work_Idcert_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_idverification_on;
        }

        private void pb_work_Idcert_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_idverification_over;
        }

        private void pb_work_Idcert_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_idverification;
        }

        private void pb_work_Idcert_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_idverification_over;
        }

        private void pb_work_IdSelDel_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel_on;
        }

        private void pb_work_IdSelDel_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel_over;
        }

        private void pb_work_IdSelDel_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel;
        }

        private void pb_work_IdSelDel_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel_over;
        }

        private void pb_work_ContentList_Add_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_registration_over;
        }

        private void pb_work_ContentList_Add_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_registration_on;
        }

        private void pb_work_ContentList_Add_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_registration;
        }

        private void pb_work_ContentList_Add_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_registration;
        }

        private void pb_work_ContentSelDel_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel_on;
        }

        private void pb_work_ContentSelDel_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel_over;
        }

        private void pb_work_ContentSelDel_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel;
        }

        private void pb_work_ContentSelDel_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel_over;
        }

        private void pb_keyword_Write_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add_on;
        }

        private void pb_keyword_Write_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add_over;
        }

        private void pb_keyword_Write_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add;
        }

        private void pb_keyword_Write_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add;
        }

        private void pb_keyword_Add_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add_on;
        }

        private void pb_keyword_Add_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add_over;
        }

        private void pb_keyword_Add_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add;
        }

        private void pb_keyword_Add_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add;
        }

        private void pb_work_KeywordEdit_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_edit_over;
        }

        private void pb_work_KeywordEdit_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_edit_on;
        }

        private void pb_work_KeywordEdit_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_edit;
        }

        private void pb_work_KeywordEdit_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_edit;
        }

        private void pb_work_IdList_Sel_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_selec_on;
        }

        private void pb_work_IdList_Sel_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_selec_over;
        }

        private void pb_work_IdList_Sel_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_selec;
        }

        private void pb_work_IdList_Sel_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_selec_over;
        }

        private void pb_work_CafeURL_Search_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_selec_on;
        }

        private void pb_work_CafeURL_Search_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_selec_over;
        }

        private void pb_work_CafeURL_Search_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_selec;
        }

        private void pb_work_CafeURL_Search_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_selec;
        }

        private void pb_work_CafeContentList_Sel_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_selec_on;
        }

        private void pb_work_CafeContentList_Sel_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_selec_over;
        }

        private void pb_work_CafeContentList_Sel_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_selec;
        }

        private void pb_work_CafeContentList_Sel_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_selec;
        }

        private void pb_work_dateset_sel_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_re_over;
        }

        private void pb_work_dateset_sel_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_re_on;
        }

        private void pb_work_dateset_sel_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_re;
        }

        private void pb_work_dateset_sel_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_re;
        }

        private void pb_work_Start_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_start_on;
        }

        private void pb_work_Start_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_start_over;
        }

        private void pb_work_Start_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_start;
        }

        private void pb_work_Start_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_start;
        }

        private void pb_work_Stop_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_stop_on;
        }

        private void pb_work_Stop_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_stop_over;
        }

        private void pb_work_Stop_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_stop;
        }

        private void pb_work_Stop_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_stop;
        }

        private void pb_work_WorkList_Seldel_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel_on;
        }

        private void pb_work_WorkList_Seldel_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel_over;
        }

        private void pb_work_WorkList_Seldel_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel;
        }

        private void pb_work_WorkList_Seldel_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel;
        }

        private void pb_work_KeywordSelDel_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel_on;
        }

        private void pb_work_KeywordSelDel_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel_over;
        }

        private void pb_work_KeywordSelDel_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel;
        }

        private void pb_work_KeywordSelDel_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel;
        }

        private void pb_work_Log_Save_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_save_on;
        }

        private void pb_work_Log_Save_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_save_over;
        }

        private void pb_work_Log_Save_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_save;
        }

        private void pb_work_Log_Save_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_save;
        }

        private void pb_Slogin_Day_MouseDown(object sender, MouseEventArgs e)
        {
            if (LoginMember.RemainDate == "-1")
            {
                PictureBox pb = (PictureBox)sender;
                pb.BackgroundImage = Properties.Resources.btn_payment_over;
            }
        }

        private void pb_Slogin_Day_MouseEnter(object sender, EventArgs e)
        {
            if (LoginMember.RemainDate == "-1")
            {
                PictureBox pb = (PictureBox)sender;
                pb.BackgroundImage = Properties.Resources.btn_payment_on;
            }
        }

        private void pb_Slogin_Day_MouseLeave(object sender, EventArgs e)
        {
            if (LoginMember.RemainDate == "-1")
            {
                PictureBox pb = (PictureBox)sender;
                pb.BackgroundImage = Properties.Resources.btn_payment;
            }
        }

        private void pb_Slogin_Day_MouseUp(object sender, MouseEventArgs e)
        {
            if (LoginMember.RemainDate == "-1")
            {
                PictureBox pb = (PictureBox)sender;
                pb.BackgroundImage = Properties.Resources.btn_payment;
            }
        }

        private void pb_header_homepage_Click(object sender, EventArgs e)
        {
            try { Process.Start(url + URL); }
            catch { }
        }

        private void lv_work_WorkList_DoubleClick(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;
            string op_status = string.Empty;
            string keywords = string.Empty;
            string sel_page_First = string.Empty;
            string sel_page_Last = string.Empty;
            if (((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[5] as string).Equals("1"))
                op_status = "모든페이지 모두 댓글 작성";
            else if (((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[5] as string).Equals("2"))
                op_status = "1페이지 글에 1개만 댓글작성";
            else if (((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[5] as string).Equals("3"))
                op_status = "1페이지 게시글에 모두 댓글작성";
            else if (((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[5] as string).Equals("4"))
                op_status = "선택 페이지 1개씩 댓글 작성";
            else if (((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[5] as string).Equals("5"))
                op_status = "선택 페이지 모두 댓글 작성";

            if (((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[6] as List<string>).Count > 0)
            {
                for (int i = 0; i < ((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[6] as List<string>).Count; i++)
                    keywords += ((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[6] as List<string>)[i].Trim() + ',';
            }
            if (((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[5] as string).Equals("4") ||
                ((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[5] as string).Equals("5"))
            {
                if ((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[9] != null)
                {
                    sel_page_First = ((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[9] as string).Split('|')[0].Trim();
                    sel_page_Last = ((lv.Items[lv.FocusedItem.Index].Tag as List<object>)[9] as string).Split('|')[1].Trim();
                }
                Log("설정상태 -------\r\n" + lv.Items[lv.FocusedItem.Index].SubItems[1].Text + "번째 카페 : " + lv.Items[lv.FocusedItem.Index].SubItems[2].Text + "\r\n게시판 : " +
                lv.Items[lv.FocusedItem.Index].SubItems[3].Text + "\r\n아이디 : " + lv.Items[lv.FocusedItem.Index].SubItems[4].Text + "\r\n옵션 : " +
                op_status + "\r\n키워드 : " + keywords + "\r\n딜레이 : " + (lv.Items[lv.FocusedItem.Index].Tag as List<object>)[7].ToString() + "초\r\n선택페이지 : " +
                sel_page_First + '~' + sel_page_Last);
            }
            else
            {
                Log("설정상태 -------\r\n" + lv.Items[lv.FocusedItem.Index].SubItems[1].Text + "번째 카페 : " + lv.Items[lv.FocusedItem.Index].SubItems[2].Text + "\r\n게시판 : " +
                lv.Items[lv.FocusedItem.Index].SubItems[3].Text + "\r\n아이디 : " + lv.Items[lv.FocusedItem.Index].SubItems[4].Text + "\r\n옵션 : " +
                op_status + "\r\n키워드 : " + keywords + "\r\n딜레이 : " + (lv.Items[lv.FocusedItem.Index].Tag as List<object>)[7].ToString() + "초");
            }

        }

        private void tb_work_log_Enter(object sender, EventArgs e)
        {
            pb_work_Log_Save.Focus();
        }

        private void pb_Test_Click(object sender, EventArgs e)
        {
            Thread test_thr = new Thread(new ThreadStart(delegate
            {
                //https://m.cafe.naver.com/ArticleList.nhn?search.clubid=29226357&search.menuid=1&search.boardtype=L&search.page=1
                
                string result = WebController.GET("https://m.cafe.naver.com/ArticleList.nhn?search.clubid=29226357&search.menuid=1&search.boardtype=L&search.page=1", "https://cafe.naver.com/yspsus/105", cookie, Encoding.UTF8);
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                Log(result);
                MessageBox.Show("check");
                doc.LoadHtml(result);
                doc.LoadHtml(doc.DocumentNode.SelectSingleNode("//div[@class='list_board']").InnerHtml);
                foreach (var _item in doc.DocumentNode.SelectNodes("//li"))
                {
                    HtmlAgilityPack.HtmlDocument innerdoc = new HtmlAgilityPack.HtmlDocument();
                    innerdoc.LoadHtml(_item.InnerHtml);
                    Log(innerdoc.DocumentNode.SelectSingleNode("//strong[@class='tit']").InnerText.Trim() + " | " + innerdoc.DocumentNode.SelectSingleNode("//a").Attributes["href"].Value);
                }
                
            }));
            test_thr.IsBackground = true;
            test_thr.Start();

        }

        private string GetNaverSessionInfo(CookieContainer cookie)
        {
            string strSessionInfo = String.Empty;



            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("https://nid.naver.com/login/ext/keys.nhn");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "*/*";
            req.Host = "nid.naver.com";
            req.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";
            req.Referer = "https://nid.naver.com/nidlogin.login";
            req.CookieContainer = cookie;



            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();



                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }



            return strSessionInfo;

            req = (HttpWebRequest)HttpWebRequest.Create("https://www.naver.com/");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            //req.Host = "nid.naver.com";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            //req.Referer = "https://nid.naver.com/nidlogin.login";
            req.CookieContainer = cookie;
            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("첫번째 https://www.naver.com/ : " + strSessionInfo);
            MessageBox.Show("첫번째 쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://nid.naver.com/nidlogin.login?mode=form&url=https%3A%2F%2Fwww.naver.com");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            //req.Host = "nid.naver.com";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://www.naver.com/";
            req.CookieContainer = cookie;
            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("두번째 https://nid.naver.com/nidlogin.login?mode=form&url=https%3A%2F%2Fwww.naver.com : " + strSessionInfo);
            MessageBox.Show("2 쿠키 개수 : " + cookie.Count);
            req = (HttpWebRequest)HttpWebRequest.Create("https://nid.naver.com/login/ext/keys.nhn");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "*/*";
            req.Host = "nid.naver.com";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://nid.naver.com/nidlogin.login?mode=form&url=https%3A%2F%2Fwww.naver.com";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("세번째 https://nid.naver.com/login/ext/keys.nhn : " + strSessionInfo);
            MessageBox.Show("3 쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://pm.pstatic.net/js/c/nlog_v181107.js");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "*/*";
            req.Host = "pm.pstatic.net";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://www.naver.com/";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("네번째 https://pm.pstatic.net/js/c/nlog_v181107.js : " + strSessionInfo);
            MessageBox.Show("4 쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://ssl.pstatic.net/tveta/libs/assets/js/common/min/probe.min.js");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "*/*";
            req.MaximumAutomaticRedirections = 4;
            req.MaximumResponseHeadersLength = 4;
            req.Credentials = CredentialCache.DefaultCredentials;
            req.Host = "ssl.pstatic.net";
            req.Headers.Add("Accept-Encoding", "gzip, deflate");
            req.Headers.Add("Accept-Language", "ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7");
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://www.naver.com/";
            req.CookieContainer = cookie;


            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("다섯번째 https://ssl.pstatic.net/tveta/libs/assets/js/common/min/probe.min.js : " + strSessionInfo);
            MessageBox.Show("5 쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://pm.pstatic.net/js/c/jindo_v180212.js");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "*/*";
            req.Host = "pm.pstatic.net";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://www.naver.com/";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("여섯번째 https://ssl.pstatic.net/tveta/libs/assets/js/common/min/probe.min.js : " + strSessionInfo);
            MessageBox.Show("6 쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://nv.veta.naver.com/fxshow?su=SU10079&nrefreshx=0");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            req.Host = "nv.veta.naver.com";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://www.naver.com/";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("7 째 https://nv.veta.naver.com/fxshow?su=SU10079&nrefreshx=0 : " + strSessionInfo);
            MessageBox.Show("7 쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://nv.veta.naver.com/fxshow?su=SU10078&nrefreshx=0");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            req.Host = "nv.veta.naver.com";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://www.naver.com/";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("8 째 https://nv.veta.naver.com/fxshow?su=SU10079&nrefreshx=0 : " + strSessionInfo);
            MessageBox.Show("8 쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://nv.veta.naver.com/fxshow?su=SU10082&nrefreshx=0");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            req.Host = "nv.veta.naver.com";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://www.naver.com/";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("9째 https://nv.veta.naver.com/fxshow?su=SU10082&nrefreshx=0 : " + strSessionInfo);
            MessageBox.Show("9쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://ssl.pstatic.net/tveta/libs/assets/js/pc/main/min/pc.veta.core.min.js?20170421");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            req.Host = "ssl.pstatic.net";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://nv.veta.naver.com/fxshow?su=SU10079&nrefreshx=0";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("10째 https://ssl.pstatic.net/tveta/libs/assets/js/pc/main/min/pc.veta.core.min.js?20170421 : " + strSessionInfo);
            MessageBox.Show("10쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://ssl.pstatic.net/tveta/libs/assets/js/pc/main/min/pc.veta.core.min.js?20170222");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            req.Host = "ssl.pstatic.net";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://nv.veta.naver.com/fxshow?su=SU10082&nrefreshx=0";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("11째 https://ssl.pstatic.net/tveta/libs/assets/js/pc/main/min/pc.veta.core.min.js?20170222 : " + strSessionInfo);
            MessageBox.Show("11쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://ssl.pstatic.net/tveta/libs/assets/js/pc/main/min/pc.veta.core.min.js?20180328");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            req.Host = "ssl.pstatic.net";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://nv.veta.naver.com/fxshow?su=SU10078&nrefreshx=0";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("12째 https://ssl.pstatic.net/tveta/libs/assets/js/pc/main/min/pc.veta.core.min.js?20180328 : " + strSessionInfo);
            MessageBox.Show("12쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://ssl.pstatic.net/tveta/libs/external/js/jquery-1.8.0.min.js?20170206");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            req.Host = "ssl.pstatic.net";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://nv.veta.naver.com/fxshow?su=SU10082&nrefreshx=0";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("13째 https://ssl.pstatic.net/tveta/libs/external/js/jquery-1.8.0.min.js?20170206 : " + strSessionInfo);
            MessageBox.Show("13쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://pm.pstatic.net/js/c/nmain_v181128.js");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "*/*";
            req.Host = "pm.pstatic.net";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://www.naver.com/";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("14 째 https://pm.pstatic.net/js/c/nmain_v181128.js : " + strSessionInfo);
            MessageBox.Show("14쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://ssl.pstatic.net/tveta/libs/assets/js/pc/main/min/pc.veta.core.min.js?20180330");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "*/*";
            req.Host = "ssl.pstatic.net";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://www.naver.com/";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("15 째 https://ssl.pstatic.net/tveta/libs/assets/js/pc/main/min/pc.veta.core.min.js?20180330 : " + strSessionInfo);
            MessageBox.Show("15쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://s.pstatic.net/imgshopping/static/sb/js/jindo/jindo_mobile_component_flicking_v1.js?v=2018111312");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = "*/*";
            req.Host = "s.pstatic.net";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://www.naver.com/";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("16 째 https://s.pstatic.net/imgshopping/static/sb/js/jindo/jindo_mobile_component_flicking_v1.js?v=2018111312 : " + strSessionInfo);
            MessageBox.Show("16쿠키 개수 : " + cookie.Count);

            req = (HttpWebRequest)HttpWebRequest.Create("https://cc.naver.com/cc?a=log_off.login&r=&i=&bw=1903&px=1383&py=289&sx=1383&sy=289&m=1&nsc=navertop.v3&u=https%3A%2F%2Fnid.naver.com%2Fnidlogin.login%3Fmode%3Dform%26url%3Dhttps%253A%252F%252Fwww.naver.com");
            req.Method = WebRequestMethods.Http.Get;
            req.Accept = @"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
            req.Host = "cc.naver.com";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36";
            req.Referer = "https://www.naver.com/";
            req.CookieContainer = cookie;

            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    strSessionInfo = strResult;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }
            Log("17 째 https://cc.naver.com/cc?a=log_off.login&r=&i=&bw=1903&px=1383&py=289&sx=1383&sy=289&m=1&nsc=navertop.v3&u=https%3A%2F%2Fnid.naver.com%2Fnidlogin.login%3Fmode%3Dform%26url%3Dhttps%253A%252F%252Fwww.naver.com : " + strSessionInfo);
            MessageBox.Show("17쿠키 개수 : " + cookie.Count);

            var test = cookie.List();
            foreach(var _item in test)
            {
                Log("쿠키 : " + _item.ToString());
            }
            List<object> ret_obj = new List<object>();
            ret_obj.Add(strSessionInfo);
            ret_obj.Add(cookie);
            //return ret_obj;
        }
        private string ConvertPassword(string strSessionKey, string strId, string strPassword)
        {
            string strResult = String.Empty;

            strResult += Convert.ToChar(strSessionKey.Length).ToString();
            strResult += strSessionKey;
            strResult += Convert.ToChar(strId.Length).ToString();
            strResult += strId;
            strResult += Convert.ToChar(strPassword.Length).ToString();
            strResult += strPassword;

            return strResult;
        }
        private string EncryptRSA(string strPublicModulusKey, string strPublicExponentKey, string strTarget)
        {
            string strResult = String.Empty;



            // 공개키 생성
            RSAParameters publicKey = new RSAParameters()
            {
                Modulus = Enumerable.Range(0, strPublicModulusKey.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(strPublicModulusKey.Substring(x, 2), 16))
                .ToArray()
                ,
                Exponent = Enumerable.Range(0, strPublicExponentKey.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(strPublicExponentKey.Substring(x, 2), 16))
                .ToArray()
            };



            try
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.ImportParameters(publicKey);



                // 암호화 및 Byte => String 변환

                byte[] enc = rsa.Encrypt(Encoding.UTF8.GetBytes(strTarget), false);
                strResult = BitConverter.ToString(enc).Replace("-", "").ToLower();
            }
            catch (CryptographicException ex)
            {
                strResult = String.Empty;
            }



            return strResult;
        }

        private bool TryNaverLogin(CookieContainer cookie, string strEncName, string strEncValue)
        {
            string strNextURL = String.Empty;


            // 특정 파라미터가 데이터 객체인듯 한데 여러개의 아이디를 사용해도 딱히 변동사항이 없었다. 그래서 그대로 사용하기로 했다.
            string strParamFormat = @"bvsd=
&enctp=1&encpw={0}&encnm={1}&svctype=0&svc=&viewtype=0&locale=ko_KR&postDataKey=&smart_LEVEL=1&logintp=&url=https%3A%2F%2Fwww.naver.com%2F&localechange=&ls=&xid=&pre_id=&resp=&ru=&id=&pw=";



            string strParam = String.Format(strParamFormat/*, System.Guid.NewGuid().ToString() + "-0"*/, strEncValue, strEncName);
            Log(strParam);

            MessageBox.Show(String.Format(strParamFormat/*, System.Guid.NewGuid().ToString() + "-0"*/, strEncValue, strEncName));

            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("https://nid.naver.com/nidlogin.login");
            req.Method = WebRequestMethods.Http.Post;
            req.Accept = "text/html, application/xhtml+xml, image/jxr, */*";
            req.Host = "nid.naver.com";
            req.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";
            req.Referer = "https://nid.naver.com/nidlogin.login?mode=form&url=https%3A%2F%2Fwww.naver.com";
            req.CookieContainer = cookie;
            MessageBox.Show("로그인 시도시 쿠키 개수 : " + cookie.Count);
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = strParam.Length;



            StreamWriter sw = new StreamWriter(req.GetRequestStream());
            sw.Write(strParam);
            sw.Close();

            MessageBox.Show("check2");
            using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
            {
                StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();

                try
                {
                    string strStartIndexString = @"location.replace(""";
                    string strEndIndexString = @""");";


                    MessageBox.Show("check3");
                    Log(strResult);
                    if (strResult.IndexOf(strStartIndexString) < 0)
                        return false;
                    MessageBox.Show("check4");
                    Log(strResult);

                    // SSO 인증 페이지 주소 가져오기

                    strNextURL = strResult.Substring(strResult.IndexOf(strStartIndexString) + strStartIndexString.Length);
                    strNextURL = strNextURL.Substring(0, strNextURL.IndexOf(strEndIndexString));
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }



            // SSO 인증 페이지로 접속

            HttpWebRequest reqSSO = (HttpWebRequest)WebRequest.Create(strNextURL);
            reqSSO.Method = WebRequestMethods.Http.Get;
            reqSSO.Accept = "text/html, application/xhtml+xml, image/jxr, */*";
            reqSSO.Host = "nid.naver.com";
            reqSSO.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";
            reqSSO.Referer = "https://nid.naver.com/nidlogin.login";
            reqSSO.CookieContainer = cookie;

            using (HttpWebResponse resSSO = (HttpWebResponse)reqSSO.GetResponse())
            {
                StreamReader sr = new StreamReader(resSSO.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();



                try
                {
                    string strStartIndexString = @"<html><script language=javascript>window.location.replace(""";



                    if (strResult.IndexOf(strStartIndexString) < 0)
                        return false;
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }



            // 이부분은 최종적으로 네이버홈으로 다시 접속하는 부분이다.

            HttpWebRequest reqMain = (HttpWebRequest)WebRequest.Create("https://www.naver.com/");
            reqMain.Method = WebRequestMethods.Http.Get;
            reqMain.Accept = "text/html, application/xhtml+xml, image/jxr, */*";
            reqMain.Host = "www.naver.com";
            reqMain.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";
            reqMain.Referer = strNextURL;
            reqMain.CookieContainer = cookie;



            using (HttpWebResponse resMain = (HttpWebResponse)reqMain.GetResponse())
            {
                StreamReader sr = new StreamReader(resMain.GetResponseStream(), Encoding.UTF8);
                string strResult = sr.ReadToEnd();



                try
                {
                    // 필요한거 작성
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
            }

            return true;
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
       
            //HtmlDocument doc = webBrowser1.Document;
            //var id = doc.GetElementById("id");
            //var pw = doc.GetElementById("pw");

            //id.SetAttribute("value", "cocovv11");
            //pw.SetAttribute("value", "1Q2w3e4r5t!@");
            //pw.Focus();
            //Thread.Sleep(3000);
            //SendKeys.Send("{ENTER}");

            //cookie = GetUriCookieContainer(webBrowser1.Url);
            //var links = webBrowser1.Document.GetElementsByTagName("fieldset");
            //MessageBox.Show(links.Count.ToString());
        }
        CookieContainer cookie = new CookieContainer();
        [DllImport("wininet.dll", SetLastError = true)]
        public static extern bool InternetGetCookieEx(
    string url,
    string cookieName,
    StringBuilder cookieData,
    ref int size,
    Int32 dwFlags,
    IntPtr lpReserved);

        private const Int32 InternetCookieHttponly = 0x2000;

        /// <summary>  
        /// Gets the URI cookie container.  
        /// </summary>  
        /// <param name="uri">The URI.  
        /// <returns></returns>  
        public static CookieContainer GetUriCookieContainer(Uri uri)
        {
            CookieContainer cookies = null;
            // Determine the size of the cookie  
            int datasize = 8192 * 16;
            StringBuilder cookieData = new StringBuilder(datasize);
            if (!InternetGetCookieEx(uri.ToString(), null, cookieData, ref datasize, InternetCookieHttponly, IntPtr.Zero))
            {
                if (datasize < 0)
                    return null;
                // Allocate stringbuilder large enough to hold the cookie  
                cookieData = new StringBuilder(datasize);
                if (!InternetGetCookieEx(
                    uri.ToString(),
                    null, cookieData,
                    ref datasize,
                    InternetCookieHttponly,
                    IntPtr.Zero))
                    return null;
            }
            if (cookieData.Length > 0)
            {
                cookies = new CookieContainer();
                cookies.SetCookies(uri, cookieData.ToString().Replace(';', ','));
                //MessageBox.Show(cookieData.ToString());  
            }
            return cookies;
        }

        private void killedprocess()
        {
            try
            {
                Process[] p = Process.GetProcessesByName(Application.ProductName);
                //MessageBox.Show(p.GetLength(0).ToString());
                if (p.GetLength(0) > 1)
                {
                    if (MessageBox.Show("프로그램이 실행 중입니다.\n이전 프로그램을 종료하시겠습니까?", Application.ProductName, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        foreach (Process cp in p)
                            if (cp.Id.ToString() != Process.GetCurrentProcess().Id.ToString()) cp.Kill();
                    }
                }
                Process[] c = Process.GetProcessesByName("chromedriver");
                Process[] d = Process.GetProcessesByName("conhost");

                if (c.GetLength(0) > 1)
                {
                    foreach (Process cp in c)
                        if (cp.Id.ToString() != Process.GetCurrentProcess().Id.ToString()) cp.Kill();
                }
                if (d.GetLength(0) > 1)
                {
                    foreach (Process cp in d)
                        if (cp.Id.ToString() != Process.GetCurrentProcess().Id.ToString()) cp.Kill();
                }
            }
            catch{}
        }

        private void lv_work_ContentList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            foreach(var str in (sender as ListView).FocusedItem.Tag as List<string>)
            {
                Log(str);
            }
        }

        private void cb_Tethering_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;

            if (cb.Checked)
            {
                if (MessageBoxEx.Show(this, "테더링 기능에 대한 가이드가 필요하시나요?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    MessageBoxEx.Show(this, "우선 해당 OneClick_Tethering 프로그램을 이용하여 테더링 사용전 셋팅까지 완료하여주세요");
                    Process.Start("https://marketingmonster.kr/detail.siso?CODE=32");
                }
                cb.Margin = new Padding(-1, -1, 0, 0);
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
            }
            else
            {
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));

            }
        }

        private void cb_ChangeNick_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;

            if (cb.Checked)
            {
                cb.Margin = new Padding(-1, -1, 0, 0);
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
            }
            else
            {
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));
            }
        }

        private void pb_Option_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (pn_Option.Visible)
            {
                 pn_ExcepID.Visible = pn_Nick.Visible = pn_Option.Visible = false;
            }
            else
            {
                pn_Option.Location = new Point(662, 47);
                pn_Nick.Location = new Point(662, 184);
                pn_Nick.Visible = pn_Option.Visible = true;
                pn_Option.Focus();
            }
            pn_URL.Visible = false;
        }

        private void chk_IdReplyMatch_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;

            if (cb.Checked)
            {
                cb.Margin = new Padding(-1, -1, 0, 0);
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
            }
            else
            {
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));
            }
        }

        private void tb_Nick_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("닉네임 입력칸")) tb.Text = "";
        }

        private void tb_Nick_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("닉네임 입력칸") || tb.Text.Equals("")) tb.Text = "닉네임 입력칸";
        }

        private void tb_Nick_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == System.Windows.Forms.Keys.Enter) ADD_Nick();
        }

        private void pb_Nick_Add_Click(object sender, EventArgs e)
        {
            ADD_Nick();
        }

        private void ADD_Nick()
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (tb_Nick.Text == string.Empty || tb_Nick.Text.Equals("닉네임 입력칸"))
            {
                MessageBoxEx.Show(this, "아이디나 비밀번호중 공란이 있습니다. 다시한번 확인해주세요", "확인");
                return;
            }
            try
            {
                if (lv_Nick.FindItemWithText(tb_Nick.Text.Trim()).Index.ToString() != null)
                {
                    MessageBoxEx.Show(this, "동일한 닉네임이 이미 닉네임 리스트에 있습니다. 추가시 닉네임 리스트에 있는\r\n" +
                        (lv_Nick.FindItemWithText(tb_Nick.Text.Trim()).Index + 1).ToString()
                        + "번째의 닉네임 [ " + lv_Nick.Items[(lv_Nick.FindItemWithText(tb_Nick.Text.Trim())).Index].SubItems[1].Text +" ]을 삭제해주세요.");
                    return;
                }
            }
            catch (NullReferenceException) { }

            ListViewItem lvi = new ListViewItem((lv_Nick.Items.Count+1).ToString());
            lvi.SubItems.Add(tb_Nick.Text.Trim());
            lv_Nick.Items.Add(lvi);
            lv_Nick.EnsureVisible(lv_Nick.Items.Count - 1);
            ConstNICK.LIST_NICK.Add(tb_Nick.Text.Trim());
            ConstNICK.Save();
            tb_Nick.Text = string.Empty;
            tb_Nick.Focus();
      
            ///SavedFormData_server();
        }
        private void pb_Nick_SelDel_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (lv_Nick.CheckedItems.Count == 0)
            {
                MessageBoxEx.Show(this, "선택된 항목이 없습니다.");
                return;
            }
            List<string> LIST_NICK = new List<string>();
            if (MessageBoxEx.Show(this, "선택하신 항목이 삭제 됩니다.\r계속 하시겠습니까?", "항목 삭제", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                for (int i = lv_Nick.CheckedItems.Count - 1; i >= 0; i--)
                {
                    LIST_NICK.Add(lv_Nick.CheckedItems[i].SubItems[1].Text);
                    lv_Nick.Items.Remove(lv_Nick.CheckedItems[i]);
                }
            }
            for (int i = 0; i < lv_Nick.Items.Count; i++)
            {
                lv_Nick.Items[i].SubItems[0].Text = (i + 1).ToString();
            }
            foreach (var nick in LIST_NICK) ConstNICK.LIST_NICK.Remove(nick);
            ConstNICK.Save();
        }

        private void pb_Option_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_option_on;
        }

        private void pb_Option_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_option_ove;
        }

        private void pb_Option_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_option_off;
        }

        private void pb_Option_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_option_off;
        }

        private void chk_Nick_AllSel_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                cb.Margin = new Padding(-1, -1, 0, 0);
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
                for (int i = 0; i < lv_Nick.Items.Count; i++) lv_Nick.Items[i].Checked = true;
            }
            else
            {
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));
                for (int i = 0; i < lv_Nick.Items.Count; i++) lv_Nick.Items[i].Checked = false;
            }
        }

        private void pb_Nick_File_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBox.Show("로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBox.Show("결제 후 이용해 주세요");
                return;
            }
            OpenFile(lv_Nick,
                "파일이 열려있거나 파일형식이 맞지 않습니다.\r\n" +
                "■텍스트파일(.txt)일 경우\r\n" +
                "닉네임1\r\n" +
                "닉네임2\r\n" +
                "닉네임3\r\n" +
                "■엑셀파일(.csv)일 경우\r\n" +
                "A열이 닉네임입니다.");
            ConstNICK.LIST_NICK.Clear();
            foreach(ListViewItem LVI_NICK in lv_Nick.Items) ConstNICK.LIST_NICK.Add(LVI_NICK.SubItems[1].Text);
            ConstNICK.Save();
        }
        public static void OpenFile(ListView lv, string ExceptionString)
        {
            try
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.Filter = "허용파일 (*.txt, *csv)|*.txt;*.csv";
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    lv.Items.Clear();
                    Encoding encode = System.Text.Encoding.Default;
                    FileStream fs = new FileStream(fd.FileName, FileMode.Open, FileAccess.Read);
                    StreamReader objReader = new StreamReader(fs, encode);

                    string sLine = "";
                    List<string> arrText = new List<string>();
                    while (sLine != null)
                    {
                        sLine = objReader.ReadLine();
                        if (sLine != null)
                            arrText.Add(sLine);
                    }
                    objReader.Close();
                    foreach (string sOutput in arrText)
                    {
                        try
                        {
                            if (!(sOutput.Split(',')[0].Trim().Equals(string.Empty)))
                            {
                                ListViewItem lvi = new ListViewItem((lv.Items.Count + 1).ToString());
                                lvi.SubItems.Add(sOutput.Split(',')[0].Trim());
                                lvi.SubItems.Add(string.Empty);
                                lv.Items.Add(lvi);
                                lv.EnsureVisible(lv.Items.Count - 1);
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show(ExceptionString);
            }
        }

        private void chk_URL_AllSel_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                cb.Margin = new Padding(-1, -1, 0, 0);
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
                for (int i = 0; i < lv_URL.Items.Count; i++) lv_URL.Items[i].Checked = true;
            }
            else
            {
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));
                for (int i = 0; i < lv_URL.Items.Count; i++) lv_URL.Items[i].Checked = false;
            }
        }

        private void pb_URL_Start_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (lv_URL.CheckedItems.Count < 1)
            {
                MessageBoxEx.Show(this, "작업을할 대상 게시글주소를 추가한 후 추가된 항목을[체크] 해주세요.");
                return;
            }
            try
            {
                if (Time_thr != null && Time_thr.IsAlive)
                {
                    if (MessageBoxEx.Show(this, "현재 예약 대기중인 스케줄이 있습니다. 이전 예약을 취소하고 현재 작업을 진행하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        Time_thr.Abort();
                    else return;
                }
                if (Main_thr != null && Main_thr.IsAlive)
                {
                    if (MessageBoxEx.Show(this, "현재 진행중인 작업이 있습니다. 이전작업을 취소하고 현재작업을 진행하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        Main_thr.Abort();
                    else return;
                }
                if (chk_IdReplyMatch.Checked)
                {
                    int CertCnt = 0;
                    for (int i = 0; i < lv_work_IdList.Items.Count; i++) if (lv_work_IdList.Items[i].SubItems[3].Text.Equals("O")) CertCnt++;
                    if (CertCnt != lv_work_ContentList.Items.Count)
                    {
                        MessageBoxEx.Show(this, "아이디와 댓글 매칭기능이 설정 되어 있으나 검증 받은 아이디개수와 댓글 개수가 서로 다릅니다.\r\n" +
                            "검증받으신 아이디와 댓글 개수를 일치시켜주세요");
                        return;
                    }
                }
                Write_Reply(1);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
         
        }

        private void pb_URL_Stop_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            try
            {
                if (Time_thr != null && Time_thr.IsAlive)
                {
                    if (MessageBoxEx.Show(this, "현재 예약 대기중인 스케줄이 있습니다. 예약을 취소하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Time_thr.Abort();
                        Log("작업이 중지되었습니다.");
                        MessageBoxEx.Show(this, "작업이 중지되었습니다.");
                        Set_Status("※ 작업 대기중");
                    }
                }
                if (Main_thr != null && Main_thr.IsAlive)
                {
                    if (MessageBoxEx.Show(this, "현재 진행중인 작업이 있습니다. 작업을 취소하시겠습니까?", "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Main_thr.Abort();
                        Set_Status("※ 작업 대기중");
                        Log("작업이 중지되었습니다.");
                        MessageBoxEx.Show(this, "작업이 중지되었습니다.");
                    }
                }
            }
            catch
            {
                MessageBoxEx.Show(this, "잠시 후 다시 시도해주세요.");
            }
        }

        private void pb_URL_SelDel_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (lv_URL.CheckedItems.Count == 0)
            {
                MessageBoxEx.Show(this, "선택된 항목이 없습니다.");
                return;
            }
            if (MessageBoxEx.Show(this, "선택하신 항목이 삭제 됩니다.\r계속 하시겠습니까?", "항목 삭제", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                for (int i = lv_URL.CheckedItems.Count - 1; i >= 0; i--)lv_URL.Items.Remove(lv_URL.CheckedItems[i]);
            }
            for (int i = 0; i < lv_URL.Items.Count; i++)
            {
                lv_URL.Items[i].SubItems[0].Text = (i + 1).ToString();
            }
        }

        private void tb_URL_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("게시글 주소를 입력해주세요.")) tb.Text = "";
        }

        private void tb_URL_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("게시글 주소를 입력해주세요.") || tb.Text.Equals("")) tb.Text = "게시글 주소를 입력해주세요.";
        }

        private void tb_URL_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == System.Windows.Forms.Keys.Enter) ADD_URL();
        }

        private void pb_URL_Add_Click(object sender, EventArgs e)
        {
            ADD_URL();
        }

        private void ADD_URL()
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (tb_URL.Text == string.Empty || tb_URL.Text.Equals("게시글 주소를 입력해주세요."))
            {
                MessageBoxEx.Show(this, "URL에 공란이 있습니다. 다시한번 확인해주세요", "확인");
                tb_URL.Focus();
                return;
            }
            try
            {
                if (lv_URL.FindItemWithText(tb_URL.Text.Trim()).Index.ToString() != null)
                {
                    MessageBoxEx.Show(this, "동일한 URL이 이미 URL 리스트에 있습니다. 추가시 URL 리스트에 있는\r\n" +
                        (lv_URL.FindItemWithText(tb_URL.Text.Trim()).Index + 1).ToString()
                        + "번째의 URL [ " + lv_URL.Items[(lv_URL.FindItemWithText(tb_URL.Text.Trim())).Index].SubItems[1].Text + " ]을 삭제해주세요.");
                    return;
                }
            }
            catch (NullReferenceException) { }

            ListViewItem lvi = new ListViewItem((lv_URL.Items.Count + 1).ToString());
            lvi.SubItems.Add(tb_URL.Text.Trim());
            lvi.SubItems.Add("대기");
            lv_URL.Items.Add(lvi);
            lv_URL.EnsureVisible(lv_URL.Items.Count - 1);
            tb_URL.Text = string.Empty;
            tb_URL.Focus();
        }

        private void pb_URL_File_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBox.Show("로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBox.Show("결제 후 이용해 주세요");
                return;
            }
            OpenFile(lv_URL,
                "파일이 열려있거나 파일형식이 맞지 않습니다.\r\n" +
                "■텍스트파일(.txt)일 경우\r\n" +
                "http://url-1.com\r\n" +
                "http://url-2.com\r\n" +
                "http://url-3.com\r\n" +
                "■엑셀파일(.csv)일 경우\r\n" +
                "A열이 URL입니다.");
        }

        private void pb_Nick_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (pn_URL.Visible)
            {
                pn_ExcepID.Visible = pn_Option.Visible = pn_URL.Visible= false;
            }
            else
            {
                pn_URL.Location = new Point(662, 47);
                pn_URL.Visible = true;
                pn_URL.Focus();
            }
            pn_Nick.Visible = false;
        }

        private void pb_Nick_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_url_on;
        }

        private void pb_Nick_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_url_ove;
        }

        private void pb_Nick_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_url_off;
        }

        private void pb_Nick_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_url_off;
        }

        private void pb_URL_Add_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add_on;
        }

        private void pb_URL_Add_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add_over;
        }

        private void pb_URL_Add_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add;
        }

        private void pb_URL_Add_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_add;
        }

        private void pb_URL_Start_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_start_on;
        }

        private void pb_URL_Start_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_start_over;
        }

        private void pb_URL_Start_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_start;
        }

        private void pb_URL_Start_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_start;
        }

        private void pb_URL_Stop_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_stop_on;
        }

        private void pb_URL_Stop_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_stop_over;
        }

        private void pb_URL_Stop_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_stop;
        }

        private void pb_URL_Stop_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_stop;
        }

        private void pb_URL_SelDel_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel_on;
        }

        private void pb_URL_SelDel_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel_over;
        }

        private void pb_URL_SelDel_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel;
        }

        private void pb_URL_SelDel_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.btn_seldel;
        }

        private void chk_while_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;

            if (cb.Checked)
            {
                cb.Margin = new Padding(-1, -1, 0, 0);
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
            }
            else
            {
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));
            }
        }

        private void chk_Overlap_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;

            if (cb.Checked)
            {
                cb.Margin = new Padding(-1, -1, 0, 0);
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
            }
            else
            {
                cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));
            }
        }

        private void bt_ExceptID_Click(object sender, EventArgs e)
        {
            if (LoginMember.Memcode == "-1")
            {
                MessageBoxEx.Show(this, "로그인 후 이용해 주세요");
                return;
            }
            if (!LoginMember.Payment)
            {
                MessageBoxEx.Show(this, "결제 후 이용해 주세요");
                return;
            }
            if (pn_ExcepID.Visible)
            {
                pn_ExcepID.Visible = false;
            }
            else
            {
                pn_Nick.Visible = pn_Option.Visible = false;
                pn_ExcepID.Location = new Point(662, 47);
                pn_ExcepID.Visible = true;
            }
        }

        private void tb_ExcepID_Enter(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb.Text.Equals(string.Empty) || tb.Text.Equals("댓글 작성 제외아이디")) tb.Text = string.Empty;
        }

        private void tb_ExcepID_Leave(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb.Text.Equals(string.Empty) || tb.Text.Equals("댓글 작성 제외아이디")) tb.Text = "댓글 작성 제외아이디";
        }

        private void tb_ExcepID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == System.Windows.Forms.Keys.Enter) ADDExceptID();
        }

        private void ADDExceptID()
        {
            string ExceptID = tb_ExcepID.Text;
            if (ExceptID.Equals(string.Empty) || ExceptID.Equals("댓글 작성 제외아이디"))
            {
                MessageBox.Show("제외 아이디가 공란입니다.");
                tb_ExcepID.Focus();
                return;
            }
            ListViewItem lvi = new ListViewItem(string.Empty);
            lvi.SubItems.Add((lv_ExceptID.Items.Count + 1).ToString());
            lvi.SubItems.Add(ExceptID);
            this.lv_ExceptID.Items.Add(lvi);
            this.lv_ExceptID.EnsureVisible(this.lv_ExceptID.Items.Count - 1);
            tb_ExcepID.Text = string.Empty;
            tb_ExcepID.Focus();
        }

        private void pb_ExcepIDFile_Click(object sender, EventArgs e)
        {
            ControlsEvent.OpenFile(lv_ExceptID,
               "파일이 열려있거나 파일형식이 맞지 않습니다.\r\n" +
                 "■텍스트파일(.txt)일 경우\r\n" +
                 "제외 아이디1\r\n" +
                 "제외 아이디2\r\n" +
                 "제외 아이디3\r\n" +
                 "■엑셀파일(.csv)일 경우\r\n" +
                 "A열이 제외 아이디 입니다.");
        }

        private void pb_ExcepIDAdd_Click(object sender, EventArgs e)
        {
            ADDExceptID();
        }

        private void pb_ExcepIDSelDel_Click(object sender, EventArgs e)
        {
            ControlsEvent.SELDEL(lv_ExceptID);
        }

        private void chk_ExcepAllSel_CheckedChanged(object sender, EventArgs e)
        {
            ControlsEvent.CHECKBOX_CHECKED(sender as CheckBox, lv_ExceptID);
        }

        private void chk_MonitorID_CheckedChanged(object sender, EventArgs e)
        {
            ControlsEvent.CHECKBOX_CHECKED(sender as CheckBox, null);
        }

        private void tb_MonitorID_Enter(object sender, EventArgs e)
        {
            ControlsEvent.TEXTBOX_ENTERED(sender as TextBox, "모니터링할 아이디를 입력해주세요 , (쉼표) 로 구분합니다.");
        }

        private void tb_MonitorID_Leave(object sender, EventArgs e)
        {
            ControlsEvent.TEXTBOX_LEAVED(sender as TextBox, "모니터링할 아이디를 입력해주세요 , (쉼표) 로 구분합니다.");
        }

        private string FindFile(string strFilter = null)
        {
            try
            {
                OpenFileDialog OFD = new OpenFileDialog();

                OFD.Title = "파일 찾기";
                if (strFilter != null)
                {
                    OFD.Filter = strFilter;
                }
                DialogResult result = OFD.ShowDialog();
                if (result == DialogResult.OK)
                {
                    return OFD.FileName;
                }
                else
                {
                    return "";
                }
            }
            catch
            { }
            return "";
        }

        private void AddFile_Click(object sender, EventArgs e)
        {
            string path = FindFile("텍스트 파일.txt|*.txt");
            try
            {
                FileStream file_stream;
                try
                {
                    file_stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                    if (file_stream == null)
                    {
                        return;
                    }
                }
                catch
                {
                    return;
                }
                StreamReader Reader = new StreamReader(file_stream, System.Text.Encoding.Default);

                while (!Reader.EndOfStream)
                {
                    string value = Reader.ReadLine();
                    string isImage = "";
                    List<string> total = new List<string>();
                    

                    try
                    {
                        ListViewItem lvi = new ListViewItem();
                       
                        if (value.Split('\t').Length >= 2)
                        {
                            lvi.SubItems.Add(value.Split('\t')[0]);
                            lvi.SubItems.Add("X");
                            total.Add(value.Split('\t')[0]);
                            total.Add(string.Empty);
                            if (value.Split('\t')[1].Contains(".jpg") || value.Split('\t')[1].Contains(".png") ||  
                            value.Split('\t')[1].Contains(".PNG") || value.Split('\t')[1].Contains(".JPG") )
                            {
                                lvi.SubItems.Add("O");
                                total.Add(value.Split('\t')[1]);
                                isImage = value.Split('\t')[1];
                            }
                            else
                            {
                                lvi.SubItems.Add("X");
                                total.Add(string.Empty);
                            }

                        }
                        else if (value.Split(',').Length >= 2)
                        {
                            lvi.SubItems.Add(value.Split(',')[0]);
                            lvi.SubItems.Add("X");
                            total.Add(value.Split(',')[0]);
                            total.Add(string.Empty);
                            if (value.Split(',')[1].Contains(".jpg") || value.Split(',')[1].Contains(".png") ||
                              value.Split(',')[1].Contains(".PNG") || value.Split(',')[1].Contains(".JPG"))
                            {
                                lvi.SubItems.Add("O");
                                total.Add(value.Split(',')[1]);
                                isImage = value.Split(',')[1];
                            }
                            else
                            {
                                lvi.SubItems.Add("X");
                                total.Add(string.Empty);
                            }
                        }
                        else
                        {
                            lvi.SubItems.Add("X");
                            total.Add(string.Empty);
                        }

                        lvi.Tag = total;
                        lv_work_ContentList.Items.Add(lvi);
                        SavedFormData_server();
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.ToString());
                    }



                }
                Reader.Close();
                file_stream.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                return;
            }
        }

        private void AddFile_MouseDown(object sender, MouseEventArgs e)
        {
            AddFile.BackgroundImage = Properties.Resources.btn_file_on;
        }

        private void AddFile_MouseEnter(object sender, EventArgs e)
        {
            AddFile.BackgroundImage = Properties.Resources.btn_file_ove;
        }

        private void AddFile_MouseLeave(object sender, EventArgs e)
        {
            AddFile.BackgroundImage = Properties.Resources.btn_file_off;
        }

        private void AddFile_MouseUp(object sender, MouseEventArgs e)
        {
            AddFile.BackgroundImage = Properties.Resources.btn_file_off;
        }
        //protected virtual System.Net.WebRequest GetWebRequest(Uri address);

        //protected override WebRequest GetWebRequest(Uri uri)
        //{
        //    HttpWebRequest webRequest = (HttpWebRequest).GetWebRequest(uri);
        //    webRequest.KeepAlive = true;
        //    return webRequest;
        //}
    }


    public static class StringExtension
    {
        public static string[] strSplit(this string toSplit, string splitOn)
        {
            return toSplit.Split(new string[] { splitOn }, StringSplitOptions.None);
        }
        public static bool ContainsAny(this string input, IEnumerable<string> containsKeywords, StringComparison comparisonType)
        {
            return containsKeywords.Any(keyword => input.IndexOf(keyword, comparisonType) >= 0);
        }
    }

    public static class WebDriverExtensions
    {
        public static IWebElement FindElement(this IWebDriver driver, By by, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                return wait.Until(drv => drv.FindElement(by));
            }
            return driver.FindElement(by);
        }
    }
}
