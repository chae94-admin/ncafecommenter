﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NCafeCommenter
{
    public partial class PostWrite : Form
    {
        ListView main_lv;
        int checked_idx = 0;
        bool EditMode = false;
        public PostWrite(ListView lv)
        {
            InitializeComponent();
            main_lv = lv;
        }

        private void tb_reply_write_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("댓글을 남겨보세요.")) {
                tb.ForeColor = Color.Black;
                tb.Text = "";
            }
        }

        private void tb_reply_write_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("댓글을 남겨보세요.") || tb.Text.Equals("")) {
                tb.ForeColor = Color.Gray;
                tb.Text = "댓글을 남겨보세요.";
            } 
        }

        private void lb_reply_clear_Click(object sender, EventArgs e)
        {
            tb_reply_write.Text = "댓글을 남겨보세요.";
        }

        private void bt_reply_image_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "이미지파일 (*.jpg, *.gif, *.png, *.bmp) | *.jpg; *.gif; *.png; *.bmp";
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                if (ofd.FileName.Equals(string.Empty))
                {
                    return;
                }
                if (!File.Exists(ofd.FileName))
                {
                    MessageBox.Show("대상 파일이 있는지 확인해주세요.", "확인");
                    return;
                }
                bt.Tag = ofd.FileName;
            }
        }

        private void bt_reply_Add_Click(object sender, EventArgs e)
        {
            if (tb_reply_write.Text.Equals("댓글을 남겨보세요.") || tb_reply_write.Text.Equals(""))
            {
                MessageBox.Show("댓글을 입력해주세요.");
                return;
            }

            List<String> image = new List<string>();
            image.Add("");
            image.Add("");

            ListViewItem lvi = new ListViewItem();
            lvi.SubItems.Add((lv_reply_data.Items.Count + 1).ToString());
            lvi.SubItems.Add(tb_reply_write.Text.Trim());
            if (bt_reply_stiker.Tag != null) {
                image[0] = bt_reply_stiker.Tag.ToString();
                lvi.SubItems.Add("O");
            } 
            else lvi.SubItems.Add("X");
            if (bt_reply_image.Tag != null) {
                image[1] = bt_reply_image.Tag.ToString();
                lvi.SubItems.Add("O");
            } 
            else lvi.SubItems.Add("X");
            lvi.Tag = image;
            lv_reply_data.Items.Add(lvi);
            lv_reply_data.EnsureVisible(lv_reply_data.Items.Count - 1);
        }

        private void bt_reply_seldel_Click(object sender, EventArgs e)
        {
            if (lv_reply_data.CheckedItems.Count == 0)
            {
                MessageBoxEx.Show(this, "선택된 항목이 없습니다.");
                return;
            }
            if (MessageBoxEx.Show(this, "선택하신 항목이 삭제 됩니다.\r계속 하시겠습니까?", "항목 삭제", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                for (int i = lv_reply_data.CheckedItems.Count - 1; i >= 0; i--)
                    lv_reply_data.Items.Remove(lv_reply_data.CheckedItems[i]);
            }
            else return;
            if (lv_reply_data.Items.Count < 1) cb_reply_all.Checked = false;
        }

        private void cb_reply_all_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            Thread chk_thr = new Thread(new ThreadStart(delegate
            {
                if (cb.Checked)
                {
                    cb.Margin = new Padding(-1, -1, 0, 0);
                    cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
                    lv_reply_data.BeginUpdate();
                    int cnt = 0;
                    for (int i = 0; i < lv_reply_data.Items.Count; i++)
                    {
                        cnt++;
                        lv_reply_data.Items[i].Checked = true;
                        if (cnt % 100 == 0)
                        {
                            lv_reply_data.EndUpdate();
                            lv_reply_data.Refresh();
                            lv_reply_data.BeginUpdate();
                        }
                    }
                    lv_reply_data.EndUpdate();
                }
                else
                {
                    cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));
                    for (int i = 0; i < lv_reply_data.Items.Count; i++)
                        lv_reply_data.Items[i].Checked = false;
                }
                lv_reply_data.EndUpdate();
            }));
            chk_thr.IsBackground = true;
            chk_thr.Start();
        }

        private void lv_reply_data_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if(lv_reply_data.FocusedItem.Index != -1)
            {
                var lvi = lv_reply_data.Items[lv_reply_data.FocusedItem.Index];
                tb_reply_write.Text = lvi.SubItems[2].Text;
                if (lvi.SubItems[3].Text.Equals("O")) bt_reply_stiker.Tag = (lvi.Tag as List<String>)[0];
                if (lvi.SubItems[4].Text.Equals("O")) bt_reply_image.Tag = (lvi.Tag as List<String>)[1];
            }
        }

        private void bt_reply_stiker_Click(object sender, EventArgs e)
        {

        }

        private void tb_keyworkd_word_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("키워드"))
            {
                tb.ForeColor = Color.Black;
                tb.Text = "";
            }
        }

        private void tb_keyworkd_word_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("키워드") || tb.Text.Equals(""))
            {
                tb.ForeColor = Color.Gray;
                tb.Text = "키워드";
            }
        }

        private void tb_keyworkd_cnt_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("개수"))
            {
                tb.ForeColor = Color.Black;
                tb.Text = "";
            }
        }

        private void tb_keyworkd_cnt_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Equals("개수") || tb.Text.Equals(""))
            {
                tb.ForeColor = Color.Gray;
                tb.Text = "개수";
            }
        }

        private void bt_keyworkd_add_Click(object sender, EventArgs e)
        {
            if (tb_keyworkd_word.Text.Equals("키워드") || tb_keyworkd_word.Text.Equals(""))
            {
                MessageBox.Show("작업을 진행할 키워드를 입력해주세요.");
                return;
            }
            if (tb_keyworkd_cnt.Text.Equals("개수") || tb_keyworkd_cnt.Text.Equals(""))
            {
                MessageBox.Show("키워드에 대한 개수를 입력해주세요");
                return;
            }
            try
            {
                int.Parse(tb_keyworkd_cnt.Text);
            }
            catch
            {
                MessageBox.Show("개수입력시 숫자만 입력해주세요.");
                return;
            }
            ListViewItem lvi = new ListViewItem("");
            lvi.SubItems.Add((lv_keyworkd_data.Items.Count + 1).ToString());
            lvi.SubItems.Add((tb_keyworkd_word.Text.Trim()));
            lvi.SubItems.Add((tb_keyworkd_cnt.Text.Trim()));
        }

        private void bt_keyworkd_seldel_Click(object sender, EventArgs e)
        {
            if (lv_keyworkd_data.CheckedItems.Count == 0)
            {
                MessageBoxEx.Show(this, "선택된 항목이 없습니다.");
                return;
            }
            if (MessageBoxEx.Show(this, "선택하신 항목이 삭제 됩니다.\r계속 하시겠습니까?", "항목 삭제", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                for (int i = lv_keyworkd_data.CheckedItems.Count - 1; i >= 0; i--)
                    lv_keyworkd_data.Items.Remove(lv_keyworkd_data.CheckedItems[i]);
            }
            else return;
            if (lv_keyworkd_data.Items.Count < 1) cb_keyword_all.Checked = false;
        }

        private void cb_keyword_all_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            Thread chk_thr = new Thread(new ThreadStart(delegate
            {
                if (cb.Checked)
                {
                    cb.Margin = new Padding(-1, -1, 0, 0);
                    cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk_on));
                    lv_keyworkd_data.BeginUpdate();
                    int cnt = 0;
                    for (int i = 0; i < lv_keyworkd_data.Items.Count; i++)
                    {
                        cnt++;
                        lv_keyworkd_data.Items[i].Checked = true;
                        if (cnt % 100 == 0)
                        {
                            lv_keyworkd_data.EndUpdate();
                            lv_keyworkd_data.Refresh();
                            lv_keyworkd_data.BeginUpdate();
                        }
                    }
                    lv_keyworkd_data.EndUpdate();
                }
                else
                {
                    cb.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.bg_chk));
                    for (int i = 0; i < lv_keyworkd_data.Items.Count; i++)
                        lv_keyworkd_data.Items[i].Checked = false;
                }
                lv_keyworkd_data.EndUpdate();
            }));
            chk_thr.IsBackground = true;
            chk_thr.Start();
        }

        private void chk_1page_one_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                cb.Checked = false;
                cb.Tag = "N";
            }
            else {
                cb.Checked = true;
                cb.Tag = "Y";
            }
        }

        private void chk_1page_all_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                cb.Checked = false;
                cb.Tag = "N";
            }
            else
            {
                cb.Checked = true;
                cb.Tag = "Y";
            }
        }

        private void chk_Selpage_one_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                cb.Checked = false;
                cb.Tag = "N";
            }
            else
            {
                cb.Checked = true;
                cb.Tag = "Y";
            }
        }

        private void chk_Selpage_All_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                cb.Checked = false;
                cb.Tag = "N";
            }
            else
            {
                cb.Checked = true;
                cb.Tag = "Y";
            }
        }
    }
}
