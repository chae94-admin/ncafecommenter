﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NCafeCommenter
{
    public partial class CardPayEvent : Form
    {
        ExtendedWebBrowser webBrowser;
        string Id = "";
        string Idx = "";
        string UserName = "";
        string Email = "";
        string HP = "";
        public CardPayEvent()
        {
            InitializeComponent();
            GetMemberData();
            try
            {
                webBrowser = new ExtendedWebBrowser();
                webBrowser.Location = new Point(0, 0);
                webBrowser.Size = new Size(800, 480); //560

                webBrowser.ScriptErrorsSuppressed = true;
                webBrowser.ScrollBarsEnabled = false;

                //webBrowser.

                this.Controls.Add(webBrowser);
                //MessageBox.Show(string.Format("PRODUCTNAME=DBMakerPro&PRICE=" + (1000 * (1.1)) + "&ID={0}&CODE={1}&NAME={2}&EMAIL={3}&TEL={4}&ZEROEXTEND1={5}&CPID=CCP21773&LITEIS=N&ZEROEXTEND2=DBPro&CONNECTCODE=PC&siteUrl=marketingmonster.kr",
                //    Id, Idx, UserName, Email, HP, PayWeb.hdd()));
                string strPostData = string.Format("PRODUCTNAME=엔카페코멘터&PRICE=" + (299000 * (1.1)) + "&ID={0}&CODE={1}&NAME={2}&EMAIL={3}&TEL={4}&ZEROEXTEND1={5}&returnUrl={6}&complate_script={7}&CPID=CCP21773&LITEIS=N&ZEROEXTEND2={8}&CONNECTCODE=PC&siteUrl=" + Form1.URL,
                    Id, Idx, UserName, Email, HP, LoginMember.hdd(), Base64Encoding("https://marketingmonster.kr/payment/result.siso"), Base64Encoding("alert(\"프로그램 종료후 재실행해주시기 바랍니다.\"); window.close();"), Form1.Classname);
                byte[] PostData = Encoding.UTF8.GetBytes(strPostData);
                webBrowser.Navigate("https://marketingmonster.kr/payment/card.paymnet.siso", null, PostData, "Content-Type: application/x-www-form-urlencoded; charset=utf8");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public CardPayEvent(string CODE, string price)
        {
            InitializeComponent();
            GetMemberData();

            try
            {
                webBrowser = new ExtendedWebBrowser();
                webBrowser.Location = new Point(0, 0);
                webBrowser.Size = new Size(800, 480); //560

                webBrowser.ScriptErrorsSuppressed = true;
                webBrowser.ScrollBarsEnabled = false;

                this.Controls.Add(webBrowser);
                //MessageBox.Show(string.Format("PRODUCTNAME=하드웨어인증&PRICE=" + (int.Parse(price) * (1.1)) + "&ID={0}&CODE={1}&NAME={2}&EMAIL={3}&TEL={4}&ZEROEXTEND1={5}&returnUrl={6}&complate_script={7}&CPID=CCP21773&LITEIS=N&ZEROEXTEND2=nsearcher&CONNECTCODE=PC&siteUrl=marketingmonster.kr",
                //    Id, Idx, UserName, Email, HP, CODE, Base64Encoding("https://marketingmonster.kr/payment/mac.result.siso"), Base64Encoding("alert(\"프로그램 종료후 재실행해주시기 바랍니다.\"); window.close();")));
                string strPostData = string.Format("PRODUCTNAME=하드웨어인증&PRICE=" + (int.Parse(price) * (1.1)) + "&ID={0}&CODE={1}&NAME={2}&EMAIL={3}&TEL={4}&ZEROEXTEND1={5}&returnUrl={6}&complate_script={7}&CPID=CCP21773&LITEIS=N&ZEROEXTEND2={8}&CONNECTCODE=PC&siteUrl=marketingmonster.kr",
                    Id, Idx, UserName, Email, HP, CODE, Base64Encoding("https://marketingmonster.kr/payment/mac.result.siso"), Base64Encoding("alert(\"프로그램 종료후 재실행해주시기 바랍니다.\"); window.close();"), Form1.Classname);
                byte[] PostData = Encoding.UTF8.GetBytes(strPostData);
                webBrowser.Navigate("https://marketingmonster.kr/payment/card.paymnet.siso", null, PostData, "Content-Type: application/x-www-form-urlencoded; charset=utf8");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public CardPayEvent(string CODE, string price, string N)
        {
            InitializeComponent();

            GetMemberData();

            try
            {
                webBrowser = new ExtendedWebBrowser();
                webBrowser.Location = new Point(0, 0);
                webBrowser.Size = new Size(800, 480); //560

                webBrowser.ScriptErrorsSuppressed = true;
                webBrowser.ScrollBarsEnabled = false;

                this.Controls.Add(webBrowser);
                //MessageBox.Show(string.Format("PRODUCTNAME=하드웨어인증&PRICE=" + (int.Parse(price) * (1.1)) + "&ID={0}&CODE={1}&NAME={2}&EMAIL={3}&TEL={4}&ZEROEXTEND1={5}&returnUrl={6}&complate_script={7}&CPID=CCP21773&LITEIS=N&ZEROEXTEND2=nsearcher&CONNECTCODE=PC&siteUrl=marketingmonster.kr",
                //    Id, Idx, UserName, Email, HP, CODE, Base64Encoding("https://marketingmonster.kr/payment/mac.result.siso"), Base64Encoding("alert(\"프로그램 종료후 재실행해주시기 바랍니다.\"); window.close();")));
                string strPostData = string.Format("PRODUCTNAME=월사용료&PRICE=" + (int.Parse(price) * (1.1)) + "&ID={0}&CODE={1}&NAME={2}&EMAIL={3}&TEL={4}&ZEROEXTEND1={5}&returnUrl={6}&complate_script={7}&CPID=CCP21773&LITEIS=N&ZEROEXTEND2={8}&CONNECTCODE=PC&siteUrl=marketingmonster.kr",
                    Id, Idx, UserName, Email, HP, CODE, Base64Encoding("https://marketingmonster.kr/payment/monthly.result.siso"), Base64Encoding("alert(\"프로그램 종료후 재실행해주시기 바랍니다.\"); window.close();"), Form1.Classname);
                byte[] PostData = Encoding.UTF8.GetBytes(strPostData);
                webBrowser.Navigate("https://marketingmonster.kr/payment/card.paymnet.siso", null, PostData, "Content-Type: application/x-www-form-urlencoded; charset=utf8");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static string Base64Encoding(string EncodingText, System.Text.Encoding oEncoding = null)
        {
            if (oEncoding == null)
                oEncoding = System.Text.Encoding.UTF8;

            byte[] arr = oEncoding.GetBytes(EncodingText);
            return System.Convert.ToBase64String(arr);
        }
        private void GetMemberData()
        {
            JObject jObject = WebController.ServerSender("https://marketingmonster.kr/lib/control.siso", string.Format("dbControl=getMemberInfo&siteUrl=marketingmonster.kr&CONNECTCODE=PC&CLASS={1}&_APP_MEM_IDX={0}", LoginMember.Memcode, Form1.Classname));

            Id = (string)jObject["m_id"];
            Idx = (string)jObject["m_idx"]; ;
            UserName = (string)jObject["m_name"];
            Email = (string)jObject["m_email"];
            HP = (string)jObject["m_hp"];
        }
        private void pb_close_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.card_icon_close_on;
        }

        private void pb_close_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.card_icon_close_on;
        }

        private void pb_close_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.card_icon_close;
        }

        private void pb_close_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BackgroundImage = Properties.Resources.card_icon_close_on;
        }

        private void pb_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pb_close_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
