﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NCafeCommenter
{
    public partial class Register : Form
    {
        bool Term1 = false;
        bool Term2 = false;
        bool Term_All = false;
        bool b_Id = false;
        bool b_email = false;
        bool b_cert = false;
        string CerNum = "";
        public Register()
        {
            InitializeComponent();
        }

        private void pb_Register_temp1_Click(object sender, EventArgs e)
        {
            if (Term_All)
            {
                pb_Register_temp2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.btn_checkbox));
                pb_Register_temp3.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.btn_checkbox));
                pb_Register_temp1.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.btn_checkbox));
                Term_All = Term1 = Term2 = false;
            }
            else
            {
                pb_Register_temp2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.btn_checkbox_on));
                pb_Register_temp3.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.btn_checkbox_on));
                pb_Register_temp1.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.btn_checkbox_on));
                Term_All = Term1 = Term2 = true;
            }
        }

        private void pb_Register_temp2_Click(object sender, EventArgs e)
        {
            if (Term1)
            {
                Term1 = false;
                pb_Register_temp2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.btn_checkbox));
            }
            else
            {
                Term1 = true;
                pb_Register_temp2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.btn_checkbox_on));
            }
        }

        private void pb_Register_temp3_Click(object sender, EventArgs e)
        {
            if (Term2)
            {
                Term2 = false;
                pb_Register_temp3.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.btn_checkbox));
            }
            else
            {
                Term2 = true;
                pb_Register_temp3.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.btn_checkbox_on));
            }
        }

        private void bt_Register_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_Register_Agree_Click(object sender, EventArgs e)
        {
            if (Term2 && Term1)
            {
                pn_Register2.Location = new System.Drawing.Point(0, 0);
                pn_Register2.Visible = true;
            }
            else
                MessageBox.Show("약관에 모두 동의 해주시기 바랍니다.");
        }

        private void bt_Register2_idchk_Click(object sender, EventArgs e)
        {
            if (tb_Register2_id.Text.Length == 0)
            {
                MessageBox.Show("id를 입력 해주세요.");
                return;
            }
            JObject jObject = WebController.ServerSender(Form1.url + Form1.URL + "/lib/db.confirm.siso/",
                string.Format("dbControl=duplicationMemberIDCK&m_id={1}&siteUrl=" + Form1.URL + "&m_lite_is=N&CONNECTCODE=PC&CLASS={0}", Form1.Classname, tb_Register2_id.Text));
            if (((string)jObject["result"]).Equals("Y"))
            {
                MessageBox.Show("사용 가능한 아이디 입니다.");
                b_Id = true;
            }
            else
                MessageBox.Show((string)jObject["message"]);
        }

        private void bt_Register2_emailchk_Click(object sender, EventArgs e)
        {
            if (tb_Register2_email.Text.Length == 0)
            {
                MessageBox.Show("email을 입력 해주세요.");
                return;
            }
            JObject jObject = WebController.ServerSender(Form1.url + Form1.URL + "/lib/db.confirm.siso/",
                string.Format("dbControl=memberEMAILConfirm&m_email={1}&siteUrl=" + Form1.URL + "&CONNECTCODE=PC&m_lite_is=N&CLASS={0}", Form1.Classname, tb_Register2_email.Text));

            if (((string)jObject["result"]).Equals("Y"))
            {
                MessageBox.Show("사용 가능한 이메일 입니다.");
                b_email = true;
            }
            else
                MessageBox.Show((string)jObject["message"]);
        }

        private void bt_Register2_cert_Click(object sender, EventArgs e)
        {
            if (tb_Register2_num1.Text.Length == 0 || tb_Register2_num2.Text.Length == 0 || tb_Register2_num3.Text.Length == 0)
            {
                MessageBox.Show("전화번호를 입력 해주세요.");
                return;
            }
            JObject jObject = WebController.ServerSender(Form1.url + Form1.URL + "/lib/control.siso/",
                string.Format("FROMTEL={0}&dbControl={1}&m_lite_is=N&CONNECTCODE=PC&siteUrl=" + Form1.URL + "&CLASS={2}",
                tb_Register2_num1.Text.Trim() + tb_Register2_num2.Text.Trim() + tb_Register2_num3.Text.Trim(), "setCustomerMemberOTPPC", Form1.Classname)
                );

            CerNum = (string)jObject["NUM"];
            if (CerNum.Length == 0)
                CerNum = "none";

            if (((string)jObject["result"]).Equals("Y"))
            {
                MessageBox.Show("인증 번호를 발송했습니다.");
                bt_Register2_certchk.Enabled = true;
            }
            else
                MessageBox.Show((string)jObject["message"]);
        }

        private void bt_Register2_certchk_Click(object sender, EventArgs e)
        {
            if (CerNum == tb_Register2_cert.Text && tb_Register2_cert.Text.Length > 0)
            {
                b_cert = true;
                MessageBox.Show("인증완료 되었습니다.");
            }
            else
                MessageBox.Show("인증 실패 인증번호를 다시 확인하여 주세요");
        }

        private void pn_Register2_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pn_Register2_Regidit_Click(object sender, EventArgs e)
        {
            if (tb_Register2_pw.Text != tb_Register2_pwchk.Text)
            {
                MessageBox.Show("비밀번호와 비밀번호 확인이 일치하지 않습니다.");
                return;
            }
            if (b_cert && b_email && b_Id)
            {
                JObject jObject = WebController.ServerSender(Form1.url + Form1.URL + "/lib/db.confirm.siso/", (string.Format("m_id={0}&m_pass={1}&m_pass_confirm={2}&m_name={3}&m_hp1={4}&m_hp2={5}&m_hp3={6}&m_email={7}&dbControl={8}&partnerIDX=2&partnerID=coreplanet&m_lite_is=Y&m_hardware_no={9}&CONNECTCODE=PC&siteUrl=" + Form1.URL + "&CLASS=" + Form1.Classname,
                       tb_Register2_id.Text, tb_Register2_pw.Text, tb_Register2_pwchk.Text, tb_Register2_name.Text, tb_Register2_num1.Text, tb_Register2_num2.Text, tb_Register2_num3.Text, tb_Register2_email.Text, "setMemberUserRegi", LoginMember.hdd())));

                if (((string)jObject["result"]).Equals("Y"))
                {
                    MessageBox.Show("회원가입 성공");
                    this.Close();
                }
                else
                    MessageBox.Show((string)jObject["message"]);
            }
            else if (!b_Id)
            {
                MessageBox.Show("아이디 중복확인을 해주시기 바랍니다");
            }
            else if (!b_email)
            {
                MessageBox.Show("이메일 중복확인을 해주시기 바랍니다");
            }
            else if (!b_cert)
            {
                MessageBox.Show("전화번호 인증을 해주시기 바랍니다.");
            }
        }

        private void tb_Register2_name_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            set_text(tb);
        }

        private void tb_Register2_id_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            set_text(tb);
        }

        private void tb_Register2_email_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            set_text(tb);
        }

        private void tb_Register2_num1_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            set_text(tb);
        }

        private void tb_Register2_cert_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            set_text(tb);
        }
        private void set_text(TextBox tb)
        {
            tb.Text = "";
        }
    }
}
