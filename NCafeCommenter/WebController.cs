﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinHttp;

namespace NCafeCommenter
{
    class WebController
    {
        public static string GET(string url, string Referer, CookieContainer cookieContainer, Encoding en_type)
        {
            string result = "";
            try
            {
                //MessageBox.Show(url);
                HttpWebRequest hreq = (HttpWebRequest)WebRequest.Create(url);
                hreq.Method = "GET";
                //MessageBox.Show(Referer);
                hreq.Referer = Referer;
                hreq.ContentType = "application/x-www-form-urlencoded";
                hreq.CookieContainer = cookieContainer;
                HttpWebResponse hres = (HttpWebResponse)hreq.GetResponse();

                Stream dataStream1 = hres.GetResponseStream();
                StreamReader srr = new StreamReader(dataStream1, en_type);
                string RET = srr.ReadToEnd();
              //  MessageBox.Show(RET);

                if (hres.StatusCode == HttpStatusCode.OK)
                {

                    Stream dataStream = hres.GetResponseStream();
                    StreamReader sr = new StreamReader(dataStream, en_type);
                    result = RET;
                    hres.Close();
                    dataStream.Close();
                    sr.Close();
                }
                else return string.Empty;
                //Log(result);
            }
            catch(UriFormatException)
            {
                MessageBox.Show("입력한 URL 형식이 맞지않습니다.");
                return result;
            }
            catch (NotSupportedException)
            {
                MessageBox.Show("입력한 URL 형식이 맞지않습니다.");
                return result;
            }
            catch (WebException ex)
            {
                MessageBox.Show("입력한 URL을 수동으로 접속하시고 접속이 되는경우 다시 한번 시도해주세요.\r\n수동으로도 접속이 안될경우 인터넷 연결상태를 확인해주세요.\r\n"+ex.ToString());
                return result;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return result;
        }
        public static string POST(Dictionary<String, String> map, string url, string Referer, CookieContainer cookieContainer, Encoding encoding, string proxy, Dictionary<string, string> headers = null)
        {

            string result = "";

            try
            {
                String body = "";
                if (map != null)
                {
                    foreach (string key in map.Keys)
                        body += key + "=" + map[key] + "&";
                }
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                WebResponse response = null;
                var data = Encoding.Default.GetBytes(body);
                request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = "POST";
                request.Referer = Referer;
                if (url.Equals("https://upconvert.editor.naver.com/cafe/html/components?documentWidth=800&userId="))
                    request.ContentType = "text/plain";
                else request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                request.CookieContainer = cookieContainer;
                if (!string.IsNullOrEmpty(proxy))
                {
                    WebProxy myProxy = new WebProxy(proxy);
                    request.Proxy = myProxy;
                }
                if (headers != null)
                {
                    foreach (var header in headers)
                    {
                        if (header.Key.Equals("Host")) request.Host = headers["Host"];
                        else if (header.Key.Equals("User-Agent")) request.UserAgent = headers["User-Agent"];
                        else if (header.Key.Equals("Accept")) request.Accept = headers["Accept"];
                        else if (header.Key.Equals("Content-Type")) request.ContentType = headers["Content-Type"];
                        else request.Headers.Add(header.Key, header.Value);
                    }
                }
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                void CallbackMethod(IAsyncResult resulter)
                {
                    response = request.EndGetResponse(resulter);
                }

                request.BeginGetResponse(new AsyncCallback(CallbackMethod), null);
                Stream ststream = response.GetResponseStream();
                StreamReader reader = new StreamReader(ststream, encoding, true);  //이렇게 하면 안깨짐 
                result = reader.ReadToEnd();//<<
                return result;
            }
            catch (Exception ex)
            {

                //System.Windows.Forms.MessageBox.Show(ex.ToString());
                return result;
            }
        }

        public static string POST(Dictionary<String, String> map, string url, string Referer, CookieContainer cookieContainer, Encoding encoding)
        {
            string result = "";
            try
            {
                String body = "";
                if (map != null)
                {
                    foreach (string key in map.Keys)
                        body += key + "=" + map[key] + "&";
                }
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                WebResponse response = null;
                var data = Encoding.Default.GetBytes(body);
                request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = "POST";
                request.Referer = Referer;
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                request.CookieContainer = cookieContainer;
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                void CallbackMethod(IAsyncResult resulter)
                {
                    response = request.EndGetResponse(resulter);
                }

                request.BeginGetResponse(new AsyncCallback(CallbackMethod), null);
                Stream ststream = response.GetResponseStream();
                StreamReader reader = new StreamReader(ststream, encoding, true);  //이렇게 하면 안깨짐 
                result = reader.ReadToEnd();//<<
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return result;
        }
        public static JObject ServerSender(string url, string text)
        {
            try
            {
                WinHttpRequest http = new WinHttpRequest();                   //name phone mac seller

                http.Open("POST", string.Format(url));
                http.SetRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36");
                http.SetRequestHeader("Accept", "text/html, application/xhtml+xml, image/jxr, */*");
                http.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                http.SetRequestHeader("Accept-Language", "ko-KR");
                http.SetRequestHeader("Accept-Encoding", "gzip, deflate");

                http.Send(text);
                //MessageBox.Show(http.ResponseText);
                return JObject.Parse(http.ResponseText);
            }
            catch
            {
                MessageBox.Show("서버에서 잘못된 응답을 반환하였습니다. 잠시 후 다시 시도해주세요.");
                return null;
            }
        }
    }
}
