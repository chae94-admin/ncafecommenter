﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinHttp;

namespace NSearcherUpdateModule
{ //AlrigoUpdateModule
    public partial class Form1 : Form
    {
        string url = "https://";
        string URL = "marketingmonster.kr";
        string productname = "NCafeCommenter";
        string classname = "ncafecommenter";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            upgrade();
        }
        List<string> ulist = new List<string>();
        WebClient client = new WebClient();
        public void upgrade()
        {

            // char[] chr = { '\n' };
            //   string[] str;

            WinHttpRequest http = new WinHttpRequest();
            //MessageBox.Show("Test");
            //http.Open("GET", "?dbControl=getStartCheck&siteUrl=marketingmonster.kr&CONNECTCODE=PC&CLASS=nsearcher");
            //http.SetRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");
            //http.SetRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            //http.Send();
           
            /////////////////////////////////////
            http.Open("POST", string.Format(url + URL + "/lib/control.siso"));
            http.SetRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0");
            http.SetRequestHeader("Accept", "text/html, application/xhtml+xml, image/jxr, */*");
            http.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            http.SetRequestHeader("Accept-Language", "ko-KR");
            http.SetRequestHeader("Accept-Encoding", "gzip, deflate");
            http.Send(string.Format("dbControl=getStartCheck&siteUrl="+URL+"&CONNECTCODE=PC&CLASS="+classname));
           //MessageBox.Show(http.ResponseText);
            Newtonsoft.Json.Linq.JObject mapList = Newtonsoft.Json.Linq.JObject.Parse(http.ResponseText);

            

            ulist.Add(url + URL + (string)mapList["updateFile"]);

            string s = (string)mapList["updateFile"];
            try
            {
                client.DownloadFile(new Uri(ulist[0]), Application.StartupPath + "/" + s.Split('/')[3]);
                zipfile(s.Split('/')[3]);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            ///////zip  파일 압축풀기////////////////////////
            //Thread.Sleep(1000);
            //string Filename = ulist[0].Trim();

           
            Process proc = new Process();
            proc.StartInfo.FileName = productname+".exe";
            proc.StartInfo.Arguments = System.Windows.Forms.Application.StartupPath;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            //bool result = 

            try
            { 
                proc.Start();
                //MessageBox.Show("업데이트가 완료되었습니다");
                Application.Exit();
            }
            catch (Exception e)
            {
                MessageBox.Show("업그레이드 실패");
            }

            

        }
        private void zipfile(string m_filename)
        {
            if (File.Exists(m_filename))
            {
                ZipInputStream zipInputStream = new ZipInputStream(File.OpenRead(m_filename));
                try
                {
                    ZipEntry theEntry;

                    while ((theEntry = zipInputStream.GetNextEntry()) != null)
                    {
                        string directoryName = Path.GetDirectoryName(theEntry.Name); // 폴더
                        string fileName = Path.GetFileName(theEntry.Name); // 파일

                        // 파일 이름이 있는 경우
                        if (fileName != String.Empty)
                        {
                            try
                            {
                                // 파일 스트림 생성.(파일생성)
                                FileStream streamWriter = File.Create((Application.StartupPath + "\\" + theEntry.Name));
                                int size = 2048;
                                byte[] data = new byte[2048];

                                // 파일 복사
                                while (true)
                                {
                                    size = zipInputStream.Read(data, 0, data.Length);

                                    if (size > 0)
                                    {
                                        streamWriter.Write(data, 0, size);
                                    }
                                    else
                                        break;
                                }
                                // 파일스트림 종료
                                streamWriter.Close();
                            }
                            catch
                            {
                                continue;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
                finally
                {
                    // ZIP 파일 스트림 종료
                    zipInputStream.Close();
                    File.Delete(m_filename);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
